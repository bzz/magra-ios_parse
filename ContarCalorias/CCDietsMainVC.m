//
//  CCDietsMainVC.m
//  ContarCalorias
//
//  Created by andres portillo on 05/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCDietsMainVC.h"
#import "CCSideMenuVC.h"
#import "CCAPI.h"
#import "SWRevealViewController.h"
#import "UIImageView+AFNetworking.h"
#import "CCDietDetailTVC.h"
#import "CCLoadingView.h"
#import "CCTodaysMealsView.h"
#import "CCRecipeTVC.h"
#import "MealPlan.h"
#import "User.h"
#import "CCShoppingListTVC.h"

typedef NS_ENUM(NSInteger, CCDietMainScreenTab) {
    CCDietMainScreenTabMyDiet = 0,
    CCDietMainScreenTabAllDiets
};
@interface CCDietsMainVC ()<UITableViewDataSource, UITableViewDelegate,CCLoadingViewDelegate, CCTodaysMealsViewDelegate>

@end

@implementation CCDietsMainVC
{
    UIBarButtonItem *_btnShoppingList;
    NSArray *_arrDiets;
    NSDictionary *_dictTodaysMeals;
    User *_user;
    CCDietMainScreenTab _selectedTab;
    CCLoadingView *_loadingViewAllDiets;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    _user = [[CCAPI sharedInstance] getUser];
    if (_selectedTab == CCDietMainScreenTabMyDiet) {
        [self.tblAllDiets setAlpha:0];
        [_todaysMealsView setAlpha:1];
    } else {
        [_todaysMealsView setAlpha:0];
        [self.tblAllDiets setAlpha:1];
        [self.tblAllDiets reloadData];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _user = [[CCAPI sharedInstance] getUser];
    
    [self setBackButton];
    
    _btnShoppingList = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"shopping-cart"] style:UIBarButtonItemStyleBordered target:self action:@selector(showShoppingList)];
    
    if (_user.mealPlan) {
        self.navigationItem.rightBarButtonItem = _btnShoppingList;
    }
  
    [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Diets"]];
    
    _loadingViewAllDiets = [[[NSBundle mainBundle] loadNibNamed:@"CCLoadingView" owner:self options:nil] firstObject];
    [_loadingViewAllDiets  setFrame:CGRectMake(0, 0, 320, _tblAllDiets.frame.size.height)];
    _loadingViewAllDiets.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    
    [_loadingViewAllDiets setDelegate:self];
    [_loadingViewAllDiets startLoadingMode];
    [_tblAllDiets setTableFooterView:_loadingViewAllDiets];
    
    [_todaysMealsView setDelegate:self];

        //get diets
    [self getAllDiets];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNewDietSelected) name:kCCNotificationUserStartedDiet object:nil];

    [_segmentedControl setTitle:[TSLanguageManager localizedString:@"My diet"] forSegmentAtIndex:0];
    [_segmentedControl setTitle:[TSLanguageManager localizedString:@"All diets"] forSegmentAtIndex:1];
}


- (void)onNewDietSelected
{
    [_segmentedControl setSelectedSegmentIndex:CCDietMainScreenTabMyDiet];
    [self onSegmentedControlChanged:_segmentedControl];
}
- (void)getAllDiets
{
    _arrDiets = [MealPlan MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"locale = %@",[TSLanguageManager selectedLanguage]]];
    [_tblAllDiets setTableFooterView:[UIView new]];
    [_tblAllDiets reloadData];
    
}
-(void)setBackButton{
    
    UIButton *btn = [[UIButton alloc] init];
    [btn setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
    [btn setFrame:CGRectMake(0, 0, 20, 15)];
    [btn addTarget:self.revealViewController action:@selector(revealToggleAnimated:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* button = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = button;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)showShoppingList
{
    [self performSegueWithIdentifier:@"Shopping List Segue" sender:self];
}
#pragma mark - UITableview Data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tblAllDiets)
    {
        return _arrDiets.count;
    }
    
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblAllDiets)
        return 100;
    
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *identifier = @"Diet Cell";
    
    UITableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    MealPlan *mealPlan = _arrDiets[indexPath.row];
    
    [(UILabel *)[cell viewWithTag:2] setText:mealPlan.title];
    [(UILabel *)[cell viewWithTag:3] setText:mealPlan.descriptionMealplan];
    
    UIImage *dietImg = [UIImage imageNamed:mealPlan.image];
    [(UIImageView *)[cell viewWithTag:1] setImage:dietImg];
    
    
    return cell;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Diet Detail Segue"]) {
        CCDietDetailTVC *dietDetailTVC =(CCDietDetailTVC *)segue.destinationViewController;
        NSIndexPath * selectedIndex = [_tblAllDiets indexPathForSelectedRow];
        dietDetailTVC.mealPlan = _arrDiets[selectedIndex.row];
    } else if ([segue.identifier isEqualToString:@"Shopping List Segue"]) {
        CCShoppingListTVC *shoppingListTVC =(CCShoppingListTVC *)[(UINavigationController *)segue.destinationViewController topViewController];
        shoppingListTVC.mealPlan =  _user.mealPlan;
    }
}
- (IBAction)onSegmentedControlChanged:(id)sender {
    
    _selectedTab = ((UISegmentedControl *)sender).selectedSegmentIndex;
    if (_selectedTab == CCDietMainScreenTabMyDiet) {
        
        [_todaysMealsView setAlpha:1];
        [_todaysMealsView updateViewsContent];
        
        if (_user.mealPlan)
            self.navigationItem.rightBarButtonItem = _btnShoppingList;
        
        [self.tblAllDiets setAlpha:0];
    } else {
        [_todaysMealsView setAlpha:0];
        [_tblAllDiets setAlpha:1];
        [_tblAllDiets reloadData];
        self.navigationItem.rightBarButtonItem = nil;
    }
}
#pragma mark - SWRevealViewController Delegate
- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}
#pragma mark - CCNoDietSelectedView Delegate
- (void)didTapPickADietButton
{
    [_segmentedControl setSelectedSegmentIndex:CCDietMainScreenTabAllDiets];
    [self onSegmentedControlChanged:_segmentedControl];
}
- (void)didTapContactDietitianButton
{
    CCSideMenuVC *sideMenu = (CCSideMenuVC *) self.revealViewController.rearViewController;
    [sideMenu setNutritionistVC];
    
}
- (void)didTapSeeRecipeButtonWithRecipeId:(NSInteger)recipeId
{
    UIStoryboard *dietsStoryBoard = [UIStoryboard storyboardWithName:@"Diets" bundle:nil];
    CCRecipeTVC *recipeTVC = [dietsStoryBoard instantiateViewControllerWithIdentifier:@"Recipe TVC"];
    recipeTVC.recipeId = recipeId;
    [self.navigationController pushViewController:recipeTVC animated:YES];
    
}
#pragma mark - CCLoadingView Delegate
- (void)theRefreshButtonWasTappedOnLoadingView:(CCLoadingView *)loadingView
{
    if (loadingView == _loadingViewAllDiets) {
        [self getAllDiets];
    }
}

@end
