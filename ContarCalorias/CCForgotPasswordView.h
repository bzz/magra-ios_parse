//
//  CCForgotPasswordVC.h
//  ContarCalorias
//
//  Created by andres portillo on 11/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCForgotPasswordView : UIView
- (IBAction)onResetPasswordButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblForgotPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnRecoverPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblIfYouForgot;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)onBackButtonTapped:(id)sender;
@end
