//
//  AddExerciseViewController.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 12/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DiaryItemInfo.h"
@class Exercise;
@interface AddExerciseViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewWrapperInfo;
@property (weak, nonatomic) IBOutlet UITextField *txtMinutes;
@property (weak, nonatomic) IBOutlet UILabel *caloriesLbl;
@property (nonatomic, retain) Exercise *exercise;
@property (nonatomic, retain) DiaryItemInfo *diaryItemInfo;
@property (weak, nonatomic) IBOutlet UILabel *currentExerciseLbl;
@property (weak, nonatomic) IBOutlet UILabel *lblMinutesPerformed;
@property (weak, nonatomic) IBOutlet UILabel *lblCaloriesBurned;

@end
