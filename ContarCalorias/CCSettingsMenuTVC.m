//
//  CCSettingsMenuTVC.m
//  ContarCalorias
//
//  Created by andres portillo on 3/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCSettingsMenuTVC.h"
#import "SWRevealViewController.h"
#import "CCAPI.h"
#import "User.h"

@interface CCSettingsMenuTVC ()<UIActionSheetDelegate, SWRevealViewControllerDelegate>

@end

@implementation CCSettingsMenuTVC
{
    User *_user;
}

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.revealViewController.delegate = self;
    
    [self setBackButton];
    [self.navigationItem  setTitle:[TSLanguageManager localizedString:@"Settings"]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadScreen)
                                                 name:@"LanguageWasChanged"
                                               object:nil];
    
    _user = [[CCAPI sharedInstance] getUser];
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
   
}
- (void)reloadScreen
{
    [self.navigationItem  setTitle:[TSLanguageManager localizedString:@"Settings"]];
    [self.tableView reloadData];
}
- (void)setBackButton{
    
    UIButton *btn = [[UIButton alloc] init];
    [btn setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
    [btn setFrame:CGRectMake(0, 0, 20, 15)];
    [btn addTarget:self.revealViewController action:@selector(revealToggleAnimated:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* button = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = button;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1 && indexPath.row == 0) {
    
        [self onLogoutButtonTapped];
    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0 && indexPath.row == 0 && ! _user.didCreateWeightLossPlanValue) {
        [cell setAlpha:0];
    }
    if (indexPath.row == 0 && indexPath.section == 0) {
        [cell.textLabel setText:[TSLanguageManager localizedString:@"Weight loss plan"]];
    } else if (indexPath.row == 1 && indexPath.section == 0) {
        [cell.textLabel setText:[TSLanguageManager localizedString:@"Reminders"]];
    } else if (indexPath.row == 2 && indexPath.section == 0) {
        [cell.textLabel setText:[TSLanguageManager localizedString:@"Language"]];
    } else if (indexPath.row == 3 && indexPath.section == 0) {
        [cell.textLabel setText:[NSString stringWithFormat:@"%@ %@",[TSLanguageManager localizedString:@"Version"],[[CCAPI sharedInstance] getVersionNumber]]];
    }else if (indexPath.row == 0 && indexPath.section == 1) {
        [cell.textLabel setText:[TSLanguageManager localizedString:@"Logout"]];
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        if(indexPath.row == 0 && ! _user.didCreateWeightLossPlanValue) {
            return 0.0;
        } else {
            return 56.0;
        }
        
    }
    return 40.0;
}
- (void)onLogoutButtonTapped
{
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:[TSLanguageManager localizedString:@"Are you sure you want to logout?"] delegate:self cancelButtonTitle:[TSLanguageManager localizedString:@"Close"] destructiveButtonTitle:[TSLanguageManager localizedString:@"Log me out"] otherButtonTitles:nil];
    [as showInView:self.view];
}
- (void)performLogout
{
    [[CCAPI sharedInstance] destroyActiveSession];
}
#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex ==  actionSheet.destructiveButtonIndex) {
        [self performLogout];
    }
}
#pragma mark - SWRevealViewController Delegate
- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}
@end
