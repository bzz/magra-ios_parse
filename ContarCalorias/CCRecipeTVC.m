//
//  CCRecipeTVC.m
//  ContarCalorias
//
//  Created by andres portillo on 07/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCRecipeTVC.h"
#import "CCAPI.h"
#import "SWRevealViewController.h"
#import "UIImageView+AFNetworking.h"
#import "Recipe.h"
#import "NutritionalFact.h"
#import "NutritionalFact_Recipe.h"
#import "Ingredient.h"

typedef NS_ENUM(NSInteger, CCRecipeScreenTab) {
    CCRecipeScreenTabOverview = 0,
    CCRecipeScreenTabInstructions,
    CCRecipeScreenTabNutrition
};

@interface CCRecipeTVC ()

@end


@implementation CCRecipeTVC
{
    Recipe  *_recipe;
    CCRecipeScreenTab _selectedTab;
    UIButton *_btnOverview, *_btnInstructions, *_btnNutrition;
    NSArray *_arrIngredients, *_arrNutrients;
    UIImageView *_tabBg;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _selectedTab = CCRecipeScreenTabOverview;
    
    _btnOverview = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 105, 56)];
    [_btnOverview setTitle:[TSLanguageManager localizedString:@"Ingredients"] forState:UIControlStateNormal];
    [_btnOverview setTitle:[TSLanguageManager localizedString:@"Ingredients"] forState:UIControlStateSelected];
    
    [_btnOverview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnOverview setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_btnOverview.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [_btnOverview.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_btnOverview addTarget:self action:@selector(onOverviewTapped) forControlEvents:UIControlEventTouchUpInside];
    
    _btnInstructions = [[UIButton alloc] initWithFrame:CGRectMake(105,0, 110, 56)];
    [_btnInstructions setTitle:[TSLanguageManager localizedString:@"Instructions"] forState:UIControlStateNormal];
    [_btnInstructions setTitle:[TSLanguageManager localizedString:@"Instructions"] forState:UIControlStateSelected];
    [_btnInstructions.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [_btnInstructions setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_btnInstructions setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
    [_btnInstructions.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_btnInstructions addTarget:self action:@selector(onInstructionsTapped) forControlEvents:UIControlEventTouchUpInside];
    
    
    _btnNutrition = [[UIButton alloc] initWithFrame:CGRectMake(215, 0, 105, 56)];
    [_btnNutrition setTitle:[TSLanguageManager localizedString:@"Nutrition"] forState:UIControlStateNormal];
    [_btnNutrition setTitle:[TSLanguageManager localizedString:@"Nutrition"] forState:UIControlStateSelected];
    [_btnNutrition.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [_btnNutrition setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_btnNutrition setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
    [_btnNutrition.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_btnNutrition addTarget:self action:@selector(onNutritionTapped) forControlEvents:UIControlEventTouchUpInside];
    
    _tabBg =[[ UIImageView alloc] initWithImage:[UIImage imageNamed:@"tab-selection-indicator"]];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self getRecipe];
    
}
- (void)getRecipe
{
    _recipe = [Recipe MR_findFirstByAttribute:@"idRecipe" withValue:@(_recipeId)];
        //_arrIngredients = _dictRecipe[@"ingredients"];
    _arrNutrients =  [_recipe.nutritionalFacts allObjects];
    _arrIngredients = [_recipe.ingredients allObjects];
    [self.tableView reloadData];

}
- (void)onOverviewTapped
{
    _selectedTab = CCRecipeScreenTabOverview;
    NSRange range = NSMakeRange(1, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationAutomatic];
}
- (void)onInstructionsTapped
{
    _selectedTab = CCRecipeScreenTabInstructions;
    NSRange range = NSMakeRange(1, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationAutomatic];
}
- (void)onNutritionTapped
{
    _selectedTab = CCRecipeScreenTabNutrition;
    NSRange range = NSMakeRange(1, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationAutomatic];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    } else if (section == 1) {
    
        return _selectedTab == CCRecipeScreenTabOverview ?_arrIngredients.count:_selectedTab == CCRecipeScreenTabNutrition?  _arrNutrients.count : 1;
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"";
    UITableViewCell *cell;
    
    if (indexPath.section == 0) {
        identifier = @"Header Cell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        if (! cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        [(UILabel *)[cell.contentView viewWithTag:2] setText:_recipe.title];
        UIImage *img = [UIImage imageNamed:[_recipe.image stringByReplacingOccurrencesOfString:@"jpeg" withString   :@"jpg"]];
        [(UIImageView *)[cell.contentView viewWithTag:1] setImage:img];
        
        
        
        
    } else if (_selectedTab == CCRecipeScreenTabOverview) {
        
        if (indexPath.section == 1) {
            identifier = @"Ingridient Cell";
            cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
            if (! cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            if (fmodf(indexPath.row, 2) != 0) {
                [cell.contentView setBackgroundColor:[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1]];
            } else {
                [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            }
            Ingredient *ingredient = _arrIngredients[indexPath.row];
            [(UILabel *)[cell.contentView viewWithTag:1] setText:[NSString stringWithFormat:@"• %@",ingredient.name]];
        
        }
    } else if (_selectedTab == CCRecipeScreenTabNutrition) {
        
        if (indexPath.section == 1 && indexPath.row == 0) {
            identifier = @"Nutrition Header";
            cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
            if (! cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            
            [(UILabel *)[cell.contentView viewWithTag:1] setText:[[TSLanguageManager localizedString:@"Info"] uppercaseString]];
            [(UILabel *)[cell.contentView viewWithTag:2] setText:[TSLanguageManager localizedString:@"Weight"]];
            
            
        } else if (indexPath.section == 1 && indexPath.row >= 1) {
            identifier = @"Nutrient Cell";
            cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
            if (! cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            
            NutritionalFact_Recipe *nutrientRecipe = _arrNutrients[indexPath.row - 1];
            NSString *nutrientName = nutrientRecipe.nutritionalFact.name;
            NSString *nutrientValue = [NSString stringWithFormat:@"%.2f", nutrientRecipe.valueValue];
            [(UILabel *)[cell.contentView viewWithTag:1] setText:[TSLanguageManager localizedString:[nutrientName capitalizedString]]];
            [(UILabel *)[cell.contentView viewWithTag:2] setText:nutrientValue];
        }
        
    }
    else if (_selectedTab == CCRecipeScreenTabInstructions) {
        
        identifier = @"Instructions Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        if (! cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        NSString *instructions  = _recipe.instructions;
        NSAttributedString *attributedText =
        [[NSAttributedString alloc]
         initWithString:instructions
         attributes:@
         {
         NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]
         }];
        CGRect rect = [attributedText boundingRectWithSize:(CGSize){300, CGFLOAT_MAX}
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                    context:nil];
        [(UILabel *)[cell.contentView viewWithTag:1]  setFrame:CGRectMake(10, 20,300, rect.size.height + 20)];
        [(UILabel *)[cell.contentView viewWithTag:1] setText:instructions];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        return 245;
    } else if (indexPath.section == 1) {
        if (_selectedTab == CCRecipeScreenTabNutrition || _selectedTab == CCRecipeScreenTabOverview) {
            return 45;
        } else {
            NSString *instructions  = _recipe.instructions;
            NSAttributedString *attributedText =
            [[NSAttributedString alloc]
             initWithString:instructions
             attributes:@
             {
             NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]
             }];
            CGRect rect = [attributedText boundingRectWithSize:(CGSize){300, CGFLOAT_MAX}
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil];
            return rect.size.height + 20;
        }
        
    }

    return 0;

}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        
        UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        [header setBackgroundColor:[UIColor whiteColor]];
     
        UIColor *separatorColor = [UIColor colorWithRed:222.0/255.0 green:222.0/255.0 blue:222.0/255.0 alpha:1];
        
        UIView *verticalSeparator1 = [[UIView alloc] initWithFrame:CGRectMake(105, 0, 1, 50)];
        [verticalSeparator1 setBackgroundColor:separatorColor];
        
        UIView *verticalSeparator2 = [[UIView alloc] initWithFrame:CGRectMake(215, 0, 1, 50)];
        [verticalSeparator2 setBackgroundColor:separatorColor];
        
        UIImageView *topBorder = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 320, 1)];
        topBorder.backgroundColor = [UIColor colorWithRed:222.0/255.0 green:222.0/255.0 blue:222.0/255.0 alpha:1];
        
        UIImageView *bottomBorder = [[UIImageView alloc] initWithFrame:CGRectMake(0, 49, 320, 1)];
        bottomBorder.backgroundColor = [UIColor colorWithRed:222.0/255.0 green:222.0/255.0 blue:222.0/255.0 alpha:1];
        
        [header addSubview:verticalSeparator1];
        [header addSubview:verticalSeparator2];
                [header addSubview:topBorder];
        [header addSubview:bottomBorder];
        
        
        if (_selectedTab == CCRecipeScreenTabOverview) {
          
            [_tabBg setFrame:CGRectMake(0, 0, 105, 56)];
            [_btnOverview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_btnInstructions setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [_btnNutrition setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            
        } else if (_selectedTab == CCRecipeScreenTabInstructions) {
           
            [_tabBg setFrame:CGRectMake(105, 0, 110, 56)];
            [_btnOverview setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [_btnInstructions setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_btnNutrition setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        } else if (_selectedTab == CCRecipeScreenTabNutrition) {
            
            [_tabBg setFrame:CGRectMake(215, 0, 105, 56)];
            [_btnOverview setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [_btnInstructions setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [_btnNutrition setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        }
        [header addSubview:_btnOverview];
        [header addSubview:_btnInstructions];
        [header addSubview:_btnNutrition];
        [header insertSubview:_tabBg aboveSubview:bottomBorder];
        
        return header;
        
    }
    
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 50.0;
    }
    return 0.0;
}
@end
