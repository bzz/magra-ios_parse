//
//  ViewUtils.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 02/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ViewUtils : NSObject

+(void)setLeftViewOnTextField:(UITextField *)textField withImage:(UIImage *)image;
+(void)setLeftViewOnTextField:(UITextField *)textField withImage:(UIImage *)image andText:(NSString *)text;
+(void)setLeftViewOnTextField:(UITextField *)textField withText:(NSString *)text;
+(void)drawBasicShadowOnView:(UIView *)view;
+(void)fixSeparatorsHeightInView:(UIView *)views;
+(UIColor *)greenColor;
+(UIColor *)greenColorPressed;
+(UIColor *)lightBlueColor;
+(UIColor *)lightGrayColor;
+(UIColor *)orangeColor;

@end
