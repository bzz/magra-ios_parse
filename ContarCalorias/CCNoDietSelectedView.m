//
//  CCNoDietSelectedView.m
//  ContarCalorias
//
//  Created by andres portillo on 10/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCNoDietSelectedView.h"

@implementation CCNoDietSelectedView
- (void)awakeFromNib
{
    [_lblNotFollowingAnyDiet setText:[TSLanguageManager localizedString:@"You are not following any diet"]];
    [_btnPickADiet setTitle:[TSLanguageManager localizedString:@"Pick a diet"] forState:UIControlStateNormal];
    [_lblOr setText:[TSLanguageManager localizedString:@"Or"]];
    [_btnTalkToNutritionist setTitle:[TSLanguageManager localizedString:@"Talk to the nutritionist"] forState:UIControlStateNormal];
}

- (IBAction)onPickADietTapped:(id)sender {
    [self.delegate didTapPickADietButton];
}
- (IBAction)onTalkToNutritionistTapped:(id)sender {
    [self.delegate didTapContactDietitianButton];
}

@end
