//
//  APLineChart.h
//  ContarCalorias
//
//  Created by andres portillo on 12/8/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APLineChart : UIView
@property (strong,nonatomic) NSArray *arrMarkers;
@property(nonatomic) CGFloat userGoalWeight;
@property(nonatomic) NSInteger maximumNumberOfMarkers;
- (void)render;
@end
