//
//  CCFirstLaunchVC.m
//  ContarCalorias
//
//  Created by andres portillo on 9/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCLoginVC.h"
#import "CCConnectView.h"
#import "CCLoginView.h"
#import "CCForgotPasswordView.h"
#import "CCRegistrationView.h"

const CGFloat parallaxBackgroundInitialPositionX = -90;
@interface CCLoginVC ()<UIScrollViewDelegate, CCConnectViewDelegate, CCLoginViewDelegate>

@property (assign) CGPoint lastScrollPosition;
@end

@implementation CCLoginVC
{
    BOOL _introductionWasDismissed;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.screenName = @"Login/Registration Screen";
    self.scrollview.contentOffset = self.lastScrollPosition;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.lastScrollPosition = CGPointZero;
    
    CGFloat yDelta = 0;
    if([[UIScreen mainScreen] bounds].size.height <= 480.0) {
        yDelta = -20;
        [self.targetView setImage:[UIImage imageNamed:@"launch-screens-bg-iphone4"]];
        [self.scrollview setContentOffset:CGPointZero];
        [self.targetView setFrame:CGRectMake(0,0, self.targetView.frame.size.width, 480)];
        
    } else {
        
        [self.targetView setImage:[UIImage imageNamed:@"launch-screens-bg"]];
        [self.targetView setFrame:CGRectMake(0,0, self.targetView.frame.size.width, 568)];
        
    }
    
        
    for (UILabel *lbl in _lblsCollection) {
        lbl.center = CGPointMake(lbl.center.x,lbl.center.y + yDelta);
    }
    
    NSInteger count = 0;
    
    for (UIImageView *img in _imgsCollection) {
        count = count + 1;
        NSString *imgName = [NSString stringWithFormat:@"%d-%@",count,[TSLanguageManager selectedLanguage]];
        [img setImage:[UIImage imageNamed:imgName]];
        img.center = CGPointMake(160 + (320 *(count - 1)),img.center.y + yDelta);
         
    }
    
    self.connectView.delegate = self;
    self.loginView.delegate = self;
    // Do any additional setup after loading the view.
    
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:kCCAppIntroductionWasPresented]) {
        
        self.connectView.alpha = 1;
        [self.scrollview setContentSize:CGSizeMake(320 * 8, self.scrollview.frame.size.height)];
        [self.scrollview setContentOffset:CGPointMake(1600, 0)];
        self.lastScrollPosition = CGPointMake(1600, 0);
        
        [self.scrollview setScrollEnabled:NO];
        [self.bottomView setFrame:CGRectMake( self.bottomView.frame.origin.x, 800, self.bottomView.frame.size.width, self.bottomView.frame.size.height)];
        [self.btnStart setFrame:CGRectMake( 0, 800, self.btnStart.frame.size.width, self.btnStart.frame.size.height)];
        [self.pageControl setFrame:CGRectMake( self.pageControl.frame.origin.x, 800, self.pageControl.frame.size.width, self.pageControl.frame.size.height)];
    } else {
        self.lastScrollPosition = CGPointZero;
    }
    [self setLocalizedStrings];
}
- (void)setLocalizedStrings
{
    [_lblArticles setText:[TSLanguageManager localizedString:@"The articles and challenges"]];
    [_lblKeepTrack setText:[TSLanguageManager localizedString:@"Keep track of"]];
    [_lblLearnHow setText:[TSLanguageManager localizedString:@"Learn how to stay healthy"]];
    [_lblLogMeals setText:[TSLanguageManager localizedString:@"Log your meals with ease"]];
    [_lblMagraTasks setText:[TSLanguageManager localizedString:@"Magra gives you daily tasks"]];
    [_btnStart setTitle:[TSLanguageManager localizedString:@"Start"] forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onStartNowButtonTapped:(id)sender
{
    self.lastScrollPosition = CGPointMake(1600, 0);
    self.connectView.alpha = 1;
    [self.scrollview setContentSize:CGSizeMake(320 * 8, self.scrollview.frame.size.height)];
    [self.scrollview scrollRectToVisible:CGRectMake( _scrollview.frame.size.width * 5, 0, _scrollview.frame.size.width, _scrollview.frame.size.height) animated:NO];
    
    [UIView animateWithDuration:0.24 animations:^{
        
        [self.bottomView setFrame:CGRectMake( self.bottomView.frame.origin.x, 800, self.bottomView.frame.size.width, self.bottomView.frame.size.height)];
        
        [self.btnStart setFrame:CGRectMake( 0, 800, self.btnStart.frame.size.width, self.btnStart.frame.size.height)];
        
        [self.pageControl setFrame:CGRectMake( self.pageControl.frame.origin.x, 800, self.pageControl.frame.size.width, self.pageControl.frame.size.height)];
        
    } completion:^(BOOL finished) {
        [self.scrollview setScrollEnabled:NO];
    }];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kCCAppIntroductionWasPresented];
    [[NSUserDefaults standardUserDefaults] synchronize];
    _introductionWasDismissed = YES;
}
#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
   
    if (scrollView == _scrollview) {
        NSInteger currentPage = (_scrollview.contentOffset.x/_scrollview.frame.size.width);
        [ _pageControl setCurrentPage:currentPage];
        if((_scrollview.contentOffset.x / 320) < 6)
            [self.targetView setFrame:CGRectMake(parallaxBackgroundInitialPositionX-(scrollView.contentOffset.x/9), self.targetView.frame.origin.y, self.targetView.frame.size.width, self.targetView.frame.size.height)];
    }
}
#pragma mark - CCConnectView Delegate
- (void)theEmailLoginButtonWasPressedOntheConnectView
{
    self.loginView.alpha = 1;
    self.registrationView.alpha = 0;
    
    [self.scrollview setContentSize:CGSizeMake(320 * 8, self.scrollview.frame.size.height)];
    [self.scrollview scrollRectToVisible:CGRectMake( _scrollview.frame.size.width * 6, 0, _scrollview.frame.size.width, _scrollview.frame.size.height) animated:YES];
}
- (void)theEmailRegistrationButtonWasPressedOntheConnectView
{
    self.registrationView.alpha = 1;
    self.loginView.alpha =  0;
    [self.scrollview setContentSize:CGSizeMake(320 * 8, self.scrollview.frame.size.height)];
    [self.scrollview scrollRectToVisible:CGRectMake( _scrollview.frame.size.width * 6, 0, _scrollview.frame.size.width, _scrollview.frame.size.height) animated:YES];
}
#pragma mark - CCLoginDelegate
- (void)theForgotPasswordButtonWasPressedOntheLoginView
{
    [self.scrollview setContentSize:CGSizeMake(340 * 8, self.scrollview.frame.size.height)];
    [self.scrollview scrollRectToVisible:CGRectMake( _scrollview.frame.size.width * 7, 0, _scrollview.frame.size.width, _scrollview.frame.size.height) animated:YES];
}
#pragma mark - UINavigation 


- (void)dismissModalViewControllerAnimated:(BOOL)animated
{
    self.lastScrollPosition = self.scrollview.contentOffset;
    [super dismissModalViewControllerAnimated:animated];
}

@end
