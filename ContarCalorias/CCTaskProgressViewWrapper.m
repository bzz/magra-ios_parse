//
//  CCTaskProgressView.m
//  ContarCalorias
//
//  Created by andres portillo on 13/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCTaskProgressViewWrapper.h"
#import "CCTaskProgressView.h"


@implementation CCTaskProgressViewWrapper

- (void)awakeFromNib
{
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self.layer setCornerRadius:4];
        self.clipsToBounds = YES;
        self.progressView = [[CCTaskProgressView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 10)];
        [self addSubview:self.progressView];
    }
    return self;
}

- (void)setProgress:(CGFloat)progress
{
    [_progressView setProgress:progress];
}
@end
