//
//  CCMealPlanCell.m
//  ContarCalorias
//
//  Created by andres portillo on 14/11/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCMealPlanCell.h"
#import "CCAPI.h"
#import "User.h"
#import "Meal.h"
#import "MealPlan.h"
#import "MealType.h"
#import "CCUtils.h"
#import "APCSeeMoreLabel.h"
#import "SWRevealViewController.h"

@interface CCMealPlanCell ()<UIScrollViewDelegate>

@end

@implementation CCMealPlanCell
{
    User *_user;
    NSArray *_arrMeals;
    NSInteger _dietDay;
    NSArray *_arrTypesOfMeals;
    NSInteger _countTypeOfMeal;
}
- (void)awakeFromNib {
    _user = [[CCAPI sharedInstance] getUser];
    
    [_scrollView setDelegate:self];
    
    UITapGestureRecognizer *taprecognizer = [[UITapGestureRecognizer alloc] initWithTarget :self action: @selector(sendSelectionNotification)];
    [taprecognizer setNumberOfTouchesRequired : 1];
    [_viewContainer addGestureRecognizer: taprecognizer];

    
    _dietDay = ([CCUtils numberOfDaysWithinFirstDate:_user.mealPlanStartDate andSecondDate:[NSDate date]] / _user.mealPlan.days.integerValue) + 1;
    
    _arrMeals = [[_user.mealPlan.meals allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"dayOfPlan = %d",_dietDay]];
    _arrTypesOfMeals = [MealType MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"ANY meals IN %@",_arrMeals]];
    
    
    MealType *breakfastType = [[_arrTypesOfMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name = %@",@"breakfast"]] firstObject];
    MealType *morningSnackType = [[_arrTypesOfMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name = %@",@"morningsnack"]] firstObject];
    MealType *lunchType = [[_arrTypesOfMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name = %@",@"lunch"]] firstObject];
    MealType *dinnerType = [[_arrTypesOfMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name = %@",@"dinner"]] firstObject];
    MealType *eveningsnackType = [[_arrTypesOfMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name = %@",@"eveningsnack"]] firstObject];
 
    if (breakfastType) {
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15 + (300 * _countTypeOfMeal), 15, 200, 20)];
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13]];
        [lblTitle setText:[[TSLanguageManager localizedString:@"Breakfast"] uppercaseString]];
        [lblTitle setTextColor:[UIColor blackColor]];
        [_scrollView addSubview:lblTitle];
        
        APCSeeMoreLabel *lblDesc= [[APCSeeMoreLabel alloc] initWithFrame:CGRectMake(15 + (300 * _countTypeOfMeal), 35, 270, 75)];
        [lblDesc setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
        [lblDesc setTextColor:CCColorDarkGray];
        [lblDesc setNumberOfLines:0];
        NSArray *arrBreakfastMeals = [_arrMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mealType = %@",breakfastType]];
        
        NSString *mealDesc = @"";
        for (Meal *meal in arrBreakfastMeals) {
            if (meal == [arrBreakfastMeals firstObject]) {
                mealDesc = [mealDesc stringByAppendingString:[NSString stringWithFormat:@"%@",meal.title]];
            } else {
                    mealDesc = [mealDesc stringByAppendingString:[NSString stringWithFormat:@", %@",meal.title]];
            }
        }
        [lblDesc setText:mealDesc withSeeMoreLocalizedText:[TSLanguageManager localizedString:@"See more"]];
        [lblDesc sizeToFit];
        
        [_scrollView addSubview:lblDesc];
        
        _countTypeOfMeal ++;
        
    }
    if (morningSnackType) {
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15 + (300 * _countTypeOfMeal), 15, 200, 20)];
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13]];
        [lblTitle setText:[[TSLanguageManager localizedString:@"Morningsnack"] uppercaseString]];
        [lblTitle setTextColor:[UIColor blackColor]];
        [_scrollView addSubview:lblTitle];
        
        APCSeeMoreLabel *lblDesc= [[APCSeeMoreLabel alloc] initWithFrame:CGRectMake(15 + (300 * _countTypeOfMeal), 35, 270, 75)];
        [lblDesc setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
        [lblDesc setTextColor:CCColorDarkGray];
        [lblDesc setNumberOfLines:0];
        NSArray *arrMorningSnackMeals = [_arrMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mealType = %@",morningSnackType]];
        
        NSString *mealDesc = @"";
        for (Meal *meal in arrMorningSnackMeals) {
            if (meal == [arrMorningSnackMeals firstObject]) {
                mealDesc = [mealDesc stringByAppendingString:[NSString stringWithFormat:@"%@",meal.title]];
            } else {
                mealDesc = [mealDesc stringByAppendingString:[NSString stringWithFormat:@", %@",meal.title]];
            }
        }
        [lblDesc setText:mealDesc withSeeMoreLocalizedText:[TSLanguageManager localizedString:@"See more"]];
        [lblDesc sizeToFit];
        
        [_scrollView addSubview:lblDesc];
        
        _countTypeOfMeal ++;
        
    }
    if (lunchType) {
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15 + (300 * _countTypeOfMeal), 15, 200, 20)];
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13]];
        [lblTitle setText:[[TSLanguageManager localizedString:@"Lunch"] uppercaseString]];
        [lblTitle setTextColor:[UIColor blackColor]];
        [_scrollView addSubview:lblTitle];
        
        APCSeeMoreLabel *lblDesc= [[APCSeeMoreLabel alloc] initWithFrame:CGRectMake(15 + (300 * _countTypeOfMeal), 40, 270, 75)];
        [lblDesc setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
        [lblDesc setTextColor:CCColorDarkGray];
        [lblDesc setNumberOfLines:0];
        NSArray *arrLunchMeals = [_arrMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mealType = %@",lunchType]];
        
        NSString *mealDesc = @"";
        for (Meal *meal in arrLunchMeals) {
            if (meal == [arrLunchMeals firstObject]) {
                mealDesc = [mealDesc stringByAppendingString:[NSString stringWithFormat:@"%@",meal.title]];
            } else {
                mealDesc = [mealDesc stringByAppendingString:[NSString stringWithFormat:@", %@",meal.title]];
            }
        }
        [lblDesc setText:mealDesc withSeeMoreLocalizedText:[TSLanguageManager localizedString:@"See more"]];
        [lblDesc sizeToFit];
        
        [_scrollView addSubview:lblDesc];
        
        _countTypeOfMeal ++;
        
    }
    if (dinnerType) {
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15 + (300 * _countTypeOfMeal), 15, 200, 20)];
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13]];
        [lblTitle setText:[[TSLanguageManager localizedString:@"Dinner"] uppercaseString]];
        [lblTitle setTextColor:[UIColor blackColor]];
        [_scrollView addSubview:lblTitle];
        
        APCSeeMoreLabel *lblDesc= [[APCSeeMoreLabel alloc] initWithFrame:CGRectMake(15 + (300 * _countTypeOfMeal), 40, 270, 75)];
        [lblDesc setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
        [lblDesc setTextColor:CCColorDarkGray];
        [lblDesc setNumberOfLines:0];
        NSArray *arrDinnerMeals = [_arrMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mealType = %@",dinnerType]];
        
        NSString *mealDesc = @"";
        for (Meal *meal in arrDinnerMeals) {
            if (meal == [arrDinnerMeals firstObject]) {
                mealDesc = [mealDesc stringByAppendingString:[NSString stringWithFormat:@"%@",meal.title]];
            } else {
                mealDesc = [mealDesc stringByAppendingString:[NSString stringWithFormat:@", %@",meal.title]];
            }
        }
        [lblDesc setText:mealDesc withSeeMoreLocalizedText:[TSLanguageManager localizedString:@"See more"]];
        [lblDesc sizeToFit];
        [_scrollView addSubview:lblDesc];
        
        _countTypeOfMeal ++;
        
    }
    if (eveningsnackType) {
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15 + (300 * _countTypeOfMeal), 15, 200, 20)];
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13]];
        [lblTitle setText:[[TSLanguageManager localizedString:@"Eveningsnack"] uppercaseString]];
        [lblTitle setTextColor:[UIColor blackColor]];
        [_scrollView addSubview:lblTitle];
        
        APCSeeMoreLabel *lblDesc= [[APCSeeMoreLabel alloc] initWithFrame:CGRectMake(15 + (300 * _countTypeOfMeal), 40, 270, 75)];
        [lblDesc setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
        [lblDesc setTextColor:CCColorDarkGray];
        [lblDesc setNumberOfLines:0];
        NSArray *arrEveningsnackMeals = [_arrMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mealType = %@",eveningsnackType]];
        
        NSString *mealDesc = @"";
        for (Meal *meal in arrEveningsnackMeals) {
            if (meal == [arrEveningsnackMeals firstObject]) {
                mealDesc = [mealDesc stringByAppendingString:[NSString stringWithFormat:@"%@",meal.title]];
            } else {
                mealDesc = [mealDesc stringByAppendingString:[NSString stringWithFormat:@", %@",meal.title]];
            }
        }
        [lblDesc setText:mealDesc withSeeMoreLocalizedText:[TSLanguageManager localizedString:@"See more"]];
        [lblDesc sizeToFit];
        [_scrollView addSubview:lblDesc];
        
        _countTypeOfMeal ++;
        
    }
    [_scrollView setContentSize:CGSizeMake(300 * _countTypeOfMeal, _scrollView.frame.size.height)];
    
    [_pageControl setNumberOfPages:_countTypeOfMeal];
}
- (void)sendSelectionNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kCCDietCellContentWasTapped object:nil];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger page = (scrollView.contentOffset.x / 300);
    [_pageControl setCurrentPage:page];
}
@end
