//
//  WelcomeViewController.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 29/12/13.
//  Copyright (c) 2013 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CCConnectViewDelegate
@optional
- (void)theEmailLoginButtonWasPressedOntheConnectView;
- (void)theEmailRegistrationButtonWasPressedOntheConnectView;
@end

@interface CCConnectView : UIView
@property (nonatomic, weak) id <CCConnectViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginFB;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UILabel *lblConnect;
@property (weak, nonatomic) IBOutlet UILabel *lblFBLogin;
@property (weak, nonatomic) IBOutlet UILabel *lblWithEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblWeWontPost;
@property (weak, nonatomic) IBOutlet UIButton *btnPrivacyPolicy;
@property (weak, nonatomic) IBOutlet UILabel *lblByRegisteringYouAgree;
@property (weak, nonatomic) IBOutlet UILabel *lblOr;
@property (weak, nonatomic) IBOutlet UIView *viewPrivacyPolicy;
@property (weak, nonatomic) IBOutlet UIView *viewLoginContainer;

    //actions

- (IBAction)performFacebookAuthentication:(id)sender;
- (IBAction)onEmailRegistrationButtonTapped:(id)sender;
- (IBAction)onEmailLoginButtonTapped:(id)sender;

@end
