//
//  RegistrationViewController.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 30/12/13.
//  Copyright (c) 2013 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CCRegistrationView : UIView 

@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIView *formWrapperView;
@property (nonatomic) NSString *emailAddress;
@property (nonatomic,strong) IBOutlet UITextField *txtFirstAndLastName;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic,strong) IBOutlet UITextField *txtCountry;
@property (nonatomic,strong) IBOutlet UITextField *txtEmail;
@property (nonatomic,strong) IBOutlet UITextField *txtPassword;
@property (nonatomic,strong) IBOutlet UILabel *lblRegistration;
- (IBAction)onRegisterUserButtonTapped:(id)sender;

- (IBAction)onBackButtonTapped:(id)sender;
@end
