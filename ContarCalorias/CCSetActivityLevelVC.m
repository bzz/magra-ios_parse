//
//  CCSetActivityLevelVC.m
//  ContarCalorias
//
//  Created by andres portillo on 12/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//
#import "CCSetActivityLevelVC.h"
#import "CCSetGoalWeightVC.h"
#import "CCAPI.h"
#import "User.h"
#import "CCActivityLevelCell.h"
#import "FontAwesomeKit.h"

@interface CCSetActivityLevelVC ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation CCSetActivityLevelVC{
    CCActivityLevel _activityLevel;
    NSArray *_arrCellTitles, *_arrCellDescriptions;
    User *_user;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
        //User
    _user = [[CCAPI sharedInstance] getUser];
  
    
    UIImageView *imgTitleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"activity-level-title-view-img"]];
    self.navigationItem.titleView = imgTitleView;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self addButtonItem];
        
    });
    // Do any additional setup after loading the view.
    
    _activityLevel = CCActivityLevelSedentary;
    
    _arrCellTitles = @[[TSLanguageManager localizedString:@"Sedentary"],  [TSLanguageManager localizedString:@"Slightly Active"],[TSLanguageManager localizedString:@"Active"],[TSLanguageManager localizedString:@"Very Active"]];
    _arrCellDescriptions =@[[TSLanguageManager localizedString:@"Description for sedentary"],[TSLanguageManager localizedString:@"Description for slightly active"], [TSLanguageManager localizedString:@"Description for active"], [TSLanguageManager localizedString:@"Description for very active"]];
    
    [_lblScreenDescription setText:[TSLanguageManager localizedString:@"Activity level screen description"]];
    [_lblScreentitle setText:[TSLanguageManager localizedString:@"What's your activity level"]];
    
}
-(void)addButtonItem
{
    FAKFontAwesome *nextIcon = [FAKFontAwesome arrowRightIconWithSize:28];
    [nextIcon addAttribute:NSForegroundColorAttributeName value:CCColorOrange];
    UIImage *imgNext = [[nextIcon imageWithSize:CGSizeMake(25, 25)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *btnNext = [[UIBarButtonItem alloc] initWithImage:imgNext style:UIBarButtonItemStyleBordered target:self action:@selector(onNextButtonTapped)];
    [self.navigationItem setRightBarButtonItem:btnNext];
    
}
- (void)onNextButtonTapped
{
    [self performSegueWithIdentifier:@"Next Screen Segue" sender:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Next Screen Segue"]) {
        _user.activityLevelValue = _activityLevel;
    }

}
#pragma mark - UITable view Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrCellDescriptions.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    NSString *cellIdentifier = @"Activity Level Cell";
    CCActivityLevelCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if( cell== nil) {
        
        cell = [[CCActivityLevelCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    cell.lblTitle.text = [_arrCellTitles objectAtIndex:row];
    cell.lblDescription.text = [_arrCellDescriptions objectAtIndex:row];
    
    if ( row == _activityLevel) {
        [cell.imgState  setImage:[UIImage imageNamed:@"select-btn-active"]];
    } else{
        [cell.imgState setImage:[UIImage imageNamed:@"select-btn"]];
    }
    return cell;
}
#pragma  mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _activityLevel = indexPath.row;
    [self.tableView reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0;
}
@end
