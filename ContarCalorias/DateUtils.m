//
//  DateUtils.m
//  ContarCalorias
//
//  Created by Adrian Ghitun on 1/28/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "DateUtils.h"

@implementation DateUtils

+(NSString *)standardStringFromDate:(NSDate *)date{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MM-yyyy"];
    return [df stringFromDate:date];
}

+(NSString *)clientTimeZone
{
    NSTimeZone *timezone = [NSTimeZone systemTimeZone];
    NSString *timezoneName = timezone.name;
    
    return timezoneName;
}

+(float)utcOffsetFromDate:(NSDate *)date
{
    NSTimeZone *timezone = [NSTimeZone systemTimeZone];
    float offset = [timezone secondsFromGMTForDate:date];
    
    return offset;
}

+ (NSDate *)dateByAddingSeconds:(float)seconds toDate:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setSecond:-seconds];
    
    NSDate *finalDate = [calendar dateByAddingComponents:comps toDate:date options:0];
    
    return finalDate;
}

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *fromDate;
    NSDate *toDate;
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

@end
