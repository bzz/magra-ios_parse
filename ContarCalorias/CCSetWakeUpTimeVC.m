//
//  CCSetWakeUpTimeVC.m
//  ContarCalorias
//
//  Created by andres portillo on 17/11/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCSetWakeUpTimeVC.h"
#import "CCSetWeightLossPlanResultsVC.h"
#import "User.h"
#import "CCAPI.h"
#import "CCUtils.h"
#import "FontAwesomeKit.h"

@interface CCSetWakeUpTimeVC ()

@end

@implementation CCSetWakeUpTimeVC
{
    User *_user;
    NSDate *_wakeupTime;
    UIDatePicker *_datePicker;
    NSDateFormatter *_tf;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _user = [[CCAPI sharedInstance] getUser];
    _datePicker = [[UIDatePicker alloc] init];
    [_datePicker setBackgroundColor:[UIColor whiteColor]];
    [_datePicker setDatePickerMode:UIDatePickerModeTime];
    [_datePicker addTarget:self action:@selector(didUpdateWakeupTime:) forControlEvents:UIControlEventValueChanged];
    
    [_txtWakeupTime setInputView:_datePicker];
    _tf = [NSDateFormatter new];
    [_tf setDateFormat:@"HH:mm a"];
    _wakeupTime = [CCUtils getDefaultWakeupTime];
    [_datePicker setDate:_wakeupTime];
    
    if ([[TSLanguageManager selectedLanguage] isEqualToString:@"es"]) {
        [_tf setLocale:[NSLocale localeWithLocaleIdentifier:@"es_es"]];
    } else {
        [_tf setLocale:[NSLocale localeWithLocaleIdentifier:@"pt_br"]];
    }
    
    [_txtWakeupTime setText:[_tf stringFromDate:_wakeupTime]];
    [_txtWakeupTime becomeFirstResponder];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self addButtonItem];
        
    });
    
    [_lblScreenTitle setText:[TSLanguageManager localizedString:@"What time do you wake up?"]];
}
- (void)addButtonItem
{
    FAKFontAwesome *nextIcon = [FAKFontAwesome arrowRightIconWithSize:28];
    [nextIcon addAttribute:NSForegroundColorAttributeName value:CCColorOrange];
    UIImage *imgNext = [[nextIcon imageWithSize:CGSizeMake(25, 25)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *btnNext = [[UIBarButtonItem alloc] initWithImage:imgNext style:UIBarButtonItemStyleBordered target:self action:@selector(onNextButtonTapped)];
    [self.navigationItem setRightBarButtonItem:btnNext];
}
- (void)didUpdateWakeupTime:(UIDatePicker *)datePicker
{
    _wakeupTime = datePicker.date;
    [_txtWakeupTime setText:[_tf stringFromDate:_wakeupTime]];
}
- (void)onNextButtonTapped
{
    [self performSegueWithIdentifier:@"Results Segue" sender:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"Results Segue"]) {
    
        _user.wakeupTime = _wakeupTime;
        [_user setMealTimesUsingWakeupTime];
        _user.firstDayDate = [NSDate date];
        _user.didCreateWeightLossPlanValue = YES;
        [_user updateDateReachingDesiredWeight];
        [[CCAPI sharedInstance] setFirstdayDate:[NSDate date]];
        [[CCAPI sharedInstance] scheduleLocalNotificationsForArticles];
        [[CCAPI sharedInstance] saveUserWithCompletion:^{
                [[CCAPI sharedInstance] updateMagraUser];
        }];
        
        
    }
}


@end
