//
//  CCForgotPasswordVC.m
//  ContarCalorias
//
//  Created by andres portillo on 11/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCForgotPasswordView.h"
#import "CCUtils.h"
#import "CCAPI.h"
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
@interface CCForgotPasswordView ()

@end

@implementation CCForgotPasswordView

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    gestureRecognizer.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:gestureRecognizer];
    
    [self setLocalizedStrings];
}
- (void)setLocalizedStrings
{
    [_btnRecoverPassword setTitle:[TSLanguageManager localizedString:@"Password recovery"] forState:UIControlStateNormal];
    [_lblForgotPassword setText:[TSLanguageManager localizedString:@"Forgot password?"]];
    [_lblIfYouForgot setText:[TSLanguageManager localizedString:@"If you forgot"]];
    [_txtEmail setPlaceholder:[TSLanguageManager localizedString:@"Email"]];
}
- (void)dismissKeyboard
{
    [self endEditing:YES];
}

- (IBAction)onResetPasswordButtonTapped:(id)sender {
    [self endEditing:YES];
    if(_txtEmail.text.length > 0 && [CCUtils emailIsValid:_txtEmail.text])
    {
        NSDictionary *params = @{@"email":_txtEmail.text,@"locale":[TSLanguageManager selectedLanguage]};
        [_activityIndicator startAnimating];
        [[CCAPI sharedInstance]  resetUserPassword:params success:^(id responseObject) {
            [_activityIndicator stopAnimating];
            
            [UIAlertView showWithTitle:@"" message:[TSLanguageManager localizedString:@"An email with your password"] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [_txtEmail setText:@""];
                [self sendScrollViewToPreviousPage];
            }];
        } failure:^(NSError *error) {
            [_activityIndicator stopAnimating];
            NSMutableDictionary *userInfo = [error.userInfo mutableCopy];
            NSInteger statusCode = [[userInfo objectForKey:@"AFNetworkingOperationFailingURLResponseErrorKey"] statusCode];
            if (statusCode == 404) {
                [UIAlertView showWithTitle:@"" message:[TSLanguageManager localizedString:@"This email is not registered"] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:nil];
            } else {
                [UIAlertView showWithTitle:@"" message:[TSLanguageManager localizedString:@"An error occurred. Please try again."] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:nil];
            }
        }];
    } else {
        [UIAlertView showWithTitle:@"" message:[TSLanguageManager localizedString:@"Please enter a valid email"] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:nil];
    }
}
- (void)sendScrollViewToPreviousPage
{
    
    [(UIScrollView *)self.superview scrollRectToVisible:CGRectMake(self.frame.origin.x - 320, self.frame.origin.y, self.frame.size.width, self.frame.size.height) animated:YES];
}
- (IBAction)onBackButtonTapped:(id)sender
{
    [self dismissKeyboard];
    [ (UIScrollView *)self.superview scrollRectToVisible:CGRectMake(self.frame.origin.x
                                                                    - 320, self.frame.origin.y , self.frame.size.width, self.frame.size.height) animated:YES];
}
#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtEmail) {
        [self endEditing:YES];
        [self onResetPasswordButtonTapped:nil];
    }
    return YES;
}
@end
