//
//  CCAdCell.m
//  ContarCalorias
//
//  Created by andres portillo on 17/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCAdCell.h"

@implementation CCAdCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setRating:(double)rating
{
    if(rating > 0.5) {
        [_firstStar setImage:[UIImage imageNamed:@"rating-star-yellow"]];
        if(rating > 1.5) {
            [_secondStar setImage:[UIImage imageNamed:@"rating-star-yellow"]];
            if(rating > 3.5) {
                [_thirdStar setImage:[UIImage imageNamed:@"rating-star-yellow"]];
                if(rating > 3.5) {
                    [_forthStar setImage:[UIImage imageNamed:@"rating-star-yellow"]];
                    if(rating > 4.5) {
                        [_fifthStar setImage:[UIImage imageNamed:@"rating-star-yellow"]];
                    }
                }
                
            }
            
        }
    }
    
}
@end
