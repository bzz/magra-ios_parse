//
//  DateUtils.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 1/28/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtils : NSObject

+(NSString *)standardStringFromDate:(NSDate *)date;
+(NSString *)clientTimeZone;
+(float)utcOffsetFromDate:(NSDate *)date;
+(NSDate *)dateByAddingSeconds:(float)seconds toDate:(NSDate *)date;
+(NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;

@end
