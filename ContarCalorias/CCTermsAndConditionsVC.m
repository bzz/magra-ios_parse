//
//  CCTermsAndConditionsVC.m
//  ContarCalorias
//
//  Created by andres portillo on 29/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCTermsAndConditionsVC.h"

@interface CCTermsAndConditionsVC ()<UIWebViewDelegate>

@end

@implementation CCTermsAndConditionsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *fullPath = @"";
    if ([[TSLanguageManager selectedLanguage] isEqualToString:@"pt"]) {
        fullPath = [[NSBundle mainBundle] pathForResource:@"privacy-policy-pt"
                                                             ofType:@"html"];

    } else {
        fullPath = [[NSBundle mainBundle] pathForResource:@"privacy-policy-es"
                                                             ofType:@"html"];
    }
    
    NSString *htmlString = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:nil];
    [self.webView loadHTMLString:htmlString baseURL:nil];
    [_activityIndicator startAnimating];
    
    [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Privacy policy"]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onDoneButtonTapped:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [_activityIndicator stopAnimating];
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.querySelector('meta[name=viewport]').setAttribute('content', 'width=%d;', false); ", (int)webView.frame.size.width]];
}
@end
