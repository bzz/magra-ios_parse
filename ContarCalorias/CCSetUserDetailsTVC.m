//
//  CCWeightLossPlanFIrstVC.m
//  ContarCalorias
//
//  Created by andres portillo on 12/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCSetUserDetailsTVC.h"
#import "CCSetActivityLevelVC.h"
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import "DDUnitConversion.h"
#import "CCAPI.h"
#import "CCUtils.h"
#import "User.h"
#import "FontAwesomeKit.h"

@interface CCSetUserDetailsTVC () <UIPickerViewDataSource,UIPickerViewDelegate, UITextFieldDelegate>
@end

@implementation CCSetUserDetailsTVC {
    UIToolbar *_inputAccessory;
    UIPickerView *_pickerHeight;
    UIPickerView *_pickerWeight;
    UIDatePicker *_datePicker;
    NSDateFormatter *_dateFormatter;
    UILabel *_lblHeightFirstUnit;
    UILabel *_lblHeightSecondUnit;
    NSInteger _currentHeightCentimeters, _age;
    NSDate *birthdate;
    CGFloat _currentWeightKilos;
    CCGender _gender;
    User *_user;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView setTableFooterView:[UIView new]];
    [self setInputAccessories];
    
  
    _pickerHeight = [[UIPickerView alloc] init];
    [_pickerHeight setBackgroundColor:[UIColor whiteColor]];
    [_pickerHeight setDelegate:self];
    
    _lblHeightFirstUnit = [[UILabel alloc] initWithFrame:CGRectMake( 130, 97, 40, 20)];
    [_lblHeightFirstUnit setText:[TSLanguageManager localizedString:@"Meter Abbrev"]];
    [_lblHeightFirstUnit setTextAlignment:NSTextAlignmentCenter];
    [_lblHeightFirstUnit setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [_lblHeightFirstUnit setTextColor:[UIColor lightGrayColor]];
    [_pickerHeight addSubview:_lblHeightFirstUnit];
    
    _lblHeightSecondUnit  = [[UILabel alloc] initWithFrame:CGRectMake( 220, 97, 40, 20)];
    [_lblHeightSecondUnit setText:[TSLanguageManager localizedString:@"Centimeter Abbrev"]];
    [_lblHeightSecondUnit setTextAlignment:NSTextAlignmentCenter];
    [_lblHeightSecondUnit setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [_lblHeightSecondUnit setTextColor:[UIColor lightGrayColor]];
    [_pickerHeight addSubview:_lblHeightSecondUnit];
    [_pickerHeight selectRow:1 inComponent:0 animated:NO];
    [_pickerHeight selectRow:70 inComponent:1 animated:NO];
    [_txtHeight setInputView:_pickerHeight];
    [_txtHeight setText:@"170"];
    _currentHeightCentimeters = 170;
    
    _user = [[CCAPI sharedInstance] getUser];
    
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    _datePicker = [[UIDatePicker alloc] init];
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    [_datePicker addTarget:self action:@selector(onBirthdateChanged:) forControlEvents:UIControlEventValueChanged];
    [_datePicker setBackgroundColor:[UIColor whiteColor]];
    
    if (_user.birthdate)
        [_datePicker setDate:_user.birthdate];
    
    if ([[TSLanguageManager selectedLanguage] isEqualToString:@"es"]) {
        [_datePicker setLocale:[NSLocale localeWithLocaleIdentifier:@"ES_es"]];
        
    } else {
        [_datePicker setLocale:[NSLocale localeWithLocaleIdentifier:@"pt_br"]];
    }
    
    
    [_txtBirthdate setInputView:_datePicker];
   
    if (_user.authenticationTypeValue == CCAuthenticationTypeFacebook) {
    
        if (_user.genderValue == CCGenderMale) {
            [self onMaleButtonTapped:nil];
        } else {
            [self onFemaleButtonTapped:nil];
        }
        
        _age = [CCUtils ageForUserWithBirthdate:_user.birthdate];
        _user.ageValue = _age;
        _txtBirthdate.text = [_dateFormatter stringFromDate:_user.birthdate];
        _gender = _user.genderValue;
    } else {
        [_datePicker setDate:[NSDate dateWithTimeIntervalSince1970:60*60*24*365*20]];
        _txtBirthdate.text = [_dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:60*60*24*365*20]];
        _gender = CCGenderMale;
        
    }
    
    if (_gender == CCGenderFemale) {
    
        _currentWeightKilos = 60;
        
    } else {
        _currentWeightKilos = 75;
    }
    
   
    _pickerWeight =[[UIPickerView alloc] init];
    [_pickerWeight setBackgroundColor:[UIColor whiteColor]];
    [_pickerWeight setDelegate:self];
    
    [_txtWeight setInputView:_pickerWeight];
    [_txtWeight setText:[NSString stringWithFormat:@"%.1f",_currentWeightKilos]];
    
    NSInteger weightFirstUnit = _currentWeightKilos;
    NSInteger weightSecondUnit = (_currentWeightKilos - weightFirstUnit) * 10;
    
    if (weightFirstUnit >= 14) {
        [_pickerWeight selectRow:MAX((weightFirstUnit - 14),0) inComponent:0 animated:NO];
        [_pickerWeight selectRow:weightSecondUnit inComponent:1 animated:NO];
    }
    
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithTitle:[TSLanguageManager localizedString:@"Cancel"] style:UIBarButtonItemStyleBordered target:self action:@selector(onCancelButtonTapped:)];
    [self.navigationItem setLeftBarButtonItem:btnCancel];
    
    FAKFontAwesome *nextIcon = [FAKFontAwesome arrowRightIconWithSize:28];
    [nextIcon addAttribute:NSForegroundColorAttributeName value:CCColorOrange];
    UIImage *imgNext = [[nextIcon imageWithSize:CGSizeMake(25, 25)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *btnNext = [[UIBarButtonItem alloc] initWithImage:imgNext style:UIBarButtonItemStyleBordered target:self action:@selector(onNextButtonTapped)];
    [self.navigationItem setRightBarButtonItem:btnNext];
    
    
    [self setLocalizedStrings];
}
- (void)setLocalizedStrings
{
    UIFont* font =[UIFont fontWithName:@"HelveticaNeue-Medium" size:15];
    CGRect frame = CGRectMake(0, 0, [[TSLanguageManager localizedString:@"Weight loss plan"] sizeWithAttributes:@{NSFontAttributeName:font}].width, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = font;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:58.0/255.0 green: 54.0/255.0 blue: 69.0/255.0 alpha:1];
    label.text = [TSLanguageManager localizedString:@"Weight loss plan"];
    self.navigationItem.titleView = label;
    
    [_lblIm setText:[TSLanguageManager localizedString:@"I am"]];
    [_lblMale setText:[TSLanguageManager localizedString:@"Man"]];
    [_lblFemale setText:[TSLanguageManager localizedString:@"Woman"]];
    [_lblMyHeight setText:[TSLanguageManager localizedString:@"My height"]];
    [_lblMyWeight setText:[TSLanguageManager localizedString:@"My weight"]];
    [_lblMyBirthdate setText:[TSLanguageManager localizedString:@"My birthdate"]];
    [_lblWeight setText:[TSLanguageManager localizedString:@"Kilos"]];
    [_lblHeight setText:[TSLanguageManager localizedString:@"Centimeters"]];
}
- (void)setInputAccessories
{
    _inputAccessory = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 40)];
    UIBarButtonItem  *spacer = [[UIBarButtonItem alloc]    initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [_inputAccessory setBarStyle:UIBarStyleDefault];
    [_inputAccessory setBarTintColor:CCColorGreen];
    [_inputAccessory setTintColor:[UIColor whiteColor]];
    
    FAKFontAwesome *checkIcon = [FAKFontAwesome checkIconWithSize:28];
    [checkIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    [_inputAccessory setItems:[NSArray arrayWithObjects:spacer,
                              [[UIBarButtonItem alloc] initWithImage:[checkIcon imageWithSize:CGSizeMake(25, 25)] style:UIBarButtonItemStyleDone target:self action:@selector(endEditing)],nil ] animated:YES];
    [_txtBirthdate setInputAccessoryView:_inputAccessory];
    [_txtWeight setInputAccessoryView:_inputAccessory];
    [_txtHeight setInputAccessoryView:_inputAccessory];
    
}
- (void)onBirthdateChanged:(UIDatePicker *)datePicker
{
    NSDate *date = datePicker.date;
    [_txtBirthdate setText:[_dateFormatter stringFromDate:date]];
}
- (BOOL)fieldsWereProperlyFilled {
    return _txtBirthdate.text.integerValue > 0 && _txtHeight.text.floatValue > 0.0 && _txtWeight.text.floatValue > 0.0;
}
- (void)endEditing
{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)onNextButtonTapped
{
    if ([self fieldsWereProperlyFilled]) {
      
        [self performSegueWithIdentifier:@"Next Screen Segue" sender:nil];
        
    } else {
    
        [UIAlertView showWithTitle:[TSLanguageManager localizedString:@"Error"] message:[TSLanguageManager localizedString:@"Please enter all fields"] cancelButtonTitle:[TSLanguageManager localizedString:@"Ok"] otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            
        }];
        
    }
}
- (IBAction)onMaleButtonTapped:(id)sender
{
    [_switchButtonMale setImage:[UIImage imageNamed:@"select-btn-active"] forState:UIControlStateNormal];
    [_switchButtonFemale setImage:[UIImage imageNamed:@"select-btn"] forState:UIControlStateNormal];
    [_lblMale setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15]];
    [_lblFemale setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:15]];
    [_lblMale setTextColor:[UIColor blackColor]];
    [_lblFemale setTextColor:CCColorGray];
    _gender = CCGenderMale;
    
}
- (IBAction)onFemaleButtonTapped:(id)sender
{
    [_switchButtonMale setImage:[UIImage imageNamed:@"select-btn"] forState:UIControlStateNormal];
    [_switchButtonFemale setImage:[UIImage imageNamed:@"select-btn-active"] forState:UIControlStateNormal];
    [_lblFemale setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15]];
    [_lblMale setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:15]];
    [_lblFemale setTextColor:[UIColor blackColor]];
    [_lblMale setTextColor:CCColorGray];
    _gender = CCGenderFemale;
}
- (void)onCancelButtonTapped:(id)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - UITextField
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _txtWeight) {
        _currentWeightKilos = [textField.text floatValue];
    } else if(textField == _txtBirthdate) {
        NSCalendar *calendar=[NSCalendar currentCalendar];
        NSDateComponents *components=[calendar components:NSYearCalendarUnit fromDate:_datePicker.date toDate:[NSDate date]  options:0];
        _age = [components year];
    }
    
}
#pragma mark - UIPickerView DataSource
    // returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView == _pickerHeight) {
        return 2;
    }
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == _pickerHeight) {
        switch (component) {
            case 0:
                return 3;
                break;
            case 1:
                return 100;
                break;
            default:
                break;
        }

    } else if (pickerView == _pickerWeight){
        switch (component) {
            case 0:
                return 412;
                break;
            case 1:
                return 10;
                break;
            case 2:
                return 1;
                break;
            default:
                break;
        }
    }
    return  0;
}

#pragma mark - UIPickerView Delegate

    // returns width of column and height of row for each component.
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (pickerView == _pickerHeight) {
        
        return 80;
        
    } else if(pickerView == _pickerWeight) {
        if(component == 0) {
            return 50.0;
        } else if(component == 1) {
            return 25.0;
        } else if(component == 2) {
            return 20.0;
        }
    }
    return 0.0;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 34;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* lblTitle = (UILabel*)view;
    
    if (!lblTitle) {
       
        if (pickerView == _pickerHeight) {
            
            lblTitle = [[UILabel alloc] init];
            
            if (component == 0) {
                [lblTitle setTextAlignment:NSTextAlignmentCenter];
            } else {
                [lblTitle setTextAlignment:NSTextAlignmentCenter];
            }
            
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
            
            [lblTitle setText:[NSString stringWithFormat:@"%d",row]];
            
        } else if(pickerView == _pickerWeight) {
            
            if (component == 0) {
                lblTitle = [[UILabel alloc] init];
                [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
                [lblTitle setText:[NSString stringWithFormat:@"%d",(row + 14)]];
                [lblTitle setTextAlignment:NSTextAlignmentRight];
                [lblTitle setTextColor:[UIColor blackColor]];
                [view addSubview:lblTitle];
            } else if (component == 1) {
                lblTitle = [[UILabel alloc] init];
                [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
                [lblTitle setText:[NSString stringWithFormat:@".%d",row]];
                [lblTitle setTextAlignment:NSTextAlignmentCenter];
                [lblTitle setTextColor:[UIColor blackColor]];
                [view addSubview:lblTitle];
            } else if (component == 2) {
                lblTitle = [[UILabel alloc] init];
                [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
                [lblTitle setText:[NSString stringWithFormat:@"Kg"]];
                [lblTitle setTextAlignment:NSTextAlignmentLeft];
                [lblTitle setTextColor:[UIColor blackColor]];
                [view addSubview:lblTitle];
            }
        }
    }
    
    return lblTitle;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == _pickerHeight) {
        NSInteger firstRow = [pickerView selectedRowInComponent:0];
        NSInteger secondRow = [pickerView selectedRowInComponent:1];
        [_txtHeight setText:[NSString stringWithFormat:@"%d%d",firstRow, secondRow]];
        
        _currentHeightCentimeters = firstRow * 100 + secondRow;
    } else if (pickerView == _pickerWeight) {
        
        NSInteger firstRow = [pickerView selectedRowInComponent:0] + 14;
        NSInteger secondRow = [pickerView selectedRowInComponent:1];
        [_txtWeight setText:[NSString stringWithFormat:@"%d.%d",firstRow, secondRow]];
        
        _currentWeightKilos = [_txtWeight.text floatValue];
    }
    
    
}

#pragma mark - Segues
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Next Screen Segue"]) {
       
        _currentWeightKilos = [_txtWeight.text floatValue];
        [_user setHeightCmValue:_currentHeightCentimeters];
        [_user setCurrentWeightKgValue:_currentWeightKilos];
        [_user setInitialWeightKgValue:_currentWeightKilos];
        _user.ageValue = _age;
        _user.birthdate = _datePicker.date;
        _user.genderValue = _gender;
        _user.unitSystemValue = CCUnitSystemMetric;
    
    }
}

@end
