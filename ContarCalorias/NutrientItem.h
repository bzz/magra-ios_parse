//
//  NutrientItem.h
//  ContarCalorias
//
//  Created by Lucian Gherghel on 26/02/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NutrientItem : NSObject

@property (strong, nonatomic) NSString *quantity;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *value;

@end
