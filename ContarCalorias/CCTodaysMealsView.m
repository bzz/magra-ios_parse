//
//  CCTodaysMealsView.m
//  ContarCalorias
//
//  Created by andres portillo on 12/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCTodaysMealsView.h"
#import "CCAPI.h"
#import "User.h"
#import "CCNoDietSelectedView.h"
#import "CCLoadingView.h"
#import "CCMealTableFoodCell.h"
#import "MealPlan.h"
#import "MealType.h"
#import "Meal.h"
#import "Recipe.h"

@interface CCTodaysMealsView ()<CCNoDietSelectedViewDelegate,UITableViewDataSource, UITableViewDelegate,CCLoadingViewDelegate, CCMealTableFoodCellDelegate>

@end
@implementation CCTodaysMealsView
{
    User *_user;
    CCNoDietSelectedView *_noDietSelectedView;
    UIScrollView *_scrollView;
    UITableView *_tblBreakfast,*_tblMorningSnack,*_tblLunch,*_tblDinner, *_tblEveningSnack;
    CCLoadingView *_loadingView;
    NSArray *_arrBreakfastItems, *_arrMorningSnackItems, *_arrLunchItems, *_arrDinnerItems, *_arrEveningSnackItems,*_arrMeals;
    MealPlan *_mealPlan;
    NSDateFormatter *_timeFormatter;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)awakeFromNib
{
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, self.frame.size.height)];
    [_scrollView setPagingEnabled:YES];
    _scrollView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    
    [_scrollView setScrollEnabled:NO];
    [_scrollView setDelegate:self];
    
    _tblBreakfast = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, self.frame.size.height)];
    _tblBreakfast.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    _tblBreakfast.tableFooterView = [UIView new];
    _tblMorningSnack = [[UITableView alloc] initWithFrame:CGRectMake(320, 0, 320, self.frame.size.height)];
    _tblMorningSnack.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    _tblMorningSnack.tableFooterView = [UIView new];
    _tblLunch = [[UITableView alloc] initWithFrame:CGRectMake(640, 0, 320, self.frame.size.height)];
    _tblLunch.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    _tblLunch.tableFooterView = [UIView new];
    _tblDinner = [[UITableView alloc] initWithFrame:CGRectMake(960, 0, 320, self.frame.size.height)];
    _tblDinner.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    _tblDinner.tableFooterView = [UIView new];
    _tblEveningSnack = [[UITableView alloc] initWithFrame:CGRectMake(1280, 0, 320, self.frame.size.height)];
    _tblEveningSnack.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    _tblEveningSnack.tableFooterView = [UIView new];
    
    [_tblBreakfast setDelegate:self];
    [_tblBreakfast setDataSource:self];
    
    [_tblMorningSnack setDelegate:self];
    [_tblMorningSnack setDataSource:self];
    
    [_tblLunch setDelegate:self];
    [_tblLunch setDataSource:self];
    
    [_tblDinner setDelegate:self];
    [_tblDinner setDataSource:self];
    
    [_tblEveningSnack setDelegate:self];
    [_tblEveningSnack setDataSource:self];
    
    [_scrollView addSubview:_tblBreakfast];
    [_scrollView addSubview:_tblMorningSnack];
    [_scrollView addSubview:_tblLunch];
    [_scrollView addSubview:_tblDinner];
    [_scrollView addSubview:_tblEveningSnack];
    
    [self  insertSubview:_scrollView belowSubview:_pageControl];
    
    [_scrollView setContentSize:CGSizeMake(320 * 5, self.frame.size.height)];
    
    UINib *headerCellNIB = [UINib nibWithNibName:@"CCMealTableHeaderCell" bundle:nil];
    [_tblBreakfast registerNib:headerCellNIB forCellReuseIdentifier:@"Header Cell"];
    [_tblMorningSnack registerNib:headerCellNIB forCellReuseIdentifier:@"Header Cell"];
    [_tblLunch registerNib:headerCellNIB forCellReuseIdentifier:@"Header Cell"];
    [_tblDinner registerNib:headerCellNIB forCellReuseIdentifier:@"Header Cell"];
    [_tblEveningSnack registerNib:headerCellNIB forCellReuseIdentifier:@"Header Cell"];
    
    UINib *foodCellNIB = [UINib nibWithNibName:@"CCMealTableFoodCell" bundle:nil];
    [_tblBreakfast registerNib:foodCellNIB forCellReuseIdentifier:@"Food Cell"];
    [_tblMorningSnack registerNib:foodCellNIB forCellReuseIdentifier:@"Food Cell"];
    [_tblLunch registerNib:foodCellNIB forCellReuseIdentifier:@"Food Cell"];
    [_tblDinner registerNib:foodCellNIB forCellReuseIdentifier:@"Food Cell"];
    [_tblEveningSnack registerNib:foodCellNIB forCellReuseIdentifier:@"Food Cell"];
    
        //no diet selected yet
    
    _noDietSelectedView = [[[NSBundle mainBundle] loadNibNamed:@"CCNoDietSelectedView" owner:self options:nil] firstObject];
    [_noDietSelectedView  setFrame:CGRectMake(35, 15, 250, 350)];
    [_noDietSelectedView setDelegate:self];
    [self addSubview:_noDietSelectedView];
    
        //loadding view
    
    _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CCLoadingView" owner:self options:nil] firstObject];
    [_loadingView  setFrame:CGRectMake(0, 0, 320, self.frame.size.height)];
    _loadingView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    [_loadingView setDelegate:self];
    [_loadingView startLoadingMode];
    
    [self addSubview:_loadingView];
    
    [_scrollView setAlpha:0];

    _user = [[CCAPI sharedInstance] getUser];
    
    if (_user.mealPlan.idMealPlanValue == 0) {
        [_noDietSelectedView setAlpha:1];
        [_scrollView setAlpha:0];
        [_loadingView setAlpha:0];
    } else {
        [_noDietSelectedView setAlpha:0];
        [_scrollView setAlpha:1];
        [self getTodaysMeals];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getTodaysMeals) name:kCCNotificationUserStartedDiet object:nil];
    
    _timeFormatter = [[NSDateFormatter alloc] init];
    [_timeFormatter setDateFormat:@"HH:mm a"];
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)updateViewsContent
{
    _user = [[CCAPI sharedInstance] getUser];
    
    if (_user.mealPlan.idMealPlanValue == 0) {
        [_noDietSelectedView setAlpha:1];
        [_scrollView setAlpha:0];
        [_loadingView setAlpha:0];
    } else {
        [_noDietSelectedView setAlpha:0];
        [_scrollView setAlpha:1];
    }
}
- (void)reloadTables
{
    [_tblBreakfast reloadData];
    [_tblMorningSnack reloadData];
    [_tblLunch reloadData];
    [_tblDinner reloadData];
    [_tblEveningSnack reloadData];
}
- (void)getTodaysMeals
{
    [_scrollView setContentOffset:CGPointZero];
    _user = [[CCAPI sharedInstance] getUser];
    [_loadingView startLoadingMode];
    [_loadingView setAlpha:1];
  
    NSDate *mealPlanStartDate =  _user.mealPlanStartDate;
    NSInteger mealPlanId = _user.mealPlan.idMealPlanValue;
    
    _mealPlan = [MealPlan MR_findFirstByAttribute:@"idMealPlan" withValue:@(mealPlanId)];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *currentDateComponents = [gregorianCalendar components:NSDayCalendarUnit
                                                                   fromDate:mealPlanStartDate toDate:[NSDate date] options:0];
    NSInteger currentDay = currentDateComponents.day + 1;
    
    _arrMeals  =[[_mealPlan.meals allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"dayOfPlan = %@",@(currentDay)]];
   
    _arrBreakfastItems =  [_arrMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mealType = %@",[MealType MR_findFirstByAttribute:@"name" withValue:@"breakfast"]]];
    _arrMorningSnackItems =  [_arrMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mealType = %@",[MealType MR_findFirstByAttribute:@"name" withValue:@"morningsnack"]]];
    _arrLunchItems = [_arrMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mealType = %@",[MealType MR_findFirstByAttribute:@"name" withValue:@"lunch"]]];
    _arrEveningSnackItems = [_arrMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mealType = %@",[MealType MR_findFirstByAttribute:@"name" withValue:@"eveningsnack"]]];
    _arrDinnerItems = [_arrMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mealType = %@",[MealType MR_findFirstByAttribute:@"name" withValue:@"dinner"]]];
    
    [_loadingView setAlpha:0];
    [_pageControl setAlpha:1];
    [_scrollView setAlpha:1];
    [_scrollView setScrollEnabled:YES];
    
    [_tblBreakfast reloadData];
    [_tblMorningSnack reloadData];
    [_tblLunch reloadData];
    [_tblDinner reloadData];
    [_tblEveningSnack reloadData];
}

#pragma mark - CCLoadingView Delegate
- (void)theRefreshButtonWasTappedOnLoadingView:(CCLoadingView *)loadingView
{
   if (loadingView == _loadingView){
        [self getTodaysMeals];
    }
    
}

#pragma mark - UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tblBreakfast) {
        return [_arrBreakfastItems count] + 1;
    } else if (tableView == _tblMorningSnack) {
        return [_arrMorningSnackItems count] + 1;
    } else if (tableView == _tblLunch) {
        return [_arrLunchItems count] + 1;
    } else if (tableView == _tblEveningSnack) {
        return [_arrEveningSnackItems count] + 1;
    } else if (tableView == _tblDinner) {
        return [_arrDinnerItems count] + 1;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier;
    UITableViewCell *cell;
    if (indexPath.row == 0) {
        identifier = @"Header Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (! cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        NSString *cal;
        if (tableView == _tblBreakfast) {
            [(UILabel *)[cell viewWithTag:1] setText:[[TSLanguageManager localizedString:@"Breakfast"] uppercaseString]];
            [(UILabel *)[cell viewWithTag:3] setText:[_timeFormatter stringFromDate:_user.breakfastTime]];
      
        } else if (tableView == _tblMorningSnack) {
            [(UILabel *)[cell viewWithTag:1] setText:[[TSLanguageManager localizedString:@"Morningsnack"] uppercaseString]];
            [(UILabel *)[cell viewWithTag:3] setText:[_timeFormatter stringFromDate:_user.firstSnackTime]];
        } else if (tableView == _tblLunch) {
            [(UILabel *)[cell viewWithTag:1] setText:[[TSLanguageManager localizedString:@"Lunch"] uppercaseString]];
            [(UILabel *)[cell viewWithTag:3] setText:[_timeFormatter stringFromDate:_user.lunchTime]];
        } else if (tableView == _tblDinner) {
            [(UILabel *)[cell viewWithTag:1] setText:[[TSLanguageManager localizedString:@"Dinner"] uppercaseString]];
            [(UILabel *)[cell viewWithTag:3] setText:[_timeFormatter stringFromDate:_user.secondSnackTime]];
        } else if (tableView == _tblEveningSnack) {
            [(UILabel *)[cell viewWithTag:1] setText:[[TSLanguageManager localizedString:@"Eveningsnack"] uppercaseString]];
            [(UILabel *)[cell viewWithTag:3] setText:[_timeFormatter stringFromDate:_user.dinnerTime]];
        }
        [(UILabel *)[cell viewWithTag:2] setText:cal];
        
        
    } else {
    
        identifier = @"Food Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (! cell) {
            cell = [[CCMealTableFoodCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        ((CCMealTableFoodCell*)cell).delegate = self;
        NSString *title = @"";
        if (tableView == _tblBreakfast) {
            Meal *meal = _arrBreakfastItems[indexPath.row - 1];
            if(meal.recipe == nil) {
                [((CCMealTableFoodCell*)cell).btnSeeRecipe setAlpha:0];
            }
            title = meal.title;
            ((CCMealTableFoodCell*)cell).recipeId = (int)meal.idMealValue;
        } else if (tableView == _tblMorningSnack) {
            Meal *meal = _arrMorningSnackItems[indexPath.row - 1];
            title = meal.title;
            ((CCMealTableFoodCell*)cell).recipeId = (int)meal.idMealValue;
            if(meal.recipe == nil) {
                [((CCMealTableFoodCell*)cell).btnSeeRecipe setAlpha:0];
            }
        } else if (tableView == _tblLunch) {
            Meal *meal = _arrLunchItems[indexPath.row - 1];
            title = meal.title;
            ((CCMealTableFoodCell*)cell).recipeId = (int)meal.idMealValue;
            if(meal.recipe == nil) {
                [((CCMealTableFoodCell*)cell).btnSeeRecipe setAlpha:0];
            }
        } else if (tableView == _tblDinner) {
            Meal *meal = _arrDinnerItems[indexPath.row - 1];
            title = meal.title;
            ((CCMealTableFoodCell*)cell).recipeId = (int)meal.idMealValue;
            if(meal.recipe == nil) {
                [((CCMealTableFoodCell*)cell).btnSeeRecipe setAlpha:0];
            }
        } else if (tableView == _tblEveningSnack) {
            Meal *meal = _arrEveningSnackItems[indexPath.row - 1];
            title = meal.title;
            ((CCMealTableFoodCell*)cell).recipeId = (int)meal.idMealValue;
            if(meal.recipe == nil) {
                [((CCMealTableFoodCell*)cell).btnSeeRecipe setAlpha:0];
            }
        }
        if (fmodf(indexPath.row,2) != 0) {
            
            [cell.contentView setBackgroundColor:[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1]];
        } else {
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        }
       
        [((CCMealTableFoodCell*)cell).lblTitle setText:title];
        
    }
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0;
}

#pragma mark - CCNoDietSelectedView Delegate
- (void)didTapPickADietButton
{
    [self.delegate didTapPickADietButton];
}
- (void)didTapContactDietitianButton
{
    [self.delegate didTapContactDietitianButton];
}
#pragma  mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger page = scrollView.contentOffset.x / 320;
    [_pageControl setCurrentPage:page];
}

#pragma  mark - CCMealTableFoodCell Delegate
- (void)didTapSeeRecipeButtonWithRecipeId:(NSInteger)recipeId
{
    [self.delegate didTapSeeRecipeButtonWithRecipeId:recipeId];
}
@end
