//
//  CCProgressVC.h
//  ContarCalorias
//
//  Created by andres portillo on 12/8/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GAITrackedViewController.h"

@class APLineChart;
@interface CCProgressVC : GAITrackedViewController
@property (weak, nonatomic) IBOutlet APLineChart *lineChart;

@property (weak, nonatomic) IBOutlet UILabel *lblLostWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblGoal;
@property (weak, nonatomic) IBOutlet UILabel *lblGoalWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblNoData;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleLostWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleGoalWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleGoal;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdateWeight;
@end
