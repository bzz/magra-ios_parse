    //
    //  CCHomeTVC.m
    //  ContarCalorias
    //
    //  Created by andres portillo on 15/6/14.
    //  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
    //

#import "CCHomeVC.h"
#import <Crashlytics/Crashlytics.h>
#import "CCDietsIntroductionVC.h"
#import "UIAlertView+Blocks.h"
#import "SWRevealViewController.h"
#import "CCAPI.h"
#import "CCTaskProgressView.h"
#import "CCTaskProgressViewWrapper.h"
#import "User.h"
#import "UserFood.h"
#import "UserWater.h"
#import "UserExercise.h"
#import "FoodServing.h"
#import "Article.h"
#import "CCArticleVC.h"
#import "CCConstants.h"
#import "CCArticleCell.h"
#import "CCMealPlanCell.h"
#import "UIImageView+AFNetworking.h"
#import "CCDairyTopBar.h"
#import "CCAdCell.h"
#import "TSMessage.h"
#import "ATConnect.h"
#import "UIImage-Categories/UIImage+Resize.h"
#import "CCLoadingCell.h"

#define kCCHomeScreenActivityIndicatorCellIdentifier   @"Activity Indicator Cell"
#define kCCHomeScreenArticleCellIdentifier   @"Article Cell"
#define kCCHomeScreenMealPlanCellIdentifier   @"Meal Plan Cell"
#define kCCHomeScreenWeightLossPlanCellIdentifier   @"Weight Loss Plan Cell"
#define kCCHomeScreenLoadingCellIdentifier   @"Loading Cell"


typedef NS_ENUM(NSUInteger, CCHomeScreenSection)
{
    CCHomeScreenSectionMealPlanSection = 0,
    CCHomeScreenSectionPostsSection,
    CCHomeScreenSectionLoadingSection
};

@interface CCHomeVC ()<UITableViewDataSource, UITableViewDelegate, SWRevealViewControllerDelegate,CCArticleCellDelegate, CCLoadingCellDelegate>

@end

@implementation CCHomeVC
{
    NSArray *_arrPosts;
    NSMutableArray *_arrFeedItems;
    CCUserStage _userStage;
    User *_user;
    UIStoryboard *_storyboardSetWeight;
    UINavigationController *_weightLossPlanNV;
    NSInteger _selectedArticleIndex;
    CGFloat _previousScrollViewYOffset;
    CGFloat _previousScrollViewYOffsetDiaryBar;
    CGFloat _diaryBarWasHidden;
    NSDateFormatter *_dateFormatter;
    BOOL _initialSynchronizationWasPerformed, _isLoadingNewPosts, _isLoadingOlderPosts, _initialAutoLayoutConstraintsWereSet;
    UIRefreshControl *_refreshControl;
    CCLoadingCell *_loadingCell;
}

#pragma  mark - UIViewController Life Cycle
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:NO];
    self.screenName = @"Home Screen";
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
    if (! _initialAutoLayoutConstraintsWereSet) {
        _initialAutoLayoutConstraintsWereSet = YES;
        if (_userStage == CCUserStageSetWeightLossPlan) {
            
            [_btnAddDiaryItem setAlpha:0];
            [_constraintDiaryBarTopSpace setConstant:-60];
            [_constraintTableHeight setConstant:self.view.frame.size.height];
            
        } else if (_userStage == CCUserStageFirstStage) {
            
            [_btnAddDiaryItem setAlpha:1];
            [_constraintDiaryBarTopSpace setConstant:0];
            [_constraintTableHeight setConstant:self.view.frame.size.height - 60];
        }
    }
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _user  = [[CCAPI sharedInstance] getUser];
    
    _initialSynchronizationWasPerformed = [[NSUserDefaults standardUserDefaults] boolForKey:kCCInitialSynchronizationWasPerformed];
    
    self.tableView.tableFooterView = [UIView new];
    
    [self.navigationController.navigationBar setTintColor:CCColorGreen];
    
    [self setTitleView];
    
    if (_user.didCreateWeightLossPlanValue) {
        [self setRefreshControl];
        
    }
    
    self.revealViewController.delegate = self;
    [self setBackButton];
    
    _userStage = _user.didCreateWeightLossPlanValue? CCUserStageFirstStage: CCUserStageSetWeightLossPlan;
    
    if (_userStage == CCUserStageSetWeightLossPlan) {
        
        _storyboardSetWeight = [UIStoryboard storyboardWithName:@"SetWeightLossPlan" bundle:nil];
        _weightLossPlanNV = [_storyboardSetWeight instantiateInitialViewController];
        
    } else {
    
    
    }
    
    [self setNotificationsObservers];
    
    
    _dateFormatter = [NSDateFormatter new];
    [_dateFormatter setDateFormat:@"dd MMM"];
    
    
    [self checkNumberOfAppLaunches];
    
    
        //Register Cells
    [self.tableView registerNib:[UINib nibWithNibName:@"CCMealPlanCell" bundle:nil] forCellReuseIdentifier:kCCHomeScreenMealPlanCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"CCLoadingCell" bundle:nil] forCellReuseIdentifier:kCCHomeScreenLoadingCellIdentifier];
    
    
    [self updateScreenContent];
    
    _arrPosts = [[CCAPI sharedInstance] getPosts];
    _arrFeedItems = [_arrPosts mutableCopy];
    
        //load most recent posts
    if (_arrPosts.count > 0) {
        [self loadNewPosts];
        [_refreshControl beginRefreshing];
    }
    [self.tableView reloadData];
}
- (void)checkNumberOfAppLaunches
{
    NSInteger launchesCount = [[[NSUserDefaults standardUserDefaults] objectForKey:kCCLaunchesCount] integerValue];
    if (launchesCount == 7) {
        [[ATConnect sharedConnection] engage:@"third_launch" fromViewController:self];
    }
}
- (void)setRefreshControl
{
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(handlePostsRefresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_refreshControl];
    
}
- (void)setTitleView
{
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 22)];
    [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:13]];
    [lblTitle setTextColor:CCColorOrange];
    [lblTitle setText:[@"Magra" uppercaseString]];
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    [self.navigationItem setTitleView:lblTitle];
}
- (void)setNotificationsObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onNewMessageReceived)
                                                 name:kCCNotificationNewMessageReceived object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onMealLogged:)
                                                 name:kCCNotificationMealWasLogged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onExerciseLogged:)
                                                 name:kCCNotificationExerciseWasLogged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onWaterLogged:)
                                                 name:kCCNotificationWaterWasLogged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onUserCreatedWeightLossPlan)
                                                 name:kCCNotificationUserCreatedWeightLossPlan object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateScreenContent)
                                                 name:kCCNotificationHomeScreenNeedsUpdate object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(switchToDietsSection)
                                                 name:kCCDietCellContentWasTapped object:nil];
    
   
}
#pragma mark - Class Methods
- (CCHomeScreenSection)sectionTypeForSectionIndex:(NSInteger)section
{
    CCHomeScreenSection sectionType = NSNotFound;
    if (section == 0 && [self shouldShowMealCell]) {
        sectionType = CCHomeScreenSectionMealPlanSection;
    } else if (section == ([self shouldShowMealCell]? 1:0)) {
        sectionType = CCHomeScreenSectionPostsSection;
    } else if(section == ([self shouldShowMealCell]? 2:1)) {
        sectionType = CCHomeScreenSectionLoadingSection;
    }
    return sectionType;
}
- (BOOL)shouldShowMealCell
{
    return _user.mealPlan && _user.hasProFeatures;
}
- (void)noOlderPostsAvailable
{
    [_loadingCell loadingDidFail];
}
- (void)loadNewPosts
{
    _isLoadingNewPosts = YES;
    [[CCAPI sharedInstance] getPostUpdatesWithSuccess:^{
        _isLoadingNewPosts = NO;
        _arrPosts = [[CCAPI sharedInstance] getPosts];
        _arrFeedItems = [_arrPosts mutableCopy];
        [self.tableView reloadData];
        [_refreshControl endRefreshing];
    } Failure:^(NSError *error) {
        [_refreshControl endRefreshing];
        _isLoadingNewPosts = NO;
        
    }];
}
- (void)handlePostsRefresh
{
    if (! _isLoadingNewPosts) {
        [self loadNewPosts];
    }
}
- (void)loadOlderPosts
{
    NSInteger _postsCount = _arrPosts.count;
    _isLoadingOlderPosts = YES;
    [[CCAPI sharedInstance] getOlderPostsWithSuccess:^{
        _isLoadingOlderPosts = NO;
        _arrPosts = [[CCAPI sharedInstance] getPosts];
        if (_postsCount < _arrPosts.count) {
            _arrFeedItems = [_arrPosts mutableCopy];
            [self.tableView reloadData];
            
        } else {
            [self noOlderPostsAvailable];
        }
        [_refreshControl endRefreshing];
        
        
    } Failure:^(NSError *error) {
        [_loadingCell loadingDidFail];
        _isLoadingOlderPosts = NO;
        [_refreshControl endRefreshing];
    }];
}
- (void)updateScreenContent
{
    _userStage = _user.didCreateWeightLossPlanValue? CCUserStageFirstStage: CCUserStageSetWeightLossPlan;
    _user = [[CCAPI sharedInstance] getUser];
    
    if (_initialSynchronizationWasPerformed != [[NSUserDefaults standardUserDefaults] boolForKey:kCCInitialSynchronizationWasPerformed]) {
        _initialSynchronizationWasPerformed = [[NSUserDefaults standardUserDefaults] boolForKey:kCCInitialSynchronizationWasPerformed];
        [self.tableView reloadData];
    }
    
    
    if (_userStage == CCUserStageSetWeightLossPlan) {
        
        [_btnAddDiaryItem setAlpha:0];
        [_constraintDiaryBarTopSpace setConstant:-60];
        [_constraintTableHeight setConstant:self.view.frame.size.height];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
    } else if (_userStage == CCUserStageFirstStage) {
        
        [_btnAddDiaryItem setAlpha:1];
        
        [_constraintDiaryBarTopSpace setConstant:0];
        [_constraintTableHeight setConstant:self.view.frame.size.height - 60];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
         [_diaryTopBar updateContent];
    }
    
}
#pragma mak - UIActions

-(void)setBackButton{
    
    UIButton *btn = [[UIButton alloc] init];
    [btn setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
    [btn setFrame:CGRectMake(0, 0, 20, 15)];
    [btn addTarget:self.revealViewController action:@selector(revealToggleAnimated:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* button = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = button;
}
-(IBAction)onAddDiaryItemButtonTapped:(id)sender
{
    [self performSegueWithIdentifier:@"Add Diary Item Segue" sender:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}
-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableView DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier;
    
    if (_initialSynchronizationWasPerformed) {
        
        if (_userStage == CCUserStageSetWeightLossPlan) {
            identifier =  kCCHomeScreenWeightLossPlanCellIdentifier;
        } else {
            CCHomeScreenSection sectionType = [self sectionTypeForSectionIndex:indexPath.section];
            if (sectionType == CCHomeScreenSectionMealPlanSection) {
                identifier = kCCHomeScreenMealPlanCellIdentifier;
            } else if (sectionType == CCHomeScreenSectionPostsSection) {
                identifier = kCCHomeScreenArticleCellIdentifier;
            } else if (sectionType == CCHomeScreenSectionLoadingSection){
                identifier = kCCHomeScreenLoadingCellIdentifier;
            }
            
        }
    } else {
        identifier =  kCCHomeScreenActivityIndicatorCellIdentifier;
    }
    
    UITableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    if( _userStage == CCUserStageSetWeightLossPlan) {
        if (! _initialSynchronizationWasPerformed) {
            [((UIActivityIndicatorView *)[((UITableViewCell *)cell).contentView viewWithTag:1]) startAnimating];
            [((UILabel *)[((UITableViewCell *)cell).contentView viewWithTag:2]) setText:[TSLanguageManager localizedString:@"Loading..."]];
        } else  {
            [((UILabel *)[((UITableViewCell *)cell).contentView viewWithTag:1]) setText:[TSLanguageManager localizedString:@"Set up your weight loss plan"]];
        }
    } else  if( _userStage == CCUserStageFirstStage) {
        if (_initialSynchronizationWasPerformed) {
            CCHomeScreenSection sectionType = [self sectionTypeForSectionIndex:indexPath.section];
            
            if (sectionType == CCHomeScreenSectionMealPlanSection) {
                
            } else if (sectionType == CCHomeScreenSectionPostsSection) {
                
                if ([_arrFeedItems[indexPath.row] isKindOfClass:[Article class]]) {
                    
                    Article *article  = _arrFeedItems [indexPath.row];
                    CCArticleCell *articleCell = (CCArticleCell *) cell;
                    [articleCell.lblPostTitle setText:article.title];
                    [articleCell.lblTime setText:[_dateFormatter stringFromDate:article.date]];
                    articleCell.index = indexPath.row;
                    articleCell.delegate = self;
                    
                    if (article.imageData == nil ) {
                        [articleCell.imgPostImage setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:article.imageUrl]] placeholderImage:[UIImage imageNamed:@"article-image-placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                            
                            article.imageData = UIImagePNGRepresentation(image);
                            
                            [NSKeyedArchiver archiveRootObject:[_arrPosts objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0,MIN(10, _arrPosts.count))]] toFile:[[CCAPI sharedInstance] getPostsFilePath]];
                            
                            
                            if (article.completed == NO && [[CCAPI sharedInstance] isSessionAvailable]) // this check prevents the row to be updated multiple times simultaneously
                                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                            article.completed = YES;
                            
                            
                        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                            
                        }];
                    } else {
                        UIImage *articleImg = [UIImage imageWithData:article.imageData];
                        if (articleImg.size.width >= 320) {
                            CGFloat multiplier = 320 / articleImg.size.width;
                            UIImage *resizedImg = [articleImg resizedImage:CGSizeMake(320, articleImg.size.height * multiplier) interpolationQuality:kCGInterpolationLow];
                            [articleCell.imgPostImage setImage:resizedImg];
                        } else {
                            [articleCell.imgPostImage setImage:articleImg];
                        }
                        
                    }
                    

                }
            } else if (sectionType == CCHomeScreenSectionLoadingSection) {
                _loadingCell = (CCLoadingCell *) cell;
                _loadingCell.delegate = self;
                
            }
        }
        
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numberOfSections = 0;
    if (! _initialSynchronizationWasPerformed)
        numberOfSections = 1;
    else {
        if(_userStage == CCUserStageSetWeightLossPlan) {
            numberOfSections = 1;
        } else {
            numberOfSections = 2 + ([self shouldShowMealCell]? 1:0);
        }
    }
    
    return numberOfSections;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (_initialSynchronizationWasPerformed) {
        
        if (_userStage == CCUserStageSetWeightLossPlan) {
            return 270;
        } else {
            CCHomeScreenSection sectionType = [self sectionTypeForSectionIndex:indexPath.section];
            if (sectionType == CCHomeScreenSectionMealPlanSection) {
                return 220;
            } else if (sectionType == CCHomeScreenSectionPostsSection) {
                
                if ([_arrFeedItems[indexPath.row ] isKindOfClass:[Article class]]) {
                    CGFloat cellHeight = [self heightForArticleCellAtIndexPath:indexPath];
                    return cellHeight;
                }
            } else if (sectionType == CCHomeScreenSectionLoadingSection){
                return 110.0;
            }
        }
    } else {
        return 81;
    }
    
    return 0;
}
- (void)  tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self sectionTypeForSectionIndex:indexPath.section] == CCHomeScreenSectionLoadingSection && ! _isLoadingOlderPosts && _userStage == CCUserStageFirstStage)
    {
        [(UIActivityIndicatorView *)[cell.contentView viewWithTag:1] startAnimating];
        
        [self loadOlderPosts];
    }
}
- (CGFloat)heightForArticleCellAtIndexPath:(NSIndexPath *)indexPath {
    static CCArticleCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:kCCHomeScreenArticleCellIdentifier];
    });
    
    [self configureCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
        // Return the number of rows in the section.
    BOOL isPostsSection = [self sectionTypeForSectionIndex:section] == CCHomeScreenSectionPostsSection ;
    if (_userStage == CCUserStageFirstStage && isPostsSection && _initialSynchronizationWasPerformed)
        numberOfRows =  [_arrFeedItems count];
    else
        numberOfRows = 1;
    return numberOfRows;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (_userStage == CCUserStageSetWeightLossPlan) {
        [self.revealViewController presentViewController:_weightLossPlanNV animated:YES completion:nil];
    } else {
        if ([[tableView cellForRowAtIndexPath:indexPath] isKindOfClass:[CCMealPlanCell class]]) {
            [self switchToDietsSection];
        }
    }
}
#pragma mark - Notifications
- (void)switchToDietsSection
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Diets" bundle:nil];
    UINavigationController *DietsMainVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"Main NC"];
    [DietsMainVC.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    [self.revealViewController setFrontViewController:DietsMainVC animated:YES];
}
- (void)onNewMessageReceived
{
    [TSMessage showNotificationInViewController:self title:@"Hey There!" subtitle:@"You have a new message from the support team" image:nil type:TSMessageNotificationTypeSuccess  duration:TSMessageNotificationDurationAutomatic callback:nil buttonTitle:@"" buttonCallback:^{
        
    } atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
    
}
- (void)onMealLogged:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self updateScreenContent];
    });
}
- (void)onWaterLogged:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self updateScreenContent];
    });
}
- (void)onExerciseLogged:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self updateScreenContent];
    });
}
- (void)onUserCreatedWeightLossPlan
{
    _userStage = CCUserStageFirstStage;
    [self updateScreenContent];
    [self.tableView reloadData];
}
#pragma mark - UISegues
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Article Segue"]) {
        
        Article *selectedArticle  = [_arrPosts objectAtIndex:_selectedArticleIndex];
        CCArticleVC *articleVC = segue.destinationViewController;
        articleVC.article = selectedArticle;
        
    }
}
#pragma mark - CCArticleCell Delegate
- (void)didTapOnCellWithIndex:(NSInteger)index
{
    _selectedArticleIndex = index;
    [self performSegueWithIdentifier:@"Article Segue" sender:nil];
}
#pragma mark - SWRevealViewController Delegate
- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (_user.didCreateWeightLossPlanValue) {
    
        CGFloat y= _constraintDiaryBarTopSpace.constant;
        CGFloat height = _diaryTopBar.frame.size.height;
        CGFloat scrollOffset = scrollView.contentOffset.y;
        CGFloat scrollDiff = scrollOffset - _previousScrollViewYOffsetDiaryBar;
        CGFloat scrollHeight = scrollView.frame.size.height;
        CGFloat scrollContentInsetBottom =  scrollView.contentInset.bottom;
        CGFloat scrollContentSizeHeight = scrollView.contentSize.height + scrollContentInsetBottom;
        CGFloat scrollContentInsetTop = scrollView.contentInset.top;
        
        if (scrollOffset <= - scrollContentInsetTop) {
            y = 0;
        } else if ((scrollOffset + scrollHeight) >= scrollContentSizeHeight) {
            y = - height;
        } else if (scrollOffset < height){
            y = MIN(0, MAX(- height, (y - scrollDiff)));
        }
        
        [_constraintTableHeight setConstant:self.view.frame.size.height - (height + y)];
        [_constraintDiaryBarTopSpace setConstant:y];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        _previousScrollViewYOffsetDiaryBar = scrollOffset;
    }

    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (_user.didCreateWeightLossPlanValue) {
        [self stoppedScrolling];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate
{
    if (!decelerate && _user.didCreateWeightLossPlanValue) {
        [self stoppedScrolling];
    }
}
- (void)stoppedScrolling
{
    CGRect diaryBarframe = self.diaryTopBar.frame;
    if (diaryBarframe.origin.y < - 40) {
        
        [self animateDiaryBarTo:-diaryBarframe.size.height];
       
        CGFloat tableViewHeight = self.view.frame.size.height;
        
        [UIView animateWithDuration:0.2 animations:^{
            [_constraintTableHeight setConstant:tableViewHeight];
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
        }];
    }
}
- (void)animateDiaryBarTo:(CGFloat)y
{
    [UIView animateWithDuration:0.2 animations:^{
        
        [_constraintDiaryBarTopSpace setConstant:y];
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
    }];
}
#pragma mark - Loading Cell Delegate
- (void)theRefreshButtonWasTappedOnLoadingCell:(CCLoadingCell *)loadingCell
{
    [self loadOlderPosts];
}
@end
