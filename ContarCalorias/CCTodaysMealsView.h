//
//  CCTodaysMealsView.h
//  ContarCalorias
//
//  Created by andres portillo on 12/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CCTodaysMealsViewDelegate
- (void)didTapPickADietButton;
- (void)didTapContactDietitianButton;
- (void)didTapSeeRecipeButtonWithRecipeId:(NSInteger)recipeId;
@end
@interface CCTodaysMealsView : UIView
@property(nonatomic, weak) id<CCTodaysMealsViewDelegate>delegate;
@property(nonatomic, strong) IBOutlet UIPageControl *pageControl;
- (void)getTodaysMeals;
- (void)updateViewsContent;
@end
