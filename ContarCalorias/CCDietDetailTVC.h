//
//  CCDietDetailTVC.h
//  ContarCalorias
//
//  Created by andres portillo on 06/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MealPlan;
@interface CCDietDetailTVC : UITableViewController
@property (nonatomic, strong) MealPlan *mealPlan;
- (IBAction)didTapFollowPlanButton:(id)sender;
@end
