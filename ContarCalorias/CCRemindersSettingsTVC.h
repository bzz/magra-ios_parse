//
//  CCRemindersSettingsTVC.h
//  ContarCalorias
//
//  Created by andres portillo on 3/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCRemindersSettingsTVC : UITableViewController
@property (weak, nonatomic) IBOutlet UITextField *txtBreakfastTime;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstSnackTime;
@property (weak, nonatomic) IBOutlet UITextField *txtLunchTime;
@property (weak, nonatomic) IBOutlet UITextField *txtSecondSnackTime;
@property (weak, nonatomic) IBOutlet UITextField *txtDinnerTime;
@property (weak, nonatomic) IBOutlet UISwitch *switchMealReminders;
@property (weak, nonatomic) IBOutlet UISwitch *switchArticlesReminders;
- (IBAction)onSaveButtonTapped:(id)sender;
@end
