//
//  WelcomeViewController.m
//  ContarCalorias
//
//  Created by Adrian Ghitun on 29/12/13.
//  Copyright (c) 2013 Adrian Ghitun. All rights reserved.
//

#import "CCConnectView.h"
#import "CCAPI.h"
#import "User.h"
#import <QuartzCore/QuartzCore.h>
#import <UIAlertView+Blocks.h>
#import "CCAPI.h"
#import "helper.h"
#import "AppDelegate.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import <FacebookSDK/FacebookSDK.h>

@interface CCConnectView ()

@end

@implementation  CCConnectView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setLocalizedStrings];

}
- (void)setLocalizedStrings
{
    
    [_lblConnect setText:[TSLanguageManager localizedString:@"Connect"]];
    [_lblFBLogin setText:[TSLanguageManager localizedString:@"Login with Facebook"]];
    [_lblWithEmail setText:[TSLanguageManager localizedString:@"With Email"]];
    [_lblWeWontPost setText:[TSLanguageManager localizedString:@"We won't post on facebook"]];
    
    NSMutableAttributedString *ppAttrString = [[NSMutableAttributedString alloc] initWithString:[TSLanguageManager localizedString:@"Privacy policy"]];
    
    [ppAttrString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(0, ppAttrString.length)];
    [ppAttrString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor whiteColor]
                             range:NSMakeRange(0, [ppAttrString length])];
    
    [_btnPrivacyPolicy setAttributedTitle:ppAttrString forState:UIControlStateNormal];
    
    [_lblByRegisteringYouAgree setText:[TSLanguageManager localizedString:@"By registering"]];
    [_lblOr setText:[TSLanguageManager localizedString:@"Or"]];
    
    NSMutableAttributedString *signUpAttrString = [[NSMutableAttributedString alloc] initWithString:[TSLanguageManager localizedString:@"Sign in"]];
    
    [signUpAttrString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(0, signUpAttrString.length)];
    [signUpAttrString addAttribute:NSForegroundColorAttributeName
                 value:[UIColor whiteColor]
                 range:NSMakeRange(0, [signUpAttrString length])];
    
    [_signUpBtn setAttributedTitle:signUpAttrString forState:UIControlStateNormal];

    NSMutableAttributedString *loginAttrString = [[NSMutableAttributedString alloc] initWithString:[TSLanguageManager localizedString:@"Login"]];
    
    [loginAttrString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(0, loginAttrString.length)];
    
    [loginAttrString addAttribute:NSForegroundColorAttributeName
                     value:[UIColor whiteColor]
                     range:NSMakeRange(0, [loginAttrString length])];
    
    [_loginBtn setAttributedTitle:loginAttrString forState:UIControlStateNormal];
    
    [self arrangeLayout];
}
- (void)arrangeLayout
{
    [_btnPrivacyPolicy sizeToFit];
    [_lblByRegisteringYouAgree sizeToFit];
    [_lblByRegisteringYouAgree setFrame:CGRectMake(0, 6,_lblByRegisteringYouAgree.frame.size.width, _lblByRegisteringYouAgree.frame.size.height)];
    [_btnPrivacyPolicy setFrame:CGRectMake(_lblByRegisteringYouAgree.frame.size.width + 3, 0, _btnPrivacyPolicy.frame.size.width, _btnPrivacyPolicy.frame.size.height)];
    
    [_viewPrivacyPolicy setFrame:CGRectMake(0,_viewPrivacyPolicy.frame.origin.y, _lblByRegisteringYouAgree.frame.size.width + 3 +  _btnPrivacyPolicy.frame.size.width, 20)];
    [_viewPrivacyPolicy setCenter:CGPointMake(160, _viewPrivacyPolicy.center.y)];
    
    [_signUpBtn sizeToFit];
    [_lblOr sizeToFit];
    [_lblWithEmail sizeToFit];
    [_loginBtn sizeToFit];
    
    
    [_loginBtn setFrame:CGRectMake(0, 0, _loginBtn.frame.size.width, _loginBtn.frame.size.height)];
    [_lblOr setFrame:CGRectMake(_loginBtn.frame.size.width + 3,6, _lblOr.frame.size.width, _lblOr.frame.size.height)];
    [_signUpBtn setFrame:CGRectMake(_lblOr.frame.origin.x + _lblOr.frame.size.width +3,0, _signUpBtn.frame.size.width, _signUpBtn.frame.size.height)];
    [_lblWithEmail setFrame:CGRectMake(_signUpBtn.frame.origin.x + _signUpBtn.frame.size.width + 3,6, _lblWithEmail.frame.size.width, _lblWithEmail.frame.size.height)];
    [_viewLoginContainer setFrame:CGRectMake(0, _viewLoginContainer.frame.origin.y, _lblWithEmail.frame.origin.x + _lblWithEmail.frame.size.width, _viewLoginContainer.frame.size.height)];
    
    [_viewLoginContainer setCenter:CGPointMake(160, _viewLoginContainer.center.y)];
    
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (IBAction)performFacebookAuthentication:(id)sender
{
    [_activityIndicator startAnimating];
        // If the session state is any of the two "open" states when the button is clicked
    
   [FBSession openActiveSessionWithReadPermissions:@[@"public_profile",@"email"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         
             // Retrieve the app delegate
         AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
             // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
         [appDelegate sessionStateChanged:session State:state error:error];
     }];
    
    
}
- (IBAction)onEmailRegistrationButtonTapped:(id)sender
{
    [self.delegate theEmailRegistrationButtonWasPressedOntheConnectView];
}
- (IBAction)onEmailLoginButtonTapped:(id)sender
{
    [self.delegate theEmailLoginButtonWasPressedOntheConnectView];
}
@end
