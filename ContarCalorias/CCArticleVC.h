
//  CCMiniChallenveVC.h
//  ContarCalorias
//
//  Created by andres portillo on 15/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Article;
@class CCArticleVC;

@interface CCArticleVC : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIWebView *webViewDescription;
@property (strong, nonatomic) Article *article;
@end
