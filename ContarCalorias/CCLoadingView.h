//
//  CCLoadingView.h
//  ContarCalorias
//
//  Created by andres portillo on 10/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CCLoadingView;
@protocol CCLoadingViewDelegate
- (void)theRefreshButtonWasTappedOnLoadingView:(CCLoadingView *)loadingView;
@end
@interface CCLoadingView : UIView
@property (weak, nonatomic) id<CCLoadingViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *lblLoading;
@property (weak, nonatomic) IBOutlet UILabel *lblErrorLoading;
@property (weak, nonatomic) IBOutlet UIButton *btnReload;
- (void)setLoadingFailedMode;
- (void)startLoadingMode;
@end
