//
//  CCDietsIntroductionVC.m
//  ContarCalorias
//
//  Created by andres portillo on 15/09/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCDietsIntroductionVC.h"

#import "CCIAPHelper.h"
#import <StoreKit/StoreKit.h>
#import "UIAlertView+Blocks.h"
#import "CCAPI.h"
#import "UIAlertView+Blocks.h"
#import "CCSideMenuVC.h"
#import "SWRevealViewController.h"
#import "CCUtils.h"

@interface CCDietsIntroductionVC ()<UIScrollViewDelegate>

@end

@implementation CCDietsIntroductionVC
{
    NSArray *_products;
    NSNumberFormatter * _priceFormatter;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_btnBuy setAlpha:1];
    [_btnRestore setAlpha:1];
    
    _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [_btnBuy setEnabled:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchaseDone) name:kCCNotificationProFeaturesPurchased object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchaseFailed) name:IAPHelperProductPurchaseFailedNotification object:nil];
    [self reload];
    
    [self setLocalizedTexts];
}
- (void)setLocalizedTexts
{
    [_btnRestore setTitle:[TSLanguageManager localizedString:@"Restore"] forState:UIControlStateNormal];
    [_lblLargeText1 setText:[[TSLanguageManager localizedString:@"Pro large text 1"] uppercaseString]];
    [_lblSmallText1 setText:[TSLanguageManager localizedString:@"Pro small text 1"]];
    [_lblLargeText2 setText:[[TSLanguageManager localizedString:@"Pro large text 2"] uppercaseString]];
    [_lblSmallText2 setText:[TSLanguageManager localizedString:@"Pro small text 2"]];
    [_lblLargeText3 setText:[[TSLanguageManager localizedString:@"Pro large text 3"] uppercaseString]];
    [_lblSmallText3 setText:[TSLanguageManager localizedString:@"Pro small text 3"]];
    [_lblLargeText4 setText:[[TSLanguageManager localizedString:@"Pro large text 4"] uppercaseString]];
    [_lblSmallText4 setText:[TSLanguageManager localizedString:@"Pro small text 4"]];
    
    [_lblFirstWeekFree setText:[TSLanguageManager localizedString:@"Your first week is free!"]];
}
- (void)viewDidLayoutSubviews
{
    [_scrollView setCenter:CGPointMake(160, self.view.frame.size.height / 2)];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)purchaseFailed
{
    [UIAlertView showWithTitle:@"Error" message:@"The purchase failed" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }];
}
- (void)purchaseDone
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)onDismissScreenTapped:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)reload {
    _products = nil;
    [[CCIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            _products = products;
            SKProduct *product = _products[0];
            [_priceFormatter setLocale:product.priceLocale];
            NSString *buttonTitle = [NSString stringWithFormat:@"%@ %@",[_priceFormatter stringFromNumber:product.price],[TSLanguageManager localizedString:@"Per month"]];
            [_btnBuy setTitle:buttonTitle forState:UIControlStateNormal];
        } else {
        
          [UIAlertView showWithTitle:@"Sorry!" message:@"Error connecting to apple" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
              
              [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
          }];
        }
        [_activityIndicator stopAnimating];
        [_btnBuy setEnabled:YES];
    }];
}

#pragma mark - UIScrollview Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger currentPage = scrollView.contentOffset.x / 320;
    [_pageControl setCurrentPage:currentPage];
}
#pragma mark - UIActions
- (IBAction)buyButtonTapped:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kCCShouldTakeUserToDietsSection];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [_activityIndicator startAnimating];
    SKProduct *product = _products[0];
    [[CCIAPHelper sharedInstance] buyProduct:product];
}
- (IBAction)onRestoreButtonTapped:(id)sender {
    [[CCIAPHelper sharedInstance] restoreCompletedTransactions];
}

@end
