//
//  CCTaskProgressView.m
//  ContarCalorias
//
//  Created by andres portillo on 25/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCTaskProgressView.h"
#import "CCConstants.h"

@implementation CCTaskProgressView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)drawRect:(CGRect)rect
{
    CGFloat height = _userDefinedHeight == 0?10.0:_userDefinedHeight;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, height);
    UIColor *progressColor = _progressColor == nil?[UIColor colorWithRed:255.0/255.0 green:171.0/255.0 blue:45.0/255.0 alpha:1]:_progressColor;
    
    [self setProgressTintColor: progressColor];
    [self setTrackTintColor:CCColorProgressViewTrackColor];
}


@end
