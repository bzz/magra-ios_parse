//
//  CCShoppingListTVC.h
//  ContarCalorias
//
//  Created by andres portillo on 27/11/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MealPlan;
@interface CCShoppingListTVC : UITableViewController
@property(nonatomic, strong) MealPlan *mealPlan;
- (IBAction)onDoneButtonTapped:(id)sender;
@end
