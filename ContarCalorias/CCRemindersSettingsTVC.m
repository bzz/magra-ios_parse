//
//  CCRemindersSettingsTVC.m
//  ContarCalorias
//
//  Created by andres portillo on 3/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCRemindersSettingsTVC.h"
#import "UIAlertView+blocks.h"
#import "CCAPI.h"
#import "User.h"
#import "FontAwesomeKit.h"
@interface CCRemindersSettingsTVC ()<UITextFieldDelegate>

@end

@implementation CCRemindersSettingsTVC{
    UIToolbar *_inputAccessory;
    UIDatePicker *_breakfastPicker, *_firstSnackPicker, *_lunchPicker, *_secondSnackPicker, *_dinnerPicker;
    NSDateFormatter *_dateFormatter;
    NSDate *_breakfastTime, *_firstSnackTime, *_lunchTime, *_secondSnackTime, *_dinnerTime;
    BOOL _mealRemindersEnabled,_articlesRemindersEnabled, _doSelection;
    UILabel *_lblCurrentField;
    NSInteger _activeTextFieldIndexPath;
    User *_user;
}

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView setTableFooterView:[UIView new]];
    
    [self configureInputAccessory];
    _user  = [[CCAPI sharedInstance] getUser];
    
    _breakfastTime = _user.breakfastTime ;
    _firstSnackTime= _user.firstSnackTime ;
    _lunchTime = _user.lunchTime;
    _secondSnackTime = _user.secondSnackTime;
    _dinnerTime = _user.dinnerTime;
    
    
    [_txtBreakfastTime setInputAccessoryView:_inputAccessory];
    [_txtLunchTime setInputAccessoryView:_inputAccessory];
    [_txtFirstSnackTime setInputAccessoryView:_inputAccessory];
    [_txtLunchTime setInputAccessoryView:_inputAccessory];
    [_txtSecondSnackTime setInputAccessoryView:_inputAccessory];
    [_txtDinnerTime setInputAccessoryView:_inputAccessory];
    
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"HH:mm a"];
    
    if ([[TSLanguageManager selectedLanguage] isEqualToString:@"es"]) {
        [_dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"es_es"]];
    } else {
        [_dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"pt_br"]];
    }

    _mealRemindersEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:kCCMealNotificationsEnabled];
    [_switchMealReminders setOn:_mealRemindersEnabled];
    _articlesRemindersEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:kCCArticlesNotificationsEnabled];
    [_switchArticlesReminders setOn:_articlesRemindersEnabled];
    
    _breakfastPicker = [[UIDatePicker alloc] init];
    [_breakfastPicker setBackgroundColor:[UIColor whiteColor]];
    [_breakfastPicker setDatePickerMode:UIDatePickerModeTime];
    [_breakfastPicker setTimeZone:[NSTimeZone defaultTimeZone]];

    if (_breakfastTime)
        [_breakfastPicker setDate:_breakfastTime];
    [_txtBreakfastTime setInputView:_breakfastPicker];
    [_txtBreakfastTime setText:[_dateFormatter stringFromDate:_breakfastTime]];
    
    _firstSnackPicker = [[UIDatePicker alloc] init];
    [_firstSnackPicker setBackgroundColor:[UIColor whiteColor]];
    [_firstSnackPicker setDatePickerMode:UIDatePickerModeTime];
    [_firstSnackPicker setTimeZone:[NSTimeZone defaultTimeZone]];
   
    if (_firstSnackTime)
        [_firstSnackPicker setDate:_firstSnackTime];
    [_txtFirstSnackTime setInputView:_firstSnackPicker];
    [_txtFirstSnackTime setText:[_dateFormatter stringFromDate:_firstSnackTime]];
    
    _lunchPicker = [[UIDatePicker alloc] init];
    [_lunchPicker setBackgroundColor:[UIColor whiteColor]];
    [_lunchPicker setDatePickerMode:UIDatePickerModeTime];
    [_lunchPicker setTimeZone:[NSTimeZone defaultTimeZone]];
    
    if (_lunchTime)
        [_lunchPicker setDate:_lunchTime];
    [_txtLunchTime setInputView:_lunchPicker];
    [_txtLunchTime setText:[_dateFormatter stringFromDate:_lunchTime]];
    
    
    _secondSnackPicker = [[UIDatePicker alloc] init];
    [_secondSnackPicker setBackgroundColor:[UIColor whiteColor]];
    [_secondSnackPicker setDatePickerMode:UIDatePickerModeTime];
    [_secondSnackPicker setTimeZone:[NSTimeZone defaultTimeZone]];
    
    if (_secondSnackTime)
        [_secondSnackPicker setDate:_secondSnackTime];
    [_txtSecondSnackTime setInputView:_secondSnackPicker];
    [_txtSecondSnackTime setText:[_dateFormatter stringFromDate:_secondSnackTime]];
    
    
    _dinnerPicker = [[UIDatePicker alloc] init];
    [_dinnerPicker setBackgroundColor:[UIColor whiteColor]];
    [_dinnerPicker setDatePickerMode:UIDatePickerModeTime];
    [_dinnerPicker setTimeZone:[NSTimeZone defaultTimeZone]];
   
    if (_dinnerTime)
        [_dinnerPicker setDate:_dinnerTime];
    [_txtDinnerTime setInputView:_dinnerPicker];
    [_txtDinnerTime setText:[_dateFormatter stringFromDate:_dinnerTime]];
    
    
    
        //meal reminders switch
    [_switchMealReminders addTarget:self action:@selector(onMealReminderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
        //Articles reminders switch
    [_switchArticlesReminders addTarget:self action:@selector(onArticlesReminderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
        //nav item
    
    [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Reminders"]];
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [(UILabel *)[cell viewWithTag:1] setText:[TSLanguageManager localizedString:@"My wake up time"]];
        } else if (indexPath.row == 1) {
            [(UILabel *)[cell viewWithTag:1] setText:[TSLanguageManager localizedString:@"Remind me to complete my missions"]];
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0 ) {
            [(UILabel *)[cell viewWithTag:1] setText:[TSLanguageManager localizedString:@"Remind me to log my meals"]];
        }
    }
    
    
}
- (void)configureInputAccessory
{
    
    _inputAccessory = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 40)];
    UIBarButtonItem  *spacer = [[UIBarButtonItem alloc]    initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [_inputAccessory setBarStyle:UIBarStyleDefault];
    [_inputAccessory setTintColor:[UIColor whiteColor]];
    [_inputAccessory setBarTintColor:CCColorGreen];
    
    _lblCurrentField = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, 220, 22)];
    [_lblCurrentField setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16]];
    [_lblCurrentField setTextColor:[UIColor whiteColor]];
    [_lblCurrentField setTextAlignment:NSTextAlignmentCenter];
    [_inputAccessory addSubview:_lblCurrentField];
    
    FAKFontAwesome *checkIcon = [FAKFontAwesome checkIconWithSize:28];
    [checkIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    FAKFontAwesome *cancelIcon = [FAKFontAwesome closeIconWithSize:28];
    [cancelIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    UIBarButtonItem *checkButton = [[UIBarButtonItem alloc] initWithImage:[checkIcon imageWithSize:CGSizeMake(25, 25)] style:UIBarButtonItemStyleDone target:self action:@selector(doSelection)];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithImage:[cancelIcon imageWithSize:CGSizeMake(25, 25)] style:UIBarButtonItemStyleDone target:self action:@selector(cancelSelection)];
    
    [_inputAccessory setItems:[NSArray arrayWithObjects:cancelButton, spacer,checkButton,nil] animated:YES];
    
}
- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)doSelection
{
    _doSelection = YES;
    [self dismissKeyboard];
}
- (void)cancelSelection
{
    _doSelection = NO;
    [self dismissKeyboard];
}
#pragma mark - UITextField Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
  
   if (textField == _txtBreakfastTime) {
        [_lblCurrentField setText:[TSLanguageManager localizedString:@"Breakfast Time"]];
    }  else if (textField == _txtFirstSnackTime) {
        [_lblCurrentField setText:[TSLanguageManager localizedString:@"Snack Time"]];
    }  else if (textField == _txtLunchTime) {
        [_lblCurrentField setText:[TSLanguageManager localizedString:@"Lunch Time"]];
    }  else if (textField == _txtSecondSnackTime) {
        [_lblCurrentField setText:[TSLanguageManager localizedString:@"Snack Time"]];
    }  else if (textField == _txtDinnerTime) {
        [_lblCurrentField setText:[TSLanguageManager localizedString:@"Dinner Time"]];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _txtBreakfastTime && _doSelection) {
        _breakfastTime = _breakfastPicker.date;
        [textField setText:[_dateFormatter stringFromDate:_breakfastTime]];
    }  else if (textField == _txtFirstSnackTime && _doSelection) {
        _firstSnackTime = _firstSnackPicker.date;
        [textField setText:[_dateFormatter stringFromDate:_firstSnackTime]];
    }  else if (textField == _txtLunchTime && _doSelection) {
        _lunchTime = _lunchPicker.date;
        [textField setText:[_dateFormatter stringFromDate:_lunchTime]];
    }  else if (textField == _txtSecondSnackTime && _doSelection) {
        _secondSnackTime = _secondSnackPicker.date;
        [textField setText: [_dateFormatter stringFromDate:_secondSnackTime]];
    }  else if (textField == _txtDinnerTime && _doSelection) {
        _dinnerTime = _dinnerPicker.date;
        [textField setText: [_dateFormatter stringFromDate:_dinnerTime]];
    }
}
#pragma mark - UI Events
- (void)onMealReminderValueChanged:(UISwitch *)mealReminderSwitch
{
    if(mealReminderSwitch.isOn && _breakfastTime && _firstSnackTime && _lunchTime && _secondSnackTime && _dinnerTime){
        
        _mealRemindersEnabled = YES;
        
    } else if (mealReminderSwitch.isOn && ! (_breakfastTime && _firstSnackTime && _lunchTime && _secondSnackTime && _dinnerTime)){
       
        [UIAlertView showWithTitle:[TSLanguageManager localizedString:@"Error"]  message:[TSLanguageManager localizedString:@"Please enter a time for every meal"]  cancelButtonTitle:[TSLanguageManager localizedString:@"Ok"] otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        }];
        [mealReminderSwitch setOn:NO animated:YES];
        
        _mealRemindersEnabled = NO;
    
    } else {
        _mealRemindersEnabled = NO;
        
    }
   
}
- (void)onArticlesReminderValueChanged:(UISwitch *)articlesReminderSwitch
{
    _articlesRemindersEnabled = articlesReminderSwitch.isOn;
}
- (IBAction)onSaveButtonTapped:(id)sender {
    
    
    _user.breakfastTime = _breakfastTime;
    _user.firstSnackTime = _firstSnackTime;
    _user.lunchTime = _lunchTime;
    _user.secondSnackTime = _secondSnackTime;
    _user.dinnerTime = _dinnerTime;
    
    [[CCAPI sharedInstance] saveUserWithCompletion:nil];
    
    [[NSUserDefaults standardUserDefaults] setBool:_mealRemindersEnabled forKey:kCCMealNotificationsEnabled];
    [[NSUserDefaults standardUserDefaults] setBool:_articlesRemindersEnabled forKey:kCCArticlesNotificationsEnabled];
    
    if (_articlesRemindersEnabled) {
        
        [[CCAPI sharedInstance] scheduleLocalNotificationsForArticles];
  
    } else {
    
        [[CCAPI sharedInstance] cancelLocalNotificationsForArticles];
    
    }
    
    if (_mealRemindersEnabled) {
    
        [[CCAPI sharedInstance] scheduleLocalNotificationsForMeals];
        
    } else {
        
        [[CCAPI sharedInstance] cancelLocalNotificationsForMeals];
    
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
