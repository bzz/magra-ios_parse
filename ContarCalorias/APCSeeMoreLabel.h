//
//  APCSeeMoreLabel.h
//  ContarCalorias
//
//  Created by andres portillo on 19/11/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APCSeeMoreLabel : UILabel
- (void)setText:(NSString *)text withSeeMoreLocalizedText:(NSString *)seeMore;
@property (nonatomic, strong) NSDictionary *seeMoreTextAttributes;
@end
