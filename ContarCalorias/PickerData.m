//
//  PickerData.m
//  ContarCalorias
//
//  Created by Adrian Ghitun on 26/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "PickerData.h"

@implementation PickerData

-(id)initWithInterfaceValue:(NSString *)interfaceValue andAPIValue:(NSString *)apiValue{
    self = [self init];
    
    self.interfaceValue = interfaceValue;
    self.apiValue = apiValue;
    
    return self;
}

@end
