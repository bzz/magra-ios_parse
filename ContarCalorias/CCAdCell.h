//
//  CCAdCell.h
//  ContarCalorias
//
//  Created by andres portillo on 17/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCAdCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgAdIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imgScreenshot;
@property (weak, nonatomic) IBOutlet UILabel *lblAction;
@property (weak, nonatomic) IBOutlet UIImageView *firstStar;
@property (weak, nonatomic) IBOutlet UIImageView *secondStar;
@property (weak, nonatomic) IBOutlet UIImageView *thirdStar;
@property (weak, nonatomic) IBOutlet UIImageView *forthStar;
@property (weak, nonatomic) IBOutlet UIImageView *fifthStar;
- (void)setRating:(double)rating;
@end
