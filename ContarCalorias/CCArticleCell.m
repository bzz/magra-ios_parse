//
//  CCArticleCell.m
//  ContarCalorias
//
//  Created by andres portillo on 23/09/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCArticleCell.h"

@implementation CCArticleCell
- (void)onCellTapped
{
    [self.delegate didTapOnCellWithIndex:self.index];
}
- (void)awakeFromNib
{
    UITapGestureRecognizer *tapRec1 = [[UITapGestureRecognizer alloc] init];
    [tapRec1 setNumberOfTapsRequired:1];
    [tapRec1 addTarget:self action:@selector(onCellTapped)];
    UITapGestureRecognizer *tapRec2 = [[UITapGestureRecognizer alloc] init];
    [tapRec2 setNumberOfTapsRequired:1];
    [tapRec2 addTarget:self action:@selector(onCellTapped)];
    _lblPostTitle.userInteractionEnabled = true;
    [_lblPostTitle addGestureRecognizer:tapRec1];
    _imgPostImage.userInteractionEnabled = true;
    [_imgPostImage addGestureRecognizer:tapRec2];
}
- (void)setArticleImage:(UIImage *)image
{
    CGFloat aspect = image.size.width / image.size.height;

    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self.imgPostImage attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.imgPostImage attribute:NSLayoutAttributeHeight multiplier:aspect constant:0.0f];
    [self addConstraint:constraint];
    [self.imgPostImage setImage:image];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
@end
