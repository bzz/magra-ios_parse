//
//  CCSettingsVC.h
//  ContarCalorias
//
//  Created by andres portillo on 2/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCWLPSettingsVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnMetricSystem;
@property (weak, nonatomic) IBOutlet UIButton *btnBritishSystem;
@property (weak, nonatomic) IBOutlet UILabel *lblMetricSystem;
@property (weak, nonatomic) IBOutlet UILabel *lblBritishSystem;
- (IBAction)onMetricSystemButtonTapped:(id)sender;
- (IBAction)onBritishSystemButtonTapped:(id)sender;

@end
