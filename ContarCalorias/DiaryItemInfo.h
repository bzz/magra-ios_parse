//
//  FoodItem.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 12/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiaryItemInfo : NSObject

-(id)initWithTile:(NSString *)title andDescription:(NSString *)description;

@property NSString *title;
@property NSString *description;
@property NSString *databaseItemId;
@property NSString *inDiaryItemId;
@property int met;

@property NSString *sugar;
@property NSString *carbs;
@property NSString *fats;
@property NSString *proteins;

@property NSString *kcal;

@end
