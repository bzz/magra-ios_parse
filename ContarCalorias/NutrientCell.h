//
//  NutrientCell.h
//  ContarCalorias
//
//  Created by Lucian Gherghel on 26/02/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NutrientCell : UITableViewCell

@property (strong, nonatomic) UILabel *nutrientName;
@property (strong, nonatomic) UILabel *quantity;
@property (strong, nonatomic) UILabel *value;

@end
