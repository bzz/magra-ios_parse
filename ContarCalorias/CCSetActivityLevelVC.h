//
//  CCSetActivityLevelVC.h
//  ContarCalorias
//
//  Created by andres portillo on 12/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class  User;

@interface CCSetActivityLevelVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblScreentitle;
@property (weak, nonatomic) IBOutlet UILabel *lblScreenDescription;
@end
