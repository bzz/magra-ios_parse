//
//  UIColor+ColorFromHexString.h
//  CynnySocialCloud
//
//  Created by Dan Vulpe on 4/13/13.
//  Copyright (c) 2013 Dan Vulpe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorFromHexString)

+ (UIColor *) colorFromHexString:(NSString *)hexString;
+ (NSString*) colorToHex:(UIColor*)color;

@end
