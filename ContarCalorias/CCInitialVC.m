//
//  CCInitialVC.m
//  ContarCalorias
//
//  Created by andres portillo on 26/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCInitialVC.h"

@interface CCInitialVC ()

@end

@implementation CCInitialVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *bg;
    if ([[UIScreen mainScreen] applicationFrame].size.height > 480) {
        bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"launch-bg-536h"]];
        
    } else{
        bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"launch-bg"]];

    }
    [self.view addSubview:bg];
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(40, (self.view.frame.size.height / 2) + 40, 240, 80)];
    [containerView.layer setCornerRadius:5];
    [containerView setAlpha:0.75];
    [containerView setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 45, 240, 14)];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
    [lbl setText:[TSLanguageManager localizedString:@"Loading"]];
    [containerView addSubview:lbl];
    
    UIActivityIndicatorView *activityIndicator = [UIActivityIndicatorView new];
    [activityIndicator setFrame:CGRectMake(120 - (activityIndicator.frame.size.width/2), 30 - (activityIndicator.frame.size.width/2), 0, 0)];
    [activityIndicator startAnimating];
    [activityIndicator setColor:[UIColor lightGrayColor]];
    [containerView addSubview:activityIndicator];
    
    [self.view addSubview:containerView];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
