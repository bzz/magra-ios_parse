//
//  CCDairyTopBar.m
//  ContarCalorias
//
//  Created by andres portillo on 23/09/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCDairyTopBar.h"
#import "CCAPI.h"
#import "User.h"
@implementation CCDairyTopBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)awakeFromNib
{
    [self updateContent];
}
- (void)updateContent
{
        //water
    
    NSInteger todaysDrinkedWaterMililiters = [[CCAPI sharedInstance] getTodaysDrinkedWaterMililiters];
    CGFloat drinkedWaterPercentage = (float)todaysDrinkedWaterMililiters  / (float)2000;
    [_progressViewWater setProgress:drinkedWaterPercentage];
    [_lblWaterText setText:[TSLanguageManager localizedString:@"Water"]];
    [_lblCurrentWater setText:[NSString stringWithFormat:@"%d",todaysDrinkedWaterMililiters]];
        //food
    
    User * user =[[CCAPI sharedInstance] getUser];
    NSInteger dailyCaloriesGoal = user.dailyCaloriesValue;;
    
    CGFloat currentCalories = [[CCAPI sharedInstance] getTodaysConsumedCalories] - [[CCAPI sharedInstance] getTodaysBurnedCalories];
    NSInteger remainingCalories = dailyCaloriesGoal - currentCalories;
    NSInteger consumedCalories = [[CCAPI sharedInstance] getTodaysConsumedCalories];
    NSInteger burnedCalories = [[CCAPI sharedInstance] getTodaysBurnedCalories];
    
    [_lblGoalValue setText:[NSString stringWithFormat:@"%d",dailyCaloriesGoal]];
    
    [_lblRemainingValue setText:[NSString stringWithFormat:@"%d",remainingCalories]];
    
    [_lblFoodValue setText:[NSString stringWithFormat:@"%d",consumedCalories]];
    
    [_lblExerciseValue setText:[NSString stringWithFormat:@"%d",burnedCalories]];

    [_lblFoodText setText:[TSLanguageManager localizedString:@"Food"]];
    [_lblRemainingText setText:[TSLanguageManager localizedString:@"Remaining"]];
    [_lblGoalText setText:[TSLanguageManager localizedString:@"Goal"]];
    [_lblExerciseText setText:[TSLanguageManager localizedString:@"Exercise"]];
    
}
@end
