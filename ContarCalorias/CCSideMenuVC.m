//
//  CCSideMenuVC.m
//  ContarCalorias
//
//  Created by andres portillo on 20/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCSideMenuVC.h"

#import "SWRevealViewController.h"
#import "CCWLPSettingsVC.h"
#import "CCDiaryVC.h"
#import "CCHomeVC.h"
#import <CTAssetsPickerController.h>
#import "UIImage+Resize.h"
#import "CCAPI.h"
#import "User.h"
#import "CCConstants.h"
#import "CCGetInTouchVC.h"
#import "CCUtils.h"
#import "CCIAPHelper.h"
#include "ATConnect.h"
#import <StoreKit/StoreKit.h>
#import "CCDietsIntroductionVC.h"

@interface CCSideMenuVC ()<UIActionSheetDelegate, CTAssetsPickerControllerDelegate>

@end
typedef NS_ENUM(NSInteger, CCSideMenuScreensSectionPreWLPCell) {   //WLP = weight loss plan
    CCSideMenuScreensSectionPreWLPCellHome = 0,
    CCSideMenuScreensSectionPreWLPCellGetInTouch,
    CCSideMenuScreensSectionPreWLPCellSettings,
};
typedef NS_ENUM(NSInteger, CCSideMenuScreensSectionCell) {
    CCSideMenuScreensSectionCellHome = 0,
    CCSideMenuScreensSectionCellDiary,
    CCSideMenuScreensSectionCellProgress,
    CCSideMenuScreensSectionCellGetInTouch,
    CCSideMenuScreensSectionCellSettings,
    CCSideMenuScreensSectionCellNutritionist,
    CCSideMenuScreensSectionCellDiets
};
typedef NS_ENUM(NSInteger, CCSideMenuSection) {
    CCSideMenuSectionheader = 0,
    CCSideMenuSectionScreens,
    CCSideMenuSectionSync
};
@implementation CCSideMenuVC
{
    CCSideMenuScreensSectionCell _selectedScreenCell;
    NSMutableArray *_arrCellTitles, *_arrCellImages, *_arrCellBackgroundImages;
    UIImageView *_syncIcon,*_profileImageView,*_imgDownArrow;
    UIImage *_profileImage;
    User * _user;
    NSDateFormatter  *_df;
    UILabel *_lblLastUpdated, *_lblWeightLost, *_lblWeightLostValue, *_lblGoalDate, *_lblGoal;
    NSArray *_ipaProducts;
}
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
 
    _user = [[CCAPI sharedInstance] getUser];
    
        //notifications
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNewMessageReceived) name:kCCNotificationNewMessageReceived object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startAnimatingSyncIcon) name:kCCNotificationSynchronizationStarted object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopAnimatingSyncIcon) name:kCCNotificationSynchronizationEnded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userPurchasedPro) name:kCCNotificationProFeaturesPurchased object:nil];
    
    
    _profileImage = [[CCAPI sharedInstance] getProfileImage];
    if (_profileImage == nil && _user != nil) {
        _profileImage = [UIImage imageNamed:@"account-default"];
        
        if(_user.authenticationTypeValue == CCAuthenticationTypeFacebook && _user.profilePictureUrl.length > 0)
            [self loadFacebookProfilePic];
    }
    [self setOverView:self.myOverView];
    UITapGestureRecognizer *tapRec= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onProfilePhotoTapped)];
    [tapRec setNumberOfTapsRequired:1];
    [tapRec setNumberOfTouchesRequired:1];
    
    _selectedScreenCell = CCSideMenuScreensSectionCellHome;
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    
    _arrCellTitles = [@[[TSLanguageManager localizedString:@"Home"],
                       [TSLanguageManager localizedString:@"Get In touch"],
                       [TSLanguageManager localizedString:@"Settings"],
                       ] mutableCopy];
    
    _arrCellImages = [@[@"home-menu-icon",
                       @"contactus-menu-icon",
                       @"settings-menu-icon",
                       ] mutableCopy];
    _arrCellBackgroundImages = [@[@"home-menu-cell-active-bg",
                                 @"contactus-menu-cell-active-bg",
                                 @"settings-menu-cell-active-bg",
                                 ] mutableCopy];
    if (_user.heightCmValue > 0) {
        
        [_arrCellTitles insertObject:[TSLanguageManager localizedString:@"Diary"] atIndex:1];
        [_arrCellImages insertObject:@"diary-menu-icon" atIndex:1];
        [_arrCellBackgroundImages insertObject:@"diary-menu-cell-active-bg" atIndex:1];
        
        [_arrCellTitles insertObject:[TSLanguageManager localizedString:@"Progress"] atIndex:2];
        [_arrCellImages insertObject:@"progress-menu-icon" atIndex:2];
        [_arrCellBackgroundImages insertObject:@"progress-menu-cell-active-bg" atIndex:2];
        
        [_arrCellTitles insertObject:[TSLanguageManager localizedString:@"Diets"] atIndex:5];
        [_arrCellImages insertObject:@"diets-menu-icon" atIndex:5];
        [_arrCellBackgroundImages insertObject:@"diets-menu-cell-active-bg" atIndex:5];
        
        [_arrCellTitles insertObject:[TSLanguageManager localizedString:@"Talk to Ivana"] atIndex:5];
        [_arrCellImages insertObject:@"nutritionist-menu-icon" atIndex:5];
        [_arrCellBackgroundImages insertObject:@"nutritionist-menu-cell-active-bg" atIndex:5];

    }
    UIImage *syncImage = [UIImage imageNamed:@"sync-menu-icon"];
    _syncIcon = [[UIImageView alloc] initWithImage:syncImage];
    [_syncIcon setFrame:CGRectMake(20, 14, 26, 26)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadScreen) name:@"LanguageWasChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadScreen) name:kCCNotificationUserCreatedWeightLossPlan object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadScreen) name:kCCNotificationUserProfileWasUpdated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadScreen) name:kCCNotificationProFeaturesPurchased object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUserWeightUpdated) name:kCCNotificationUserUpdatedWeight object:nil];
    
    
}
- (void)userPurchasedPro
{
    _user = [[CCAPI sharedInstance] getUser];
    [self setDietsVC];
}
- (void)reloadScreen
{
    _user = [[CCAPI sharedInstance] getUser];
    
    _arrCellTitles = [@[[TSLanguageManager localizedString:@"Home"],
                        [TSLanguageManager localizedString:@"Get In touch"],
                        [TSLanguageManager localizedString:@"Settings"],
                        ] mutableCopy];
    
    _arrCellImages = [@[@"home-menu-icon",
                        @"contactus-menu-icon",
                        @"settings-menu-icon",
                        ] mutableCopy];
    _arrCellBackgroundImages = [@[@"home-menu-cell-active-bg",
                                  @"contactus-menu-cell-active-bg",
                                  @"settings-menu-cell-active-bg",
                                  ] mutableCopy];
    if (_user.heightCmValue > 0) {
        
        [_arrCellTitles insertObject:[TSLanguageManager localizedString:@"Diary"] atIndex:1];
        [_arrCellImages insertObject:@"diary-menu-icon" atIndex:1];
        [_arrCellBackgroundImages insertObject:@"diary-menu-cell-active-bg" atIndex:1];
        
        [_arrCellTitles insertObject:[TSLanguageManager localizedString:@"Progress"] atIndex:2];
        [_arrCellImages insertObject:@"progress-menu-icon" atIndex:2];
        [_arrCellBackgroundImages insertObject:@"progress-menu-cell-active-bg" atIndex:2];
        
        [_arrCellTitles insertObject:[TSLanguageManager localizedString:@"Diets"] atIndex:5];
        [_arrCellImages insertObject:@"diets-menu-icon" atIndex:5];
        [_arrCellBackgroundImages insertObject:@"diets-menu-cell-active-bg" atIndex:5];
        
        [_arrCellTitles insertObject:[TSLanguageManager localizedString:@"Chat with nutritionist"] atIndex:5];
        [_arrCellImages insertObject:@"nutritionist-menu-icon" atIndex:5];
        [_arrCellBackgroundImages insertObject:@"nutritionist-menu-cell-active-bg" atIndex:5];

    }
    
    [self.tableView reloadData];
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)loadFacebookProfilePic
{
    UIApplication* app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:_user.profilePictureUrl]];
        UIImage *FBProfileImage = [UIImage imageWithData:imageData];
        [[CCAPI sharedInstance] saveFacebookProfileImage:FBProfileImage];
        _profileImage = FBProfileImage;
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            if (_profileImage) {
                [self updateViewHeader];
                [self setMainImage:_profileImage];
            }
            UIApplication* app = [UIApplication sharedApplication];
            app.networkActivityIndicatorVisible = NO;
            
        });
    });
    
}
- (void)onUserWeightUpdated
{
    _user = [[CCAPI sharedInstance] getUser];
        // Update lost weight label
    CGFloat lostWeight = _user.initialWeightKgValue - _user.currentWeightKgValue;
    [_lblWeightLostValue setText:[NSString stringWithFormat:@"%.2f", lostWeight]];
    
    
        //update date reaching ideal weight
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd MMM, yyyy"];
    if ([[TSLanguageManager selectedLanguage] isEqualToString:@"es"]) {
        [df setLocale:[NSLocale localeWithLocaleIdentifier:@"es_ES"]];
    } else{
        [df setLocale:[NSLocale localeWithLocaleIdentifier:@"pt_BR"]];
    }
    [_lblGoalDate setText:[df stringFromDate:_user.dateReachingDesiredWeight]];
    
    
}
- (UIView *)myOverView {
  
    UIView *view = [[UIView alloc] initWithFrame:self.overView.bounds];
    view.tag = 1;
        //Add an example imageView
    _profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20.0,20.0, 60.0, 60.0)];
    [_profileImageView  setUserInteractionEnabled:YES];
    [_profileImageView setContentMode:UIViewContentModeScaleAspectFill];
    [_profileImageView setClipsToBounds:YES];
    [_profileImageView setImage:_profileImage];
    [_profileImageView.layer setCornerRadius:_profileImageView.frame.size.width/2.0];
    _profileImageView.tag = 1;
    UITapGestureRecognizer *tapRec= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onProfilePhotoTapped)];
    [tapRec setNumberOfTapsRequired:1];
    [tapRec setNumberOfTouchesRequired:1];
    [_profileImageView addGestureRecognizer:tapRec];
    
    
    [view addSubview:_profileImageView];
    
    _lblWeightLost = [[UILabel alloc] initWithFrame:CGRectMake(100, 50, 100, 16)];
    [_lblWeightLost setText:[TSLanguageManager localizedString:@"Lost"]];
    [_lblWeightLost setTextColor:[UIColor whiteColor]];
    [_lblWeightLost setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10]];
    [view addSubview:_lblWeightLost];
    
    _lblWeightLostValue = [[UILabel alloc] initWithFrame:CGRectMake(100, 35, 100, 16)];
    CGFloat lostWeight = _user.initialWeightKgValue - _user.currentWeightKgValue;
    [_lblWeightLostValue setText:[NSString stringWithFormat:@"%.2f", lostWeight]];
    [_lblWeightLostValue setTextColor:[UIColor whiteColor]];
    [_lblWeightLostValue setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12]];
    [view addSubview: _lblWeightLostValue];
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd MMM, yyyy"];
    if ([[TSLanguageManager selectedLanguage] isEqualToString:@"es"]) {
        [df setLocale:[NSLocale localeWithLocaleIdentifier:@"es_ES"]];
    } else{
        [df setLocale:[NSLocale localeWithLocaleIdentifier:@"pt_BR"]];
    }
    _lblGoalDate = [[UILabel alloc] initWithFrame:CGRectMake(150, 35, 100, 16)];
    [_lblGoalDate setText:[df stringFromDate:_user.dateReachingDesiredWeight]];
    [_lblGoalDate setTextColor:[UIColor whiteColor]];
    [_lblGoalDate setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12]];
    [view addSubview: _lblGoalDate];
    
    _lblGoal = [[UILabel alloc] initWithFrame:CGRectMake(150, 50, 100, 16)];
    [_lblGoal setText:[TSLanguageManager localizedString:@"Goal"]];
    [_lblGoal setTextColor:[UIColor whiteColor]];
    [_lblGoal setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10]];
    [view addSubview:_lblGoal];
    
    _imgDownArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lost-weight-icon"]];
    [_imgDownArrow setFrame:CGRectMake(90, 38, 8, 13)];
    [view addSubview:_imgDownArrow];
    
    return view;
}
- (void)updateViewHeader
{
    [self setOverView:self.myOverView];
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - UITableView Delegate & Datasource

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if(section == 0)
        return [super tableView:tableView viewForHeaderInSection:section];
    
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 0)
        return 95;
    
    return 0.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section > 0)
        return 50.0;
    return 0.0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 1) {
        return _arrCellTitles.count;
    }
    if(section == 2) {
    NSInteger rows = 0;
    if (_user.heightCmValue > 0) {
        rows = 1;
    }
    return _user.hasProFeatures? rows:(rows + 1);
        
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    NSString *identifier = @"Cell Identifier";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;

    if(section == CCSideMenuSectionScreens) {
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(60, 14, 160, 20)];
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
        [lblTitle setTextColor:[UIColor blackColor]];
        [lblTitle setTag:1];
        [cell.contentView addSubview:lblTitle];
        
        if (row == _selectedScreenCell) {
            if (_selectedScreenCell != CCSideMenuScreensSectionCellDiets || _user.isProUserValue) {
                NSString *imageName = [_arrCellBackgroundImages objectAtIndex:row];
                UIImageView *bgImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
                [bgImg setFrame:CGRectMake(0, 0, 270, 50)];
                [cell setBackgroundView:bgImg];
            }
            
        } else {
            [cell setBackgroundView:[UIView new]];
        }
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.textLabel setTextColor:[UIColor blackColor]];
        UIImageView *cellImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[_arrCellImages objectAtIndex:row]]];
        [cellImg setFrame:CGRectMake(20, 12, 26, 26)];

        
        [cell.contentView addSubview:cellImg];
        [(UILabel *)[cell.contentView viewWithTag:1] setText:[_arrCellTitles objectAtIndex:row]];
        
        if (row == CCSideMenuScreensSectionCellNutritionist && ! _user.hasProFeatures)
        {
            UIImageView *imgPro = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pro-icon"]];
            [imgPro setCenter:CGPointMake(220, 25)];
            [cell.contentView addSubview:imgPro];
        }
        if (row == CCSideMenuScreensSectionCellDiets && ! _user.hasProFeatures)
        {
            UIImageView *imgPro = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pro-icon"]];
            [imgPro setCenter:CGPointMake(220, 25)];
            [cell.contentView addSubview:imgPro];
        } else  if (row == CCSideMenuScreensSectionCellGetInTouch)
        {
            NSUInteger unreadMessageCount = [[ATConnect sharedConnection] unreadMessageCount];
            if (unreadMessageCount > 0) {
                UILabel *lblUnreadMessagesCount = [[UILabel alloc] initWithFrame:CGRectMake(38, 30, 16, 16)];
                [lblUnreadMessagesCount setBackgroundColor:[UIColor redColor]];
                
                
                NSString *unreadCount = [NSString stringWithFormat:@"%d",unreadMessageCount];
                [lblUnreadMessagesCount setText:unreadCount];
                CGRect labelRect = [unreadCount
                                    boundingRectWithSize:CGSizeMake(0,25)
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:9]
                                                 }
                                    context:nil];
                labelRect.size.width = MIN(MAX(18,labelRect.size.width), 40);
                labelRect.size.height = 18;
                labelRect.origin.x = 38;
                labelRect.origin.y = 30;
                [lblUnreadMessagesCount setFrame:labelRect];
                [lblUnreadMessagesCount setTextColor:[UIColor whiteColor]];
                [lblUnreadMessagesCount setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:9]];
                [lblUnreadMessagesCount setClipsToBounds:YES];
                [lblUnreadMessagesCount setTextAlignment:NSTextAlignmentCenter];
                lblUnreadMessagesCount.layer.cornerRadius = 9;
                [cell.contentView addSubview:lblUnreadMessagesCount];
            }
        }

        
    } else if (section == CCSideMenuSectionSync) {
        if (row == 0) {
            [cell setBackgroundColor:[UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1]];
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(60, 10, 160, 20)];
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
            [lblTitle setTextColor:[UIColor blackColor]];
            [lblTitle setText:[TSLanguageManager localizedString:@"Sync"]];
            [cell.contentView addSubview:lblTitle];
            
            _lblLastUpdated = [[UILabel alloc] initWithFrame:CGRectMake(60, 28, 160, 20)];
            [_lblLastUpdated setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
            [_lblLastUpdated setTextColor:[UIColor blackColor]];
            
            _df = [[NSDateFormatter alloc] init];
            [_df setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
            [cell.contentView addSubview:_lblLastUpdated];
            [cell.contentView addSubview:_syncIcon];
        
            
            NSDate *lastSync = [[NSUserDefaults standardUserDefaults] objectForKey:kCCLastSyncDate];
            if (lastSync) {
                if ([CCUtils datesHaveSameDay:[NSDate date] SecondDate:lastSync]) {
                    [_df setDateFormat:@"hh:mm a"];
                } else {
                    [_df setDateFormat:@"dd MMM, hh:mm a"];
                }
                [_lblLastUpdated setText:[NSString stringWithFormat:@"%@: %@", [TSLanguageManager localizedString:@"Last"],[_df stringFromDate:lastSync]]];
            } else {
                [_lblLastUpdated setText:@""];
            }
        }
        else if (row == 1) {
            
            UIImageView * imgProButton = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"go-pro-button"]];
            [imgProButton setCenter:CGPointMake(130, 25)];
            [cell.contentView addSubview:imgProButton];
        
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(50, 15, 180, 20)];
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:14]];
            [lblTitle setTextColor:[UIColor whiteColor]];
            [lblTitle setTextAlignment:NSTextAlignmentCenter];
            [lblTitle setText:[TSLanguageManager localizedString:@"Upgrade to magra pro"]];
            [cell.contentView addSubview:lblTitle];
            
        }
        
    }
 
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == CCSideMenuSectionSync) {
        
        if (indexPath.row == 0) {
            [[CCAPI sharedInstance] performSynchronization];
        } else {
            
                UIStoryboard *dietsStoryboard = [UIStoryboard storyboardWithName:@"Diets" bundle:nil];
                UIViewController *dietsIntroductionVC = [dietsStoryboard instantiateInitialViewController];
                [self.revealViewController presentViewController:dietsIntroductionVC animated:YES completion:nil];
            
        }
        
    }else{
        
        if (_user.heightCmValue > 0) {
           
            if (indexPath.row == CCSideMenuScreensSectionCellHome) {
                [self setHomeVC];
            } else if (indexPath.row == CCSideMenuScreensSectionCellDiary) {
                [self setDiaryVC];
            } else if (indexPath.row == CCSideMenuScreensSectionCellProgress) {
                [self setProgressVC];
            } else if (indexPath.row == CCSideMenuScreensSectionCellNutritionist) {
                [self setNutritionistVC];
            } else if (indexPath.row == CCSideMenuScreensSectionCellGetInTouch) {
                [self setGetInTouchVC];
            } else  if (indexPath.row == CCSideMenuScreensSectionCellSettings) {
                [self setSettingsVC];
            } else  if (indexPath.row == CCSideMenuScreensSectionCellDiets) {
                [self setDietsVC];
            }
            
            if (indexPath.row != CCSideMenuScreensSectionCellGetInTouch)
                _selectedScreenCell = indexPath.row;

        } else {
            if (indexPath.row == CCSideMenuScreensSectionPreWLPCellHome) {
                [self setHomeVC];
            } else if (indexPath.row == CCSideMenuScreensSectionPreWLPCellGetInTouch) {
                [self setGetInTouchVC];
            } if (indexPath.row == CCSideMenuScreensSectionPreWLPCellSettings) {
                [self setSettingsVC];
            }
            if (indexPath.row != CCSideMenuScreensSectionPreWLPCellGetInTouch)
                _selectedScreenCell = indexPath.row;
        }
        
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:CCSideMenuSectionScreens] withRowAnimation:UITableViewRowAnimationNone];
    }
}
- (void)onNewMessageReceived
{
    [self.tableView reloadData];
}
- (void) runSpinAnimationOnView:(UIView*)view
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0];
    rotationAnimation.duration = 1;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = HUGE_VALF;
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

#pragma mark - Menu methods

- (void)setNutritionistVC
{
    if (_user.hasProFeatures) {
        [[ATConnect sharedConnection] presentMessageCenterFromViewController:self];
    } else {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Diets" bundle:nil];
        CCDietsIntroductionVC  *dietsIntroVC = [mainStoryboard instantiateInitialViewController];
        [self.revealViewController presentViewController:dietsIntroVC animated:YES completion:nil];
    }
}
- (void)setProgressVC
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *progressNC = [mainStoryboard instantiateViewControllerWithIdentifier:@"Progress"];
    [progressNC.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    [self.revealViewController setFrontViewController:progressNC animated:YES];
}
- (void)setSettingsVC
{
    UIStoryboard *settingsStoryboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UINavigationController *settingsNC = [settingsStoryboard instantiateInitialViewController];
    [settingsNC.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    
    [self.revealViewController setFrontViewController:settingsNC animated:YES];
}
- (void)setDiaryVC
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CCDiaryVC *diaryVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"Diary"];
    UINavigationController *diaryNC = [[UINavigationController alloc] initWithRootViewController:diaryVC];
    [diaryNC.navigationBar setTranslucent:NO];
    [diaryNC.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    
    [self.revealViewController setFrontViewController:diaryNC animated:YES];
    
}
- (void)setHomeVC
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *homeNC = [mainStoryboard instantiateInitialViewController];
    [homeNC.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    
    [self.revealViewController setFrontViewController:homeNC animated:YES];
    
}
- (void)setGetInTouchVC
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CCGetInTouchVC *getInTouchVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"Get In Touch"];
    UINavigationController *getInTouchNC = [[UINavigationController alloc] initWithRootViewController:getInTouchVC];
    [getInTouchNC.navigationBar setTranslucent:NO];
    
    [self.revealViewController  presentViewController:getInTouchNC animated:YES completion:nil];
}
- (void)setDietsVC
{
    if (_user.hasProFeatures) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Diets" bundle:nil];
        UINavigationController *DietsMainVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"Main NC"];
        [DietsMainVC.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.revealViewController setFrontViewController:DietsMainVC animated:YES];
    } else {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Diets" bundle:nil];
        CCDietsIntroductionVC  *dietsIntroVC = [mainStoryboard instantiateInitialViewController];
        [self.revealViewController presentViewController:dietsIntroVC animated:YES completion:nil];
    }
}
- (void)startAnimatingSyncIcon
{
    [self runSpinAnimationOnView:_syncIcon];
}
- (void)stopAnimatingSyncIcon
{
    [_syncIcon.layer removeAnimationForKey:@"rotationAnimation"];
    NSDate *lastSync = [[NSUserDefaults standardUserDefaults] objectForKey:kCCLastSyncDate];
    
    if (lastSync) {
        if ([CCUtils datesHaveSameDay:[NSDate date] SecondDate:lastSync]) {
            
            [_df setDateFormat:@"hh:mm a"];
            
        } else {
            
            [_df setDateFormat:@"dd MMM, hh:mm a"];
            
        }
        [_lblLastUpdated setText:[NSString stringWithFormat:@"%@: %@", [TSLanguageManager localizedString:@"Last"],[_df stringFromDate:lastSync]]];
    } else {
        
        [_lblLastUpdated setText:@""];
    }
    [self reloadScreen];
}
#pragma mark - UIActions
- (void)onProfilePhotoTapped
{
    UIActionSheet *actionSheet;
    if (_user.authenticationTypeValue == CCAuthenticationTypeFacebook && ! [[NSUserDefaults standardUserDefaults] boolForKey:kCCUseFacebookAvatar])
    {
        actionSheet = [[UIActionSheet alloc] initWithTitle:[TSLanguageManager localizedString:@"Update profile photo"] delegate:self cancelButtonTitle:[TSLanguageManager localizedString:@"Cancel"] destructiveButtonTitle:nil otherButtonTitles:[TSLanguageManager localizedString:@"Load photo from library"], [TSLanguageManager localizedString:@"Load photo from Facebook"], nil];
    } else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:[TSLanguageManager localizedString:@"Update profile photo"] delegate:self cancelButtonTitle:[TSLanguageManager localizedString:@"Cancel"] destructiveButtonTitle:nil otherButtonTitles:[TSLanguageManager localizedString:@"Load photo from library"], nil];
    }

    [actionSheet showInView:self.view];
}
#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
        picker.delegate = self;
        picker.title = [TSLanguageManager localizedString:@"Select profile photo"];
        picker.assetsFilter = [ALAssetsFilter allPhotos]; // Only pick photos.
        picker.showsNumberOfAssets = NO;
        [self presentViewController:picker animated:YES completion:nil];
        
    } else if(buttonIndex == 1) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kCCUseFacebookAvatar];
        [[NSUserDefaults standardUserDefaults] synchronize];
        _profileImage = [[CCAPI sharedInstance] getProfileImage];
        if (_profileImage == nil) {
            _profileImage = [UIImage imageNamed:@"account-default"];
            [self loadFacebookProfilePic];
            
        } else {
            [self setOverView:self.myOverView];
        }
    }
}
#pragma mark - CTAssetsPickerController delegate
- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(ALAsset *)asset
{
    return (picker.selectedAssets.count < 1);
}
- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    ALAsset *asset = [assets firstObject];
    UIImage *assetImage = [UIImage imageWithCGImage:[[asset defaultRepresentation]  fullScreenImage]];
    if(assetImage.size.width >640) {
        _profileImage = [assetImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds: CGSizeMake(640, 1136) interpolationQuality:1];
    } else {
        _profileImage = assetImage;
    }
    assetImage = nil;
    
    [[CCAPI sharedInstance] saveProfileImage:_profileImage];
    [self setMainImage:_profileImage];
    [self setOverView:self.myOverView];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kCCUseFacebookAvatar];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
}
@end
