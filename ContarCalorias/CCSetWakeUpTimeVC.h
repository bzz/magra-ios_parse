//
//  CCSetWakeUpTimeVC.h
//  ContarCalorias
//
//  Created by andres portillo on 17/11/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCSetWakeUpTimeVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblScreenTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtWakeupTime;

@end
