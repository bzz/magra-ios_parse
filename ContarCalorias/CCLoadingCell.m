//
//  CCLoadingCell.m
//  ContarCalorias
//
//  Created by andres portillo on 22/11/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCLoadingCell.h"
#import "FontAwesomeKit.h"

@implementation CCLoadingCell
{
    UIButton *_btnRefresh;
    UILabel *_lblNoMoreToLoad;
}

- (void)awakeFromNib {
    
    // Initialization code
    
    FAKFontAwesome *refreshIcon = [FAKFontAwesome refreshIconWithSize:30];
    [refreshIcon addAttribute:NSForegroundColorAttributeName value:CCColorGray];
    _btnRefresh = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [_btnRefresh setImage:[refreshIcon imageWithSize:CGSizeMake(30, 30)] forState:UIControlStateNormal];
    [_btnRefresh setCenter:CGPointMake(159, _activityIndicator.center.y)];
    [_btnRefresh addTarget:self action:@selector(refresh) forControlEvents:UIControlEventTouchUpInside];
    [_btnRefresh setAlpha:0];
    [self.contentView addSubview:_btnRefresh];
    
    
    _lblNoMoreToLoad = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 300, 30)];
    [_lblNoMoreToLoad setText:[TSLanguageManager localizedString:@"No more posts to load"]];
    [_lblNoMoreToLoad setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
    [_lblNoMoreToLoad setTextColor:CCColorGray];
    [_lblNoMoreToLoad setAlpha:0];
    
    [self.contentView addSubview:_lblNoMoreToLoad];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)loadingDidFail
{
    [_activityIndicator stopAnimating];
    [_btnRefresh setAlpha:1];
}
- (void)refresh
{
    [_activityIndicator startAnimating];
    [_btnRefresh setAlpha:0];
    [self.delegate theRefreshButtonWasTappedOnLoadingCell:self];
}
- (void)noMoreToLoad
{
    [_lblNoMoreToLoad setAlpha:1];
}
@end
