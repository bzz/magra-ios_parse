//
//  CCMiniChallenveVC.m
//  ContarCalorias
//
//  Created by andres portillo on 15/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCArticleVC.h"
#import "Article.h"
#import "UIImageView+AFNetworking.h"
#import "CCAPI.h"

@interface CCArticleVC () <UIWebViewDelegate, UIScrollViewDelegate>

@end

@implementation CCArticleVC


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.scrollView.frame = CGRectMake(0, 0, 320, self.view.bounds.size.height - 64);
    self.scrollView.delegate = self;
    // Do any additional setup after loading the view.
    
        //nav item title
    NSString *screenTitle = [self.article.type isEqualToString:@"mini challenge"]? [TSLanguageManager localizedString:@"Challenge"]:[TSLanguageManager localizedString:@"Nutrition Tip"];
    UIFont* font =[UIFont fontWithName:@"HelveticaNeue-Medium" size:17];
    CGRect frame = CGRectMake(0, 0, [screenTitle sizeWithAttributes:@{NSFontAttributeName:font}].width, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = font;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:58.0/255.0 green: 54.0/255.0 blue: 69.0/255.0 alpha:1];
    label.text = screenTitle;
    self.navigationItem.titleView = label;
    
   NSString *webViewHTMLString =  _article.content;
 
    [_webViewDescription loadHTMLString:webViewHTMLString baseURL:nil];
    [_webViewDescription.scrollView setScrollEnabled:YES];
    [_webViewDescription setDelegate:self];
    [_webViewDescription setFrame:CGRectMake(0,0, 320, _webViewDescription.scrollView.frame.size.height)];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
