//
//  CCTaskProgressView.h
//  ContarCalorias
//
//  Created by andres portillo on 13/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CCTaskProgressView;

@interface CCTaskProgressViewWrapper : UIView
@property(nonatomic,strong) CCTaskProgressView *progressView;
- (void)setProgress:(CGFloat)progress;
@end
