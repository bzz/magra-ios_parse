//
//  ViewUtils.m
//  ContarCalorias
//
//  Created by Adrian Ghitun on 02/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "ViewUtils.h"

#import <QuartzCore/QuartzCore.h>

@implementation ViewUtils

+(UIColor *)greenColor{
    return [UIColor colorWithRed:(float)186/255 green:(float)201/255 blue:(float)81/255 alpha:1];
}

+(UIColor *)greenColorPressed{
    return [UIColor colorWithRed:(float)186/255 green:(float)201/255 blue:(float)81/255 alpha:0.8];
}

+(UIColor *)lightBlueColor{
    return [UIColor colorWithRed:(float)89/255 green:(float)197/255 blue:(float)253/255 alpha:1];
}

+(UIColor *)lightGrayColor{
    return [UIColor colorWithRed:(float)234/255 green:(float)237/255 blue:(float)241/255 alpha:1];
}

+(UIColor *)orangeColor{
    return [UIColor colorWithRed:255/255.0f green:171/255.0f blue:45/255.0f alpha:1];
}

+(void)drawBasicShadowOnView:(UIView *)view{
    view.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0, 1);
    view.layer.shadowRadius = 1;
    view.layer.shadowOpacity = 0.3;
    [view.layer setShadowPath:[[UIBezierPath bezierPathWithRoundedRect:view.bounds cornerRadius:view.layer.cornerRadius] CGPath]];
}


+(void)fixSeparatorsHeightInView:(UIView *)views{
    for (UIView *view in views.subviews){
        if (view.tag == 1){
            CGRect frame = view.frame;
            frame.size = CGSizeMake(frame.size.width, 1/[[UIScreen mainScreen] scale]);
            view.frame = frame;
        }
        else if (view.tag == 2){
            CGRect frame = view.frame;
            frame.size = CGSizeMake(1/[[UIScreen mainScreen] scale], frame.size.height);
            view.frame = frame;
        }
    }
}

+(void)setLeftViewOnTextField:(UITextField *)textField withImage:(UIImage *)image{
    UIView *leftView = [[UIImageView alloc] initWithImage:image];
    leftView.contentMode = UIViewContentModeScaleAspectFit;
    [leftView setFrame:CGRectMake(0, 0, 25, 15)];
    textField.leftView = leftView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}


+(void)setLeftViewOnTextField:(UITextField *)textField withImage:(UIImage *)image andText:(NSString *)text{
    
    UIView *leftView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:image];
    [imgView setFrame:CGRectMake(0, 0, 35, 20)];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [leftView addSubview:imgView];
    UILabel *label = [[UILabel alloc] init];
    [label setFrame:CGRectMake(35, 1, 0, 0)];
    label.text = text;
    label.textColor = [UIColor lightGrayColor];
    label.font = [UIFont systemFontOfSize:13];
    [label sizeToFit];
    leftView.frame = CGRectMake(0, 0, 35+label.frame
                                .size.width+5, 20);
    [leftView addSubview:label];
    
    textField.leftView = leftView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}


+(void)setLeftViewOnTextField:(UITextField *)textField withText:(NSString *)text{
    UIView *leftView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    UILabel *label = [[UILabel alloc] init];
    [label setFrame:CGRectMake(0, 0, 0, 0)];
    label.text = text;
    label.textColor = [UIColor lightGrayColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    [label sizeToFit];
    
    leftView.frame = CGRectMake(0, 0,label.frame
                                .size.width+5, textField.frame.size.height);
    [leftView addSubview:label];
    
    textField.leftView = leftView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

@end
