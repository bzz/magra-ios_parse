//
//  ContarCalorias.h
//  ContarCalorias
//
//  Created by Lucian Gherghel on 22/02/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

#define Calendar [ContarCalendar sharedInstance]

@interface ContarCalendar : UIView

@property (nonatomic, strong) NSDate *dateShowing;
@property (nonatomic, strong) NSDate *previouseDate;
@property (nonatomic, strong) NSDictionary *currentDayInfo;
@property (nonatomic, strong) NSDictionary *daysInfo;

+ (instancetype)sharedInstance;
- (void)moveCalendarToPreviousDay;
- (void)moveCalendarToNextDay;

- (BOOL)date:(NSDate *)date1 isSameDayAsDate:(NSDate *)date2;
@end
