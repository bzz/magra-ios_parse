//
//  DateButton.m
//  ContarCalorias
//
//  Created by Lucian Gherghel on 22/02/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "DateButton.h"

#import <QuartzCore/QuartzCore.h>
#import "ViewUtils.h"

@implementation DateButton

- (void)setDefaultStyle
{
    self.layer.cornerRadius = self.frame.size.width/2;
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.clipsToBounds = YES;
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0];
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
}

- (void)setCurrentDate
{
    if (self.date)
    {
        NSDateComponents *comps = [self.calendar components:NSDayCalendarUnit|NSMonthCalendarUnit fromDate:self.date];
        [self setTitle:[NSString stringWithFormat:@"%li", (long)comps.day] forState:UIControlStateNormal];
    }
    else
        [self setTitle:@"" forState:UIControlStateNormal];
}

- (void)setNonCurrentMonthDay
{
    self.layer.borderColor = [UIColor clearColor].CGColor;
    [self setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
}

- (void)highlightCurrentDay
{
    [self setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
}

- (void)markAsDayWithActivity
{
    self.layer.borderColor = [ViewUtils greenColor].CGColor;
}

@end
