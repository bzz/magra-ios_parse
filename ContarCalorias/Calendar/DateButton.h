//
//  DateButton.h
//  ContarCalorias
//
//  Created by Lucian Gherghel on 22/02/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateButton : UIButton

@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSCalendar *calendar;

- (void)setDefaultStyle;
- (void)setNonCurrentMonthDay;
- (void)highlightCurrentDay;
- (void)setCurrentDate;
- (void)markAsDayWithActivity;

@end
