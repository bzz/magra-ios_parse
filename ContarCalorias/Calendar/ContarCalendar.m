//
//  ContarCalorias.m
//  ContarCalorias
//
//  Created by Lucian Gherghel on 22/02/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "ContarCalendar.h"

#import <QuartzCore/QuartzCore.h>

#import "UIColor+ColorFromHexString.h"
#import "DateButton.h"
#import "User.h"
#import "CCAPI.h"
#import <UIAlertView+Blocks.h>

static ContarCalendar *sharedInstance;

@interface ContarCalendar()


@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSCalendar *calendar;
@property (nonatomic, assign) float defaultCellDateButtonWidth;
@property (nonatomic, assign) float defaultCellDateButtonHeight;
@property (nonatomic, assign) float defaultDateCellYSpacing;
@property (nonatomic, assign) float defaultDateCellXSpacing;
@property (nonatomic, assign) float cellWrapperYSpacing;
@property (nonatomic, assign) float cellWrapperXSpacing;

@end

@implementation ContarCalendar
{
    UIView *calendarView;
    
    
    UIView *calendarHeader;
    UILabel *currentDayLabel;
    UIButton *prevDayButton;
    UIButton *nextDayButton;
    
    
    UIView *daysOfTheWeekWrapper;
    NSMutableArray *daysOfTheWeekLabels;
    
    UIView *lineSeparatorView1;
    
    UIView *datesWrapper;
    NSMutableArray *dateButtons;
    
    UIView *lineSeparatorView2;
    
    UIView *calendarHistory;
}
+ (instancetype)sharedInstance
{
    if(sharedInstance == nil)
    {
        sharedInstance = [[ContarCalendar alloc] init];
        [sharedInstance buildComponents];
    }
    
    return sharedInstance;
}

- (void)buildComponents
{
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    self.dateShowing = [NSDate new];
    self.previouseDate = self.dateShowing;
    
    self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [self.calendar setLocale:[NSLocale currentLocale]];
    [self.calendar setFirstWeekday:2];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    [self setDefaultCellDateSizes];
    
    //general calendar configuration
    self.frame = CGRectMake(0.0, 0.0, screenSize.width, screenSize.height-64.0);
    //self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3];
    calendarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, screenSize.width, 374.0)];
    
    //configure header
    calendarHeader = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, screenSize.width, 44.0)];
    calendarHeader.backgroundColor = [UIColor colorFromHexString:@"bac951"];
    
    currentDayLabel = [[UILabel alloc] initWithFrame:calendarHeader.frame];
    currentDayLabel.textAlignment = NSTextAlignmentCenter;
    currentDayLabel.backgroundColor = [UIColor clearColor];
    currentDayLabel.textColor = [UIColor whiteColor];
    currentDayLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0];
    currentDayLabel.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    
    prevDayButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 37.0, 44.0)];
    [prevDayButton setImage:[UIImage imageNamed:@"calendar_leftarrow_white.png"] forState:UIControlStateNormal];
    prevDayButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
    [prevDayButton addTarget:self action:@selector(moveCalendarToPreviousMonth) forControlEvents:UIControlEventTouchUpInside];
    prevDayButton.backgroundColor = [UIColor clearColor];
    
    nextDayButton = [[UIButton alloc] initWithFrame:CGRectMake(screenSize.width-37.0, 0.0, 37.0, 44.0)];
    [nextDayButton setImage:[UIImage imageNamed:@"calendar_rightarrow_white.png"] forState:UIControlStateNormal];
    nextDayButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
    [nextDayButton addTarget:self action:@selector(moveCalendarToNextMonth) forControlEvents:UIControlEventTouchUpInside];
    nextDayButton.backgroundColor = [UIColor clearColor];
    
    [calendarHeader addSubview:currentDayLabel];
    [calendarHeader addSubview:prevDayButton];
    [calendarHeader addSubview:nextDayButton];
    
    
    //week days
    daysOfTheWeekWrapper = [[UIView alloc] initWithFrame:CGRectMake(0.0, calendarHeader.frame.origin.y+44.0, screenSize.width, 35.0)];
    daysOfTheWeekWrapper.backgroundColor = [UIColor whiteColor];
    
    daysOfTheWeekLabels = [NSMutableArray new];
    float cellWidth = (floorf(300 / 7.0));
    CGRect lastDayFrame = CGRectMake(13.5, 0.0, 0.0, 0.0);
    for (int i = 0; i < 7; i++)
    {
        UILabel *dayOfWeekLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lastDayFrame), lastDayFrame.origin.y, cellWidth, 35.0)];
        dayOfWeekLabel.textAlignment = NSTextAlignmentCenter;
        dayOfWeekLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
        dayOfWeekLabel.textColor = [UIColor blackColor];
        dayOfWeekLabel.backgroundColor = [UIColor clearColor];
        [daysOfTheWeekLabels addObject:dayOfWeekLabel];
        [daysOfTheWeekWrapper addSubview:dayOfWeekLabel];
        lastDayFrame = dayOfWeekLabel.frame;
    }
    
    [self setDaysOfTheWeek];
    
    lineSeparatorView1 = [[UIView alloc] initWithFrame:CGRectMake(0.0, daysOfTheWeekWrapper.frame.origin.y+35.0, screenSize.width, 1.0)];
    lineSeparatorView1.backgroundColor = [UIColor colorFromHexString:@"cacaca"];
    
    
    //dates
    datesWrapper = [[UIView alloc] initWithFrame:CGRectMake(0.0, lineSeparatorView1.frame.origin.y+1.0, screenSize.width, 240.0)];
    datesWrapper.backgroundColor =  [UIColor whiteColor];
    
    dateButtons = [NSMutableArray array];
    for (int i = 1; i <= 42; i++) {
        DateButton *dateButton = [DateButton new];
        dateButton.calendar = self.calendar;
        [dateButtons addObject:dateButton];
    }
    
    lineSeparatorView2 = [[UIView alloc] initWithFrame:CGRectMake(0.0, datesWrapper.frame.origin.y+230.0, screenSize.width, 1.0)];
    lineSeparatorView2.backgroundColor = [UIColor colorFromHexString:@"cacaca"];
    
    calendarHistory = [[UIView alloc] initWithFrame:CGRectMake(0.0, lineSeparatorView2.frame.origin.y+1, screenSize.width, 60.0)];
    calendarHistory.backgroundColor = [UIColor whiteColor];
    
    UIView *activeDays = [[UIView alloc] initWithFrame:CGRectMake(20.0, 13.0, 33.0, 33.0)];
    activeDays.layer.cornerRadius = activeDays.frame.size.width/2;
    activeDays.layer.borderWidth = 1.0;
    activeDays.layer.borderColor = [UIColor colorFromHexString:@"bac951"].CGColor;
    activeDays.clipsToBounds = YES;
    
    UILabel *activeDaysLabel = [[UILabel alloc] initWithFrame:CGRectMake(activeDays.frame.origin.x+33+8.0,13.0, 90.0, 33)];
    activeDaysLabel.textAlignment = NSTextAlignmentLeft;
    activeDaysLabel.backgroundColor = [UIColor clearColor];
    activeDaysLabel.textColor = [UIColor lightGrayColor];
    activeDaysLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0];
    activeDaysLabel.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    activeDaysLabel.text = @"Días llenos";
    
    UIView *lostDays = [[UIView alloc] initWithFrame:CGRectMake(163.0, 13.0, 33.0, 33.0)];
    lostDays.layer.cornerRadius = lostDays.frame.size.width/2;
    lostDays.layer.borderWidth = 1.0;
    lostDays.layer.borderColor = [UIColor lightGrayColor].CGColor;
    lostDays.clipsToBounds = YES;
    
    UILabel *lostDaysLabel = [[UILabel alloc] initWithFrame:CGRectMake(lostDays.frame.origin.x+33.0+8.0,13.0, 100.0, 33.0)];
    lostDaysLabel.textAlignment = NSTextAlignmentLeft;
    lostDaysLabel.backgroundColor = [UIColor clearColor];
    lostDaysLabel.textColor = [UIColor lightGrayColor];
    lostDaysLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0];
    lostDaysLabel.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    lostDaysLabel.text = @"Días perdidos";
    
    [calendarHistory addSubview:activeDays];
    [calendarHistory addSubview:activeDaysLabel];
    [calendarHistory addSubview:lostDays];
    [calendarHistory addSubview:lostDaysLabel];
    
    [calendarView addSubview:calendarHeader];
    [calendarView addSubview:daysOfTheWeekWrapper];
    
    [calendarView addSubview:lineSeparatorView1];
    
    [calendarView addSubview:datesWrapper];
    
    [calendarView addSubview:lineSeparatorView2];
    
    [calendarView addSubview:calendarHistory];
    
    [self addSubview:calendarView];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"MMMM, yyyy"];
    
    currentDayLabel.text = [dateFormatter stringFromDate:self.dateShowing];
    [currentDayLabel setNeedsDisplay];

}

- (void)setDaysOfTheWeek
{
    NSString *langID = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSLocale *locale;
    
    if([langID isEqualToString:@"pt"])
        locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt"];
    else
        locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
    
    [self.dateFormatter setLocale:locale];
    NSArray *weekdays = [self.dateFormatter shortWeekdaySymbols];
    
    NSInteger firstWeekdayIndex = [self.calendar firstWeekday] - 1;
    if (firstWeekdayIndex > 0) {
        weekdays = [[weekdays subarrayWithRange:NSMakeRange(firstWeekdayIndex, 7 - firstWeekdayIndex)] arrayByAddingObjectsFromArray:[weekdays subarrayWithRange:NSMakeRange(0, firstWeekdayIndex)]];
    }
    
    int i = 0;
    for (NSString *day in weekdays)
    {
        [daysOfTheWeekLabels[i] setText:[day uppercaseString]];
        i++;
    }
}

- (void)setDefaultCellDateSizes
{
    self.defaultCellDateButtonWidth = 35;
    self.defaultCellDateButtonHeight = 35;
    self.defaultDateCellXSpacing = 8.0;
    self.defaultDateCellYSpacing = 8.0;
    self.cellWrapperYSpacing = 13.0;
    self.cellWrapperXSpacing = 13.0;
}
- (void)updateCurrentDayLabel
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSString *langID = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSLocale *locale;
    
    if([langID isEqualToString:@"pt"])
        locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt"];
    else
        locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
    [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"MMMM, yyyy"];
    
    currentDayLabel.text = [dateFormatter stringFromDate:self.dateShowing];
    [currentDayLabel setNeedsDisplay];
    
}


#pragma mark - Buttons Delegates

- (void)moveCalendarToPreviousMonth
{
    self.dateShowing = [self _previousMonth:self.dateShowing];
}

- (void)moveCalendarToNextMonth
{
    self.dateShowing = [self _nextMonth:self.dateShowing];
}

- (void)moveCalendarToPreviousDay
{
    self.dateShowing = [self _previousDay:self.dateShowing];
    [self updateCurrentDayLabel];
}

- (void)moveCalendarToNextDay
{
    NSDate *newDate = [self _nextDay:self.dateShowing];
    
    if(![self date:newDate isDayAfterDate:[NSDate new]])
    {
        self.dateShowing = newDate;
        [self updateCurrentDayLabel];
    }
}

- (void)dateButtonPressed:(DateButton *)sender
{
    DateButton *dateButton = sender;
    NSDate *date = dateButton.date;
    
    if(![self date:date isDayAfterDate:[NSDate new]])
    {
        self.dateShowing = date;
        [self updateCurrentDayLabel];
    }
    else
        NSLog(@"Selected date is greater than current one");
}

#pragma mark - Helper Functions

- (NSDate *)_firstDayOfMonthContainingDate:(NSDate *)date {
    NSDateComponents *comps = [self.calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    comps.day = 1;
    return [self.calendar dateFromComponents:comps];
}

- (NSDate *)_firstDayOfNextMonthContainingDate:(NSDate *)date {
    NSDateComponents *comps = [self.calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    comps.day = 1;
    comps.month = comps.month + 1;
    return [self.calendar dateFromComponents:comps];
}

- (BOOL)inCurrentMonth:(NSDate *)date
{
    if(date == nil)
        return NO;
    
    NSDateComponents *currentDate = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:[NSDate new]];
    NSDateComponents *dateRef = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:date];
    
    if(currentDate.year != dateRef.year)
        return NO;
    
    if(currentDate.year == dateRef.year && currentDate.month == dateRef.month)
        return YES;
    
    return NO;
}

- (BOOL)dateIsInCurrentMonth:(NSDate *)date {
    return ([self _compareByMonth:date toDate:self.dateShowing] == NSOrderedSame);
}

- (NSComparisonResult)_compareByMonth:(NSDate *)date toDate:(NSDate *)otherDate {
    NSDateComponents *day = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:date];
    NSDateComponents *day2 = [self.calendar components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:otherDate];
    
    if (day.year < day2.year) {
        return NSOrderedAscending;
    } else if (day.year > day2.year) {
        return NSOrderedDescending;
    } else if (day.month < day2.month) {
        return NSOrderedAscending;
    } else if (day.month > day2.month) {
        return NSOrderedDescending;
    } else {
        return NSOrderedSame;
    }
}

- (NSInteger)_placeInWeekForDate:(NSDate *)date {
    NSDateComponents *compsFirstDayInMonth = [self.calendar components:NSWeekdayCalendarUnit fromDate:date];
    return (compsFirstDayInMonth.weekday - 1 - self.calendar.firstWeekday + 8) % 7;
}

- (BOOL)_dateIsToday:(NSDate *)date {
    return [self date:self.dateShowing isSameDayAsDate:date];
}

- (BOOL)date:(NSDate *)date1 isSameDayAsDate:(NSDate *)date2 {
    if (date1 == nil || date2 == nil) {
        return NO;
    }
    
    NSDateComponents *day = [self.calendar components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date1];
    NSDateComponents *day2 = [self.calendar components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date2];
    return ([day2 day] == [day day] &&
            [day2 month] == [day month] &&
            [day2 year] == [day year] &&
            [day2 era] == [day era]);
}

- (BOOL)date:(NSDate *)date1 isDayAfterDate:(NSDate *)date2 {
    if (date1 == nil || date2 == nil) {
        return NO;
    }
    
    NSDateComponents *day1 = [self.calendar components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date1];
    NSDateComponents *day2 = [self.calendar components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date2];
    
    if([day1 year] > [day2 year])
        return YES;
    
    if([day1 month] > [day2 month] && [day1 year] == [day2 year])
        return YES;
    
    if([day1 year] == [day2 year] && [day1 month] == [day2 month] && [day1 day] > [day2 day])
        return YES;
    
    return NO;
}

- (NSInteger)_numberOfWeeksInMonthContainingDate:(NSDate *)date {
    return [self.calendar rangeOfUnit:NSWeekCalendarUnit inUnit:NSMonthCalendarUnit forDate:date].length;
}

- (NSDate *)_nextDay:(NSDate *)date {
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:1];
    
    return [self.calendar dateByAddingComponents:comps toDate:date options:0];
}

- (NSDate *)_nextMonth:(NSDate *)date {
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setMonth:1];
    return [self.calendar dateByAddingComponents:comps toDate:date options:0];
}

- (NSDate *)_previousDay:(NSDate *)date {
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:-1];
    return [self.calendar dateByAddingComponents:comps toDate:date options:0];
}

- (NSDate *)_previousMonth:(NSDate *)date {
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setMonth:-1];
    return [self.calendar dateByAddingComponents:comps toDate:date options:0];
}

- (NSInteger)_numberOfDaysFromDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
    NSInteger startDay = [self.calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:startDate];
    NSInteger endDay = [self.calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:endDate];
    
    return endDay - startDay;
}

- (CGRect)_calculateDayCellFrame:(NSDate *)date currentDateCell:(int)dateCell
{
    NSInteger row = dateCell;
	
    NSInteger placeInWeek = [self _placeInWeekForDate:date];
    
    return CGRectMake(placeInWeek * (self.defaultCellDateButtonWidth+self.defaultDateCellXSpacing) + self.cellWrapperXSpacing ,(row * (self.defaultCellDateButtonHeight+self.defaultDateCellYSpacing)) + self.defaultDateCellYSpacing, self.defaultCellDateButtonWidth, self.defaultCellDateButtonHeight);
}


@end
