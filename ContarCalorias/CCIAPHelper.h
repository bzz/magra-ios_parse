//
//  CCIAPHelper.h
//  ContarCalorias
//
//  Created by andres portillo on 08/09/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "IAPHelper.h"

@interface CCIAPHelper : IAPHelper
+ (CCIAPHelper *)sharedInstance;
@end
