//
//  CCTaskProgressView.h
//  ContarCalorias
//
//  Created by andres portillo on 25/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCTaskProgressView : UIProgressView
@property (nonatomic) CGFloat userDefinedHeight;
@property(nonatomic,strong) UIColor *progressColor;
@end
