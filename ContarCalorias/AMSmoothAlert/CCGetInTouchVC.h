//
//  CCGetInTouchVC.h
//  ContarCalorias
//
//  Created by andres portillo on 24/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GCPlaceholderTextView;
@interface CCGetInTouchVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet GCPlaceholderTextView *txtMessage;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblContactUs;
@property (weak, nonatomic) IBOutlet UILabel *lblContactUsDesc;
@end
