//
//  CCGetInTouchVC.m
//  ContarCalorias
//
//  Created by andres portillo on 24/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCGetInTouchVC.h"
#import "CCAPI.h"
#import "GCPlaceholderTextView.h"
#import "User.h"
#import "FontAwesomeKit.h"

@interface CCGetInTouchVC ()<UITextViewDelegate, UITextFieldDelegate>

@end

@implementation CCGetInTouchVC{
    User *_user;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *imgTitleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"getintouch-title-img"]];
    self.navigationItem.titleView = imgTitleView;
    [self.navigationController.navigationBar setTintColor:CCColorGreen];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self configureNavigationItem];
    });
    _txtMessage.placeholder = [TSLanguageManager localizedString:@"Type your message"];
    // Do any additional setup after loading the view.
    
    [_lblContactUs setText:[TSLanguageManager localizedString:@"Contact us"]];
    [_lblContactUsDesc setText:[TSLanguageManager localizedString:@"Either you want to"]];
        //fill out default values
    _user = [[CCAPI sharedInstance] getUser];
    [_txtName setText:[NSString stringWithFormat:@"%@",_user.username]];
    [_txtName setDelegate:self];
    [_txtEmail setText:[NSString stringWithFormat:@"%@",_user.email]];
    [_txtEmail setDelegate:self];
    UIToolbar *inputAccessory = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 40)];
    UIBarButtonItem  *spacer = [[UIBarButtonItem alloc]    initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [inputAccessory setBarStyle:UIBarStyleDefault];
    [inputAccessory setTintColor:[UIColor whiteColor]];
    [inputAccessory setBarTintColor:CCColorGreen];
    
    FAKFontAwesome *checkIcon = [FAKFontAwesome checkIconWithSize:28];
    [checkIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    [inputAccessory setItems:[NSArray arrayWithObjects:spacer,
                              [[UIBarButtonItem alloc] initWithImage:[checkIcon imageWithSize:CGSizeMake(25, 25)]   style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)],nil ] animated:YES];
    [_txtMessage setInputAccessoryView:inputAccessory];
    [_txtEmail setInputAccessoryView:inputAccessory];
    [_txtName setInputAccessoryView:inputAccessory];
}
- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}
- (void)configureNavigationItem
{
    UIBarButtonItem *btnSend = [[UIBarButtonItem alloc] initWithTitle:[TSLanguageManager localizedString:@"Send"] style:UIBarButtonItemStyleBordered target:self action:@selector(onSendButtonTapped)];
    self.navigationItem.rightBarButtonItem = btnSend;
    
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithTitle:[TSLanguageManager localizedString:@"Cancel"] style:UIBarButtonItemStyleBordered target:self action:@selector(onCancelButtonTapped)];
    self.navigationItem.leftBarButtonItem = btnCancel;
}
- (void)onCancelButtonTapped
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)onSendButtonTapped
{
    if ([[_txtName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0 && [[_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0 && [[_txtMessage.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
        
        NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
        NSString *build = infoDictionary[(NSString*)kCFBundleVersionKey];
        
        NSString *subjectEs = [NSString stringWithFormat:@"Magra para iPhone v%@ - Comentarios",build];
        NSString *subjectPt = [NSString stringWithFormat:@"Resposta - Magra for iOS v%@",build];
        
        NSString *subject = [[TSLanguageManager selectedLanguage] isEqualToString:@"es"]?subjectEs: subjectPt;
        NSDictionary *params = @{@"name":_txtName.text,@"email":_txtEmail.text,@"subject":subject,@"description":_txtMessage.text};
        [[CCAPI sharedInstance] sendEmailWithParameters:params Success:^(id responseObject) {
            
            [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
                UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:[TSLanguageManager localizedString:@"The message was successfully sent"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }];
            
        } failure:^(NSError *error) {
            
            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:[TSLanguageManager localizedString:@"There was an error sending the email, please try again later"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }];

        
    } else {
       
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:[TSLanguageManager localizedString:@"Please enter all fields"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UItextview Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [_scrollView setContentOffset:CGPointMake(0, 220) animated:YES];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    [_scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - UItextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([[UIScreen mainScreen] bounds].size.height <= 480) {
        if (textField == _txtEmail) {
            [_scrollView setContentOffset:CGPointMake(0, 70) animated:YES];
        } else if(textField == _txtName) {
            [_scrollView setContentOffset:CGPointMake(0, 20) animated:YES];
        }
        

    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [_scrollView setContentOffset:CGPointZero animated:YES];
}
@end
