//
//  AddMealViewController.m
//  ContarCalorias
//
//  Created by Adrian Ghitun on 11/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//
#import "AppDelegate.h"
#import "CCSearchDiaryItemVC.h"
#import "Food.h"
#import "FoodServing.h"
#import "Exercise.h"
#import <QuartzCore/QuartzCore.h>
#import "DiaryItemInfo.h"
#import "ViewUtils.h"
#import "CCAPI.h"
#import "ContarCalendar.h"
#import "DiaryItemDetailsViewController.h"
#import "UIAlertView+Blocks.h"
#import "AddExerciseViewController.h"
#import "helper.h"
#import "CCUtils.h"

@interface CCSearchDiaryItemVC ()
{
    
}

@end

@implementation CCSearchDiaryItemVC
{
    NSString *_searchText;
    NSString *_currentDiaryItemName;
    bool _searchMode, _noItemsFound;
    int _originalTableViewHeight;
    NSArray *_arrFoodServing;
}

- (NSString *)diaryItemName
{
    if (_currentDiaryItem == CCDiaryItemTypeMeal) {
        
        if(_currentMealType == CCMealTypeBreakfast)
            return [TSLanguageManager localizedString:@"Breakfast"];
        else if(_currentMealType == CCMealTypeLunch)
            return [TSLanguageManager localizedString:@"Lunch"];
        else if(_currentMealType == CCMealTypeDinner)
            return [TSLanguageManager localizedString:@"Dinner"];
        else if(_currentMealType == CCMealTypeSnack)
            return [TSLanguageManager localizedString:@"Snack"];
    }
    return [TSLanguageManager localizedString:@"Exercise"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_txtSearch setDelegate:self];
    
    
    [self.view setBackgroundColor:[ViewUtils lightGrayColor]];
    [self.tableView setBackgroundColor:[ViewUtils lightGrayColor]];
    [self.txtSearch setBackgroundColor:[ViewUtils lightGrayColor]];
    [_lbTitle setText:[[TSLanguageManager localizedString:@"Search results"] uppercaseString]];
    [self.navigationItem setTitle:[self diaryItemName]];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _searchMode = NO;
    _noItemsFound = NO;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.numberOfTapsRequired = 1;
    //[self.view addGestureRecognizer:tap];
    _searchResults = [NSMutableArray new];
    _diaryItems =  [NSMutableArray new];
    
    if(self.currentDiaryItem == CCDiaryItemTypeExercise)
    {
        CGRect tableViewFrame = self.tableView.frame;
        tableViewFrame.size.height = self.view.frame.size.height - (45+30);
        self.tableView.frame = tableViewFrame;
    }
    
    [self.navigationController.navigationBar setTintColor:[ViewUtils greenColor]];
    
    if(self.currentDiaryItem == CCDiaryItemTypeExercise)
        [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Search for exercises"]];
    else
        [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Search for meals"]];
    [self setLocalizedStrings];
}
- (void)setLocalizedStrings
{
    [_txtSearch setPlaceholder:[TSLanguageManager localizedString:@"Search"]];
    [_btnCancel setTitle:[TSLanguageManager localizedString:@"Cancel"] forState:UIControlStateNormal];
}
- (void)viewDidAppear:(BOOL)animated{
    _originalTableViewHeight = self.tableView.frame.size.height;
}
#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    [self switchViewToSearchMode];
    
    NSString *lang = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    BOOL filterCurrentResultsArray = NO;
    if( ! [string isEqualToString:@""]) {
        
        filterCurrentResultsArray = YES;
        _searchText = [[textField.text stringByAppendingString:string] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (! ([_searchText rangeOfString:@"  "].location == NSNotFound)) {
            _searchText = [CCUtils cleanWhiteSpacesForString:_searchText];
            CFStringTransform ((CFMutableStringRef)_searchText, NULL, kCFStringTransformStripCombiningMarks, FALSE);
            _searchText = [_searchText lowercaseString];
        }
        
    } else if (textField.text.length >= 1) {
        _searchText = [[textField.text substringToIndex:textField.text.length - 1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (! ([_searchText rangeOfString:@"  "].location == NSNotFound)) {
            _searchText = [CCUtils cleanWhiteSpacesForString:_searchText];
            CFStringTransform ((CFMutableStringRef)_searchText, NULL, kCFStringTransformStripCombiningMarks, FALSE);
            _searchText = [_searchText lowercaseString];
        }
    }
    
    if(self.currentDiaryItem < CCDiaryItemTypeExercise) {
        
        if (filterCurrentResultsArray && _searchText.length > 3) {
            NSArray *arrSearchElements = [_searchText componentsSeparatedByString:@" "];
            NSMutableArray *subpredicates = [NSMutableArray array];
            for (NSString *filterKey in arrSearchElements) {
                NSPredicate *p = [NSPredicate predicateWithFormat:@"normalizedTitle contains %@",filterKey];
                [subpredicates addObject:p];
            }
            NSPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:subpredicates];
            _searchResults = [[_searchResults filteredArrayUsingPredicate:final] mutableCopy];
            
        } else {
            
            NSArray *arrSearchElements = [_searchText componentsSeparatedByString:@" "];
            NSMutableArray *subpredicates = [NSMutableArray array];
            for (NSString *filterKey in arrSearchElements) {
                NSPredicate *p = [NSPredicate predicateWithFormat:@"normalizedTitle contains %@ AND locale = %@ AND isSearchable = %@",filterKey,lang,@YES];
                [subpredicates addObject:p];
            }
            NSPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:subpredicates];
            _searchResults = [[[CCAPI sharedInstance] getAllFoodsWithPredicate:final] mutableCopy];
            
        }
        
        
    } else {
        
        NSArray *arrSearchElements = [_searchText componentsSeparatedByString:@" "];
        NSMutableArray *subpredicates = [NSMutableArray array];
        for (NSString *filterKey in arrSearchElements) {
            NSPredicate *p = [NSPredicate predicateWithFormat:@"name contains[cd] %@",filterKey];
            [subpredicates addObject:p];
        }
        NSPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:subpredicates];
        
        NSMutableArray *arrFoundExercises = [[[CCAPI sharedInstance] getAllExercisesWithPredicate:final] mutableCopy];
        _searchResults = [arrFoundExercises mutableCopy];
        
    }
    
    if (_searchResults.count == 0 && self.currentDiaryItem < CCDiaryItemTypeExercise) {
        [self showAddFoodButton];
        [_lbTitle setText:[[TSLanguageManager localizedString:@"No results found"] uppercaseString]];
    } else {
        _noItemsFound = NO;
        [_lbTitle setText:[[TSLanguageManager localizedString:@"Search results"] uppercaseString]];
    }
    [self.tableView reloadData];
    
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (_txtSearch.text.length == 0) {
        [self switchViewToDefaultMode];
    }
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Class methods
- (void)onSubmitFoodButtonTapped
{
    [self performSegueWithIdentifier:@"Submit Food Segue" sender:nil];
}
- (void)showAddFoodButton
{
    _noItemsFound = YES;
    [self.tableView reloadData];
}
- (void)switchViewToSearchMode
{
    CGRect frame = _txtSearch.frame;
    frame.size.width = 204;
    _txtSearch.frame = frame;
    _searchMode = YES;
    [_tableView reloadData];
}

- (void)switchViewToDefaultMode
{
    CGRect frame = _txtSearch.frame;
    frame.size.width = 295;
    _txtSearch.frame = frame;
    _searchMode = NO;
    [_searchResults removeAllObjects];
    [_tableView reloadData];
}
- (IBAction)cancelSearch:(id)sender
{
    _txtSearch.text = 0;
    [self switchViewToDefaultMode];
}
- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}
#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (! _noItemsFound) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Food Cell"];
        
        if (cell == nil){
            cell = [[UITableViewCell alloc] init];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (_searchMode)
        {
            if(self.currentDiaryItem == CCDiaryItemTypeExercise) {
                
                Exercise *exercise = [_searchResults objectAtIndex:indexPath.row];
                NSRange range = [exercise.name rangeOfString:_searchText options:NSCaseInsensitiveSearch];
                NSMutableAttributedString *attrTitle = [[NSMutableAttributedString alloc] initWithString:exercise.name];
                [attrTitle addAttribute:NSForegroundColorAttributeName
                                  value:CCColorOrange
                                  range:range];
                ((UILabel *)[cell viewWithTag:1]).attributedText = attrTitle;
            }
            else {
                
                NSDictionary *food= [_searchResults objectAtIndex:indexPath.row];
                NSRange range = [food[@"title"] rangeOfString:_searchText options:NSCaseInsensitiveSearch];
                
                NSMutableAttributedString *attrTitle = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",food[@"title"],[food[@"brand"] length] > 0? [NSString stringWithFormat:@" - Marca: %@",food[@"brand"]]:@""]];
                [attrTitle addAttribute:NSForegroundColorAttributeName
                                  value:CCColorOrange
                                  range:range];
                ((UILabel *)[cell viewWithTag:1]).attributedText = attrTitle;
            }
        }
        else
        {
            if(self.currentDiaryItem == CCDiaryItemTypeExercise) {
                
                Exercise *exercise = [_searchResults objectAtIndex:indexPath.row];
                ((UILabel *)[cell viewWithTag:1]).text = exercise.name;
                
            }
            else {
                
                Food *food= [_searchResults objectAtIndex:indexPath.row];
                ((UILabel *)[cell viewWithTag:1]).text = food.title;
                
            }
            
        }
        
        return cell;
        
    } else {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"No Items Cell"];
        
        if (cell == nil){
            cell = [[UITableViewCell alloc] init];
        }
        [cell setBackgroundView:[UIView new]];
        [(UIButton *)[cell.contentView viewWithTag:1] setTitle:[TSLanguageManager localizedString:@"Create food"] forState:UIControlStateNormal];
        [(UIButton *)[cell.contentView viewWithTag:1] addTarget:self action:@selector(onSubmitFoodButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        return  cell;
    }
    
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (_searchMode) {
        
        if (_noItemsFound) {
            
            return 1;
        }
        return MIN(40,_searchResults.count);
    }
    return _diaryItems.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_searchMode && ! _noItemsFound) {
        
        if(self.currentDiaryItem < CCDiaryItemTypeExercise) {
            
            NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            NSDictionary *foodItem = _searchResults[indexPath.row];
            _arrFoodServing = [FoodServing MR_findByAttribute:@"food_id" withValue:foodItem[@"food_id"]];
            
            if (_arrFoodServing.count > 0) {
                [self performSegueWithIdentifier:@"Item Detail Segue" sender:nil];
            } else {
                [UIAlertView showWithTitle:[TSLanguageManager localizedString:@"Error"] message:[TSLanguageManager localizedString:@"We weren't able to find serving types for this food"] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    
                }];
            }
            
        } else
        {
            [self performSegueWithIdentifier:@"Exercise Detail Segue" sender:nil];
        }
        
        self.txtSearch.text = @"";
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_noItemsFound)
        return 160.0;
    
    return 46.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0;
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"Item Detail Segue"]) {
        
        DiaryItemDetailsViewController *diaryItemDetailsVC = segue.destinationViewController;
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDictionary *foodItem = _searchResults[indexPath.row];
        diaryItemDetailsVC.isOnEditMode = NO;
        diaryItemDetailsVC.currentMealType = _currentMealType;
        diaryItemDetailsVC.currentDiaryItemType = _currentDiaryItem;
        
        Food *food = [Food MR_findFirstByAttribute:@"food_id" withValue:foodItem[@"food_id"]];
        diaryItemDetailsVC.food = food;
        diaryItemDetailsVC.arrFoodServings = _arrFoodServing;
        
    } else if ([segue.identifier isEqualToString:@"Exercise Detail Segue"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        AddExerciseViewController *addExerciseVC = segue.destinationViewController;
        Exercise *exercise = _searchResults[indexPath.row];
        addExerciseVC.exercise= exercise;
        
    }
}
@end