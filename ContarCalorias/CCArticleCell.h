//
//  CCArticleCell.h
//  ContarCalorias
//
//  Created by andres portillo on 23/09/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CCArticleCellDelegate
- (void)didTapOnCellWithIndex:(NSInteger)index;
@end

@interface CCArticleCell : UITableViewCell
@property(nonatomic, weak) id<CCArticleCellDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgPostImage;
@property (weak, nonatomic) IBOutlet UILabel *lblPostTitle;
@property (nonatomic) NSInteger index;
- (void)setArticleImage:(UIImage *)image;

@end
