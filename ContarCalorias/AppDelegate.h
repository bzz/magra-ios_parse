//  AppDelegate.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 27/12/13.
//  Copyright (c) 2013 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@class SWRevealViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *viewController;
- (void)sessionStateChanged:(FBSession *)session State:(FBSessionState)state error:(NSError *)error;
- (void)updateMainScreen;
@end
