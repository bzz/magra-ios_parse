//
//  CCWeightLossPlanFIrstVC.h
//  ContarCalorias
//
//  Created by andres portillo on 12/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//
@class User;
#import <UIKit/UIKit.h>

@interface CCSetUserDetailsTVC: UITableViewController
@property (weak, nonatomic) IBOutlet UITableViewCell *cellGender;

- (IBAction)onMaleButtonTapped:(id)sender;
- (IBAction)onFemaleButtonTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *txtWeight;
@property (weak, nonatomic) IBOutlet UITextField *txtBirthdate;
@property (weak, nonatomic) IBOutlet UITextField *txtHeight;

@property (weak, nonatomic) IBOutlet UIButton *switchButtonMale;
@property (weak, nonatomic) IBOutlet UIButton *switchButtonFemale;

@property (weak, nonatomic) IBOutlet UILabel *lblMale;
@property (weak, nonatomic) IBOutlet UILabel *lblFemale;
@property (weak, nonatomic) IBOutlet UILabel *lblHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblMyHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblMyWeight;

@property (weak, nonatomic) IBOutlet UILabel *lblIm;
@property (weak, nonatomic) IBOutlet UILabel *lblMyBirthdate;


@end
