//
//  setWeightLossPlanResultsVC.h
//  ContarCalorias
//
//  Created by andres portillo on 21/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class User;
@interface CCSetWeightLossPlanResultsVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblDailyCarbs;
@property (weak, nonatomic) IBOutlet UILabel *lblDailyFats;
@property (weak, nonatomic) IBOutlet UILabel *lblDailyProteins;
@property (weak, nonatomic) IBOutlet UILabel *lblDailyCalories;
@property (weak, nonatomic) IBOutlet UILabel *lblWeightDiffPerWeek;
@property (weak, nonatomic) IBOutlet UILabel *lblGoalWeightDate;
@property (weak, nonatomic) IBOutlet UILabel *lblCongrats;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblCaloriesBudget;
@property (weak, nonatomic) IBOutlet UILabel *lblWeeklyWeightLoss;
@property (weak, nonatomic) IBOutlet UILabel *lblYouShouldLose;
@property (weak, nonatomic) IBOutlet UILabel *lblRecommendedConsume;
@property (weak, nonatomic) IBOutlet UILabel *lblProteins;
@property (weak, nonatomic) IBOutlet UILabel *lblFats;
@property (weak, nonatomic) IBOutlet UILabel *lblCarbs;

@property (strong, nonatomic) User *user;
@end
