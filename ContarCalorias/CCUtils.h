//
//  CCUtils.h
//  ContarCalorias
//
//  Created by andres portillo on 30/05/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>
@class User;

@interface CCUtils : NSObject
+ (BOOL)emailIsValid:(NSString *)checkString;
+ (NSString *)stringFromTimestamp:(NSDate *)date;
+ (NSDate *)timestampFromString:(NSString *)date;
+ (NSDate *)dateFromString:(NSString *)date;
+ (NSString *)stringFromDate:(NSDate *)date;
+ (NSString *)stringFromBirthdate:(NSDate *)date;
+ (BOOL)datesHaveSameDay:(NSDate *)firstDate SecondDate:(NSDate*)secondDate;
+ (CGFloat)getHeightForText:(NSString *)text Font:(UIFont *)font andWidth:(CGFloat)width;
+ (NSInteger)numberOfDaysWithinFirstDate:(NSDate *)firstDate andSecondDate:(NSDate *)secondDate;
+ (NSInteger)ageForUserWithBirthdate:(NSDate *)birthdate;
+ (NSDate *)getDefaultWakeupTime;
+ (NSString *)cleanWhiteSpacesForString:(NSString *)string;

@end
