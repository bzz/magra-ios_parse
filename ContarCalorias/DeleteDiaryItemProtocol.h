//
//  DeleteMealProtocol.h
//  ContarCalorias
//
//  Created by Lucian Gherghel on 28/02/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DeleteDiaryItemProtocol <NSObject>

- (void)diaryItemHasBeenDeleted:(NSInteger)diaryItemIndex;

@end
