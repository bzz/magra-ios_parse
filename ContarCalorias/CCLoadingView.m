//
//  CCLoadingView.m
//  ContarCalorias
//
//  Created by andres portillo on 10/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCLoadingView.h"
#import "FontAwesomeKit.h" 
@implementation CCLoadingView
{
   
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)awakeFromNib
{
    FAKFontAwesome *reloadIcon = [FAKFontAwesome refreshIconWithSize:60];
    [reloadIcon  addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
    [_btnReload setImage:[reloadIcon imageWithSize:CGSizeMake(60, 60)] forState:UIControlStateNormal];
    [_btnReload addTarget:self action:@selector(onRefreshButtonTapped) forControlEvents:UIControlEventTouchUpInside];
}
- (void)setLoadingFailedMode
{
    [_btnReload setAlpha:1];
    [_lblErrorLoading setAlpha:1];
    [_activityIndicator setAlpha:0];
    [_lblLoading setAlpha:0];
}
- (void)startLoadingMode
{
    [_btnReload setAlpha:0];
    [_lblErrorLoading setAlpha:0];
    [_activityIndicator setAlpha:1];
    [_lblLoading setAlpha:1];
}
- (void)onRefreshButtonTapped
{
    [self startLoadingMode];
    [self.delegate theRefreshButtonWasTappedOnLoadingView:self];
}
@end
