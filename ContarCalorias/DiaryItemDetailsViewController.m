    //
//  FoodDetailsViewController.m
//  ContarCalorias
//
//  Created by Adrian Ghitun on 12/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "DiaryItemDetailsViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "ViewUtils.h"
#import "CCAPI.h"
#import "ContarCalendar.h"
#import "User.h"
#import "PickerData.h"
#import "NutrientItem.h"
#import "NutrientCell.h"
#import "UIAlertView+Blocks.h"
#import "helper.h"
#import "FoodServing.h"
#import "Food.h"
#import "UserFood.h"
#import "FontAwesomeKit.h"

@interface DiaryItemDetailsViewController ()<UIActionSheetDelegate>
{
    NSMutableArray *servingUnits;
    NSString *selectedServingUnit;
    UIPickerView *_pickerPortions;
    
    NSMutableArray *availableQuantities;
    NSString *selectedQuantity;
    NSString *previousValue;
    
    NSMutableDictionary *nutrientsMapping;
    NSDictionary *globalNutrientsData;
    
    UIToolbar *_inputAccessory;
    BOOL _doSelection;
    UILabel *_lblCurrentField;
    
    UIView *_dimView;
 
    UIBarButtonItem *_checkButton;
    UIBarButtonItem *_cancelButton;
    UIBarButtonItem *_spacer;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewWrapper;

@end

@implementation DiaryItemDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:[ViewUtils lightGrayColor]];
    [self.scrollViewWrapper setContentSize:CGSizeMake(320, 463)];
    [_viewWrapperCalories.layer setCornerRadius:4];
    [ViewUtils drawBasicShadowOnView:_viewWrapperCalories];
    [_viewWrapperQuantity.layer setCornerRadius:4];
    [ViewUtils drawBasicShadowOnView:_viewWrapperQuantity];
    [_viewWrapperNutritional.layer setCornerRadius:4];
    [ViewUtils drawBasicShadowOnView:_viewWrapperNutritional];
    
    [ViewUtils fixSeparatorsHeightInView:_viewWrapperNutritional];
    [ViewUtils fixSeparatorsHeightInView:_viewWrapperCalories];
    [ViewUtils fixSeparatorsHeightInView:_viewWrapperQuantity];
    
    if(_isOnEditMode) {
            //edit mode
        UIBarButtonItem *saveBarButton = [[UIBarButtonItem alloc] initWithTitle:[TSLanguageManager localizedString:@"Save"] style:UIBarButtonItemStylePlain target:self action:@selector(onSaveButtonTapped)];
        
        UIBarButtonItem *deleteBarButton = [[UIBarButtonItem alloc] initWithTitle:[TSLanguageManager localizedString:@"Delete"] style:UIBarButtonItemStylePlain target:self action:@selector(onDeleteButtonTapped)];
        
        [self.navigationItem setRightBarButtonItems:@[saveBarButton, deleteBarButton]];
        
        
    } else {

        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[TSLanguageManager localizedString:@"Save"] style:UIBarButtonItemStylePlain target:self action:@selector(onSaveButtonTapped)];
    }
    
    servingUnits = [NSMutableArray new];
    _pickerPortions = [[UIPickerView alloc] init];
    _pickerPortions.delegate = self;
    _pickerPortions.dataSource = self;
    [_pickerPortions setBackgroundColor:[UIColor whiteColor]];
    availableQuantities = [NSMutableArray new];
    
    _txtServingType.inputView = _pickerPortions;
    
    self.nutrientsTableView.dataSource = self;
    self.nutrientsTableView.delegate = self;
    nutrientsMapping = [NSMutableDictionary new];
    
    self.nutrientsTableView.allowsSelection = NO;
    self.currentDiaryItemLbl.text = self.food.title;
    
    [self.lbTotalCalories setAdjustsFontSizeToFitWidth:YES];
    
    [self.lbCarbs setAdjustsFontSizeToFitWidth:YES];
    [self.lbFat setAdjustsFontSizeToFitWidth:YES];
    [self.lbProteins setAdjustsFontSizeToFitWidth:YES];
    [self.lbSugar setAdjustsFontSizeToFitWidth:YES];
    
    [self displayDiaryItemDetails];
    
    [self.navigationController.navigationBar setTintColor:[ViewUtils greenColor]];
    
  
    _txtNumberOfPortions.delegate = self;
    
    [_txtNumberOfPortions addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    
    
        //Dim view
    _dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 600)];
    [_dimView setBackgroundColor:[UIColor blackColor]];
    [_dimView setAlpha:0.35];
   
        //input accesory
    
    [self configureInputAccessory];
    
    [_txtServingType setInputAccessoryView:_inputAccessory];
    [_txtNumberOfPortions setInputAccessoryView:_inputAccessory];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}
- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Keyboard Notifications 
#pragma mark - Keyboard events
- (void)keyboardWillShow:(NSNotification *)notification
{
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    [self.view addSubview:_dimView];
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    [_dimView removeFromSuperview];
}
- (void)configureInputAccessory
{
    _lblCurrentField = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, 220, 22)];
    [_lblCurrentField setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16]];
    [_lblCurrentField setTextColor:[UIColor whiteColor]];
    [_lblCurrentField setTextAlignment:NSTextAlignmentCenter];
    
    _inputAccessory = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 40)];
    _spacer = [[UIBarButtonItem alloc]    initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [_inputAccessory addSubview:_lblCurrentField];
    
    [_inputAccessory setBarStyle:UIBarStyleDefault];
    [_inputAccessory setTintColor:[UIColor whiteColor]];
    [_inputAccessory setBarTintColor:CCColorGreen];
    
    FAKFontAwesome *checkIcon = [FAKFontAwesome checkIconWithSize:28];
    [checkIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    FAKFontAwesome *cancelIcon = [FAKFontAwesome closeIconWithSize:28];
    [cancelIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    _checkButton = [[UIBarButtonItem alloc] initWithImage:[checkIcon imageWithSize:CGSizeMake(25, 25)] style:UIBarButtonItemStyleDone target:self action:@selector(doSelection)];
    
    _cancelButton = [[UIBarButtonItem alloc] initWithImage:[cancelIcon imageWithSize:CGSizeMake(25, 25)] style:UIBarButtonItemStyleDone target:self action:@selector(cancelSelection)];
    
    
    [_inputAccessory setItems:[NSArray arrayWithObjects:_cancelButton, _spacer,_checkButton,nil] animated:YES];
}
- (void)doSelection
{
    _doSelection = YES;
    [self dismissKeyboard];
}
- (void)cancelSelection
{
    _doSelection = NO;
    [self dismissKeyboard];
}

#pragma mark - Class Methods
- (void)displayDiaryItemDetails
{
    for (FoodServing *foodServing in _arrFoodServings)
    {
        NSString *currentServing = [NSString stringWithFormat:@"%@", foodServing.serving_type];
    
        [servingUnits addObject:[[PickerData alloc] initWithInterfaceValue:currentServing andAPIValue:foodServing.serving_type]];
        
        NSArray *nutrientInfoArray =@[@{@"name":[TSLanguageManager localizedString:@"Calories"],@"quantity":foodServing.calories},@{@"name":[TSLanguageManager localizedString:@"Fats"],@"quantity":foodServing.fat},@{@"name":[TSLanguageManager localizedString:@"Proteins"],@"quantity":foodServing.protein},@{@"name":[TSLanguageManager localizedString:@"Saturated fat"],@"quantity":foodServing.saturated_fat},@{@"name":[TSLanguageManager localizedString:@"Sodium"],@"quantity":foodServing.sodium},@{@"name":[TSLanguageManager localizedString:@"Fiber"],@"quantity":foodServing.fiber},@{@"name":[TSLanguageManager localizedString:@"Carbohydrates"],@"quantity":foodServing.carbohydrates},@{@"name":[TSLanguageManager localizedString:@"Sugar"],@"quantity":foodServing.sugar}];
        NSMutableArray *currentServingNutrientsInfo = [NSMutableArray new];
        
        for (NSDictionary *nutrientInfo in nutrientInfoArray)
        {
            NutrientItem *nutrient = [NutrientItem new];
            nutrient.name = nutrientInfo[@"name"];
            nutrient.quantity = [NSString stringWithFormat:@"%@", nutrientInfo[@"quantity"]];
            
            [currentServingNutrientsInfo addObject:nutrient];
        }
        
        [nutrientsMapping setObject:currentServingNutrientsInfo forKey:currentServing];
    }

    if (_isOnEditMode) {
        
        __block NSInteger indexForCurrentServingType = 0;
        
        [servingUnits enumerateObjectsUsingBlock:^(PickerData *obj, NSUInteger idx, BOOL *stop) {
            
            if([obj.apiValue isEqualToString:_userFood.foodServing.serving_type]) {
                indexForCurrentServingType = idx;
            }

        }];
        selectedServingUnit = [[servingUnits objectAtIndex:indexForCurrentServingType] apiValue];
        _txtServingType.text = [[servingUnits objectAtIndex:indexForCurrentServingType] interfaceValue];
        [_txtNumberOfPortions setText:[NSString stringWithFormat:@"%@",_userFood.numberOfServings]];
        
    } else {
        selectedServingUnit = [[servingUnits firstObject] apiValue];
        _txtServingType.text = [[servingUnits firstObject] interfaceValue];
        _txtNumberOfPortions.text = @"1";
        
    }
    
    for (int i=1; i<=20; i++)
    {
        NSString *displayName = [NSString stringWithFormat:@"%i", i];
        [availableQuantities addObject:[[PickerData alloc] initWithInterfaceValue:displayName andAPIValue:displayName]];
    }
    
    selectedQuantity = [[availableQuantities objectAtIndex:0] apiValue];
    
    self.lbTotalCalories.text = @"0";
    self.lbCarbs.text = @"0";
    self.lbFat.text = @"0";
    self.lbProteins.text = @"0";
    self.lbSugar.text = @"0";
    
    [self updateNutrientInfos];
    
    [self.nutrientsTableView reloadData];

    [self setLocalizedStrings];
}
- (void)setLocalizedStrings
{
    [_lblCalories setText:[TSLanguageManager localizedString:@"Calories"]];
    [_lblCarbs setText:[TSLanguageManager localizedString:@"Carbs"]];
    [_lblFats setText:[TSLanguageManager localizedString:@"Fats"]];
    [_lblProtein setText:[TSLanguageManager localizedString:@"Proteins"]];
    [_lblSugar setText:[TSLanguageManager localizedString:@"Sugar"]];
    [_lblWeight setText:[NSString stringWithFormat:@"%@ %@",[TSLanguageManager localizedString:@"Amount"],@"(g)"]];
}
- (void)updateNutrientInfos
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"serving_type = %@",selectedServingUnit,[TSLanguageManager selectedLanguage]];
    FoodServing *foodServing = [[_arrFoodServings filteredArrayUsingPredicate:predicate] firstObject];
 
    double totalCalories = 0.0;
    double totalSugar = 0.0;
    double totalCarbs = 0.0;
    double totalProteins = 0.0;
    double totalFats = 0.0;
    
    if ([selectedServingUnit isEqualToString:@"g"] || [selectedServingUnit isEqualToString:@"ml"]) { //this check has to be done since the DB base was improperly adapted on serving types 100 g and 100 ml, so this is a workaround that should eventually stop being needed
        
        totalCalories = (float)foodServing.caloriesValue / 100;
        totalSugar = (float)foodServing.carbohydratesValue / 100;
        totalCarbs = (float)foodServing.carbohydratesValue / 100;
        totalProteins = (float)foodServing.proteinValue / 100;
        totalFats = (float)foodServing.fatValue / 100;
        
    } else {
        
        totalCalories = foodServing.caloriesValue;
        totalSugar = foodServing.carbohydratesValue;
        totalCarbs = foodServing.carbohydratesValue;
        totalProteins = foodServing.proteinValue;
        totalFats = foodServing.fatValue;
        
    }
    
    NSInteger value = 1;
    
    if(_txtNumberOfPortions.text.length > 0)
    {
        value = [_txtNumberOfPortions.text integerValue];
    }
    
    totalCalories *= value;
    
    totalSugar *= value;
    totalCarbs *= value;
    totalProteins *= value;
    totalFats *= value;
    
    self.lbTotalCalories.text = [NSString stringWithFormat:@"%.02f", totalCalories];
    self.lbCarbs.text = [NSString stringWithFormat:@"%.02f g", totalCarbs];
    self.lbFat.text = [NSString stringWithFormat:@"%.02f g", totalFats];
    self.lbProteins.text = [NSString stringWithFormat:@"%.02f g", totalProteins];
    self.lbSugar.text = [NSString stringWithFormat:@"%.02f g", totalSugar];
    
    [self.nutrientsTableView reloadData];
    
}
- (void)deleteDiaryItem
{
    [[CCAPI sharedInstance] deleteUserFood:_userFood];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark - UIActions

- (void)onSaveButtonTapped
{
    if (_isOnEditMode) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"serving_type = %@",selectedServingUnit];
        Food_serving *foodServing = [[_arrFoodServings filteredArrayUsingPredicate:predicate] firstObject];
        [[CCAPI sharedInstance] updateUserFoodItemWithFood:_userFood withFoodServing:foodServing numberOfServings:[_txtNumberOfPortions.text integerValue] completed:^{
        
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }];
    
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"serving_type = %@",selectedServingUnit];
        
        Food_serving *foodServing = [[_arrFoodServings filteredArrayUsingPredicate:predicate] firstObject];
        
        [[CCAPI sharedInstance] logFoodItemWithFood:_food foodServing:foodServing numberOfServings:[_txtNumberOfPortions.text integerValue] andMealType:_currentMealType];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCCNotificationMealWasLogged object:nil userInfo:nil];
    }
    
    
}
- (void)onDeleteButtonTapped
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[TSLanguageManager localizedString:@"Delete?"] delegate:self cancelButtonTitle:[TSLanguageManager localizedString:@"No"] destructiveButtonTitle:[TSLanguageManager localizedString:@"Yes"] otherButtonTitles:nil];
    [actionSheet showInView:self.view];
}

#pragma mark - UIPickerView delegates

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView == _pickerPortions)
        return servingUnits.count;
    
    return availableQuantities.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView == _pickerPortions)
        return [[servingUnits objectAtIndex:row] interfaceValue];
    
    return [[availableQuantities objectAtIndex:row] interfaceValue];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
}

#pragma mark - UITextField delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _txtNumberOfPortions)
    {
        [_inputAccessory setItems:[NSArray arrayWithObjects:_spacer,_checkButton,nil] animated:YES];
        _lblCurrentField.text = [TSLanguageManager localizedString:@"Portion amount"];
    } else if (textField == _txtServingType) {
         [_inputAccessory setItems:[NSArray arrayWithObjects: _cancelButton, _spacer,_checkButton,nil] animated:YES];
        _lblCurrentField.text = [TSLanguageManager localizedString:@"Portion size"];

    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _txtServingType && _doSelection) {
        selectedServingUnit = [[servingUnits objectAtIndex:[_pickerPortions selectedRowInComponent:0]] apiValue];
        _txtServingType.text = [[servingUnits objectAtIndex:[_pickerPortions selectedRowInComponent:0]] interfaceValue];
        [self.nutrientsTableView reloadData];
        [self updateNutrientInfos];
    }
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    previousValue = textField.text;
    textField.text = @"";
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if(textField.text.length == 0)
        textField.text = previousValue;
    
    return YES;
}
- (void)textFieldDidChange:(UITextField *)textfield
{
    [self updateNutrientInfos];
}


#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSArray *nutrientsInfoForServing = [nutrientsMapping objectForKey:_txtServingType.text];
    return nutrientsInfoForServing.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NutrientCell *cell = (NutrientCell *)[tableView dequeueReusableCellWithIdentifier:@"Nutrient Cell"];
    if(cell == nil)
        cell = [[NutrientCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Nutrient Cell"];
    
    NSArray *nutrientsInfoForServing = [nutrientsMapping objectForKey:_txtServingType.text];
    NutrientItem *nutrient = [nutrientsInfoForServing objectAtIndex:indexPath.row];
    
    cell.nutrientName.text = nutrient.name;
    if ([nutrient.name isEqualToString:@"Colesterol"] || [nutrient.name isEqualToString:@"Sodio"] || [nutrient.name isEqualToString:@"Sódio"]) {
        cell.quantity.text = [NSString stringWithFormat:@"%.02f mg", [nutrient.quantity doubleValue] * [_txtNumberOfPortions.text intValue]];
    } else {
        cell.quantity.text = [NSString stringWithFormat:@"%.02f g", [nutrient.quantity doubleValue] * [_txtNumberOfPortions.text intValue]];

    }
    cell.value.text = @"10%";
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36.0;
}

#pragma mark - UIActionSheet
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == actionSheet.destructiveButtonIndex) {
        [self deleteDiaryItem];
    }
}

- (IBAction)onServingTypeTapped:(id)sender {
    [_txtServingType becomeFirstResponder];
}

- (IBAction)onNumberOfServingsTapped:(id)sender {
    [_txtNumberOfPortions becomeFirstResponder];
}
@end
