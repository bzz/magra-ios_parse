//
//  IAPHelper.h
//  ContarCalorias
//
//  Created by andres portillo on 08/09/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//
// Add to the top of the file
#import <StoreKit/StoreKit.h>
#import <Foundation/Foundation.h>


UIKIT_EXTERN NSString *const IAPHelperProductPurchaseFailedNotification;

typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray * products);

@interface IAPHelper : NSObject

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;
- (void)restoreCompletedTransactions;
- (void)buyProduct:(SKProduct *)product;
- (BOOL)productPurchased:(NSString *)productIdentifier;
@end