//
//  CCSubmitFoodTVC.m
//  ContarCalorias
//
//  Created by andres portillo on 23/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCSubmitFoodTVC.h"
#import "FontAwesomeKit.h"
#import "CCAPI.h"
#import "UIAlertView+Blocks.h"

@interface CCSubmitFoodTVC ()<UITextFieldDelegate>

@end
typedef NS_ENUM (NSUInteger,CCSubmitFoodSection) {
    CCSubmitFoodSectionFoodInfo = 0,
    CCSubmitFoodSectionNutrients
};
typedef NS_ENUM (NSUInteger,CCFoodInfoSectionCell) {
    CCFoodInfoSectionCellFoodName = 0,
    CCFoodInfoSectionCellFoodBrand,
    CCFoodInfoSectionCellFoodBarCode,
    CCFoodInfoSectionCellFoodServingType
    };
typedef NS_ENUM (NSUInteger,CCNutrientsSectionCell) {
    CCNutrientsSectionCellCalories = 0,
    CCNutrientsSectionCellFats,
    CCNutrientsSectionCellSatFats,
    CCNutrientsSectionCellCholesterol,
    CCNutrientsSectionCellSodium
};
typedef NS_ENUM (NSUInteger,CCFoodInfoSectionCellSubview) {
    CCFoodInfoSectionCellSubviewTitleLabel = 1,
    CCFoodInfoSectionCellSubviewTextField
};
typedef NS_ENUM (NSUInteger,CCNutrientsSectionCellSubview) {
    CCNutrientsSectionCellSubviewTitleLabel = 1,
    CCNutrientsSectionCellSubviewTextField
};
@implementation CCSubmitFoodTVC
{
    NSArray *_arrFoodInfoCellTitles, *_arrFoodInfoCellPlaceholders, *_arrNutrientCellTitles;
    UITextField *_txtFoodName, *_txtFoodBrand, *_txtBarCode, *_txtServingType, *_txtCalories, *_txtFats, *_txtSatFats, *_txtCholesterol, *_txtSodium;
}

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _arrFoodInfoCellTitles = @[[TSLanguageManager localizedString:@"Food Name (Required)"], [TSLanguageManager localizedString:@"Brand Name (Optional)"], [TSLanguageManager localizedString:@"Bar Code (Optional)"], [TSLanguageManager localizedString:@"Serving Type"]];
    _arrFoodInfoCellPlaceholders = @[[TSLanguageManager localizedString:@"Ex. Vanilla Yogurt"], [TSLanguageManager localizedString:@"ex. Danone"], [TSLanguageManager localizedString:@"ex. 002200008450"], [TSLanguageManager localizedString:@"1 Cup"]];
    _arrNutrientCellTitles =  @[[TSLanguageManager localizedString:@"Calories (g)"], [TSLanguageManager localizedString:@"Fats (g)"], [TSLanguageManager localizedString:@"Sat. Fats (g)"], [TSLanguageManager localizedString:@"Cholesterol (g)"], [TSLanguageManager localizedString:@"Sodium (g)"]];
    [self.tableView reloadData];
    
    FAKFontAwesome *arrowIcon = [FAKFontAwesome arrowRightIconWithSize:25];
    [arrowIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    UIImage *sendImg = [arrowIcon imageWithSize:CGSizeMake(25, 25)];
   
    
    UIButton *sendbutton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [sendbutton setImage:sendImg forState:UIControlStateNormal];
    [sendbutton addTarget:self action:@selector(onSendButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *sendButton = [[UIBarButtonItem alloc] initWithCustomView:sendbutton];
    self.navigationItem.rightBarButtonItem = sendButton;
    [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Create food"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Helper
- (BOOL)validateString:(NSString *)string withPattern:(NSString *)pattern
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = NO;
    
        // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = YES;
    
    return didValidate;
}
#pragma mark - UIEvents
- (void)onSendButtonTapped
{
    if([[_txtFoodName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0 && [[_txtServingType.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0 &&
       [[_txtCalories.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] > 0)
    {
        if([self validateString:_txtServingType.text withPattern:@"\\d+\\s+[a-zA-Z]"]) {
            NSArray *servingType = [_txtServingType.text componentsSeparatedByString:@" "];
            NSDictionary *params = @{@"title":_txtFoodName.text, @"brand":_txtFoodBrand.text, @"barcode":_txtBarCode.text, @"serving_type": [servingType objectAtIndex:1], @"serving_size": [servingType objectAtIndex:0], @"calories": _txtCalories.text, @"cholesterol": _txtCholesterol.text, @"fat": _txtFats.text, @"saturated_fat": _txtSatFats.text, @"sodium": _txtSodium.text};
            [[CCAPI sharedInstance] submitFood:params success:^(id responseObject) {
                
                [UIAlertView showWithTitle:[TSLanguageManager localizedString:@"Thanks!"] message:[TSLanguageManager localizedString:@"The food was submited"] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }];
            
            } failure:^(NSError *error) {
                
            }];
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[TSLanguageManager localizedString:@"Sorry!"]  message:[TSLanguageManager localizedString:@"Please enter the serving type in the right format. Ex '1 cup')"] delegate:nil cancelButtonTitle:[TSLanguageManager localizedString:@"Ok"] otherButtonTitles:nil];
            [alert show];
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[TSLanguageManager localizedString:@"Sorry!"]  message:[TSLanguageManager localizedString:@"Please enter all fields"] delegate:nil cancelButtonTitle:[TSLanguageManager localizedString:@"Ok"] otherButtonTitles:nil];
        [alert show];
        
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == CCSubmitFoodSectionFoodInfo) {
        return _arrFoodInfoCellTitles.count;
    } else if (section == CCSubmitFoodSectionNutrients) {
        return  _arrNutrientCellTitles.count;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section  = indexPath.section;
    NSInteger row = indexPath.row;
    
    NSString *cellIdentifier = @"";
    if (section == CCSubmitFoodSectionFoodInfo) {
        cellIdentifier = @"Food Info Cell";
    } else if (section == CCSubmitFoodSectionNutrients) {
        cellIdentifier = @"Nutrient Cell";
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if (section == CCSubmitFoodSectionFoodInfo) {
        
        NSString *cellTitle = [_arrFoodInfoCellTitles objectAtIndex:row];
        NSString *cellPlaceholder = [_arrFoodInfoCellPlaceholders objectAtIndex:row];
        
        [(UILabel *)[cell.contentView viewWithTag:CCFoodInfoSectionCellSubviewTitleLabel] setText:cellTitle];
        
        if (row == CCFoodInfoSectionCellFoodName) {
            _txtFoodName = (UITextField *)[cell.contentView viewWithTag:CCFoodInfoSectionCellSubviewTextField];
            [_txtFoodName setPlaceholder:cellPlaceholder];
        } else if (row == CCFoodInfoSectionCellFoodBrand) {
            _txtFoodBrand = (UITextField *)[cell.contentView viewWithTag:CCFoodInfoSectionCellSubviewTextField];
            [_txtFoodBrand setPlaceholder:cellPlaceholder];
        } else if (row == CCFoodInfoSectionCellFoodBarCode) {
            _txtBarCode = (UITextField *)[cell.contentView viewWithTag:CCFoodInfoSectionCellSubviewTextField];
            [_txtBarCode setPlaceholder:cellPlaceholder];
            [_txtBarCode setKeyboardType:UIKeyboardTypeNumberPad];
        } else if (row == CCFoodInfoSectionCellFoodServingType) {
            _txtServingType = (UITextField *)[cell.contentView viewWithTag:CCFoodInfoSectionCellSubviewTextField];
            [_txtServingType setPlaceholder:cellPlaceholder];
        }
       
        [(UITextField *)[cell.contentView viewWithTag:CCNutrientsSectionCellSubviewTextField] setReturnKeyType:UIReturnKeyNext];
        
    } else if (section == CCSubmitFoodSectionNutrients) {
        NSString *cellTitle = [_arrNutrientCellTitles objectAtIndex:row];
        NSString *cellPlaceholder = @"";
        if (row == CCNutrientsSectionCellCalories) {
            cellPlaceholder = [TSLanguageManager localizedString:@"(Required)"];
        } else {
            cellPlaceholder = [TSLanguageManager localizedString:@"(Optional)"];
        }
        [(UILabel *)[cell.contentView viewWithTag:CCNutrientsSectionCellSubviewTitleLabel] setText:cellTitle];
       
        if (row == CCNutrientsSectionCellCalories) {
            _txtCalories = (UITextField *)[cell.contentView viewWithTag:CCFoodInfoSectionCellSubviewTextField];
        } else if (row == CCNutrientsSectionCellFats) {
            _txtFats = (UITextField *)[cell.contentView viewWithTag:CCFoodInfoSectionCellSubviewTextField];
        } else if (row == CCNutrientsSectionCellSatFats) {
            _txtSatFats = (UITextField *)[cell.contentView viewWithTag:CCFoodInfoSectionCellSubviewTextField];
        } else if (row == CCNutrientsSectionCellCholesterol) {
            _txtCholesterol = (UITextField *)[cell.contentView viewWithTag:CCFoodInfoSectionCellSubviewTextField];
        } else if (row == CCNutrientsSectionCellSodium) {
            _txtSodium = (UITextField *)[cell.contentView viewWithTag:CCFoodInfoSectionCellSubviewTextField];
        }
        [(UITextField *)[cell.contentView viewWithTag:CCNutrientsSectionCellSubviewTextField] setPlaceholder:cellPlaceholder];
        [(UITextField *)[cell.contentView viewWithTag:CCNutrientsSectionCellSubviewTextField] setKeyboardType:UIKeyboardTypeDecimalPad];
        if (row == CCNutrientsSectionCellSodium) {
            
            [(UITextField *)[cell.contentView viewWithTag:CCNutrientsSectionCellSubviewTextField] setReturnKeyType:UIReturnKeySend];
            
        } else {
            [(UITextField *)[cell.contentView viewWithTag:CCNutrientsSectionCellSubviewTextField] setReturnKeyType:UIReturnKeyNext];
        }

        
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == CCSubmitFoodSectionFoodInfo)
        return 63.0;
    return 45.0;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(tintColor)]) {
        
            CGFloat cornerRadius = 4.f;
            cell.backgroundColor = UIColor.clearColor;
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CGMutablePathRef pathRef = CGPathCreateMutable();
            CGRect bounds = CGRectInset(cell.bounds, 10, 0);
            BOOL addLine = NO;
            if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            } else if (indexPath.row == 0) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }
            layer.path = pathRef;
            CFRelease(pathRef);
            layer.fillColor = [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
            
            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds)+5, bounds.size.height-lineHeight, bounds.size.width-5, lineHeight);
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }
            UIView *testView = [[UIView alloc] initWithFrame:bounds];
            [testView.layer insertSublayer:layer atIndex:0];
            testView.backgroundColor = UIColor.clearColor;
            cell.backgroundView = testView;
        
    }
}
#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtFoodName) {
        [_txtFoodBrand becomeFirstResponder];
    } else if (textField == _txtFoodBrand) {
        [_txtBarCode becomeFirstResponder];
    } else if (textField == _txtBarCode) {
        [_txtServingType becomeFirstResponder];
    } else if (textField == _txtServingType) {
        [_txtCalories becomeFirstResponder];
    } else if (textField == _txtCalories) {
        [_txtFats becomeFirstResponder];
    } else if (textField == _txtFats) {
        [_txtSatFats becomeFirstResponder];
    } else if (textField == _txtSatFats) {
        [_txtCholesterol becomeFirstResponder];
    } else if (textField == _txtCholesterol) {
        [_txtSodium becomeFirstResponder];
    } else if (textField == _txtSodium) {
        [self onSendButtonTapped];
    }
    return YES;
}
@end
