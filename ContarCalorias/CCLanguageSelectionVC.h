//
//  CCLanguageSelectionVC.h
//  ContarCalorias
//
//  Created by andres portillo on 01/09/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCLanguageSelectionVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
