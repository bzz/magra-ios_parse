//
//  CCActivityLevelCell.h
//  ContarCalorias
//
//  Created by andres portillo on 26/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCActivityLevelCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgState;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@end
