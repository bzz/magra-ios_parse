//
//  CCDietDetailTVC.m
//  ContarCalorias
//
//  Created by andres portillo on 06/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCDietDetailTVC.h"
#import "CCAPI.h"
#import "UIImageView+AFNetworking.h"
#import "MealPlan.h"
#import "Meal.h"
#import "MealType.h"
#import "CCUtils.h"

@interface CCDietDetailTVC ()<UIActionSheetDelegate>

@end

@implementation CCDietDetailTVC
{
    UIImage *_headerImage;
    NSArray *_arrCellIdentifiers;
    NSArray *_arrMeals;
    NSArray *_arrTimesOfDay;
    CGFloat _mealPlanDescriptionTextheight;
    NSInteger _numberOfSections;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _headerImage = [UIImage imageNamed:_mealPlan.image];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    _arrTimesOfDay = @[@"breakfast",@"morningsnack",@"lunch",@"eveningsnack",@"dinner"];
    
    [self getDietDetails];
    
    NSString *text = _mealPlan.descriptionMealplan;
    CGFloat width = 270;
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    _mealPlanDescriptionTextheight = [CCUtils getHeightForText:text Font:font andWidth:width];
    
    _numberOfSections = 2 + _mealPlan.daysValue + (_mealPlan.daysValue > 0? 1 : 0);
    
    self.navigationItem.title = _mealPlan.title;
    
}
- (void)getDietDetails
{
    _arrMeals = [[_mealPlan.meals allObjects] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"dayOfPlan" ascending:YES]]];
    [self.tableView reloadData];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section < 2 || section == (2 + _mealPlan.daysValue))
        return 1;

    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier;
    if (indexPath.section == 0) {
        identifier = @"Header Cell";
    } else if (indexPath.section == 1 || indexPath.section == (2 + _mealPlan.daysValue)) {
        identifier = @"Follow Diet Cell";
    } else if (indexPath.section > 1) {
            identifier = @"Meal Detail Cell";
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    if (indexPath.section == 0) {
        
       UIImage *dietImg = [UIImage imageNamed:_mealPlan.image];
        _headerImage = dietImg;
        
        [(UIImageView *)[cell.contentView viewWithTag:1] setImage:_headerImage];
        [(UILabel *)[cell.contentView viewWithTag:3] setText:_mealPlan.title];
        [(UITextView *)[cell.contentView viewWithTag:2] setText:_mealPlan.descriptionMealplan];
        [(UITextView *)[cell.contentView viewWithTag:2] sizeToFit];
        
        CGRect bubbleImgFrame = [(UIImageView *)[cell.contentView viewWithTag:4] frame];
        bubbleImgFrame.size.height = _mealPlanDescriptionTextheight + 60;
        [(UIImageView *)[cell.contentView viewWithTag:4] setFrame:bubbleImgFrame];
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(16, 8, 14, 8);
        UIImage *stretchedBubbleImage = [[UIImage imageNamed:@"text-bubble"]
                                          resizableImageWithCapInsets:edgeInsets];
        [(UIImageView *)[cell.contentView viewWithTag:4] setImage:stretchedBubbleImage];
        
        
    } else if (indexPath.section == 1 || indexPath.section == (2 + _mealPlan.daysValue)) {
        [(UIButton *)[cell.contentView viewWithTag:1] setTitle:[TSLanguageManager localizedString:@"Follow this plan"] forState:UIControlStateNormal];
        
    } else if (indexPath.section > 1) {
        NSNumber *dayKey = @([[NSString stringWithFormat:@"%d",indexPath.section - 1] integerValue]);
        NSArray *arrMealsForDay = [_arrMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"dayOfPlan = %@",dayKey]];
        
        NSString *keyTimeOfDay = _arrTimesOfDay[indexPath.row];
        
        [(UILabel *)[cell.contentView viewWithTag:1] setText:[[TSLanguageManager localizedString:[keyTimeOfDay capitalizedString]] uppercaseString]];
        
        MealType *mealType = [MealType MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"name = %@", keyTimeOfDay]];
        
        NSArray *arrMealElements = [arrMealsForDay filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mealType = %@",mealType]];
        
        NSString *mealDesc = @"";
        for (Meal *meal in arrMealElements)
        {
            if (meal == [arrMealElements firstObject]) {
                mealDesc = [mealDesc stringByAppendingString:[NSString stringWithFormat:@"%@",meal.title]];
            } else {
                mealDesc = [mealDesc stringByAppendingString:[NSString stringWithFormat:@", %@",meal.title]];
            }
            
        }
        [(UILabel *)[cell.contentView viewWithTag:2] setText:mealDesc];
    }

    
    return cell;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        return 350 + _mealPlanDescriptionTextheight;
        
    } else if (indexPath.section == 1 || indexPath.section == (2 + _mealPlan.daysValue)) {
        return 50;
    } else if (indexPath.section > 1) {
        return 100;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section > 1 && section  != (_numberOfSections - 1)) {
        UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
        [header setBackgroundColor:CCColorGreen];
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 12, 200, 15)];
        [lbl setText:[[NSString stringWithFormat:@"%@ %d",[TSLanguageManager localizedString:@"Day"],(section - 1)] uppercaseString]];
        [lbl setTextColor:[UIColor whiteColor]];
        [lbl setFont:[UIFont fontWithName:@"HelveticaNeue" size:15]];
        
        [header addSubview:lbl];
        
        return header;

    }
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
    [header setBackgroundColor:[UIColor whiteColor]];
    return header;
}
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section > 1) {
        
        if (section == (2 + _mealPlan.daysValue)) {
            return 15.0;
        }
       
        return 35.0;

    }
    return 0;
}
#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [[CCAPI sharedInstance] enrollUserInMealPlanWithId:_mealPlan.idMealPlanValue];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
- (IBAction)didTapFollowPlanButton:(id)sender {
    
    UIActionSheet *ac = [[UIActionSheet alloc] initWithTitle:[TSLanguageManager localizedString:@"Are you sure you want to start following this plan? if you are currently following another plan you will be switch to this new plan"] delegate:self cancelButtonTitle:[TSLanguageManager localizedString:@"Cancel"] destructiveButtonTitle:nil otherButtonTitles:[TSLanguageManager localizedString:@"Start"], nil];
    [ac showInView:self.view];
}
@end
