//
//  AddViewController.m
//  ContarCalorias
//
//  Created by Adrian Ghitun on 07/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "AddViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "CCSearchDiaryItemVC.h"
#import "ViewUtils.h"
#import "ContarCalendar.h"
#import "UIImage+ProportionalFill.h"
#import "CCConstants.h"
#import "FontAwesomeKit.h"

@interface AddViewController ()


@end

@implementation AddViewController
{
    UIView *alphaView;
    NSDateFormatter *dateFormatter;
    UILabel *_lblCurrentField;
    UIToolbar *_inputAccessory;
    BOOL _doSelection;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                        forBarMetrics:UIBarMetricsDefault];
    
    [super viewDidLoad];
    
        //Set Screen Name (Google Analytics)
    self.screenName = @"Add Food Screen";
    
    
	// Do any additional setup after loading the view.
    [[self scrollViewWrapper] setContentSize:CGSizeMake(320, 465)];
    [ViewUtils drawBasicShadowOnView:[self viewWrapperDate]];
    [ViewUtils drawBasicShadowOnView:[self viewWrapperWater]];
    _viewWrapperWater.layer.cornerRadius = 4;
    [ViewUtils drawBasicShadowOnView:[self viewWrapperMeals]];
    _viewWrapperMeals.layer.cornerRadius = 4;
    [ViewUtils drawBasicShadowOnView:[self viewWrapperExercises]];
    _viewWrapperExercises.layer.cornerRadius = 4;
    [self.view setBackgroundColor:[ViewUtils lightGrayColor]];
    [ViewUtils fixSeparatorsHeightInView:_viewWrapperMeals];
    [ViewUtils fixSeparatorsHeightInView:_viewWrapperExercises];
    
    [self.navigationController.navigationBar setTintColor:[ViewUtils greenColor]];
    [self setLocalizedStrings];
    
    
}

- (void)setLocalizedStrings
{
    [_lblAddFood setText:[TSLanguageManager localizedString:@"Add food"]];
    [_lblAddWater setText:[TSLanguageManager localizedString:@"Add water"]];
    [_lblAddExercise setText:[TSLanguageManager localizedString:@"Add exercise"]];
    [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Add"]];
}
- (void)viewDidLayoutSubviews
{
    [self updateCurrentDateLabel];
    
    self.goToPreviousDateBtn.imageView.contentMode = UIViewContentModeCenter;
    self.goToNextDateBtn.imageView.contentMode = UIViewContentModeCenter;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(currentDateDidChanged)
                                                 name:@"currentDateDidChanged"
                                               object:nil];
    
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setRightButtons{
    UIButton *btnCalendar = [[UIButton alloc] init];
    [btnCalendar setFrame:CGRectMake(0, 0, 44, 44)];
    UIImage *image = [UIImage imageNamed:@"icon_calendar"];
    [btnCalendar setImage:[image imageCroppedToFitSize:CGSizeMake(19.0, 22.0)] forState:UIControlStateNormal];
    btnCalendar.imageView.contentMode = UIViewContentModeCenter;
    [btnCalendar addTarget:self action:@selector(openCalendar) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *itemBtnCalendar = [[UIBarButtonItem alloc] initWithCustomView:btnCalendar];
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending)
    {
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        negativeSpacer.width = -16.0;
        self.navigationItem.rightBarButtonItems = @[negativeSpacer,itemBtnCalendar];
    }
    else
        self.navigationItem.rightBarButtonItem = itemBtnCalendar;
}

- (void)openCalendar
{
    ContarCalendar *calendar = [ContarCalendar sharedInstance];
    
    if(!alphaView)
    {
        alphaView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        alphaView.backgroundColor = [UIColor blackColor];
        alphaView.alpha = 0.0;
    }
    
    if(calendar.window == nil)
    {
        __block CGRect calendarFrame = calendar.frame;
        calendarFrame.origin.y = -calendarFrame.size.height;
        
        calendar.frame = calendarFrame;
        
        [UIView animateWithDuration:0.4
                         animations:^{
                             [self.view addSubview:alphaView];
                             alphaView.alpha = 0.3;
                             [self.view addSubview:calendar];
                             calendarFrame.origin.y = 0.0;
                             calendar.frame = calendarFrame;
                         }];
    }
    else
        [UIView animateWithDuration:0.4
                         animations:^{
                             CGRect calendarFrame = calendar.frame;
                             calendarFrame.origin.y = -calendarFrame.size.height;
                             calendar.frame = calendarFrame;
                             alphaView.alpha = 0.0;
                         } completion:^(BOOL finished) {
                             if(finished)
                             {
                                 [alphaView removeFromSuperview];
                                 [calendar removeFromSuperview];
                                 alphaView = nil;
                             }
                         }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   if ([segue.identifier isEqualToString:@"Add Exercise Segue"]) {
    
        CCSearchDiaryItemVC *vc = [segue destinationViewController];
        vc.currentDiaryItem = CCDiaryItemTypeExercise;
        
    }
}

- (void)currentDateDidChanged
{
    [self updateCurrentDateLabel];
}

- (void)updateCurrentDateLabel
{
    ContarCalendar *calendar = [ContarCalendar sharedInstance];
    if(!dateFormatter)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        NSString *langID = [[NSLocale preferredLanguages] objectAtIndex:0];
        
        NSLocale *locale;
        
        if([langID isEqualToString:@"pt"])
            locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt"];
        else
            locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];

        [dateFormatter setLocale:locale];
        [dateFormatter setDateFormat:@"dd MMMM"];
    }
    
    self.currentDateLabel.text = [dateFormatter stringFromDate:calendar.dateShowing];
}

- (IBAction)goToNextDateAction:(id)sender
{
    [[ContarCalendar sharedInstance] moveCalendarToNextDay];
    [self updateCurrentDateLabel];
}

- (IBAction)goToPreviousDateAction:(id)sender
{
    [[ContarCalendar sharedInstance] moveCalendarToPreviousDay];
    [self updateCurrentDateLabel];
}

@end
