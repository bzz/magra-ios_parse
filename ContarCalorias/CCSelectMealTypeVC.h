//
//  CCSelectMealTypeVC.h
//  ContarCalorias
//
//  Created by andres portillo on 24/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCSelectMealTypeVC : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewWrapper;
@property (weak, nonatomic) IBOutlet UIView *viewWrapperMealTypes;
@property (weak, nonatomic) IBOutlet UILabel *lblBreakfast;
@property (weak, nonatomic) IBOutlet UILabel *lblLunch;
@property (weak, nonatomic) IBOutlet UILabel *lblDinner;
@property (weak, nonatomic) IBOutlet UILabel *lblSnack;



- (IBAction)addBreakfast:(id)sender;
- (IBAction)addLunch:(id)sender;
- (IBAction)addDinner:(id)sender;
- (IBAction)addSnack:(id)sender;
@end
