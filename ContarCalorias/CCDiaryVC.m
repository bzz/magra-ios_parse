//
//  CCDiaryVC.m
//  ContarCalorias
//
//  Created by andres portillo on 15/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCDiaryVC.h"
#import "CCAPI.h"
#import "ContarCalendar.h"
#import "SWRevealViewController.h"
#import "User.h"
#import "CCTaskProgressView.h"
#import "CCConstants.h"
#import "UserFood.h"
#import "Food.h"
#import "FoodServing.h"
#import "CCSearchDiaryItemVC.h"
#import "DiaryItemDetailsViewController.h"
#import "CCTaskProgressViewWrapper.h"


@interface CCDiaryVC ()<UITableViewDataSource, UITableViewDelegate, SWRevealViewControllerDelegate>

@end

@implementation CCDiaryVC
{
    NSDateFormatter *_dayFormatter, *_dateFormatter;
    User *_user;
    NSArray *_arrBreakfastMeals, *_arrLunchMeals, *_arrDinnerMeals, *_arrSnackMeals, *_arrSections;
    CCMealType _mealType;
}
- (void)viewWillLayoutSubviews
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    [_constraintHeight setConstant:screenRect.size.height - self.tableView.frame.origin.y];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
    [self configureScreenHeader];
    [self configureSectionArray];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
        //Set Screen Name (google analytics)
    self.screenName = @"Diary Screen";
    
        //register for notifiactions
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onMealLogged)
                                                 name:kCCNotificationMealWasLogged object:nil];
   
    
    NSDate *currentDate = [[ContarCalendar sharedInstance]  dateShowing];
    
    _dayFormatter = [NSDateFormatter new];
    
    NSLocale *locale;
    
    if ([[TSLanguageManager selectedLanguage] isEqualToString:@"es"]) {
        locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es_ES"];
    } else if([[TSLanguageManager selectedLanguage] isEqualToString:@"pt"]){
        locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    }
    [_dayFormatter setLocale:locale];
    [_dayFormatter setDateFormat:@"EEEE"];
    
    _dateFormatter = [NSDateFormatter new];
    [_dateFormatter setDateFormat:@"MMMM dd, yyyy"];
    [_dateFormatter setLocale:locale];
    
    [_lblDay setText:[[_dayFormatter stringFromDate:currentDate] capitalizedString]];
    
    [_lblDate setText:[[_dateFormatter stringFromDate:currentDate] capitalizedString]];
    
        // get User
    _user = [[CCAPI sharedInstance] getUser];
    _viewNavigationBar.layer.shadowColor = [[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1] CGColor];
    
    
    
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView reloadData];
    
    [self configureScreenHeader];
    [self configureSectionArray];
    
    self.revealViewController.delegate = self;
    [self setLocalizedStrings];
}
- (void)setLocalizedStrings
{
    [_lblTitleGoal setText:[TSLanguageManager localizedString:@"Goal"]];
    [_lblTitleDiary setText:[TSLanguageManager localizedString:@"Diary"]];
    [_lblTitleExercise setText:[TSLanguageManager localizedString:@"Exercise"]];
    [_lblTitleFats setText:[TSLanguageManager localizedString:@"Fats"]];
    [_lblTitleNet setText:[TSLanguageManager localizedString:@"Net"]];
    [_lblTitleProteins setText:[TSLanguageManager localizedString:@"Proteins"]];
    [_lblTitleFood setText:[TSLanguageManager localizedString:@"Food"]];
    [_lblTitleRemaining setText:[TSLanguageManager localizedString:@"Remaining"]];
    [_lblTitleCarbs setText:[TSLanguageManager localizedString:@"Carbs"]];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Class methods
- (void)configureSectionArray
{
    NSArray *arrTodaysLoggedMeals = [[CCAPI sharedInstance] getTodaysLoggedMeals];
    _arrBreakfastMeals = [[arrTodaysLoggedMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@" meanEnumType = %d",CCMealTypeBreakfast]] count] > 0? [arrTodaysLoggedMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@" meanEnumType = %d",CCMealTypeBreakfast]]:@[[NSNull null]];
    
    _arrLunchMeals = [[arrTodaysLoggedMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@" meanEnumType = %d",CCMealTypeLunch]] count] > 0? [arrTodaysLoggedMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@" meanEnumType = %d",CCMealTypeLunch]]: @[[NSNull null]];
    
    _arrDinnerMeals = [[arrTodaysLoggedMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@" meanEnumType = %d",CCMealTypeDinner]] count] > 0 ? [arrTodaysLoggedMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@" meanEnumType = %d",CCMealTypeDinner]]:@[[NSNull null]];
    
    _arrSnackMeals = [[arrTodaysLoggedMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@" meanEnumType = %d",CCMealTypeSnack]] count] > 0? [arrTodaysLoggedMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@" meanEnumType = %d",CCMealTypeSnack]] : @[[NSNull null]];
    
    _arrSections = @[_arrBreakfastMeals, _arrLunchMeals, _arrDinnerMeals, _arrSnackMeals];
    
    [self.tableView reloadData];
}
- (void)configureScreenHeader
{
    NSInteger consumedCalories = [[CCAPI sharedInstance] getTodaysConsumedCalories];
    
    NSInteger burnedCalories = [[CCAPI sharedInstance] getTodaysBurnedCalories];
    CGFloat currentCarbs = [[CCAPI sharedInstance] getTodaysConsumedCarbs];
    CGFloat currentFats = [[CCAPI sharedInstance] getTodaysConsumedFats];
    CGFloat currentProteins = [[CCAPI sharedInstance] getTodaysConsumedProteins];
    NSInteger netCalories = consumedCalories - burnedCalories;
    NSInteger remainingCalories = _user.dailyCaloriesValue - netCalories;
    
    
    [_lblCaloriesBudget setText:[NSString stringWithFormat:@"%d",_user.dailyCaloriesValue]];
    [_lblConsumedCalories setText:[NSString stringWithFormat:@"%d",consumedCalories]];
    [_lblBurnedCalories setText:[NSString stringWithFormat:@"%d",burnedCalories]];
    
    [_lblNeto setText:[NSString stringWithFormat:@"%d",netCalories]];
    
    [_lblRemaining setText:[NSString stringWithFormat:@"%d",remainingCalories]];
    
    
    [_lblCarbsBudget setText:[NSString stringWithFormat:@"%dg",_user.dailyCarbsValue]];
    [_lblFatsBudget setText:[NSString stringWithFormat:@"%dg",_user.dailyFatsValue]];
    [_lblProteinsBudget setText:[NSString stringWithFormat:@"%dg",_user.dailyProteinsValue]];
    [_lblCurrentCarbs setText:[NSString stringWithFormat:@"%.0f/",currentCarbs]];
    [_lblCurrentFats setText:[NSString stringWithFormat:@"%.0f/",currentFats]];
    [_lblCurrentProteins setText:[NSString stringWithFormat:@"%.0f/",currentProteins]];
    
    CGFloat carbsProgress = currentCarbs / _user.dailyCarbsValue;
    CGFloat fatsProgress = currentFats / _user.dailyFatsValue;
    CGFloat proteinsProgress = currentProteins / _user.dailyProteinsValue;
    
    CCTaskProgressViewWrapper *carbsProgressView = [[CCTaskProgressViewWrapper alloc] initWithFrame:CGRectMake( 23, 90, 75,7)];
    
    [carbsProgressView.progressView setProgressColor:[UIColor colorWithRed:255.0/255.0 green:212.0/255.0 blue:62.0/255.0 alpha:1]];
    [carbsProgressView.progressView  setUserDefinedHeight:7];
    [carbsProgressView.progressView setProgress:carbsProgress];
    [_headerView addSubview:carbsProgressView];
    
    CCTaskProgressViewWrapper *fatsProgressView = [[CCTaskProgressViewWrapper alloc] initWithFrame:CGRectMake( 123, 90, 75,7)];
    
    [fatsProgressView.progressView setProgressColor:[UIColor colorWithRed:255.0/255.0 green:73.0/255.0 blue:24.0/255.0 alpha:1]];
    [fatsProgressView.progressView  setUserDefinedHeight:7];
    [fatsProgressView.progressView setProgress:fatsProgress];
    [_headerView addSubview:fatsProgressView];
    
    CCTaskProgressViewWrapper *proteinsProgressView = [[CCTaskProgressViewWrapper alloc] initWithFrame:CGRectMake( 231, 90, 75,7)];
    
    [proteinsProgressView.progressView setProgressColor:[UIColor colorWithRed:184.0/255.0 green:204.0/255.0 blue:65.0/255.0 alpha:1]];
    [proteinsProgressView.progressView  setUserDefinedHeight:7];
    [proteinsProgressView.progressView setProgress:proteinsProgress];
    [_headerView addSubview:proteinsProgressView];
}
- (void)updateDateLabels
{
    NSDate *currentDate = [[ContarCalendar sharedInstance]  dateShowing];
    
    [_lblDay setText:[[_dayFormatter stringFromDate:currentDate] capitalizedString]];
    
    [_lblDate setText:[[_dateFormatter stringFromDate:currentDate] capitalizedString]];
}
#pragma mark - UIActions
- (IBAction)onNextDayButtonTapped:(id)sender {
    
    [[ContarCalendar sharedInstance] moveCalendarToNextDay];
    [self updateDateLabels];
    [self configureScreenHeader];
    [self configureSectionArray];
}

- (IBAction)onPreviousDayButtonTapped:(id)sender {
    [[ContarCalendar sharedInstance] moveCalendarToPreviousDay];
    [self updateDateLabels];
    [self configureScreenHeader];
    [self configureSectionArray];
}

- (IBAction)onMenuButtonTapped:(id)sender {
    [self.revealViewController revealToggleAnimated:YES];
}

#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowsInSection = [[_arrSections objectAtIndex:section] count];
    
    return rowsInSection;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _arrSections.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell  *cell;
    NSString *identifier = @"";
    if ([[_arrSections objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] == [NSNull null]) {
        
        identifier  = @"Empty Section Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        [(UILabel *)[cell viewWithTag:1] setText:[TSLanguageManager localizedString:@"No meals added"]];
    }
    
    
    
    if ([[_arrSections objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] != [NSNull null]) {

        identifier  = @"Diary Screen Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        UserFood *userFood = [[_arrSections objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        [((UILabel *)[cell.contentView viewWithTag:1]) setText:[NSString stringWithFormat:@"%@ (%@ %@)", userFood.food.title, userFood.numberOfServings,  userFood.foodServing.serving_type]];
        
        if ([userFood.foodServing.serving_type isEqualToString:@"g"] || [userFood.foodServing.serving_type isEqualToString:@"ml"]) {
                [((UILabel *)[cell.contentView viewWithTag:2]) setText:[NSString stringWithFormat:@"%@ x %d cal", userFood.numberOfServings,(int)((float) userFood.foodServing.caloriesValue / 100)]];
        } else {
            [((UILabel *)[cell.contentView viewWithTag:2]) setText:[NSString stringWithFormat:@"%@ x %@ cal", userFood.numberOfServings, userFood.foodServing.calories]];
        }
    }
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *viewHeader  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
    [viewHeader setBackgroundColor:CCColorGreen];
    
    UIButton *btnAdd = [[UIButton alloc] initWithFrame:CGRectMake(250, 13, 70, 12)];
    [btnAdd setTitle:[TSLanguageManager localizedString:@"Add"] forState:UIControlStateNormal];
    [btnAdd.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:11]];
    [btnAdd.titleLabel setTextAlignment:NSTextAlignmentRight];

    
    NSString *headerTitle = @"";
    __block NSInteger totalCaloriesForMealType = 0;
    if ([_arrSections objectAtIndex:section] == _arrBreakfastMeals) {
        headerTitle = [TSLanguageManager localizedString:@"Breakfast"];
        
        if ([_arrBreakfastMeals objectAtIndex:0] != [NSNull null]) {
         
            [_arrBreakfastMeals enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                if ([((UserFood *)obj).foodServing.serving_type isEqualToString:@"g"] || [((UserFood *)obj).foodServing.serving_type isEqualToString:@"ml"]) {
                    totalCaloriesForMealType  = (int)(totalCaloriesForMealType + ((float)((UserFood *)obj).foodServing.caloriesValue/100) * ((UserFood *)obj).numberOfServingsValue);
                } else {
                    totalCaloriesForMealType  = (int)( totalCaloriesForMealType + ((UserFood *)obj).foodServing.caloriesValue * ((UserFood *)obj).numberOfServingsValue);
                }
                
            }];
            
        } else {
            
            totalCaloriesForMealType  = 0;
        
        }
        
        [btnAdd addTarget:self action:@selector(onAddBreakfastTapped) forControlEvents:UIControlEventTouchUpInside];
        
    } else if ([_arrSections objectAtIndex:section] == _arrLunchMeals) {
        
        headerTitle = [TSLanguageManager localizedString:@"Lunch"];
        if ([_arrLunchMeals objectAtIndex:0] != [NSNull null]) {
            [_arrLunchMeals enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                if ([((UserFood *)obj).foodServing.serving_type isEqualToString:@"g"] || [((UserFood *)obj).foodServing.serving_type isEqualToString:@"ml"]) {
                    totalCaloriesForMealType  = (int)(totalCaloriesForMealType + ((float)((UserFood *)obj).foodServing.caloriesValue/100) * ((UserFood *)obj).numberOfServingsValue);
                } else {
                    totalCaloriesForMealType  = (int)( totalCaloriesForMealType + ((UserFood *)obj).foodServing.caloriesValue * ((UserFood *)obj).numberOfServingsValue);
                }
            }];

        } else {
            totalCaloriesForMealType = 0;
        }
        
        [btnAdd addTarget:self action:@selector(onAddLunchTapped) forControlEvents:UIControlEventTouchUpInside];
        
    } else if ([_arrSections objectAtIndex:section] == _arrDinnerMeals) {
        
        headerTitle = [TSLanguageManager localizedString:@"Dinner"];
        if ([_arrDinnerMeals objectAtIndex:0] != [NSNull null]) {
            
            [_arrDinnerMeals enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                if ([((UserFood *)obj).foodServing.serving_type isEqualToString:@"g"] || [((UserFood *)obj).foodServing.serving_type isEqualToString:@"ml"]) {
                    totalCaloriesForMealType  = (int)(totalCaloriesForMealType + ((float)((UserFood *)obj).foodServing.caloriesValue/100) * ((UserFood *)obj).numberOfServingsValue);
                } else {
                    totalCaloriesForMealType  = (int)( totalCaloriesForMealType + ((UserFood *)obj).foodServing.caloriesValue * ((UserFood *)obj).numberOfServingsValue);
                }
            }];
            
        } else {
            totalCaloriesForMealType = 0;
        }
        
        [btnAdd addTarget:self action:@selector(onAddDinnerTapped) forControlEvents:UIControlEventTouchUpInside];
        
    } else if ([_arrSections objectAtIndex:section] == _arrSnackMeals) {
        
        headerTitle = [TSLanguageManager localizedString:@"Snack"];
        if ([_arrSnackMeals objectAtIndex:0] != [NSNull null]) {
            [_arrSnackMeals enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                if ([((UserFood *)obj).foodServing.serving_type isEqualToString:@"g"] || [((UserFood *)obj).foodServing.serving_type isEqualToString:@"ml"]) {
                    totalCaloriesForMealType  = (int)(totalCaloriesForMealType + ((float)((UserFood *)obj).foodServing.caloriesValue/100) * ((UserFood *)obj).numberOfServingsValue);
                } else {
                    totalCaloriesForMealType  = (int)( totalCaloriesForMealType + ((UserFood *)obj).foodServing.caloriesValue * ((UserFood *)obj).numberOfServingsValue);
                }
            }];

        } else {
            totalCaloriesForMealType = 0;
        }
        
        [btnAdd addTarget:self action:@selector(onAddSnackTapped) forControlEvents:UIControlEventTouchUpInside];
    }
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 7, 200, 20)];
    [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
    [lblTitle setTextColor:[UIColor whiteColor]];
    [lblTitle setText:[NSString stringWithFormat:@"%@: %d cal",headerTitle, totalCaloriesForMealType]];
    [viewHeader addSubview:lblTitle];
    
    [viewHeader addSubview:btnAdd];
    
    return viewHeader;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0;
}

#pragma mark - uiactions

- (void)onAddBreakfastTapped
{
    
    _mealType = CCMealTypeBreakfast;
    [self performSegueWithIdentifier:@"Add Meal Item Segue" sender:nil];
}
- (void)onAddLunchTapped
{
    
    _mealType = CCMealTypeLunch;
    [self performSegueWithIdentifier:@"Add Meal Item Segue" sender:nil];
}
- (void)onAddDinnerTapped
{
    
    _mealType = CCMealTypeDinner;
    [self performSegueWithIdentifier:@"Add Meal Item Segue" sender:nil];
}
- (void)onAddSnackTapped
{
    
    _mealType = CCMealTypeSnack;
    [self performSegueWithIdentifier:@"Add Meal Item Segue" sender:nil];
}
- (void)onMealLogged
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Add Meal Item Segue"]) {
       
        CCSearchDiaryItemVC *addDiaryItemVC = segue.destinationViewController;
        addDiaryItemVC.currentDiaryItem = CCDiaryItemTypeMeal;
        addDiaryItemVC.currentMealType = _mealType;

    } else if ([segue.identifier isEqualToString:@"Diary Item Detail Segue"]) {
        NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];
        UserFood *selectedUserFood = [[_arrSections objectAtIndex:selectedIndexPath.section] objectAtIndex:selectedIndexPath.row];
        DiaryItemDetailsViewController *diaryItemDetailsVC = segue.destinationViewController;
        diaryItemDetailsVC.currentMealType = _mealType;
        Food *food = selectedUserFood.food;
        diaryItemDetailsVC.userFood = selectedUserFood;
        diaryItemDetailsVC.food = food;
        NSArray *arrFoodServing = [FoodServing MR_findByAttribute:@"food_id" withValue:food.food_id];
        diaryItemDetailsVC.arrFoodServings = arrFoodServing;
        diaryItemDetailsVC.isOnEditMode = YES;
    }
}
#pragma mark - SWRevealViewController Delegate
- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}
@end
