//
//  FoodDetailsViewController.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 12/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DeleteDiaryItemProtocol.h"
#import "DiaryItemInfo.h"
@class Food_serving;
@class Food;
@class UserFood;

@interface DiaryItemDetailsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtServingType;
@property (weak, nonatomic) IBOutlet UITextField *txtNumberOfPortions;
@property (weak, nonatomic) IBOutlet UILabel *currentDiaryItemLbl;

@property (weak, nonatomic) IBOutlet UIView *viewWrapperQuantity;
@property (weak, nonatomic) IBOutlet UIView *viewWrapperCalories;
@property (weak, nonatomic) IBOutlet UIView *viewWrapperNutritional;

@property (weak, nonatomic) IBOutlet UILabel *lbTotalCalories;
@property (weak, nonatomic) IBOutlet UILabel *lbCarbs;
@property (weak, nonatomic) IBOutlet UILabel *lbFat;
@property (weak, nonatomic) IBOutlet UILabel *lbProteins;
@property (weak, nonatomic) IBOutlet UILabel *lbSugar;

@property (weak, nonatomic) IBOutlet UITableView *nutrientsTableView;

@property (assign, nonatomic) CCMealType currentMealType;
@property (assign, nonatomic) CCDiaryItemType currentDiaryItemType;
@property (assign, nonatomic) BOOL isOnEditMode;
@property (strong, nonatomic) NSDictionary *itemDetails;
@property (strong, nonatomic) Food *food;
@property (strong, nonatomic) UserFood *userFood;
@property (strong, nonatomic) NSArray *arrFoodServings;

    //labels
@property (weak, nonatomic) IBOutlet UILabel *lblCarbs;
@property (weak, nonatomic) IBOutlet UILabel *lblFats;
@property (weak, nonatomic) IBOutlet UILabel *lblProtein;
@property (weak, nonatomic) IBOutlet UILabel *lblSugar;
@property (weak, nonatomic) IBOutlet UILabel *lblWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblCalories;
- (IBAction)onServingTypeTapped:(id)sender;

- (IBAction)onNumberOfServingsTapped:(id)sender;

@end
