//
//  CCDietsIntroductionVC.h
//  ContarCalorias
//
//  Created by andres portillo on 15/09/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
 
@interface CCDietsIntroductionVC : UIViewController
- (IBAction)onDismissScreenTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *btnBuy;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *btnRestore;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstWeekFree;

@property (weak, nonatomic) IBOutlet UILabel *lblLargeText1;
@property (weak, nonatomic) IBOutlet UILabel *lblSmallText1;
@property (weak, nonatomic) IBOutlet UILabel *lblLargeText2;
@property (weak, nonatomic) IBOutlet UILabel *lblSmallText2;
@property (weak, nonatomic) IBOutlet UILabel *lblLargeText3;
@property (weak, nonatomic) IBOutlet UILabel *lblSmallText3;
@property (weak, nonatomic) IBOutlet UILabel *lblLargeText4;
@property (weak, nonatomic) IBOutlet UILabel *lblSmallText4;

@end
