//
//  NSMutableString+HTML.h
//  ContarCalorias
//
//  Created by andres portillo on 02/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableString (HTML)
- (NSMutableString *)xmlSimpleUnescape;
- (NSMutableString *)xmlSimpleEscape;
@end
