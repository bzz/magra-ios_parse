//
//  LoginViewController.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 29/12/13.
//  Copyright (c) 2013 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CCLoginViewDelegate
@optional
- (void)theForgotPasswordButtonWasPressedOntheLoginView;
@end

@interface CCLoginView : UIView <UITextFieldDelegate>

@property (nonatomic, weak) id <CCLoginViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginIn;
@property (weak, nonatomic) IBOutlet UILabel *lblLogin;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)onLoginButtonTapped:(id)sender;
- (IBAction)onForgotPasswordButtonTapped:(id)sender;
- (IBAction)onBackButtonTapped:(id)sender;
@end
