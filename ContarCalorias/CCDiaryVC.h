//
//  CCDiaryVC.h
//  ContarCalorias
//
//  Created by andres portillo on 15/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CCTaskProgressView;
@class CCTaskProgressViewWrapper;
#import "GAITrackedViewController.h"

@interface CCDiaryVC : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UILabel *lblCaloriesBudget;
@property (weak, nonatomic) IBOutlet UILabel *lblConsumedCalories;
@property (weak, nonatomic) IBOutlet UILabel *lblBurnedCalories;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentCarbs;
@property (weak, nonatomic) IBOutlet UILabel *lblCarbsBudget;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentFats;
@property (weak, nonatomic) IBOutlet UILabel *lblFatsBudget;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentProteins;
@property (weak, nonatomic) IBOutlet UILabel *lblProteinsBudget;
@property (weak, nonatomic) IBOutlet UILabel *lblNeto;
@property (weak, nonatomic) IBOutlet UILabel *lblRemaining;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UILabel *lblDay;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIView *viewNavigationBar;@property (weak, nonatomic) IBOutlet UITableView *tableView;


    //labels for titles
@property (weak, nonatomic) IBOutlet UILabel *lblTitleDiary;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleGoal;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleFood;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleExercise;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNet;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleRemaining;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleCarbs;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleFats;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleProteins;


- (IBAction)onNextDayButtonTapped:(id)sender;
- (IBAction)onPreviousDayButtonTapped:(id)sender;
- (IBAction)onMenuButtonTapped:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeight;
@end
