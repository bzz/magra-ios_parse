//
//  CCRecipeTVC.h
//  ContarCalorias
//
//  Created by andres portillo on 07/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCRecipeTVC : UITableViewController
@property (nonatomic) NSInteger recipeId;
@end
