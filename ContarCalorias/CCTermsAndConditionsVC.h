//
//  CCTermsAndConditionsVC.h
//  ContarCalorias
//
//  Created by andres portillo on 29/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCTermsAndConditionsVC : UIViewController
- (IBAction)onDoneButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
