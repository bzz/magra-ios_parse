//
//  CCWeightCheckInVC.m
//  ContarCalorias
//
//  Created by andres portillo on 11/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCWeightCheckInVC.h"
#import "SWRevealViewController.h"
#import "CCProgressVC.h"
#import "APLineChart.h"
#import "CCAPI.h"
#import "User.h"
#import "UserValue.h"
#import "FontAwesomeKit.h"

@interface CCWeightCheckInVC ()<UIPickerViewDelegate,UIPickerViewDataSource>

@end

@implementation CCWeightCheckInVC
{
    User *_user;
    UIPickerView *_pickerWeight;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

        //Set Screen Nanme (Google Analytics)
    self.screenName = @"Update Weight Screen";
    
    // Do any additional setup after loading the view.
    UIImageView *imgTitleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"set-goal-weight-title-view-img"]];
    self.navigationItem.titleView = imgTitleView;
    
        //reveal view controller
    [self setBackButton];
    
    _user = [[CCAPI sharedInstance] getUser];
    
    _pickerWeight =[[UIPickerView alloc] init];
    [_pickerWeight setBackgroundColor:[UIColor whiteColor]];
    [_pickerWeight setDelegate:self];
    [_txtWeight setInputView:_pickerWeight];
    [_txtWeight setText:[NSString stringWithFormat:@"%.1f",[_user getCurrentWeight]]];
    
    
    NSInteger weightFirstUnit = [_txtWeight.text integerValue];
    NSInteger weightSecondUnit = ([_txtWeight.text floatValue] - weightFirstUnit) * 10;
    
    if (weightFirstUnit >= 14) {
        [_pickerWeight selectRow:(weightFirstUnit - 14) inComponent:0 animated:NO];
        [_pickerWeight selectRow:weightSecondUnit inComponent:1 animated:NO];
    }
    
    
    UIToolbar *inputAccessory = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 40)];
    UIBarButtonItem  *spacer = [[UIBarButtonItem alloc]    initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [inputAccessory setBarStyle:UIBarStyleDefault];
    [inputAccessory setTintColor:[UIColor whiteColor]];
    [inputAccessory setBarTintColor:CCColorGreen];
    
    FAKFontAwesome *checkIcon = [FAKFontAwesome checkIconWithSize:24];
    [checkIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    [inputAccessory setItems:[NSArray arrayWithObjects:spacer,
                              [[UIBarButtonItem alloc] initWithImage:[checkIcon imageWithSize:CGSizeMake(24, 24)] style:UIBarButtonItemStylePlain target:self action:@selector(weightWasSelected)],nil ] animated:YES];
    [_txtWeight setInputAccessoryView:inputAccessory];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [singleTap setNumberOfTapsRequired:1];
    [singleTap setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:singleTap];

    [_lblEnteryourWeight setText:[TSLanguageManager localizedString:@"Please enter your current weight"]];
    
}
- (void)weightWasSelected
{
    [_txtWeight setText:[NSString stringWithFormat:@"%d.%d",([_pickerWeight selectedRowInComponent:0] + 14),[_pickerWeight selectedRowInComponent:1]]];
    [_txtWeight resignFirstResponder];
    [self addButtonItem];
}
- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}
- (void)setBackButton{
    
    UIBarButtonItem* btnCancel = [[UIBarButtonItem alloc] initWithTitle:[TSLanguageManager localizedString:@"Cancel"] style:UIBarButtonItemStyleBordered target:self action:@selector(dismissView)];
    self.navigationItem.leftBarButtonItem = btnCancel;
}
- (void)dismissView
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)addButtonItem
{
    FAKFontAwesome *checkIcon = [FAKFontAwesome checkIconWithSize:24];
    [checkIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    UIButton *checkButton = [[UIButton alloc] init];
    [checkButton setImage:[checkIcon imageWithSize:CGSizeMake(24, 24)] forState:UIControlStateNormal];
    [checkButton setFrame:CGRectMake(0, 0, 28, 28)];
    [checkButton addTarget:self action:@selector(onDoneButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *nextButtonItem = [[UIBarButtonItem alloc] initWithCustomView:checkButton];
    [self.navigationItem setRightBarButtonItem:nextButtonItem];
}
- (void)onDoneButtonTapped
{
    [_user setCurrentWeight: [_txtWeight.text floatValue]];
    [_user updateDateReachingDesiredWeight];
    [[CCAPI sharedInstance] saveUserWithCompletion:nil];
    [[CCAPI sharedInstance] logUserWeight:[_txtWeight.text floatValue] sync:YES completion:^{
   
        [self.delegate newWeightWasloggedInTheWeightCheckInVC];
        
    }];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPickerView Data Source
    // returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}
    // returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return 421;
    } else if(component == 1){
        return 10;
    } else if(component == 2) {
        return 1;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        return [NSString stringWithFormat:@"%d",(row + 14)];
    } else if (component == 1) {
        return [NSString stringWithFormat:@".%d",row];
    } else if (component == 2) {
        return [NSString stringWithFormat:@"Kg"];
    }
    return @"";
}
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if(component == 0) {
        return 50.0;
    } else if(component == 1) {
        return 25.0;
    } else if(component == 2) {
        return 20.0;
    }
    return 0.0;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblTitle = (UILabel*)view;
    
    if (lblTitle == nil) {
        
        if (component == 0) {
            lblTitle = [[UILabel alloc] init];
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
            [lblTitle setText:[NSString stringWithFormat:@"%d",(row + 14)]];
            [lblTitle setTextAlignment:NSTextAlignmentRight];
            [lblTitle setTextColor:[UIColor blackColor]];
            [view addSubview:lblTitle];
        } else if (component == 1) {
            lblTitle = [[UILabel alloc] init];
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
            [lblTitle setText:[NSString stringWithFormat:@".%d",row]];
            [lblTitle setTextAlignment:NSTextAlignmentCenter];
            [lblTitle setTextColor:[UIColor blackColor]];
            [view addSubview:lblTitle];
        } else if (component == 2) {
            lblTitle = [[UILabel alloc] init];
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
            [lblTitle setText:[NSString stringWithFormat:@"Kg"]];
            [lblTitle setTextAlignment:NSTextAlignmentLeft];
            [lblTitle setTextColor:[UIColor blackColor]];
            [view addSubview:lblTitle];
        }
    }
    return lblTitle;
}
#pragma mark - UIPickerView Delegate
@end
