//
//  CCNoDietSelectedView.h
//  ContarCalorias
//
//  Created by andres portillo on 10/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CCNoDietSelectedViewDelegate
- (void)didTapPickADietButton;
- (void)didTapContactDietitianButton;
@end
@interface CCNoDietSelectedView : UIView
@property(nonatomic, weak) id<CCNoDietSelectedViewDelegate>delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblOr;
@property (weak, nonatomic) IBOutlet UIButton *btnTalkToNutritionist;
@property (weak, nonatomic) IBOutlet UILabel *lblNotFollowingAnyDiet;
@property (weak, nonatomic) IBOutlet UIButton *btnPickADiet;
- (IBAction)onPickADietTapped:(id)sender;
@end
