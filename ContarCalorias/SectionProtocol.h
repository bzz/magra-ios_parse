//
//  SectionPressedProtocol.h
//  ContarCalorias
//
//  Created by Lucian Gherghel on 04/03/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SectionProtocol <NSObject>

- (void)didPressedSectionWithIndex:(int)index;

@end
