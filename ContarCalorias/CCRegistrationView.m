    //
    //  RegistrationViewController.m
    //  ContarCalorias
    //
    //  Created by Adrian Ghitun on 30/12/13.
    //  Copyright (c) 2013 Adrian Ghitun. All rights reserved.
    //

#import "CCRegistrationView.h"
#import <AFNetworking/AFURLResponseSerialization.h>
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "CCAPI.h"
#import "PickerData.h"
#import "UIImage+ProportionalFill.h"
#import "User.h"
#import <UIAlertView+Blocks.h>
#import "helper.h"
#import "CCUtils.h"
#import "FontAwesomeKit.h"

@interface CCRegistrationView ()<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>

@end

@implementation CCRegistrationView
{
    UIPickerView *_pickerCountry;
    NSArray *_pickerCountrySource;
}
#pragma mark - UIViewController LifeCycle
- (void)awakeFromNib
{
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    gestureRecognizer.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:gestureRecognizer];
    
    _pickerCountry = [[UIPickerView alloc] init];
    [_pickerCountry setDataSource:self];
    [_pickerCountry setBackgroundColor:[UIColor whiteColor]];
    [_pickerCountry setDelegate:self];
    _pickerCountrySource = [[CCAPI sharedInstance] getAvailableCountries];
    
    [_txtCountry setInputView:_pickerCountry];
    
    UIToolbar *inputAccessory = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 40)];
    UIBarButtonItem  *spacer = [[UIBarButtonItem alloc]    initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [inputAccessory setBarStyle:UIBarStyleDefault];
    [inputAccessory setTintColor:[UIColor whiteColor]];
    [inputAccessory setBarTintColor:CCColorGreen];
    
    FAKFontAwesome *checkIcon = [FAKFontAwesome checkIconWithSize:28];
    [checkIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    [inputAccessory setItems:[NSArray arrayWithObjects:spacer,
                              [[UIBarButtonItem alloc] initWithImage:[checkIcon imageWithSize:CGSizeMake(25, 25)] style:UIBarButtonItemStyleDone target:self action:@selector(countryWasSelected)],nil ] animated:YES];
    
    [_txtCountry setInputAccessoryView:inputAccessory];
    
    [self setLocalizedStrings];
}
- (void)setLocalizedStrings
{
    [_txtFirstAndLastName setPlaceholder:[TSLanguageManager localizedString:@"First and Lastname"]];
    [_txtCountry setPlaceholder:[TSLanguageManager localizedString:@"Country"]];
    [_txtPassword setPlaceholder:[TSLanguageManager localizedString:@"Password"]];
    [_txtEmail setPlaceholder:[TSLanguageManager localizedString:@"Email"]];
    
    [_btnRegister setTitle:[TSLanguageManager localizedString:@"Sign in"] forState:UIControlStateNormal];
    [_lblRegistration setText:[TSLanguageManager localizedString:@"Sign up"]];
}
- (void)countryWasSelected
{
    [_txtCountry resignFirstResponder];
}
- (void)dismissKeyboard
{
    [self endEditing:YES];
}
#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtFirstAndLastName) {
        [_txtEmail becomeFirstResponder];
    } else if (textField == _txtEmail) {
        [_txtPassword becomeFirstResponder];
    } else if (textField == _txtPassword) {
         [_txtCountry becomeFirstResponder];
    } else {
        [self onRegisterUserButtonTapped:nil];
    }
        
    return YES;
}
#pragma mark - picker delegates

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (pickerView == _pickerCountry)
        return _pickerCountrySource.count;
    
    return 0;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView == _pickerCountry) {
        NSDictionary *dictCountry = _pickerCountrySource [row];
        NSString *countryName = dictCountry[@"name"];
        return countryName;
    }
    return nil;
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (pickerView ==_pickerCountry)
    {
        NSDictionary *dictCountry = _pickerCountrySource [row];
        NSString *countryName = dictCountry[@"name"];
        [_txtCountry setText:countryName];
    }
}
- (BOOL)validateTextFields
{
    if(_txtFirstAndLastName.text.length > 0 && _txtCountry.text.length > 0 && _txtEmail.text.length > 0 && _txtCountry.text.length > 0) {
        if ([CCUtils emailIsValid:_txtEmail.text]) {
            if(_txtPassword.text.length >= 6) {
                return YES;
            } else {
                [UIAlertView showWithTitle:[TSLanguageManager localizedString:@"Error"] message:[TSLanguageManager localizedString:@"Password must have more than 6 characters"] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:nil];
            }
        } else {
            [UIAlertView showWithTitle:[TSLanguageManager localizedString:@"Error"] message:[TSLanguageManager localizedString:@"Please enter a correct Email"] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:nil];
        }
    } else {
        [UIAlertView showWithTitle:[TSLanguageManager localizedString:@"Error"] message:[TSLanguageManager localizedString:@"Please enter all fields"] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:nil];
    }
    
    return NO;
}
#pragma mark - button
- (IBAction)onBackButtonTapped:(id)sender
{
    [self endEditing:YES];
    [ (UIScrollView *)self.superview scrollRectToVisible:CGRectMake(self.frame.origin.x
                                                                    - 320, self.frame.origin.y , self.frame.size.width, self.frame.size.height) animated:YES];
}

- (IBAction)onRegisterUserButtonTapped:(id)sender {

    [self endEditing:YES];
    
    if([self validateTextFields]) {
        NSString *lang = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
        NSMutableDictionary *registrationData = [NSMutableDictionary new];
        registrationData[@"terms"] = @"1";
        registrationData[@"email"] = _txtEmail.text;
        registrationData[@"full_name"] = _txtFirstAndLastName.text;
        NSInteger selectedCountryIndex = [_pickerCountry selectedRowInComponent:0];
        NSDictionary *dictCountry = _pickerCountrySource[selectedCountryIndex];
        registrationData[@"country"] = dictCountry[@"name"];
        registrationData[@"password"] = _txtPassword.text;
        registrationData[@"language"] = lang;
        registrationData[@"locale"] = lang;
        registrationData[@"device"] = @"iOS";
        registrationData[@"metric_type"] = @"metric";
        
        [self.activityIndicator startAnimating];
        
        [[CCAPI sharedInstance] registerNewUser:registrationData success:^(id responseObject)
        {
            
            [self.activityIndicator stopAnimating];
            NSDictionary *userData = responseObject;
            User *newUser = [User MR_createEntity];
            newUser.email = _txtEmail.text;
            newUser.country = dictCountry[@"name"];
            newUser.username = _txtFirstAndLastName.text;
            newUser.idUserValue = [userData[@"id"] longValue];
            newUser.magraAccessToken = userData[@"token"];
            newUser.authenticationType = CCAuthenticationTypeEmail;
            [[CCAPI sharedInstance] setUser:newUser];
            [[CCAPI sharedInstance] saveUserWithCompletion:nil];
            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            [appDelegate updateMainScreen];
            
        } failure:^(NSError *error) {

            [self.activityIndicator stopAnimating];
            NSMutableDictionary *userInfo = [error.userInfo mutableCopy];
            NSInteger statusCode = [[userInfo objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
            
            if(statusCode == 406)
            {
                [UIAlertView showWithTitle:[TSLanguageManager localizedString:@"Error"]
                                   message:[TSLanguageManager localizedString:@"This email address is already registered in our system!"]
                         cancelButtonTitle:@"Ok"
                         otherButtonTitles:nil
                                  tapBlock:nil];
            } else
            {
                [UIAlertView showWithTitle:[TSLanguageManager localizedString:@"Error"]
                                   message:[TSLanguageManager localizedString:@"An error occurred. Please try again."]
                         cancelButtonTitle:@"Ok"
                         otherButtonTitles:nil
                                  tapBlock:nil];
            }
        }];
        
    }
    
}
@end
