//
//  CCMealTableFoodCell.m
//  ContarCalorias

#import "CCMealTableFoodCell.h"
#import "CCConstants.h"

@implementation CCMealTableFoodCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self initialization];
    }
    return self;
}

- (void)awakeFromNib
{
    [self initialization];
}
- (void)initialization
{
    NSAttributedString *attTitle = [[NSAttributedString alloc] initWithString:[TSLanguageManager localizedString:@"See recipe"] attributes:@{NSForegroundColorAttributeName:CCColorOrange,NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    [_btnSeeRecipe setAttributedTitle:attTitle forState:UIControlStateNormal];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onSeeRecipeButtonTapped:(id)sender {
    [self.delegate didTapSeeRecipeButtonWithRecipeId:self.recipeId];
}
@end
