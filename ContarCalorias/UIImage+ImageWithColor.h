//
//  UIImage+ImageWithColor.h
//  Cynny
//
//  Created by Lucian on 03/02/14.
//  Copyright (c) 2014 Cynny Social Cloud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageWithColor)

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

@end
