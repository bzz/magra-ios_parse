//
//  CCConstants.h
//  ContarCalorias
//
//  Created by andres portillo on 30/05/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kCCITunesSandBoxMode 1
#define CCDatabaseVersion 4

    //user defaults
#define kCCTrialExpirationMessageWasShown @"trialExpirationMessageWasShown"
#define kCCAppIntroductionWasPresented   @"launchIntroductionWasPresented"
#define kCCLanguageWasSelected   @"languageWasSelected"
#define kCCInitialSynchronizationWasPerformed   @"initialSynchronizationWasPerformed"
#define kCCInitialConfigurationWasSet   @"initialConfigurationWasSet"
#define kCCSortKeyTitle   @"title"
#define kCCSortKeyName    @"name"
#define kCCNewUserStage @"newUserStage"
#define kCCFirstDayDate @"firstDayDate"
#define kCCMealNotificationsEnabled @"mealNotificationsEnabled"
#define kCCArticlesNotificationsEnabled @"articlesNotificationsEnabled"
#define kCCUsersWakeupTime @"usersWakeupTime"
#define kCCMaxRevisionNumber @"maxRevision"
#define kCCDataChangesSet @"changesSet"
#define kCCUseFacebookAvatar @"useFacebookAvatar"
#define kCCLastSyncDate @"lastSyncDate"
#define kCCUnreadMessagesCount   @"unreadMessagesCount"
#define kCCLaunchesCount   @"launchesCount"
#define kCCDatabaseVersion   @"databaseVersion"
#define kCCMealNotificationType  @"mealNotificationType"


    //API Methods
#define  kCCAPIValidateReceiptsSandbox  @"users/validate_receipts_sandbox"
#define  kCCAPIValidateReceipts  @"users/validate_receipts"
#define  kCCAPIGetUser  @"users/get_user"
#define  kCCAPISetUser  @"users/set_user"
#define  kCCAPIUserStartedTrial  @"users/start_trial"
#define  kCCAPIGetPosts  @"sync/posts"
#define  kCCAPIGetTodaysArticlesUrl  @"sync/missions"
#define  kCCAPISyncUrl  @"sync/data"
#define  kCCAPIFBLoginUrl  @"users/fblogin"
#define  kCCAPIGpLoginUrl   @"users/googlelogin"
#define  kCCAPIEmailLoginUrl  @"users/login"
#define  kCCAPIRegisterUserUrl   @"users/register"
#define  kCCAPIUpdateImageUrl   @"users/update_image"
#define  kCCAPIForgotPasswordUrl   @"users/forgot_password"
#define  kCCAPIUserProfileUrl   @"users/profile"
#define  kCCAPIUserReminderUrl   @"users/reminder"
#define  kCCAPIUserGetDiaryurl   @"diary/information"
#define  kCCAPIUpdateReminderUrl   @"users/update_reminder"
#define  kCCAPIUpdatePasswordUrl   @"users/updatepassword"
#define  kCCAPIRegisterTokenUrl   @"users/register_token"
#define  kCCAPISubmitFood @"foods/insert_food"
#define  kCCAPISubmitFoodServing @"foods/insert_food_serving"
#define  kCCAPIGetInTouchEsUrl @"email/contact_form_es"
#define  kCCAPIGetInTouchPtUrl @"email/contact_form_pt"
#define  kCCAPIGetAllDiets @"diets/plans"
#define  kCCAPIGetDietDetails @"diets/get_plan_info"
#define  kCCAPIGetRecipe @"diets/get_recipe_info"
#define  kCCAPIGetTodaysRecipes @"diets/recipes"



    //UI Application
#define CCColorGreen [UIColor colorWithRed:173.0/255.0f green:202.0/255.0 blue:0.0/255.0 alpha:1]
#define CCColorGray [UIColor colorWithRed:121.0/255.0f green:121.0/255.0 blue:121.0/255.0 alpha:1]
#define CCColorDarkGray [UIColor colorWithRed:54.0/255.0f green:58.0/255.0 blue:69.0/255.0 alpha:1]
#define CCColorOrange [UIColor colorWithRed:255.0/255.0f green:171.0/255.0 blue:45.0/255.0 alpha:1]
#define CCColorProgressViewTrackColor [UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1]
#define CCColorProgressViewWaterColor [UIColor colorWithRed:89.0/255.0 green:197.0/255.0 blue:253.0/255.0 alpha:1]

    //notification keys
#define kCCShouldTakeUserToDietsSection  @"shouldTakeUserToDietsSection"
#define kCCDietCellContentWasTapped  @"dietCellContentWasTapped"
#define kCCNotificationNewMessageReceived  @"newMessageReceived"
#define kCCNotificationUserStartedDiet   @"userStartedDiet"
#define kCCNotificationMealWasLogged   @"mealWasLogged"
#define kCCNotificationWaterWasLogged   @"waterWasLogged"
#define kCCNotificationExerciseWasLogged   @"exerciseWasLogged"
#define kCCNotificationHomeScreenNeedsUpdate   @"homeScreenNeedsUpdate"
#define kCCNotificationUserCreatedWeightLossPlan   @"notificationUserCreatedWeightLossPlan"
#define kCCNotificationHomeScreenTopBarNeedsUpdate   @"homeScreenTopBarNeedsUpdate"
#define kCCNotificationSynchronizationStarted   @"synchronizationStarted"
#define kCCNotificationSynchronizationEnded   @"synchronizationEnded"
#define kCCNotificationProFeaturesPurchased   @"notificationProFeaturesPurchased"
#define kCCNotificationUserProfileWasUpdated   @"notificationUserProfileWasUpdated"
#define kCCNotificationUserUpdatedWeight   @"notificationUserUpdatedWeight"

    //local notifications keys
#define kCCLocalNotificationBreakfastNotification   @"breakfastNotification"
#define kCCLocalNotificationFirstSnackNotification   @"lunchNotification"
#define kCCLocalNotificationLunchNotification   @"lunchNotification"
#define kCCLocalNotificationSecondSnackNotification   @"lunchNotification"
#define kCCLocalNotificationDinnerNotification   @"dinnerNotification"
#define kCCLocalNotificationArticlesNotification   @"articlesNotification"

    //App Values
#define kCCDailyWaterMililitersGoal 2000


    //IAP
#define kCCMonthlySubscriptionIAPSharedSecret @"d78e4bcf6c784e07a803e7ebed3a5a09"
#define kCCMontlySubstriptionProductId @"com.magra.magraapp.prouser"


    //enum types
typedef NS_ENUM(NSUInteger, CCMealNotificationType)
{
    CCMealNotificationTypeLogMeal            = 0,
    CCMealNotificationTypeCookRecipe
};
typedef NS_ENUM(NSUInteger, CCAuthenticationType)
{
    CCAuthenticationTypeEmail            = 0,
    CCAuthenticationTypeFacebook
};

typedef NS_ENUM(NSUInteger, CCActivityLevel) {
    CCActivityLevelSedentary = 0,
    CCActivityLevelSlightlyActive,
    CCActivityLevelActive,
    CCActivityLevelVeryActive
};
typedef NS_ENUM(NSUInteger, CCUserStage) {
    CCUserStageSetWeightLossPlan = 0,
    CCUserStageFirstStage,
};
typedef NS_ENUM(NSUInteger, CCGoalLevel) {
    CCGoalLevelLoseHalfKilo = 0,
    CCGoalLevelLoseQuarterKilo,
    CCGoalLevelMaintainWeight,
    CCGoalLevelGainQuarterKilo,
    CCGoalLevelGainHalfKilo
};
typedef NS_ENUM(NSUInteger, CCGender) {
    CCGenderFemale    = 0,
    CCGenderMale
};

typedef NS_ENUM(NSUInteger,CCUnitSystem)
{
    CCUnitSystemMetric    = 0,
    CCUnitSystemBritish
};
typedef NS_ENUM(NSUInteger, CCDiaryItemType)
{
    CCDiaryItemTypeMeal,
    CCDiaryItemTypeExercise,
    CCDiaryItemTypeWater,
    CCDiaryItemTypeArticle,
    
};
typedef NS_ENUM(NSUInteger, CCAPIOperationEntity)
{
    CCAPIOperationEntityFood,
    CCAPIOperationEntityExercise,
    CCAPIOperationEntityWater,
    CCAPIOperationEntityUserWeight
};
typedef NS_ENUM(NSUInteger, CCMealType)
{
    CCMealTypeBreakfast = 0,
    CCMealTypeLunch,
    CCMealTypeDinner,
    CCMealTypeSnack
    
};

typedef NS_ENUM(NSInteger, CCHomeVCModeSetPlanSection) {
    CCHomeVCModeSetPlanSectionSetPlan,
};
typedef NS_ENUM(NSInteger, CCHomeVCModeNormalSection) {
    CCHomeVCModeNormalSectionCalories,
    CCHomeVCModeNormalSectionWater
};
typedef NS_ENUM(NSInteger, CCAPIOperationMethod) {
    CCAPIOperationMethodUpdate = 0,
    CCAPIOperationMethodCreate,
    CCAPIOperationMethodDelete,
};
