//
//  CCWeightCheckInVC.h
//  ContarCalorias
//
//  Created by andres portillo on 11/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@protocol CCWeightCheckInDelegate
- (void)newWeightWasloggedInTheWeightCheckInVC;
@end

@interface CCWeightCheckInVC : GAITrackedViewController

@property (nonatomic, assign) id <CCWeightCheckInDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *txtWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblEnteryourWeight;

@end
