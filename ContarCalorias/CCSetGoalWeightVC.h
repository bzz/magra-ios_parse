//
//  CCSetGoalWeightVC.h
//  ContarCalorias
//
//  Created by andres portillo on 13/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class User;
@interface CCSetGoalWeightVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblScreenTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblScreenDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtGoalWeight;
@end
