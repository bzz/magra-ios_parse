//
//  AddWaterViewController.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 09/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GAITrackedViewController.h"
@interface AddWaterViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UIView *viewWrapperGlass;
@property (weak, nonatomic) IBOutlet UILabel *lbWaterObjective;
@property (weak, nonatomic) IBOutlet UIImageView *imgGlass;

@end
