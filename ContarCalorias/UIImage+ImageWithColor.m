//
//  UIImage+ImageWithColor.m
//  Cynny
//
//  Created by Lucian on 03/02/14.
//  Copyright (c) 2014 Cynny Social Cloud. All rights reserved.
//

#import "UIImage+ImageWithColor.h"

#import <QuartzCore/QuartzCore.h>

@implementation UIImage (ImageWithColor)

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, (CGRect){.size = size});
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
