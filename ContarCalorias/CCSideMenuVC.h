//
//  CCSideMenuVC.h
//  ContarCalorias
//
//  Created by andres portillo on 20/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSpotyViewController.h"


@interface CCSideMenuVC :MGSpotyViewController
- (void)setDietsVC;
- (void)setNutritionistVC;
@end
