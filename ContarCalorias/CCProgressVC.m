//
//  CCProgressVC.m
//  ContarCalorias
//
//  Created by andres portillo on 12/8/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCProgressVC.h"

#import "APLineChart.h"
#import "SWRevealViewController.h"
#import "CCWeightCheckInVC.h"
#import "CCAPI.h"
#import "User.h"
#import "UserValue.h"
#import "CCUtils.h"

@interface CCProgressVC ()<CCWeightCheckInDelegate,SWRevealViewControllerDelegate>

@end

@implementation CCProgressVC
{
    NSArray *_arrUserWeightEntries;
    User *_user;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
        //Set Screen Name (google analytics)
    self.screenName = @"Progress Screen";
    
    // Do any additional setup after loading the view.
    [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Progress"]];
  
    self.revealViewController.delegate = self;
   
    [self setBackButton];
    _user = [[CCAPI sharedInstance] getUser];
    
    [self getWeightEntries];
    self.lineChart.arrMarkers = _arrUserWeightEntries;
    
        //get user weight entries
    if (_user.didCreateWeightLossPlanValue) {
        
        if (_arrUserWeightEntries.count == 0) {
        
            [[CCAPI sharedInstance] logUserWeight:[_user getInitialWeight] sync:NO completion:^{
                [[CCAPI sharedInstance] logUserWeight:[_user getInitialWeight] sync:YES completion:^{
                    [self updateUI];
                }];
            }];
            
            
        } else {
            [self updateUI];
        }
        
    } else {
        
        [self.view.subviews setValue:@0 forKeyPath:@"alpha"];
        [_lblNoData setAlpha:1];
    }
    [self setLocalizedStrings];
}
- (void)updateUI
{
    [self getWeightEntries];
    
    self.lineChart.arrMarkers = _arrUserWeightEntries;
    
    self.lineChart.userGoalWeight = [_user getGoalWeight];
    [_lineChart render];
    [_lblGoalWeight setText:[NSString stringWithFormat:@"%.1f %@",_user.getGoalWeight,[_user getWeightUnit]]];
    
    CGFloat lostWeight = [_user getInitialWeight] - [_user getCurrentWeight];
    [_lblLostWeight setText:[NSString stringWithFormat:@"%.1f %@",lostWeight, [_user getWeightUnit]]];
    
    [_lblGoal setText:[NSString stringWithFormat:@"%d %@",MAX(0,_user.daysToReachDesiredWeight), _user.daysToReachDesiredWeight == 1? [TSLanguageManager localizedString:@"Day"]:[TSLanguageManager localizedString:@"Days" ]]];
    
}
- (void)setLocalizedStrings
{
    [_lblTitleGoal setText:[TSLanguageManager localizedString:@"Goal"]];
    [_lblTitleGoalWeight setText:[TSLanguageManager localizedString:@"Goal Weight"]];
    [_lblTitleLostWeight setText:[TSLanguageManager localizedString:@"Lost Weight"]];
    [_btnUpdateWeight setTitle:[TSLanguageManager localizedString:@"Update your weight"] forState:UIControlStateNormal];
}
- (void)getWeightEntries
{
    NSFetchRequest *request = [UserValue MR_requestAllInContext:[NSManagedObjectContext MR_defaultContext]];
    NSSortDescriptor *ascendingSorter = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
    
    _arrUserWeightEntries = [[UserValue MR_executeFetchRequest:request inContext:[NSManagedObjectContext MR_defaultContext]] sortedArrayUsingDescriptors:@[ascendingSorter]];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setBackButton{
    
    UIButton *btn = [[UIButton alloc] init];
    [btn setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
    [btn setFrame:CGRectMake(0, 0, 20, 15)];
    [btn addTarget:self.revealViewController action:@selector(revealToggleAnimated:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* button = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = button;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Weight Check In"]) {
         CCWeightCheckInVC *weightCheckInVC = (CCWeightCheckInVC *)((UINavigationController *)segue.destinationViewController).topViewController;
        weightCheckInVC.delegate = self;
        
    }
}
#pragma mark - CCWeightCheckIn Delegate
- (void)newWeightWasloggedInTheWeightCheckInVC
{
    _user = [[CCAPI sharedInstance] getUser];
    [_user updateDateReachingDesiredWeight];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kCCNotificationUserUpdatedWeight  object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
    CGFloat lostWeight = [_user getInitialWeight] - [_user getCurrentWeight];
    [_lblLostWeight setText:[NSString stringWithFormat:@"%.1f %@",lostWeight, [_user getWeightUnit]]];
    [_lblGoal setText:[NSString stringWithFormat:@"%d %@",MAX(0,_user.daysToReachDesiredWeight), _user.daysToReachDesiredWeight == 1? [TSLanguageManager localizedString:@"Day"]:[TSLanguageManager localizedString:@"Days"]]];
    
    [self getWeightEntries];
    self.lineChart.arrMarkers = _arrUserWeightEntries;
    [self.lineChart render];
    [self.lineChart setNeedsDisplay];
}
#pragma mark - SWRevealViewController Delegate
- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        self.view.userInteractionEnabled = YES;
    } else {
        self.view.userInteractionEnabled = NO;
    }
}

@end
