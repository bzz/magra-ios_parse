//
//  AddWaterViewController.m
//  ContarCalorias
//
//  Created by Adrian Ghitun on 09/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "AddWaterViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "ViewUtils.h"
#import "UIAlertView+Blocks.h"
#import "CCAPI.h"
#import "UserWater.h"

@interface AddWaterViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewWrapper;
@end

@implementation AddWaterViewController
{
    int _glassAmount,_todaysDrinkedGlasses;
    UIActivityIndicatorView *_activityIndicator;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
        //Set Screen Name (Google Analytics)
    self.screenName = @"Add Water Screen";
    
    [_scrollViewWrapper setContentSize:CGSizeMake(320, 504)];
    [ViewUtils drawBasicShadowOnView:_viewWrapperGlass];
    _viewWrapperGlass.layer.cornerRadius = 4;
    [self.view setBackgroundColor:[ViewUtils lightGrayColor]];
    ;
    _todaysDrinkedGlasses =MIN(([[CCAPI sharedInstance] getTodaysDrinkedWaterMililiters]/ 230), 9);
    _glassAmount = 1;
    [self setImgGlassWithAmount:_glassAmount];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[TSLanguageManager localizedString:@"Save"]
 style:UIBarButtonItemStylePlain target:self action:@selector(saveWater)];
   
    [self setLocalizedStrings];
}
- (void)setLocalizedStrings
{
    [_lbWaterObjective setText:[TSLanguageManager localizedString:@"Goal: 8 glasses per day (1 Glass = 230 ml)"]];
    [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Add Water"]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addWater:(id)sender {
    _glassAmount = _glassAmount + 1 > 8 ? _glassAmount : _glassAmount + 1;
    [self setImgGlassWithAmount:_glassAmount];
    
}
- (IBAction)substractWater:(id)sender {
    _glassAmount = _glassAmount - 1 == 0  ? _glassAmount : _glassAmount - 1;
    [self setImgGlassWithAmount: _glassAmount];
}

-(void)saveWater
{
    NSInteger mililiters = _glassAmount * 230;
    [[CCAPI sharedInstance] logWaterItemWithMililiters:mililiters];
    [[NSNotificationCenter defaultCenter] postNotificationName:kCCNotificationWaterWasLogged object:nil userInfo:nil];
}

-(void)setImgGlassWithAmount:(int) amount{
  [_imgGlass setImage:[UIImage imageNamed:[NSString stringWithFormat:@"g%i", amount]]];
}
@end
