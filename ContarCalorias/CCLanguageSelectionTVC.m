//
//  CCLanguageSelectionTVC.m
//  ContarCalorias
//
//  Created by andres portillo on 05/09/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCLanguageSelectionTVC.h"
#import "FontAwesomeKit.h"
#import "CCAPI.h"
typedef NS_ENUM(NSInteger, CCLanguage) {
    CCLanguageSpanish,
    CCLanguagePortuguese
};
@interface CCLanguageSelectionTVC ()

@end

@implementation CCLanguageSelectionTVC
{
    NSArray *_arrLanguages;
    CCLanguage _selectedLanguage;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[TSLanguageManager selectedLanguage] isEqualToString:@"es"]) {
        _selectedLanguage = CCLanguageSpanish;
    } else {
        _selectedLanguage = CCLanguagePortuguese;
    }
    _arrLanguages = @[[TSLanguageManager localizedString:@"Spanish"],[TSLanguageManager localizedString:@"Portuguese"]];
    [self.tableView setTableFooterView:[UIView new]];

    FAKFontAwesome *checkIcon = [FAKFontAwesome checkIconWithSize:20];
    [checkIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:[TSLanguageManager localizedString:@"Select"] style:UIBarButtonItemStyleBordered target:self action:@selector(onLanguageSelected)]];
    
    [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Language"]];
    
    [self.tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadScreen)
                                                 name:@"LanguageWasChanged"
                                               object:nil];

}
- (void)reloadScreen
{
    [self.tableView reloadData];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)onLanguageSelected
{
    NSString *selectedLanguage = _selectedLanguage == 0? @"es":@"pt";
    if (! [[TSLanguageManager selectedLanguage] isEqualToString:selectedLanguage]) {
        [TSLanguageManager setSelectedLanguage:selectedLanguage];
        
        NSString *lang =  [TSLanguageManager selectedLanguage];
        [[NSUserDefaults standardUserDefaults] setObject:lang forKey:@"lang"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LanguageWasChanged" object:nil userInfo:nil];
        [[CCAPI sharedInstance] deleteAllPosts];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrLanguages.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier  = @"Language Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    [(UILabel *)[cell viewWithTag:1] setText:[_arrLanguages objectAtIndex:indexPath.row]];
    if (indexPath.row == _selectedLanguage) {
    
        FAKFontAwesome *checkIcon = [FAKFontAwesome checkIconWithSize:20];
        [checkIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
        [cell setAccessoryView:[[UIImageView alloc] initWithImage:[checkIcon imageWithSize:CGSizeMake(16, 26)]]];
        
    } else {
        [cell setAccessoryView:[UIImageView new]];
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedLanguage = indexPath.row;
    [self.tableView reloadData];
}
@end
