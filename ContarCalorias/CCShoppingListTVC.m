//
//  CCShoppingListTVC.m
//  ContarCalorias
//
//  Created by andres portillo on 27/11/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCShoppingListTVC.h"
#import "MealPlan.h"
#import "ShoppingListItem.h"

@interface CCShoppingListTVC ()
@end

@implementation CCShoppingListTVC
{
    NSArray *_arrShoppingListItems;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _arrShoppingListItems = [_mealPlan.shoppingListItems allObjects];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrShoppingListItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"Shopping List Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if (! cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (fmodf(indexPath.row, 2) != 0) {
        [cell.contentView setBackgroundColor:[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1]];
    } else {
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
    ShoppingListItem *shoppingListItem = _arrShoppingListItems[indexPath.row];
    [(UILabel *)[cell.contentView viewWithTag:1] setText:[NSString stringWithFormat:@"• %@ (%@)",shoppingListItem.name,shoppingListItem.amount]];

    
    return cell;
}
- (IBAction)onDoneButtonTapped:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
@end
