//
//  CCSyncEngine.h
//  ContarCalorias
//
//  Created by andres portillo on 12/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CCHTTPClient;
@class User;
@interface CCSyncEngine : NSObject
@property (nonatomic, strong) CCHTTPClient *httpClient;
@property (nonatomic, strong) User *user;
- (void)performSynchronization;
@end
