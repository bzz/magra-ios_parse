//
//  CCPersistencyManager.h
//  ContarCalorias
//
//  Created by andres portillo on 5/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CCAPISyncOp;
@class Food;
@class Article;
@class Food_serving;
@class Exercise;
@class UserFood;
@interface CCPersistencyManager : NSObject

@property (nonatomic, strong) NSMutableArray *arrSessionLoadedPosts;
- (void )setFirstdayDate:(NSDate *)date;
- (void)addOpToQueue:(CCAPISyncOp *)op andSync:(BOOL)sync;
- (NSArray *)getPosts;
- (void)deleteAllPosts;
- (NSString *)getPostsFilePath;
- (NSString *)getPrivateDocsDir;
- (NSArray *)getTodaysLoggedMeals;
- (NSInteger)getTodaysConsumedCalories;
- (NSInteger)getTodaysBurnedCalories;
- (CGFloat)getTodaysConsumedCarbs;
- (CGFloat)getTodaysConsumedFats;
- (CGFloat)getTodaysConsumedProteins;
- (NSInteger)getTodaysDrinkedWaterMililiters;
- (NSArray *)getAllFoodsWithPredicate:(NSPredicate *)predicate;
- (NSArray *)getAllUserFoodsWithPredicate:(NSPredicate *)predicate;
- (NSArray *)getAllExercisesWithPredicate:(NSPredicate *)predicate;
- (NSArray *)getAllUserExercisesWithPredicate:(NSPredicate *)predicate;
- (void)logWaterItemWithMililiters:(NSInteger )mililiters;
- (void)deleteUserFood:(UserFood *)userFood;
- (void)updateUserFoodItemWithFood:(UserFood *)userFood withFoodServing:(Food_serving *)foodServing numberOfServings:(NSInteger)numberOfServings completed:(void (^)())completed;
- (void)logFoodItemWithFood:(Food *)food foodServing:(Food_serving *)foodServing numberOfServings:(NSInteger)numberOfServings andMealType:(CCMealType)mealType;
- (void)logExercise:(Exercise *)exercise withMinutes:(NSInteger)minutes burnedCalories:(NSInteger)burnedCalories;
- (void)logUserWeight:(CGFloat)weight sync:(BOOL)sync completion:(void (^)())success;
- (void)saveProfileImage:(UIImage *)profileImage;
- (void)saveFacebookProfileImage:(UIImage *)profileImage;
- (void)deleteFacebookProfileImage;
- (void)deleteProfileImage;
- (UIImage *)getProfileImage;
- (void)enrollUserInMealPlanWithId:(NSInteger)mealPlanId;
@end
