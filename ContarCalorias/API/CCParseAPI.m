//
//  CCParseAPI.m
//  ContarCalorias
//
//  Created by Anton Anisimov on 12/18/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCParseAPI.h"
#import <Parse/Parse.h>
#import "CCAPI.h"
#import "Article.h"
#import "CCPersistencyManager.h"

@interface CCParseAPI () {
        CCPersistencyManager *_persistencyManager;
}

@end

@implementation CCParseAPI

- (id)init
{
    self = [super init];
    if (self != nil) {
        
        _persistencyManager = [[CCPersistencyManager alloc] init];
//        _sessionManager = [[CCSessionManager alloc] init];
//        _httpClient = [[CCHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:CCAPIBaseURL]];
//        _notificationsManager = [[CCNotificationsManager alloc] init];
//        _syncEngine = [[CCSyncEngine alloc] init];
//        _syncEngine.httpClient = _httpClient;
//        _syncEngine.user = [self getUser];
        
    }
    return self;
}

+ (instancetype)sharedInstance {
    static CCParseAPI *_sharedInstance;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[CCParseAPI alloc] init];
    });
    return _sharedInstance;
}

#pragma mark - user actions

- (void)registerNewUser:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure {
    [PFUser enableAutomaticUser];
    PFUser *currentUser = [PFUser currentUser];
    currentUser[@"country"] = parameters[@"country"];
    currentUser[@"device"] = parameters[@"device"];
    currentUser.email = parameters[@"email"];
    currentUser.username = parameters[@"email"];
    currentUser[@"full_name"] = parameters[@"full_name"];
    currentUser[@"language"] = parameters[@"language"];
    currentUser[@"locale"] = parameters[@"locale"];
    currentUser[@"metric_type"] = parameters[@"metric_type"];
    currentUser.password = parameters[@"password"];
    currentUser[@"terms"] = parameters[@"terms"];

    [currentUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [PFUser logInWithUsernameInBackground:currentUser.username password:currentUser.password block:^(PFUser *user, NSError *error) {
                if (!error) {
                   if (success) success(@{@"id" : [[PFUser currentUser] objectId], @"token" : @""});
                    
                } else {
                    currentUser.email = nil;
                    currentUser.password = nil;
                    currentUser.email = nil;
                    if (failure) failure(error);
                }
            }];
        } else {
            currentUser.email = nil;
            currentUser.password = nil;
            currentUser.email = nil;
            if (failure) failure(error);
        }
    }];
}

- (void)resetUserPassword:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [PFUser requestPasswordResetForEmailInBackground:parameters[@"email"] block:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            if (success) success(@{@"message" : @"OK"});
        } else {
            if (failure) failure(error);
        }
    }];
}


- (void)openEmailAccountSession:(NSDictionary *)authenticationDetails success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [PFUser logInWithUsernameInBackground:authenticationDetails[@"email"]
                                 password:authenticationDetails[@"password"]
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            
                                        } else {
                                            if (failure) failure(error);
                                        }
                                    }];

//    po responseObject
//    {
//        id = 30091;
//        profile =     {
//            activity = "";
//            avatar = "";
//            birthday = 0;
//            "breakfast_time" = "<null>";
//            "calories_per_day" = 0;
//            country = Argentina;
//            "current_weight_kg" = 0;
//            "date_firstday" = "<null>";
//            "date_registration" = 1418935122;
//            "date_sync" = 1418935122;
//            "did_create_weight_loss_plan" = 0;
//            "dinner_time" = "<null>";
//            email = "tester@invoodoo.com";
//            "firstsnack_time" = "<null>";
//            "full_name" = tester;
//            gender = "";
//            "goal_weight_date" = "<null>";
//            "goal_weight_kg" = 0;
//            "goal_weight_kg_per_week" = 0;
//            "height_cm" = 0;
//            id = 30091;
//            "initial_weight_kg" = 0;
//            language = es;
//            locale = es;
//            "lunch_time" = "<null>";
//            "meal_plan_date" = "<null>";
//            "meal_plan_id" = 0;
//            "metric_type" = metric;
//            points = 0;
//            "secondsnack_time" = "<null>";
//            "session_token" = 3oy8jbhhcum8gwo8c4ow00cg484k8gs;
//            "social_facebook" = "";
//            "social_google" = "";
//            status = free;
//            "trial_date" = "<null>";
//            "wakeup_time" = "<null>";
//            "water_per_day" = 0;
//        };
//        token = 3oy8jbhhcum8gwo8c4ow00cg484k8gs;
//    }

}


#pragma mark - posts actions


- (void)getOlderPostsWithSuccess:(void (^)())success Failure:(void (^)(NSError *error))failure
{
    
    PFUser  *user = [PFUser currentUser];
    NSArray *currentPosts = [[CCAPI sharedInstance] getPosts];
    NSDate *oldestPostDate = [currentPosts valueForKeyPath:@"@min.date"];
    int unix_timestamp = 0;
    if (oldestPostDate == nil) {
        oldestPostDate = [NSDate date];
        unix_timestamp = [oldestPostDate timeIntervalSince1970];
        NSTimeZone* timeZone = [NSTimeZone localTimeZone];
        int offset = [timeZone secondsFromGMTForDate: oldestPostDate];
        unix_timestamp = unix_timestamp - offset;
    } else {
        unix_timestamp = [oldestPostDate timeIntervalSince1970];
    }
    
//    NSDictionary *params = @{@"session_token":@"",@"locale":[TSLanguageManager selectedLanguage],@"date":@(unix_timestamp),@"getolderposts":@YES};
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"Posts"];
    
    [query whereKey:@"date" greaterThan:@(unix_timestamp)];
//    [query whereKey:@"getolderposts" equalTo:@YES];
    [query whereKey:@"locale" equalTo:[TSLanguageManager selectedLanguage]];
    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            if (failure) failure(error);
        } else {
            NSArray *arrArticlesJson = objects;
            
            NSMutableArray *arrPostsObjects = [NSMutableArray new];
            
            for (NSDictionary *dictArticle in arrArticlesJson) {
                Article *newPost = [Article new];
                newPost.articleId = [[dictArticle objectForKey:@"id"] integerValue];
                newPost.day = [[dictArticle objectForKey:@"day"] integerValue];
                newPost.content = [dictArticle objectForKey:@"description"];
                newPost.locale = [dictArticle objectForKey:@"locale"];
//                newPost.title = [[dictArticle objectForKey:@"title"] xmlSimpleUnescapeString];
                newPost.title = [dictArticle objectForKey:@"title"];
                newPost.type = [dictArticle objectForKey:@"type"];
                newPost.date = [NSDate dateWithTimeIntervalSince1970:[[dictArticle objectForKey:@"date"] integerValue]];
                newPost.imageUrl = [dictArticle objectForKey:@"image"];
                [arrPostsObjects addObject:newPost];
            }
            if (_persistencyManager.arrSessionLoadedPosts.count == 0)
            {
                NSArray *postsToStore = [arrPostsObjects objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, MIN(arrPostsObjects.count,10))]];
                [NSKeyedArchiver archiveRootObject:postsToStore toFile:[[CCAPI sharedInstance] getPostsFilePath]];
            }
            
            
            NSArray *arrAllPosts =  _persistencyManager.arrSessionLoadedPosts != nil? [_persistencyManager.arrSessionLoadedPosts arrayByAddingObjectsFromArray:arrPostsObjects]:arrPostsObjects;
            
            _persistencyManager.arrSessionLoadedPosts = [[arrAllPosts sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]]] mutableCopy];
            success();
        }
    }];
    
}


- (void)sendEmailWithParameters:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    NSString *url = [[TSLanguageManager selectedLanguage] isEqualToString:@"es"]?kCCAPIGetInTouchEsUrl:kCCAPIGetInTouchPtUrl;
    NSMutableDictionary *mutableParams = [params mutableCopy];
    [mutableParams setObject:url forKey:@"url"];
    
    [PFCloud callFunctionInBackground:@"sendEmail" withParameters:mutableParams block:^(id object, NSError *error) {
        if (!error) {
            if (failure) failure(error);
        } else {
            if (success) success(object);
        }
    }];
}

#pragma mark - diets




@end
