//
//  CCAPISyncOpe.h
//  ContarCalorias
//
//  Created by andres portillo on 5/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>
@class User;

@interface CCAPISyncOp : NSObject
@property(nonatomic) CCAPIOperationEntity operationEntity;
@property(nonatomic, strong) NSDictionary *diaryItemJSON;
@property(nonatomic) CCAPIOperationMethod method;
@property(nonatomic, strong) NSDate *date;
@property(nonatomic, strong) User *user;
@end
