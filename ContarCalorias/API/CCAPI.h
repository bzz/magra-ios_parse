//
//  ServiceLocator.h
//  ContarCalorias
//
//  Created by Lucian Gherghel on 12/02/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>
@class User;
@class CCHTTPClient;
@class UserWater;
@class UserFood;
@class Exercise;
@class Food_serving;
@class Food;
@class Article;
@class CCAPISyncOp;
@interface CCAPI : NSObject


+ (instancetype)sharedInstance;

- (NSString *)getVersionNumber;

    //Facebook Helper
- (void)getFacebookProfileWithCompletion:(void (^)(id responseObject))completion;
- (void)loginWithFacebook;

    //Persistency Manager
- (void)setFirstdayDate:(NSDate *)date;
- (NSArray *)getPosts;
- (void)deleteAllPosts;
- (NSString *)getPostsFilePath;
- (NSString *)getPrivateDocsDir;
- (NSArray *)getTodaysLoggedMeals;
- (void)updateUserFoodItemWithFood:(UserFood *)userFood withFoodServing:(Food_serving *)foodServing numberOfServings:(NSInteger)numberOfServings completed:(void (^)())completed;
- (void)deleteUserFood:(UserFood *)userFood;
- (NSArray *)getAllFoodsWithPredicate:(NSPredicate *)predicate;
- (NSArray *)getAllUserFoodsWithPredicate:(NSPredicate *)predicate;
- (NSArray *)getAllExercisesWithPredicate:(NSPredicate *)predicate;
- (NSArray *)getAllUserExercisesWithPredicate:(NSPredicate *)predicate;
- (NSInteger)getTodaysConsumedCalories;
- (NSInteger)getTodaysBurnedCalories;
- (CGFloat)getTodaysConsumedCarbs;
- (CGFloat)getTodaysConsumedFats;
- (CGFloat)getTodaysConsumedProteins;
- (NSInteger)getTodaysDrinkedWaterMililiters;
- (BOOL)todaysWaterGoalWasReached;
- (BOOL)todaysMealGoalWasReached;
- (void)logWaterItemWithMililiters:(NSInteger )mililiters;
- (void)logFoodItemWithFood:(Food *)food foodServing:(Food_serving *)foodServing numberOfServings:(NSInteger)numberOfServings andMealType:(CCMealType)mealType;
- (void)logExercise:(Exercise *)exercise withMinutes:(NSInteger)minutes burnedCalories:(NSInteger)burnedCalories;
- (void)saveProfileImage:(UIImage *)profileImage;
- (void)saveFacebookProfileImage:(UIImage *)profileImage;
- (UIImage *)getProfileImage;
- (void)logUserWeight:(CGFloat)weight sync:(BOOL)sync completion:(void (^)())success;
- (void)enrollUserInMealPlanWithId:(NSInteger)mealPlanId;
    //Sync Engine
- (void)addOpToQueue:(CCAPISyncOp *)op;
- (void)performSynchronization;

    //Notification Manager
- (void)scheduleLocalNotificationsForMeals;
- (void)scheduleLocalNotificationsForArticles;
- (void)cancelLocalNotificationsForMeals;
- (void)cancelLocalNotificationsForArticles;

    //Contar Calorias Values
- (NSString *)getLocalizedGenderNameForCCGenderIndex:(CCGender)gender;
- (NSString *)getLocalizedActivityLevelForCCActivityLevelIndex:(CCActivityLevel)activityLevel;


    //Session Manager
- (void)setApptentiveInitialValues;
- (void)userSubscriptionExpired;
- (BOOL)isProUser;
- (void)setUserAsProUser;
- (User *)getUser;
- (void)setUser:(User *)user;
- (void)setUsersActivityLevel:(CCActivityLevel)activityLevel;- (CGFloat)getMaximumRecommendedWeightForUser:(User *)user;
- (CGFloat)getMinimumRecommendedWeightForUser:(User *)user;
- (CGFloat)getIdealWeightMetricForUser:(User *)user;
- (void)destroyActiveSession;
- (void)loginToContarCaloriasWithFacebookToken:(NSString *)facebookAccessToken;
- (BOOL)isSessionAvailable;

    //HTTPClient
- (void)validateReceiptsWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)validateReceipts;
- (void)validateReceiptsSandbox;
- (CCHTTPClient *)getHttpClient;
- (void)getOlderPostsWithSuccess:(void (^)())success Failure:(void (^)(NSError *error))failure;
- (void)getPostUpdatesWithSuccess:(void (^)())success Failure:(void (^)(NSError *error))failure;
- (void)registerNewUser:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)resetUserPassword:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)openEmailAccountSession:(NSDictionary *)authenticationDetails success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)saveUserWithCompletion:(void (^)(void))completionBlock;
- (void)submitFood:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (NSArray *)getAvailableCountries;
- (void)sendEmailWithParameters:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)getAllDietsWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)getDietDetailsWithDietId:(NSInteger)dietId Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
;
- (void)getRecipeWithId:(NSInteger)recipeId Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)getTodaysMealsWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)getMagraUser;
- (void)updateMagraUser;
- (void)userStartedTrial;
@end
