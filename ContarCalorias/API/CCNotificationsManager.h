//
//  CCNotificationsManager.h
//  ContarCalorias
//
//  Created by andres portillo on 11/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCNotificationsManager : NSObject
- (void)scheduleLocalNotificationsForMeals;
- (void)scheduleLocalNotificationsForArticles;
- (void)cancelLocalNotificationsForMeals;
- (void)cancelLocalNotificationsForArticles;

@end
