//
//  CCPersistencyManager.m
//  ContarCalorias
//
//  Created by andres portillo on 5/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCPersistencyManager.h"
#import "Food.h"
#import "FoodServing.h"
#import "UserFood.h"
#import "Exercise.h"
#import "UserExercise.h"
#import "UserWater.h"
#import "UserValue.h"
#import "CCConstants.h"
#import "Article.h"
#import "CCUtils.h"
#import "CCAPI.h"
#import "CCAPISyncOp.h"
#import "ContarCalendar.h"
#import "User.h"
#import "MealPlan.h"

@implementation CCPersistencyManager
{
    ContarCalendar *_contarCalendar;
}

- (instancetype)init
{
    self = [super init];
    if (self != nil) {
        
       _contarCalendar = [ContarCalendar sharedInstance];
       _arrSessionLoadedPosts = [[self getPosts] mutableCopy];
    }
    
    return self;
}
- (NSPredicate *)todayPredicate
{
    NSPredicate *todayPredicate;
    NSDate *date = _contarCalendar.dateShowing;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    NSDate *startDate = [calendar dateFromComponents:components];
    [components setMonth:0];
    [components setDay:0];
    [components setYear:0];
    [components setHour:24];
    
    NSDate *endDate = [calendar dateByAddingComponents:components toDate:startDate options:0];
    todayPredicate = [NSPredicate predicateWithFormat:@"(date >= %@) AND (date < %@)",startDate,endDate];
    return  todayPredicate;
}
- (void )setFirstdayDate:(NSDate *)date{
    
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:kCCFirstDayDate];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSArray *)getTodaysLoggedMeals
{
    return [self getAllUserFoodsWithPredicate:[self todayPredicate]];
}
- (NSArray *)getPosts
{
    if (! _arrSessionLoadedPosts) {
        NSArray *arrPosts = [NSKeyedUnarchiver unarchiveObjectWithFile:[self getPostsFilePath]];
        NSSortDescriptor *sortDescriptor = [[ NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        if (arrPosts == nil) {
            arrPosts = @[];
        }
        return [arrPosts sortedArrayUsingDescriptors:@[sortDescriptor]];

    }
    return _arrSessionLoadedPosts;
}
- (void)deleteAllPosts
{
    _arrSessionLoadedPosts = nil;
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:[self getPostsFilePath] error:&error];
    if (error) {
        NSLog(@"error deleting posts file");
    }
}
- (NSInteger)getTodaysConsumedCalories
{
    NSArray *arrDiaryUserFoods = [UserFood MR_findAllWithPredicate:[self todayPredicate] inContext:[NSManagedObjectContext MR_defaultContext]];
    __block NSInteger consumedCalories = 0;
    [arrDiaryUserFoods enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        //these checks are dirty I know, I didn't want them there, but I had no other choice (talk to vlad)
        if([((UserFood *)obj).foodServing.serving_type isEqualToString:@"g"] || [((UserFood *)obj).foodServing.serving_type isEqualToString:@"ml"]) {
            consumedCalories  = (int)(consumedCalories + (((UserFood *)obj).foodServing.caloriesValue / 100) * ((UserFood *)obj).numberOfServingsValue);
        } else {
            consumedCalories  = (int)( consumedCalories + ((UserFood *)obj).foodServing.caloriesValue * ((UserFood *)obj).numberOfServingsValue);
            
        }
    }];
   
    return consumedCalories;
}
- (CGFloat)getTodaysConsumedCarbs
{
   
    NSArray *arrDiaryUserFoods = [UserFood MR_findAllWithPredicate:[self todayPredicate] inContext:[NSManagedObjectContext MR_defaultContext]];
    
    __block CGFloat consumedCarbs = 0;
    [arrDiaryUserFoods enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([((UserFood *)obj).foodServing.serving_type isEqualToString:@"g"] || [((UserFood *)obj).foodServing.serving_type isEqualToString:@"ml"]) {
            consumedCarbs  =  consumedCarbs + (((UserFood *)obj).foodServing.carbohydratesValue / 100) * ((UserFood *)obj).numberOfServingsValue;
        } else {
            consumedCarbs  =  consumedCarbs + ((UserFood *)obj).foodServing.carbohydratesValue * ((UserFood *)obj).numberOfServingsValue;
        }
    }];
    
    return consumedCarbs;
}
- (CGFloat)getTodaysConsumedFats
{
   NSArray *arrDiaryUserFoods = [UserFood MR_findAllWithPredicate:[self todayPredicate] inContext:[NSManagedObjectContext MR_defaultContext]];
    
    __block CGFloat consumedFats = 0;
    [arrDiaryUserFoods enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([((UserFood *)obj).foodServing.serving_type isEqualToString:@"g"] || [((UserFood *)obj).foodServing.serving_type isEqualToString:@"ml"]) {
            consumedFats  = consumedFats + (((UserFood *)obj).foodServing.fatValue / 100) * ((UserFood *)obj).numberOfServingsValue;
        } else {
            consumedFats  = consumedFats + ((UserFood *)obj).foodServing.fatValue * ((UserFood *)obj).numberOfServingsValue;
        }
        
    }];
    
    return consumedFats;
}
- (CGFloat)getTodaysConsumedProteins
{
    NSArray *arrDiaryUserFoods = [UserFood MR_findAllWithPredicate:[self todayPredicate] inContext:[NSManagedObjectContext MR_defaultContext]];
    
    __block CGFloat consumedProteins = 0;
    [arrDiaryUserFoods enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if([((UserFood *)obj).foodServing.serving_type isEqualToString:@"g"] || [((UserFood *)obj).foodServing.serving_type isEqualToString:@"ml"]) {
            consumedProteins  = consumedProteins + (((UserFood *)obj).foodServing.proteinValue / 100) * ((UserFood *)obj).numberOfServingsValue;
        } else {
            consumedProteins  = consumedProteins + ((UserFood *)obj).foodServing.proteinValue * ((UserFood *)obj).numberOfServingsValue;
        }
        
    }];
    
    return consumedProteins;
}
- (NSInteger)getTodaysBurnedCalories
{
    NSArray *arrDiaryUserExercises = [UserExercise MR_findAllWithPredicate:[self todayPredicate] inContext:[NSManagedObjectContext MR_defaultContext]];
    
    NSNumber *burnedCalories = [arrDiaryUserExercises valueForKeyPath:@"@sum.totalCalories"] ;
    return burnedCalories.integerValue;
}

- (NSInteger)getTodaysDrinkedWaterMililiters
{
    NSArray *arrDiaryUserWater = [UserWater MR_findAllWithPredicate:[self todayPredicate] inContext:[NSManagedObjectContext MR_defaultContext]];
    
    NSNumber *totalDrinkedWater = [arrDiaryUserWater valueForKeyPath:@"@sum.mililiters"];
    
    return totalDrinkedWater.integerValue;
}

- (NSArray *)getAllFoodsWithPredicate:(NSPredicate *)predicate
{
    NSFetchRequest *request = [Food MR_requestAllWithPredicate:predicate inContext:[NSManagedObjectContext MR_defaultContext]];
    [request setFetchBatchSize:20];
    [request setResultType:NSDictionaryResultType];
    [request setPropertiesToFetch:@[@"title",@"food_id",@"normalizedTitle",@"brand",@"isSearchable"]];
    NSArray *results = [Food MR_executeFetchRequest:request inContext:[NSManagedObjectContext MR_defaultContext]];
    return  results;
}
- (NSArray *)getAllUserFoodsWithPredicate:(NSPredicate *)predicate
{
    NSArray *arrUserFoods = [UserFood MR_findAllWithPredicate:predicate inContext:[NSManagedObjectContext MR_defaultContext]];
    return  arrUserFoods;
}
- (NSArray *)getAllExercisesWithPredicate:(NSPredicate *)predicate
{
    NSArray *arrExercises = [Exercise MR_findAllWithPredicate:predicate inContext:[NSManagedObjectContext MR_defaultContext]];
    return  arrExercises;
}
- (NSArray *)getAllUserExercisesWithPredicate:(NSPredicate *)predicate
{
    NSArray *arrExercises = [UserExercise MR_findAllWithPredicate:predicate inContext:[NSManagedObjectContext MR_defaultContext]];
    return  arrExercises;
}
- (void)addOpToQueue:(CCAPISyncOp *)op andSync:(BOOL)sync
{
    NSInteger newItemID = [[op.diaryItemJSON objectForKey:@"id"] integerValue];
    
    NSMutableArray *arrOperationsToRemove = [NSMutableArray new];
    NSMutableArray *arrChangesSet =  [((NSArray *)[[NSUserDefaults standardUserDefaults] objectForKey:kCCDataChangesSet]) mutableCopy];
    __block BOOL deleteOpUnnecessary = NO, convertOpToCreateType = NO;
    [arrChangesSet enumerateObjectsUsingBlock:^(NSData *opData, NSUInteger idx, BOOL *stop) {
        
        CCAPISyncOp *decodedOp = [NSKeyedUnarchiver unarchiveObjectWithData:opData];
        if (op.method == CCAPIOperationMethodUpdate) {
            
            
            NSInteger itemId = abs([[decodedOp.diaryItemJSON objectForKey:@"id"] integerValue]);
                if(itemId == newItemID)
                {
                    [arrOperationsToRemove addObject:opData];
                    
                    if(decodedOp.method == CCAPIOperationMethodCreate)
                        convertOpToCreateType = YES;
                }
            
        } else if (op.method == CCAPIOperationMethodDelete) {
            
            if (decodedOp.method == CCAPIOperationMethodCreate) {
                NSInteger itemId = abs([[decodedOp.diaryItemJSON objectForKey:@"id"] integerValue]);
                if(itemId == newItemID)
                {
                    [arrOperationsToRemove addObject:opData];
                    deleteOpUnnecessary = YES;
                }
            } else if (decodedOp.method == CCAPIOperationMethodUpdate) {
                
                [arrOperationsToRemove addObject:opData];
            }
        }
        
        
    }];
    
    [arrOperationsToRemove enumerateObjectsUsingBlock:^(NSData *opData, NSUInteger idx, BOOL *stop) {

        [arrChangesSet removeObject:opData];
        
    }];
    
    if (convertOpToCreateType) {
        
        op.method = CCAPIOperationMethodCreate;
        NSMutableDictionary *dictJson = [op.diaryItemJSON mutableCopy];
        NSString *convertedId = [NSString stringWithFormat:@"-%d",newItemID];
        [dictJson setObject:convertedId forKey:@"id"];
        op.diaryItemJSON  = dictJson;
        
    }
    NSData *encodedOp = [NSKeyedArchiver archivedDataWithRootObject:op];
    
    if (! deleteOpUnnecessary) {
        [arrChangesSet addObject:encodedOp];
    }
    
    NSArray *arrFinal = [NSArray arrayWithArray:arrChangesSet];
    [[NSUserDefaults standardUserDefaults] setObject:arrFinal forKey:kCCDataChangesSet];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (sync)
        [[CCAPI sharedInstance] performSynchronization];
}
- (void)logWaterItemWithMililiters:(NSInteger )mililiters
{
    UserWater *water = [UserWater MR_createEntity];
    NSNumber *maxId = [[UserWater MR_findAll] valueForKeyPath:@"@max.userWater_id"];
    water.date = _contarCalendar.dateShowing;
    water.userWater_id = @(maxId.integerValue + 1);
    
    water.mililitersValue = mililiters;
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        
        CCAPISyncOp *waterOp = [CCAPISyncOp new];
        NSString *date = [CCUtils stringFromTimestamp:_contarCalendar.dateShowing];
        NSString *userWaterId = [NSString stringWithFormat:@"-%@",water.userWater_id];
        NSDictionary *diaryItemJSON =  @{@"id":userWaterId,@"quantity":water.mililiters,@"date":date};
        waterOp.diaryItemJSON = diaryItemJSON;
        waterOp.date = [NSDate date];
        waterOp.operationEntity = CCAPIOperationEntityWater;
        waterOp.method = CCAPIOperationMethodCreate;
        
        [self addOpToQueue:waterOp andSync:YES];

    }];
}
- (void)deleteUserFood:(UserFood *)userFood
{
    CCAPISyncOp *foodOp = [CCAPISyncOp new];
    NSString *userFoodId = [NSString stringWithFormat:@"%@",userFood.userFood_id];
    NSDictionary *diaryItemJSON =  @{@"id":userFoodId};
    foodOp.diaryItemJSON = diaryItemJSON;
    foodOp.date = _contarCalendar.dateShowing;
    foodOp.operationEntity = CCAPIOperationEntityFood;
    foodOp.method = CCAPIOperationMethodDelete;

    [userFood MR_deleteInContext:[NSManagedObjectContext MR_defaultContext]];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        [self addOpToQueue:foodOp andSync:YES];
    }];
   
    
    
}
- (void)updateUserFoodItemWithFood:(UserFood *)userFood withFoodServing:(FoodServing *)foodServing numberOfServings:(NSInteger)numberOfServings completed:(void (^)())completed{
    
    ContarCalendar *calendar = [ContarCalendar sharedInstance];
    userFood.foodServing = foodServing;
    userFood.numberOfServingsValue = numberOfServings;

    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        
        CCAPISyncOp *foodOp = [CCAPISyncOp new];
        NSString *date = [CCUtils stringFromTimestamp:userFood.date];
        NSString *timeOfDay = @"";
        
        switch (userFood.meanEnumTypeValue) {
            case CCMealTypeBreakfast:
                timeOfDay = @"breakfast";
                break;
            case CCMealTypeLunch:
                timeOfDay = @"lunch";
                break;
            case CCMealTypeDinner:
                timeOfDay = @"dinner";
                break;
            case CCMealTypeSnack:
                timeOfDay = @"snack";
                break;
            default:
                break;
        }
        NSString *userFoodId = [NSString stringWithFormat:@"%@",userFood.userFood_id];
        NSDictionary *diaryItemJSON =  @{@"id":userFoodId,@"id_food":userFood.food.food_id,@"quantity":userFood.numberOfServings,@"date":date,@"timeofday":timeOfDay,@"id_food_serving":foodServing.id_food_serving};
        foodOp.diaryItemJSON = diaryItemJSON;
        foodOp.date = calendar.dateShowing;
        foodOp.operationEntity = CCAPIOperationEntityFood;
        foodOp.method = CCAPIOperationMethodUpdate;
        
        [self addOpToQueue:foodOp andSync:YES];
        
        completed();
    }];
    
  
}
- (void)logFoodItemWithFood:(Food *)food foodServing:(FoodServing *)foodServing numberOfServings:(NSInteger)numberOfServings andMealType:(CCMealType)mealType
{
    UserFood *userFood = [UserFood MR_createEntity];
    userFood.foodServing =  foodServing;
    userFood.numberOfServingsValue = numberOfServings;
    userFood.food = food;
    userFood.date = _contarCalendar.dateShowing;
    userFood.meanEnumTypeValue = mealType;
    NSNumber *maxId = [[UserFood MR_findAll] valueForKeyPath:@"@max.userFood_id"];
    userFood.userFood_id = @(maxId.integerValue + 1);
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        
        CCAPISyncOp *foodOp = [CCAPISyncOp new];
        NSString *date = [CCUtils stringFromTimestamp:userFood.date];
        NSString *timeOfDay = @"";
        
        switch (mealType) {
            case CCMealTypeBreakfast:
                timeOfDay = @"breakfast";
                break;
            case CCMealTypeLunch:
                timeOfDay = @"lunch";
                break;
            case CCMealTypeDinner:
                timeOfDay = @"dinner";
                break;
            case CCMealTypeSnack:
                timeOfDay = @"snack";
                break;
            default:
                break;
        }
        NSString *userFoodId = [NSString stringWithFormat:@"-%@",userFood.userFood_id];
        NSDictionary *diaryItemJSON =  @{@"id":userFoodId,@"id_food":food.food_id,@"quantity":userFood.numberOfServings,@"date":date,@"timeofday":timeOfDay,@"id_food_serving":foodServing.id_food_serving};
        foodOp.diaryItemJSON = diaryItemJSON;
        foodOp.date = userFood.date;
        foodOp.operationEntity = CCAPIOperationEntityFood;
        foodOp.method = CCAPIOperationMethodCreate;
        
        [self addOpToQueue:foodOp andSync:YES];
        
    }];
    
}
- (void)logExercise:(Exercise *)exercise withMinutes:(NSInteger)minutes burnedCalories:(NSInteger)burnedCalories
{
    UserExercise *userExercise  = [UserExercise MR_createEntity];
    userExercise.minutes = @(minutes);
    userExercise.totalCalories = @(burnedCalories);
    userExercise.exercise = exercise;
    NSNumber *maxId = [[UserExercise MR_findAll] valueForKeyPath:@"@max.userExercise_id"];
    userExercise.userExercise_id = @(maxId.integerValue + 1);
    userExercise.date = [NSDate date];
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        
        CCAPISyncOp *exOp = [CCAPISyncOp new];
        NSString *date = [CCUtils stringFromTimestamp:userExercise.date];
       
        NSString *userExerciseId = [NSString stringWithFormat:@"-%@",userExercise.userExercise_id];
        NSDictionary *diaryItemJSON =  @{@"id":userExerciseId,@"id_exercise":exercise.exercise_id,@"time":userExercise.minutes,@"date":date,@"calories":userExercise.totalCalories};
        exOp.diaryItemJSON = diaryItemJSON;
        exOp.date = userExercise.date;
        exOp.operationEntity = CCAPIOperationEntityExercise;
        exOp.method = CCAPIOperationMethodCreate;
        
        [self addOpToQueue:exOp andSync:YES];

    }];
    
}
- (void)logUserWeight:(CGFloat)weight sync:(BOOL)sync completion:(void (^)())success
{
    User *user = [[CCAPI sharedInstance] getUser];
    [user setCurrentWeight:weight];
    [[CCAPI sharedInstance] setUser:user];
    [[CCAPI sharedInstance] saveUserWithCompletion:nil];
    UserValue *userWeight = [UserValue MR_findFirstWithPredicate:[self todayPredicate] sortedBy:@"userValueId" ascending:NO];
    BOOL shouldUpdate = NO;
    if (userWeight == nil || [UserValue MR_numberOfEntities].integerValue < 2) {
    
        userWeight = [UserValue MR_createEntity];
        userWeight.valueValue = weight;
        userWeight.date = [NSDate date];
        userWeight.type = @"Weight";
        NSNumber *maxId = [[UserValue MR_findAll] valueForKeyPath:@"@max.userValueId"];
        userWeight.userValueId = @(maxId.integerValue + 1);
        
    } else {
        shouldUpdate = YES;
        userWeight.valueValue = weight;
        
    }
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL successful, NSError *error) {
        
        if(success)
            success();
        
        if (shouldUpdate) {
            CCAPISyncOp *weightOp = [CCAPISyncOp new];
            NSString *date = [CCUtils stringFromTimestamp:userWeight.date];
            NSString *userWeightId = [NSString stringWithFormat:@"%@",userWeight.userValueId];
            NSDictionary *diaryItemJSON =  @{@"id":userWeightId,@"weight":userWeight.value,@"date":date};
            weightOp.diaryItemJSON = diaryItemJSON;
            weightOp.date = userWeight.date;
            weightOp.operationEntity = CCAPIOperationEntityUserWeight;
            weightOp.method = CCAPIOperationMethodUpdate;
            
            [self addOpToQueue:weightOp andSync:sync];
        } else {
            CCAPISyncOp *weightOp = [CCAPISyncOp new];
            NSString *date = [CCUtils stringFromTimestamp:userWeight.date];
            NSString *userWeightId = [NSString stringWithFormat:@"-%@",userWeight.userValueId];
            NSDictionary *diaryItemJSON =  @{@"id":userWeightId,@"weight":userWeight.value,@"date":date};
            weightOp.diaryItemJSON = diaryItemJSON;
            weightOp.date = userWeight.date;
            weightOp.operationEntity = CCAPIOperationEntityUserWeight;
            weightOp.method = CCAPIOperationMethodCreate;
            [self addOpToQueue:weightOp andSync:sync];
        }
    }];
}
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    } else {
        NSLog(@"file at %@ excluded from backup",URL);
    }
    return success;
}
- (void)saveProfileImage:(UIImage *)profileImage
{
    NSData *pngData = UIImagePNGRepresentation(profileImage);
    NSString *privDir = [self getPrivateDocsDir];
    NSString *filePath = [privDir stringByAppendingPathComponent:@"profile-pic.png"];
    if ([pngData writeToFile:filePath atomically:YES]) {
        NSLog(@"profile pic copied");
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:privDir]];
    } else {
         NSLog(@"error copying profile pic");
    }
}
- (void)saveFacebookProfileImage:(UIImage *)profileImage
{
    NSData *pngData = UIImagePNGRepresentation(profileImage);
    NSString *privDir = [self getPrivateDocsDir];
    
    NSString *filePath = [privDir stringByAppendingPathComponent:@"facebook-profile-pic.png"];
    if ([pngData writeToFile:filePath atomically:YES]) {
        NSLog(@"fb profile pic copied");
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:privDir]];
    } else {
        NSLog(@"error copying fb profile pic");
    }
   
    
}
- (NSString *)getPrivateDocsDir {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory = [paths objectAtIndex:0];
    NSString *privateDocumentsDirectory  = [libraryDirectory stringByAppendingPathComponent:@"Private Documents"];
    
    NSError *error;
    BOOL isDir;
    if (! [[NSFileManager defaultManager]  fileExistsAtPath:privateDocumentsDirectory isDirectory:&isDir]) {
        
        [[NSFileManager defaultManager] createDirectoryAtPath:privateDocumentsDirectory withIntermediateDirectories:YES attributes:nil error:&error];
        
        if(error) {
            NSLog(@"error creating directory");
        }
        
    }
    return privateDocumentsDirectory;
    
}
- (NSString *)getPostsFilePath {
    NSString *postsFilePath = [[self getPrivateDocsDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"articles-%@.plist",[TSLanguageManager selectedLanguage]]];
    return  postsFilePath;
}
- (UIImage *)getProfileImage
{
    User *user = [[CCAPI sharedInstance] getUser];
    NSData *pngData;
    if(user.authenticationTypeValue == CCAuthenticationTypeFacebook && [[NSUserDefaults standardUserDefaults] boolForKey:kCCUseFacebookAvatar]) {
        
        NSString *privDir = [self getPrivateDocsDir];
        NSString *filePath = [privDir stringByAppendingPathComponent:@"facebook-profile-pic.png"];
        pngData = [NSData dataWithContentsOfFile:filePath];
    } else {
        NSString *privDir = [self getPrivateDocsDir];
        NSString *filePath = [privDir stringByAppendingPathComponent:@"profile-pic.png"];
        pngData = [NSData dataWithContentsOfFile:filePath];
    }
    UIImage *image = [UIImage imageWithData:pngData];
    
    return image;
}
- (void)deleteFacebookProfileImage {
    NSString *privDir = [self getPrivateDocsDir];
    NSString *filePath = [privDir stringByAppendingPathComponent:@"facebook-profile-pic.png"];
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:filePath error: &error];
    if (error)
        NSLog(@"error deleting facebook profile pic");
}
- (void)deleteProfileImage {
    NSString *privDir = [self getPrivateDocsDir];
    NSString *filePath = [privDir stringByAppendingPathComponent:@"profile-pic.png"];
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:filePath error: &error];
    if (error)
        NSLog(@"error deleting facebook profile pic");
}
- (void)enrollUserInMealPlanWithId:(NSInteger)mealPlanId
{
    MealPlan *mealPlan = [MealPlan MR_findFirstByAttribute:@"idMealPlan" withValue:@(mealPlanId)];
    User *user = [[CCAPI sharedInstance] getUser];
    user.mealPlan = mealPlan;
    user.mealPlanStartDate = [NSDate date];
    [[CCAPI sharedInstance] setUser:user];
    [[CCAPI sharedInstance] saveUserWithCompletion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kCCNotificationUserStartedDiet object:nil];
    
}

@end
