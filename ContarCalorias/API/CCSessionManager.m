//
//  CCAPIHelper.m
//  ContarCalorias
//
//  Created by andres portillo on 6/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCSessionManager.h"
#import "AppDelegate.h"
#import "User.h"
#import  "UserValue.h"
#import "UserWater.h"
#import "UserFood.h"
#import "Article.h"
#import "CCHTTPClient.h"
#import "CCUtils.h"
#import "CCAPI.h"
#import <FacebookSDK/FacebookSDK.h>

@implementation CCSessionManager
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.user = [self getUser];
    }
    return self;
}
- (BOOL)isSessionAvailable
{
    BOOL magraSessionChached = NO;
    User *user= [self getUser];
   
    if(user!= nil) {
        magraSessionChached = YES;
    }

    return magraSessionChached;
}
- (void)destroyCurrentSession
{
    [self removeAllDiaryItems];
    [FBSession.activeSession closeAndClearTokenInformation];
}
- (void)saveUserWithCompletion:(void (^)(void))completionBlock;
{
    self.user = [self getUser];
    if(_user.didCreateWeightLossPlanValue) {
        
        CGFloat calories= 0;
        CGFloat weight = _user.currentWeightKgValue;
        CGFloat height = _user.heightCmValue;
        CCGender gender = _user.genderValue;
        NSInteger activityLevel = _user.activityLevelValue;
        NSInteger age = _user.ageValue;
        
        if(gender  == CCGenderMale) {
            
            calories = 66.4730 + (13.7516 * weight) + (5.0033 * height) - (6.7550 * age);
            
        }else {
            
            calories = 655.0955 + (9.5634 * weight) + (1.8496 * height) - (4.6756 * age);
            
        }
        CGFloat weeklyWeightToLose =(_user.initialWeightKgValue > 90 ?   _user.initialWeightKgValue * 0.01 :_user.initialWeightKgValue * 0.005) * 1000;
       
        CGFloat gainValue = (weeklyWeightToLose * 3889) / 500;
        CGFloat activityValue = [self getActivityIndexForStage:activityLevel];
        calories = calories * ((100.0 + activityValue)/100.0) - (gainValue/7);
        _user.dailyCaloriesValue = lroundf(calories);
        _user.dailyCarbsValue = [self getCarbsForCalories:_user.dailyCaloriesValue];
        _user.dailyProteinsValue = [self getProteinsForCalories:_user.dailyCaloriesValue];
        _user.dailyFatsValue = [self getFatForCalories:_user.dailyCaloriesValue];
    
        if(! _user.dateReachingDesiredWeight) {
            [_user updateDateReachingDesiredWeight];
        }
    }

    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        
            if (completionBlock) {
                completionBlock();
            }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCCNotificationUserProfileWasUpdated object:nil];
    }];
   
}

- (NSInteger)getActivityIndexForStage:(NSInteger)stage {
    NSArray *arrActivityIndexes = @[@20,@35,@40,@50,@50];
    return  [arrActivityIndexes[stage] integerValue];
}
- (NSInteger)getCarbsForCalories:(NSInteger)calories {
    
    CGFloat carbs = (calories * 0.55 / 4);
    
    return lroundf(carbs);
}

- (NSInteger)getProteinsForCalories:(NSInteger)calories {
    
    CGFloat proteins = (calories * 0.15 / 4);
    
    return lroundf(proteins);
}

- (NSInteger)getFatForCalories:(NSInteger)calories {
    
    CGFloat fat = (calories * 0.30 / 9);
    
    return lroundf(fat);
}
- (User *)getUser
{
    User *user = [User MR_findFirst];
    return user;
}
- (void)removeAllDiaryItems
{
    [User MR_truncateAllInContext:[NSManagedObjectContext MR_defaultContext]];
    [UserValue MR_truncateAllInContext:[NSManagedObjectContext MR_defaultContext]];
    [UserFood MR_truncateAllInContext:[NSManagedObjectContext MR_defaultContext]];
    [UserWater MR_truncateAllInContext:[NSManagedObjectContext MR_defaultContext]];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];

    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:[[CCAPI sharedInstance] getPrivateDocsDir] error:&error];
    if (error) {
        NSLog(@"error deleting posts file");
    }
    
}


@end
