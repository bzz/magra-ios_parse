//
//  CCHTTPClient.h
//  ContarCalorias
//
//  Created by andres portillo on 5/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFHTTPSessionManager.h>
@class User;
@interface CCHTTPClient : AFHTTPSessionManager

@property(nonatomic,strong) User *user;


- (instancetype)initWithBaseURL:(NSURL *)url;
- (void)validateReceipts:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)validateReceiptsSandbox:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)registerNewUser:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)resetUserPassword:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)syncWithServerWithParams:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)getTodaysArticlesWithLastDay:(NSInteger)lastDay andCurrentDay:(NSInteger)currentDay Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)getAllDietsWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)getDietDetailsWithDietId:(NSInteger)dietId Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)getRecipeWithId:(NSInteger)recipeId Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)getTodaysMealsWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)openEmailAccountSession:(NSDictionary *)authenticationDetails success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)exchangeToken:(NSString *)token network:(NSString *)network success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
- (void)submitFood:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)submitFoodServing:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)sendEmailWithParameters:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)getPosts:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)getMagraUser:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)setMagraUser:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;
- (void)userStartedTrial:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

@end
