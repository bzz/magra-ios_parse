//
//  CCHTTPClient.m
//  ContarCalorias
//
//  Created by andres portillo on 5/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCHTTPClient.h"
#import "CCAPISyncOp.h"
#import "UserWater.h"
#import "User.h"
#import "CCAPI.h"
#import "CCUtils.h"
#import "MealPlan.h"

@implementation CCHTTPClient

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}
- (void)registerNewUser:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

{
    
    [self POST:kCCAPIRegisterUserUrl parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)  {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
    
    
}
- (void)resetUserPassword:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [self POST:kCCAPIForgotPasswordUrl  parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)  {
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
  
        failure(error);
    
    }];
}
- (void)getAllDietsWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [self POST:kCCAPIGetAllDiets parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)  {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}
- (void)getRecipeWithId:(NSInteger)recipeId Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [self POST:kCCAPIGetRecipe parameters:@{@"id_recipe":@(recipeId)} success:^(NSURLSessionDataTask *task, id responseObject)  {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}
- (void)getDietDetailsWithDietId:(NSInteger)dietId Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    NSDictionary *params = @{@"id_plan":@(dietId)};
    [self POST:kCCAPIGetDietDetails parameters:params success:^(NSURLSessionDataTask *task, id responseObject)  {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
    
}
- (void)getTodaysMealsWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    User * user = [[CCAPI sharedInstance] getUser];
    NSDate *mealPlanStartDate =  user.mealPlanStartDate;
    NSInteger mealPlanId = user.mealPlan.idMealPlanValue;
    
     NSInteger currentDay = [CCUtils numberOfDaysWithinFirstDate:mealPlanStartDate andSecondDate:[NSDate date]]  + 1;
    NSDictionary *params = @{@"id_plan":@(mealPlanId ),@"dayofplan":@(currentDay)};
    [self POST:kCCAPIGetTodaysRecipes parameters:params success:^(NSURLSessionDataTask *task, id responseObject)  {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
    
}
- (void)syncWithServerWithParams:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    
    [self POST:kCCAPISyncUrl  parameters:params success:^(NSURLSessionDataTask *task, id responseObject)  {
        
            success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
      
             failure(error);
   
    }];

}
- (void)getTodaysArticlesWithLastDay:(NSInteger)lastDay andCurrentDay:(NSInteger)currentDay Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    
    User  *user = [[CCAPI sharedInstance] getUser] ;
    NSString *token = user.magraAccessToken;
    
    NSString *lang = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    
    NSDictionary *params = @{@"session_token": token,@"fromDay":@(lastDay),@"toDay":@(currentDay),@"locale":lang};
    NSLog(@"%@",self.baseURL);
    [self POST:kCCAPIGetTodaysArticlesUrl  parameters:params  success:^(NSURLSessionDataTask *task, id responseObject)  {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        failure(error);
        
    }];
}
- (void)openEmailAccountSession:(NSDictionary *)authenticationDetails success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    
    
    [self POST:kCCAPIEmailLoginUrl  parameters:authenticationDetails  success:^(NSURLSessionDataTask *task, id responseObject)  {
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
    
}
- (void)exchangeToken:(NSString *)token
              network:(NSString *)network
              success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    [self POST:kCCAPIFBLoginUrl  parameters:@{@"token":token,@"locale":[TSLanguageManager selectedLanguage],@"device":@"iOS"} success:^(NSURLSessionDataTask *task, id responseObject)  {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(task,error);
    }];
    
}
- (void)submitFood:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [self POST:kCCAPISubmitFood  parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)  {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}
- (void)submitFoodServing:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [self POST:kCCAPISubmitFoodServing  parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)  {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}
- (void)sendEmailWithParameters:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    NSString *url = [[TSLanguageManager selectedLanguage] isEqualToString:@"es"]?kCCAPIGetInTouchEsUrl:kCCAPIGetInTouchPtUrl;
    [self POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject)  {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}
- (void)getPosts:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [self POST:kCCAPIGetPosts parameters:params success:^(NSURLSessionDataTask *task, id responseObject)  {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}
- (void)validateReceiptsSandbox:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [self POST:kCCAPIValidateReceiptsSandbox parameters:params success:^(NSURLSessionDataTask *task, id responseObject)  {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}
- (void)validateReceipts:(NSDictionary *)params success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [self POST:kCCAPIValidateReceipts  parameters:params success:^(NSURLSessionDataTask *task, id responseObject)  {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}
- (void)getMagraUser:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [self POST:kCCAPIGetUser  parameters:params success:^(NSURLSessionDataTask *task, id responseObject)  {
        NSInteger code = ((NSHTTPURLResponse *) task.response).statusCode;
        if (code == 207) {
            User *user = [[CCAPI sharedInstance] getUser];
            user.lastModified = [NSDate dateWithTimeIntervalSince1970:[responseObject[@"last_modified"] integerValue]];
            success(responseObject);
            
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}
- (void)setMagraUser:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    
    [self POST:kCCAPISetUser  parameters:params success:^(NSURLSessionDataTask *task, id responseObject)  {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}
- (void)userStartedTrial:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [self POST:kCCAPIUserStartedTrial  parameters:params success:^(NSURLSessionDataTask *task, id responseObject)  {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}
@end
