            //
//  CCSyncEngine.m
//  ContarCalorias
//
//  Created by andres portillo on 12/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCSyncEngine.h"
#import "CCHTTPClient.h"
#import "CCAPISyncOp.h"
#import "User.h"
#import "CCAPI.h"
#import "CCUtils.h"
#import "UserFood.h"
#import "Food.h"
#import "Exercise.h"
#import "FoodServing.h"
#import "UserWater.h"
#import "UserExercise.h"
#import "UserValue.h"
#import "Article.h"
#import "CCConstants.h"
#import "MealPlan.h"

@implementation CCSyncEngine
{
    NSArray *_arrTempChangesSet;
    BOOL _isSynchronizing;
}

- (void)performSynchronization
{
    if (! _isSynchronizing) {
        
        _isSynchronizing = YES;
        
        if (kCCITunesSandBoxMode) {
            [[CCAPI sharedInstance] validateReceiptsSandbox];
        } else {
            [[CCAPI sharedInstance] validateReceipts];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCCNotificationSynchronizationStarted object:nil];
        NSDictionary *params = [self getParamsForSyncRequest];
        
        [_httpClient syncWithServerWithParams:params Success:^(id responseObject) {
            
            _isSynchronizing = NO;
            
            NSDictionary *dictChangeSets = [responseObject objectForKey:@"changeSets"];
            
            NSNumber *maxRevision = [responseObject objectForKey:@"maxRevision"];
            
            if ([dictChangeSets valueForKey:@"tracking_food"] != nil) {
                
                NSDictionary *dictTrackingFood = [dictChangeSets objectForKey:@"tracking_food"];
                NSArray *arrCreate = [dictTrackingFood objectForKey:@"create"];
                NSArray *arrUpdate = [dictTrackingFood objectForKey:@"update"];
                NSArray *arrDelete = [dictTrackingFood objectForKey:@"delete"];
                NSDictionary *dictFoodAttributes = [dictTrackingFood objectForKey:@"attributes"];
                
                for (id newItemId in arrCreate) {
                    
                    NSDictionary *dictNewUserFood = [dictFoodAttributes objectForKey:[newItemId stringValue]];
                    
                    if ([dictNewUserFood objectForKey:@"id_old"]) {
                        
                        NSNumber *oldId = [dictNewUserFood objectForKey:@"id_old"];
                        NSNumber *finalId = [dictNewUserFood objectForKey:@"id_final"];
                        UserFood *userFood = [UserFood MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"userFood_id = %@",@(-1 * oldId.integerValue)]];
                        userFood.userFood_id = finalId;
                        userFood.user = _user;
                    } else {
                        
                        UserFood *newUserFood = [UserFood MR_createEntity];
                        newUserFood.userFood_id = [dictNewUserFood objectForKey:@"id_final"];
                        newUserFood.numberOfServings = [dictNewUserFood objectForKey:@"quantity"];
                        newUserFood.date = [NSDate dateWithTimeIntervalSince1970:[[dictNewUserFood objectForKey:@"date"] integerValue]];
                        if ([[dictNewUserFood objectForKey:@"timeofday"] isEqualToString:@"breakfast"]) {
                            newUserFood.meanEnumTypeValue = CCMealTypeBreakfast;
                        } else if ([[dictNewUserFood objectForKey:@"timeofday"] isEqualToString:@"lunch"]) {
                            newUserFood.meanEnumTypeValue = CCMealTypeLunch;
                        } else if ([[dictNewUserFood objectForKey:@"timeofday"] isEqualToString:@"dinner"]) {
                            newUserFood.meanEnumTypeValue = CCMealTypeDinner;
                        } else if ([[dictNewUserFood objectForKey:@"timeofday"] isEqualToString:@"snack"]) {
                            newUserFood.meanEnumTypeValue = CCMealTypeSnack;
                        }
                        
                        NSNumber *foodId = [dictNewUserFood objectForKey:@"id_food"];
                        Food *food = [Food MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"food_id = %@",foodId]];
                        
                        NSNumber *foodServingId = [dictNewUserFood objectForKey:@"id_food_serving"];
                        FoodServing *foodServing = [FoodServing MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"id_food_serving = %@",foodServingId]];
                        newUserFood.food = food;
                        newUserFood.foodServing = foodServing;
                        newUserFood.user = _user;
                    }
                    
                    
                }
                
                for (id newItemId in arrUpdate) {
                    
                    NSDictionary *dictUserFood = [dictFoodAttributes objectForKey:[newItemId stringValue]];
                    NSNumber *userFoodId = [dictUserFood objectForKey:@"id_final"];
                    UserFood *newUserFood = [UserFood MR_findFirstByAttribute:@"userFood_id" withValue:userFoodId];
                    newUserFood.numberOfServings = [dictUserFood objectForKey:@"quantity"];
                    
                    if ([[dictUserFood objectForKey:@"timeofday"] isEqualToString:@"breakfast"]) {
                        newUserFood.meanEnumTypeValue = CCMealTypeBreakfast;
                    } else if ([[dictUserFood objectForKey:@"timeofday"] isEqualToString:@"lunch"]) {
                        newUserFood.meanEnumTypeValue = CCMealTypeLunch;
                    } else if ([[dictUserFood objectForKey:@"timeofday"] isEqualToString:@"dinner"]) {
                        newUserFood.meanEnumTypeValue = CCMealTypeDinner;
                    } else if ([[dictUserFood objectForKey:@"timeofday"] isEqualToString:@"snack"]) {
                        newUserFood.meanEnumTypeValue = CCMealTypeSnack;
                    }
                    
                    NSNumber *foodId = [dictUserFood objectForKey:@"id_food"];
                    Food *food = [Food MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"food_id = %@",foodId]];
                    NSNumber *foodServingId = [dictUserFood objectForKey:@"id_food_serving"];
                    FoodServing *foodServing = [FoodServing MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"id_food_serving = %@",foodServingId]];
                    newUserFood.food = food;
                    newUserFood.foodServing = foodServing;
                    newUserFood.user = _user;
                }
                for (id newItemId in arrDelete) {
                    
                    NSDictionary *dictUserFood = [dictFoodAttributes objectForKey:[newItemId stringValue]];
                    NSNumber *userFoodId = [dictUserFood objectForKey:@"id_final"];
                    UserFood *newUserFood = [UserFood MR_findFirstByAttribute:@"userFood_id" withValue:userFoodId];
                    if (newUserFood)
                        [newUserFood MR_deleteInContext:[NSManagedObjectContext MR_defaultContext]];
                    
                }
            }
            if ([dictChangeSets valueForKey:@"tracking_exercise"] != nil) {
                
                NSDictionary *dictTrackingExercise = [dictChangeSets objectForKey:@"tracking_exercise"];
                NSArray *arrCreate = [dictTrackingExercise objectForKey:@"create"];
                NSArray *arrUpdate = [dictTrackingExercise objectForKey:@"update"];
                NSArray *arrDelete = [dictTrackingExercise objectForKey:@"delete"];
                NSDictionary *dictExerciseAttributes = [dictTrackingExercise objectForKey:@"attributes"];
                
                for (id newItemId in arrCreate) {
                    
                    NSDictionary *dictNewUserExercise = [dictExerciseAttributes objectForKey:[newItemId stringValue]];
                    
                    if ([dictNewUserExercise objectForKey:@"id_old"]) {
                        
                        NSNumber *oldId = [dictNewUserExercise objectForKey:@"id_old"];
                        NSNumber *finalId = [dictNewUserExercise objectForKey:@"id_final"];
                        UserExercise *userExercise = [UserExercise MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"userExercise_id = %@",@(-1 * oldId.integerValue)]];
                        userExercise.userExercise_id = finalId;
                        userExercise.user = _user;
                    } else {
                        
                        UserExercise  *newUserExercise = [UserExercise MR_createEntity];
                        newUserExercise.userExercise_id = [dictNewUserExercise objectForKey:@"id_final"];
                        newUserExercise.totalCalories = [dictNewUserExercise objectForKey:@"calories"];
                        newUserExercise.date = [NSDate dateWithTimeIntervalSince1970:[[dictNewUserExercise objectForKey:@"date"] integerValue]];
                        newUserExercise.minutes = [dictNewUserExercise objectForKey:@"time"];
                        
                        NSNumber *exerciseId = [dictNewUserExercise objectForKey:@"id_exercise"];
                        Exercise *exercise = [Exercise MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"exercise_id = %@",exerciseId]];
                        newUserExercise.exercise = exercise;
                        newUserExercise.user = _user;
                    }
                }
                for (id newItemId in arrUpdate) {
                    
                    NSDictionary *dictUserExercise = [dictExerciseAttributes objectForKey:[newItemId stringValue]];
                    NSNumber *userExerciseId = [dictUserExercise objectForKey:@"id_final"];
                    UserExercise *newUserExercise = [UserExercise MR_findFirstByAttribute:@"userExercise_id" withValue:userExerciseId];
                    newUserExercise.minutes = [dictUserExercise objectForKey:@"time"];
                    newUserExercise.totalCalories = [dictUserExercise objectForKey:@"calories"];
                    NSNumber *exerciseId = [dictUserExercise objectForKey:@"id_exercise"];
                    Exercise *exercise = [Exercise MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"exercise_id = %@",exerciseId]];
                    newUserExercise.exercise = exercise;
                    newUserExercise.user = _user;
                    
                }
                for (id newItemId in arrDelete) {
                    
                    NSDictionary *dictUserExercise = [dictExerciseAttributes objectForKey:[newItemId stringValue]];
                    NSNumber *userExerciseId = [dictUserExercise objectForKey:@"id_final"];
                    UserExercise *newUserExercise = [UserExercise MR_findFirstByAttribute:@"userFood_id" withValue:userExerciseId];
                    if (newUserExercise)
                        [newUserExercise MR_deleteInContext:[NSManagedObjectContext MR_defaultContext]];
                    
                }
            }
            if ([dictChangeSets valueForKey:@"tracking_water"] !=nil) {
                
                NSDictionary *dictTrackingWater = [dictChangeSets objectForKey:@"tracking_water"];
                NSArray *arrCreate = [dictTrackingWater objectForKey:@"create"];
                NSArray *arrUpdate = [dictTrackingWater objectForKey:@"update"];
                NSArray *arrDelete = [dictTrackingWater objectForKey:@"delete"];
                NSDictionary *dictWaterAttributes = [dictTrackingWater objectForKey:@"attributes"];
                
                for (id newItemId in arrCreate) {
                    
                    NSDictionary *dictNewUserWater = [dictWaterAttributes objectForKey:[newItemId stringValue]];
                    
                    if ([dictNewUserWater objectForKey:@"id_old"]) {
                        
                        NSNumber *oldId = [dictNewUserWater objectForKey:@"id_old"];
                        NSNumber *finalId = [dictNewUserWater objectForKey:@"id_final"];
                        UserWater *userWater = [UserWater MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"userWater_id = %@",oldId]];
                        userWater.userWater_id = finalId;
                        userWater.user = _user;
                        
                    } else {
                        
                        UserWater *newUserWater = [UserWater MR_createEntity];
                        newUserWater.date = [NSDate dateWithTimeIntervalSince1970:[[dictNewUserWater objectForKey:@"date"] integerValue]];
                        newUserWater.userWater_id = [dictNewUserWater objectForKey:@"id_final"];
                        newUserWater.mililiters = [dictNewUserWater objectForKey:@"quantity"];
                        newUserWater.user = _user;
                    }
                }
                for (id newItemId in arrUpdate) {
                    
                    NSDictionary *dictNewUserWater = [dictWaterAttributes objectForKey:[newItemId stringValue]];
                    NSNumber *userWaterId = [dictNewUserWater objectForKey:@"id_final"];
                    UserWater *userWater = [UserWater MR_findFirstByAttribute:@"userWater_id" withValue:userWaterId];
                    if (userWater) {
                        userWater.userWater_id = [dictNewUserWater objectForKey:@"id_final"];
                        userWater.mililiters = [dictNewUserWater objectForKey:@"quantity"];
                        userWater.user = _user;
                    }
                    
                }
                for (id newItemId in arrDelete) {
                    NSDictionary *dictNewUserWater = [dictWaterAttributes objectForKey:[newItemId stringValue]];
                    NSNumber *userWaterId = [dictNewUserWater objectForKey:@"id_final"];
                    UserWater *userWater = [UserWater MR_findFirstByAttribute:@"userWater_id" withValue:userWaterId];
                    if (userWater)
                        [userWater MR_deleteInContext:[NSManagedObjectContext MR_defaultContext]];
                }
            }
            if ([dictChangeSets valueForKey:@"tracking_weight"] != nil) {
                
                NSDictionary *dictTrackingWeight = [dictChangeSets objectForKey:@"tracking_weight"];
                NSArray *arrCreate = [dictTrackingWeight objectForKey:@"create"];
                
                NSDictionary *dictWeightAttributes = [dictTrackingWeight objectForKey:@"attributes"];
                
                for (id newItemId in arrCreate) {
                    
                    NSDictionary *dictNewUserWeight = [dictWeightAttributes objectForKey:[newItemId stringValue]];
                    
                    if ([dictNewUserWeight objectForKey:@"id_old"]) {
                        
                        NSNumber *oldId = [dictNewUserWeight objectForKey:@"id_old"];
                        NSNumber *finalId = [dictNewUserWeight objectForKey:@"id_final"];
                        UserValue *userWeight = [UserValue MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"userValueId = %@",@(-1 * oldId.integerValue)]];
                        userWeight.userValueId = finalId;
                        userWeight.user = _user;
                        
                    } else {
                        
                        UserValue *newUserWeight = [UserValue MR_createEntity];
                        newUserWeight.userValueId = [dictNewUserWeight objectForKey:@"id_final"];
                        newUserWeight.value = [dictNewUserWeight objectForKey:@"weight"];
                        newUserWeight.date = [NSDate dateWithTimeIntervalSince1970:[[dictNewUserWeight objectForKey:@"date"] integerValue]];
                        newUserWeight.type = @"weight";
                        newUserWeight.user = _user;
                    }
                }
            }
            
            
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:kCCLastSyncDate];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kCCInitialSynchronizationWasPerformed];
            [[NSUserDefaults standardUserDefaults] setObject:maxRevision forKey:kCCMaxRevisionNumber];
            [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:kCCDataChangesSet];
            [[NSUserDefaults standardUserDefaults]  synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:kCCNotificationHomeScreenNeedsUpdate object:nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kCCNotificationSynchronizationEnded object:nil];
            
        } failure:^(NSError *error) {
            
            _isSynchronizing = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:kCCNotificationSynchronizationEnded object:nil];
            NSInteger statusCode = [[error.userInfo objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
            if (statusCode == 406) {
                
                [[CCAPI sharedInstance] destroyActiveSession];
                
            }
            
        }];
    }
    
}
- (NSDictionary *)getParamsForSyncRequest
{
    NSString *token = _user.magraAccessToken;
    NSString *lang = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    NSNumber *maxRevision  =  [[NSUserDefaults standardUserDefaults] objectForKey:kCCMaxRevisionNumber];
    NSMutableDictionary *syncJson = [@{@"session_token": token,@"locale":lang,@"maxRevision": maxRevision,@"changeSets": [@{} mutableCopy]}mutableCopy];
    
    NSMutableDictionary *dictTrackingMeal  = [@{@"create":[@[] mutableCopy],@"update":[@[] mutableCopy],@"delete":[@[] mutableCopy], @"attributes":[@[] mutableCopy]} mutableCopy];
    NSMutableDictionary *dictMealItemsAttributes  = [[NSMutableDictionary alloc] init];
    
    
    NSMutableDictionary *dictTrackingWater  = [@{@"create":[@[] mutableCopy],@"update":[@[] mutableCopy],@"delete":[@[] mutableCopy], @"attributes":[@[] mutableCopy]} mutableCopy];
    NSMutableDictionary *dictWaterItemsAttributes  = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *dictTrackingExercise  = [@{@"create":[@[] mutableCopy],@"update":[@[] mutableCopy],@"delete":[@[] mutableCopy], @"attributes":[@[] mutableCopy]} mutableCopy];
    NSMutableDictionary *dictExerciseItemsAttributes  = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *dictTrackingUserWeight  = [@{@"create":[@[] mutableCopy],@"update":[@[] mutableCopy],@"delete":[@[] mutableCopy], @"attributes":[@[] mutableCopy]} mutableCopy];
    NSMutableDictionary *dictUserWeightItemsAttributes  = [[NSMutableDictionary alloc] init];

    NSArray *arrChangesSet = [[NSUserDefaults standardUserDefaults] objectForKey:kCCDataChangesSet];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kCCDataChangesSet];
    _arrTempChangesSet = arrChangesSet;
    [[NSUserDefaults standardUserDefaults] synchronize];
    [arrChangesSet enumerateObjectsUsingBlock:^(NSData *encodedOp, NSUInteger idx, BOOL *stop) {
        
        CCAPISyncOp *diaryItemOp = [NSKeyedUnarchiver unarchiveObjectWithData:encodedOp];
        if (diaryItemOp.operationEntity == CCAPIOperationEntityWater) {
            
            NSDictionary *diaryItemJson = diaryItemOp.diaryItemJSON;
            NSString *itemID = [diaryItemJson objectForKey:@"id"];
            
            [dictWaterItemsAttributes setObject:diaryItemJson forKey:itemID];
            
            if(diaryItemOp.method == CCAPIOperationMethodCreate) {
                
                [[dictTrackingWater objectForKey:@"create"] addObject:itemID];
                
            } else if(diaryItemOp.method == CCAPIOperationMethodUpdate) {
                
                [[dictTrackingWater objectForKey:@"update"] addObject:itemID];
                
            } else if(diaryItemOp.method == CCAPIOperationMethodDelete) {
                
                [[dictTrackingWater objectForKey:@"delete"] addObject:itemID];
            }
        } else if (diaryItemOp.operationEntity == CCAPIOperationEntityFood) {
            
            NSMutableDictionary *diaryItemJson = [diaryItemOp.diaryItemJSON mutableCopy];
            NSString *itemID = [diaryItemJson objectForKey:@"id"];
            
            [dictMealItemsAttributes setObject:diaryItemJson forKey:itemID];
            
            if(diaryItemOp.method == CCAPIOperationMethodCreate) {
                
                [[dictTrackingMeal objectForKey:@"create"] addObject:itemID];
                
            } else if(diaryItemOp.method == CCAPIOperationMethodUpdate) {
                
                [[dictTrackingMeal objectForKey:@"update"] addObject:itemID];
                
            } else if(diaryItemOp.method == CCAPIOperationMethodDelete) {
                
                [[dictTrackingMeal objectForKey:@"delete"] addObject:itemID];
            }
        } else if (diaryItemOp.operationEntity == CCAPIOperationEntityExercise) {
            
            NSMutableDictionary *diaryItemJson = [diaryItemOp.diaryItemJSON mutableCopy];
            NSString *itemID = [diaryItemJson objectForKey:@"id"];
            
            [dictExerciseItemsAttributes setObject:diaryItemJson forKey:itemID];
            
            if(diaryItemOp.method == CCAPIOperationMethodCreate) {
                
                [[dictTrackingExercise objectForKey:@"create"] addObject:itemID];
                
            } else if(diaryItemOp.method == CCAPIOperationMethodUpdate) {
                
                [[dictTrackingExercise objectForKey:@"update"] addObject:itemID];
                
            } else if(diaryItemOp.method == CCAPIOperationMethodDelete) {
                
                [[dictTrackingExercise objectForKey:@"delete"] addObject:itemID];
            }
            
        } else if (diaryItemOp.operationEntity == CCAPIOperationEntityUserWeight) {
            
            NSMutableDictionary *diaryItemJson = [diaryItemOp.diaryItemJSON mutableCopy];
            NSString *itemID = [diaryItemJson objectForKey:@"id"];
            
            [dictUserWeightItemsAttributes setObject:diaryItemJson forKey:itemID];
            
            if(diaryItemOp.method == CCAPIOperationMethodCreate) {
                
                [[dictTrackingUserWeight objectForKey:@"create"] addObject:itemID];
                
            } else if(diaryItemOp.method == CCAPIOperationMethodUpdate) {
                
                [[dictTrackingUserWeight objectForKey:@"update"] addObject:itemID];
                
            } else if(diaryItemOp.method == CCAPIOperationMethodDelete) {
                
                [[dictTrackingUserWeight objectForKey:@"delete"] addObject:itemID];
            }
            
        }
        
    }];
  
    [dictTrackingWater setObject:dictWaterItemsAttributes forKey:@"attributes"];
    
    [dictTrackingMeal setObject:dictMealItemsAttributes forKey:@"attributes"];
    
    [dictTrackingExercise setObject:dictExerciseItemsAttributes forKey:@"attributes"];
    
    [dictTrackingUserWeight setObject:dictUserWeightItemsAttributes forKey:@"attributes"];
    
    if ([[dictTrackingWater objectForKey:@"attributes"] count]) {
        [[syncJson objectForKey:@"changeSets"] setObject:dictTrackingWater forKey:@"tracking_water"];
    }
    if ([[dictTrackingExercise objectForKey:@"attributes"]  count]) {
        [[syncJson objectForKey:@"changeSets"] setObject:dictTrackingExercise forKey:@"tracking_exercise"];
    }
    if ([[dictTrackingUserWeight objectForKey:@"attributes"]  count]) {
        [[syncJson objectForKey:@"changeSets"] setObject:dictTrackingUserWeight forKey:@"tracking_weight"];
    }
    if ([[dictTrackingMeal objectForKey:@"attributes"] count]) {
        
        [[syncJson objectForKey:@"changeSets"] setObject:dictTrackingMeal forKey:@"tracking_food"];
    }
    
    return syncJson;
    
}
@end
