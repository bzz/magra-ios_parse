//
//  CCParseAPI.h
//  ContarCalorias
//
//  Created by Anton Anisimov on 12/18/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCParseAPI : NSObject

+ (instancetype)sharedInstance;

- (void)registerNewUser:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

- (void)openEmailAccountSession:(NSDictionary *)authenticationDetails success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

@end
