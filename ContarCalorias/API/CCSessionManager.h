//
//  CCAPIHelper.h
//  ContarCalorias
//
//  Created by andres portillo on 6/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>
@class User;
@interface CCSessionManager : NSObject

@property (nonatomic, retain) User *user;

- (void)saveUserWithCompletion:(void (^)(void))completionBlock;
- (User *)getUser;
- (BOOL)isSessionAvailable;
- (void)destroyCurrentSession;
@end
