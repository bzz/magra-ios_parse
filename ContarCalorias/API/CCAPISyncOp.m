//
//  CCAPISyncOpe.m
//  ContarCalorias
//
//  Created by andres portillo on 5/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCAPISyncOp.h"
#import "User.h"
@implementation CCAPISyncOp
- (void)encodeWithCoder:(NSCoder *)encoder {
        //Encode properties, other class variables, etc
    [encoder encodeObject:self.date forKey:@"date"];
    [encoder encodeObject:self.diaryItemJSON forKey:@"diaryItemJSON"];
    [encoder encodeInteger:self.operationEntity forKey:@"diaryItemType"];
    [encoder encodeInteger:self.method forKey:@"method"];
    [encoder encodeObject:self.user forKey:@"user"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
            //decode properties, other class vars
        self.date = [decoder decodeObjectForKey:@"question"];
        self.diaryItemJSON = [decoder decodeObjectForKey:@"diaryItemJSON"];
        self.operationEntity = [decoder decodeIntegerForKey:@"diaryItemType"];
        self.method = [decoder decodeIntegerForKey:@"method"];
        self.user =  [decoder decodeObjectForKey:@"user"];
    }
    return self;
}
@end
