//
//  CCNotificationsManager.m
//  ContarCalorias
//
//  Created by andres portillo on 11/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCNotificationsManager.h"
#import "User.h"
#import "CCAPI.h"

@implementation CCNotificationsManager

- (void)scheduleLocalNotificationsForMeals
{
    
    [self cancelLocalNotificationsForMeals];
    User *user = [[CCAPI sharedInstance] getUser];
    
    if (user.breakfastTime) {
    
    
    NSDate* reminderTime = user.breakfastTime;
   
    UILocalNotification  *mealNotification = [[UILocalNotification alloc]init];
    NSDictionary *userInfo = @{@"notificationKey":kCCLocalNotificationBreakfastNotification};
    mealNotification.userInfo = userInfo;
    mealNotification.fireDate = reminderTime;
    mealNotification.alertBody = user.hasProFeatures? [TSLanguageManager localizedString:@"Remember to cook your breakfast"]: [TSLanguageManager localizedString:@"Remember to log your breakfast"];
    mealNotification.alertAction = [TSLanguageManager localizedString:@"Reminder"];
    mealNotification.soundName = UILocalNotificationDefaultSoundName;
    mealNotification.applicationIconBadgeNumber = 0;
    mealNotification.repeatInterval = NSDayCalendarUnit;
    [[UIApplication sharedApplication] scheduleLocalNotification:mealNotification];
    
    }
    
    if (user.firstSnackTime) {
       
        NSDate* reminderTime = user.firstSnackTime;
        
        UILocalNotification  *mealNotification = [[UILocalNotification alloc]init];
        NSDictionary *userInfo = @{@"notificationKey":kCCLocalNotificationFirstSnackNotification};
        mealNotification.userInfo = userInfo;
        mealNotification.fireDate = reminderTime;
        mealNotification.alertBody = user.hasProFeatures? [TSLanguageManager localizedString:@"Remember to cook your morning snack"]: [TSLanguageManager localizedString:@"Remember to log your morning snack"];
        mealNotification.alertAction = [TSLanguageManager localizedString:@"Reminder"];
        mealNotification.soundName = UILocalNotificationDefaultSoundName;
        mealNotification.applicationIconBadgeNumber = 0;
        mealNotification.repeatInterval = NSDayCalendarUnit;
        [[UIApplication sharedApplication] scheduleLocalNotification:mealNotification];
        
    }
    
    if (user.lunchTime) {
        
        NSDate* reminderTime = user.lunchTime;
        
        UILocalNotification  *mealNotification = [[UILocalNotification alloc]init];
        NSDictionary *userInfo = @{@"notificationKey":kCCLocalNotificationLunchNotification};
        mealNotification.userInfo = userInfo;
        mealNotification.fireDate = reminderTime;
        mealNotification.alertBody = user.hasProFeatures? [TSLanguageManager localizedString:@"Remember to cook your lunch"]: [TSLanguageManager localizedString:@"Remember to log your lunch"];
        mealNotification.alertAction = [TSLanguageManager localizedString:@"Reminder"];
        mealNotification.soundName = UILocalNotificationDefaultSoundName;
        mealNotification.applicationIconBadgeNumber = 0;
        mealNotification.repeatInterval = NSDayCalendarUnit;
        [[UIApplication sharedApplication] scheduleLocalNotification:mealNotification];
        
    }
    if (user.secondSnackTime) {
        
        NSDate* reminderTime = user.secondSnackTime;
        
        UILocalNotification  *mealNotification = [[UILocalNotification alloc]init];
        NSDictionary *userInfo = @{@"notificationKey":kCCLocalNotificationSecondSnackNotification};
        mealNotification.userInfo = userInfo;
        mealNotification.fireDate = reminderTime;
        mealNotification.alertBody = user.hasProFeatures? [TSLanguageManager localizedString:@"Remember to cook your afternoon snack"]: [TSLanguageManager localizedString:@"Remember to log your afternoon snack"];
        mealNotification.alertAction = [TSLanguageManager localizedString:@"Reminder"];
        mealNotification.soundName = UILocalNotificationDefaultSoundName;
        mealNotification.applicationIconBadgeNumber = 0;
        mealNotification.repeatInterval = NSDayCalendarUnit;
        [[UIApplication sharedApplication] scheduleLocalNotification:mealNotification];
        
    }
    if (user.dinnerTime) {
        
        NSDate* reminderTime = user.secondSnackTime;
        
        UILocalNotification  *mealNotification = [[UILocalNotification alloc]init];
        NSDictionary *userInfo = @{@"notificationKey":kCCLocalNotificationDinnerNotification};
        mealNotification.userInfo = userInfo;
        mealNotification.fireDate = reminderTime;
        mealNotification.alertBody = user.hasProFeatures? [TSLanguageManager localizedString:@"Remember to cook your dinner"]: [TSLanguageManager localizedString:@"Remember to log your dinner"];
        mealNotification.alertAction = [TSLanguageManager localizedString:@"Reminder"];
        mealNotification.soundName = UILocalNotificationDefaultSoundName;
        mealNotification.applicationIconBadgeNumber = 0;
        mealNotification.repeatInterval = NSDayCalendarUnit;
        [[UIApplication sharedApplication] scheduleLocalNotification:mealNotification];
        
    }
    
    NSLog(@"remaining notificatiosn after schedulling meals: %d",[[[UIApplication sharedApplication]scheduledLocalNotifications]count]);
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kCCMealNotificationsEnabled];
    [[NSUserDefaults standardUserDefaults]  synchronize];
}
- (void)scheduleLocalNotificationsForArticles
{
    BOOL articlesNotificationsEnabled = [[NSUserDefaults standardUserDefaults] boolForKey: kCCArticlesNotificationsEnabled];
    
    if (articlesNotificationsEnabled) {
        
        [self cancelLocalNotificationsForArticles];
        
        NSDate* wakeupTime = [[NSUserDefaults standardUserDefaults] objectForKey:kCCUsersWakeupTime];
        
        if (wakeupTime == nil) {
            
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            [calendar setLocale:[NSLocale currentLocale]];
            [calendar setTimeZone:[NSTimeZone defaultTimeZone]];
            NSDateComponents *nowComponents = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:[NSDate date]];
            NSDate *today= [calendar dateFromComponents:nowComponents];
            
            wakeupTime = [today dateByAddingTimeInterval:7*60*60];
            
        }
        UILocalNotification  *articlesNotification=[[UILocalNotification alloc]init];
        NSDictionary *lunchUserInfo = @{@"notificationKey":kCCLocalNotificationArticlesNotification};
        articlesNotification.userInfo = lunchUserInfo;
        articlesNotification.fireDate= wakeupTime;
        articlesNotification.timeZone = [NSTimeZone defaultTimeZone];
        articlesNotification.alertBody = [TSLanguageManager localizedString:@"Remember to complete today's missions"];
        articlesNotification.alertAction = [TSLanguageManager localizedString:@"Reminder"];
        articlesNotification.soundName = UILocalNotificationDefaultSoundName;
        articlesNotification.applicationIconBadgeNumber = 0;
        articlesNotification.repeatInterval = NSDayCalendarUnit;
        [[UIApplication sharedApplication] scheduleLocalNotification:articlesNotification];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kCCArticlesNotificationsEnabled];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
 
}

- (void)cancelLocalNotificationsForArticles
{
 
    NSArray *arrLocalNotifications  = [[UIApplication sharedApplication]scheduledLocalNotifications];
    
    for (int j =0;j<arrLocalNotifications.count; j++)
    {
        UILocalNotification *notification = [arrLocalNotifications objectAtIndex:j];
        if ([[notification.userInfo objectForKey:@"notificationKey"] isEqualToString:kCCLocalNotificationArticlesNotification]) {
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
        }
        
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kCCArticlesNotificationsEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"remaining notificatiosn after cancel missions: %d",[[[UIApplication sharedApplication]scheduledLocalNotifications]count]);
}
- (void)cancelLocalNotificationsForMeals
{
    NSArray *arrLocalNotifications  = [[UIApplication sharedApplication]scheduledLocalNotifications];
    
    for (int j =0;j < arrLocalNotifications.count; j++)
    {
        UILocalNotification *notification = [arrLocalNotifications objectAtIndex:j];
        NSString *notificationKey = [notification.userInfo objectForKey:@"notificationKey"];
        NSLog(@"checking key: %@",notificationKey);
        if ([notificationKey isEqualToString:kCCLocalNotificationBreakfastNotification] || [[notification.userInfo objectForKey:@"notificationKey"] isEqualToString:kCCLocalNotificationLunchNotification] || [[notification.userInfo objectForKey:@"notificationKey"] isEqualToString:kCCLocalNotificationDinnerNotification] || [[notification.userInfo objectForKey:@"notificationKey"] isEqualToString:kCCLocalNotificationFirstSnackNotification] || [[notification.userInfo objectForKey:@"notificationKey"] isEqualToString:kCCLocalNotificationSecondSnackNotification]) {
         
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
       
        }
        
    }
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kCCMealNotificationsEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"remaining notificatiosn after cancel meals: %d",[[[UIApplication sharedApplication]scheduledLocalNotifications]count]);
}
@end
