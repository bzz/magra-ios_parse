
    //  ServiceLocator.m
    //  ContarCalorias
    //
    //  Created by Lucian Gherghel on 12/02/14.
    //  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
    //

#import "CCAPI.h"
#import "UIAlertView+Blocks.h"
#import "CCPersistencyManager.h"
#import "AppDelegate.h"
#import "CCSessionManager.h"
#import "CCHTTPClient.h"
#import "User.h"
#import "UserFood.h"
#import "Food.h"
#import "FoodServing.h"
#import "CCUtils.h"
#import "CCConstants.h"
#import "UserFood.h"
#import "UserWater.h"
#import "MealPlan.h"
#import "Article.h"
#import "Exercise.h"
#import "ContarCalendar.h"
#import "CCAPISyncOp.h"
#import "CCSyncEngine.h"
#import "CCNotificationsManager.h"
#import "CCUtils.h"
#import "NSString+HTML.h"
#import "CCDietsIntroductionVC.h"
#import <Crashlytics/Crashlytics.h>
#import "ATConnect.h"

static NSString * const CCAPIBaseURL = @"https://api.contarcalorias.com/v1/";

@interface CCAPI ()
@end

@implementation CCAPI
{
    CCPersistencyManager *_persistencyManager;
    CCSessionManager *_sessionManager;
    CCHTTPClient *_httpClient;
    CCNotificationsManager *_notificationsManager;
    CCSyncEngine *_syncEngine;
}
+ (instancetype)sharedInstance
{
    static CCAPI *_sharedInstance;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[CCAPI alloc] init];
    });
    return _sharedInstance;
}
- (id)init
{
    self = [super init];
    if (self != nil) {
        
        _persistencyManager = [[CCPersistencyManager alloc] init];
        _sessionManager = [[CCSessionManager alloc] init];
        _httpClient = [[CCHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:CCAPIBaseURL]];
        _notificationsManager = [[CCNotificationsManager alloc] init];
        _syncEngine = [[CCSyncEngine alloc] init];
        _syncEngine.httpClient = _httpClient;
        _syncEngine.user = [self getUser];
        
    }
    return self;
}
- (NSString *)getVersionNumber
{
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
    NSString *build = infoDictionary[(NSString*)kCFBundleVersionKey];
    
    return build;
}
#pragma mark - Persistency Manager

- (NSArray *)getTodaysLoggedMeals
{
    return [_persistencyManager getTodaysLoggedMeals];
}
- (NSArray *)getPosts
{
    return [_persistencyManager getPosts];
}
- (void)deleteAllPosts
{
    [_persistencyManager deleteAllPosts];
}
- (NSString *)getPostsFilePath
{
    return [_persistencyManager getPostsFilePath];
}
- (NSString *)getPrivateDocsDir
{
    return [_persistencyManager getPrivateDocsDir];
}
- (NSArray *)getAllFoodsWithPredicate:(NSPredicate *)predicate
{
    NSArray *arrFoods = [_persistencyManager getAllFoodsWithPredicate:predicate];
    return  arrFoods;
}
- (NSArray *)getAllUserFoodsWithPredicate:(NSPredicate *)predicate
{
    NSArray *arrUserFoods = [_persistencyManager getAllUserFoodsWithPredicate:predicate];
    return  arrUserFoods;
}
- (NSArray *)getAllExercisesWithPredicate:(NSPredicate *)predicate
{
    NSArray *arrExercises = [_persistencyManager getAllExercisesWithPredicate:predicate];
    return  arrExercises;
}
- (NSArray *)getAllUserExercisesWithPredicate:(NSPredicate *)predicate
{
    NSArray *arrExercises = [_persistencyManager getAllUserExercisesWithPredicate:predicate];
    return  arrExercises;
}

- (NSInteger)getTodaysConsumedCalories
{
    return [_persistencyManager getTodaysConsumedCalories];
}
- (NSInteger)getTodaysBurnedCalories
{
    return [_persistencyManager getTodaysBurnedCalories];
}

- (CGFloat)getTodaysConsumedCarbs
{
    return [_persistencyManager getTodaysConsumedCarbs];
}
- (CGFloat)getTodaysConsumedFats
{
    return [_persistencyManager getTodaysConsumedFats];
}
- (CGFloat)getTodaysConsumedProteins
{
    return [_persistencyManager getTodaysConsumedProteins];
}
- (NSInteger)getTodaysDrinkedWaterMililiters
{
    return [_persistencyManager getTodaysDrinkedWaterMililiters];
}
- (BOOL)todaysMealGoalWasReached
{
    NSInteger calorieBudget = _sessionManager.user.dailyCaloriesValue;
    NSInteger consumedCalories = [self getTodaysConsumedCalories];
    return consumedCalories >= calorieBudget;
}
- (BOOL)todaysWaterGoalWasReached
{
    return [_persistencyManager getTodaysDrinkedWaterMililiters] >= kCCDailyWaterMililitersGoal;
}
- (NSArray *)getAvailableCountries
{
    NSArray *countries = @[@{@"name": @"Argentina",@"key" :@(1)},
                           @{@"name": @"Bolivia",@"key" :@(2)},
                           @{@"name": @"Brasil",@"key" :@(3)},
                           @{@"name": @"Chile",@"key" :@(4)},
                           @{@"name": @"Colombia",@"key" :@(5)},
                           @{@"name": @"Costa Rica",@"key" :@(6)},
                           @{@"name": @"Cuba",@"key" :@(7)},
                           @{@"name": @"Ecuador",@"key" :@(8)},
                           @{@"name": @"El Salvador",@"key" :@(9)},
                           @{@"name": @"España",@"key" :@(10)},
                           @{@"name": @"Estados Unidos de América",@"key" :@(11)},
                           @{@"name": @"Guatemala",@"key" :@(12)},
                           @{@"name": @"Honduras",@"key" :@(13)},
                           @{@"name": @"México",@"key" :@(14)},
                           @{@"name": @"Nicaragua",@"key" :@(15)},
                           @{@"name": @"Panamá",@"key" :@(16)},
                           @{@"name": @"Perú",@"key" :@(17)},
                           @{@"name": @"Puerto Rico",@"key" :@(18)},
                           @{@"name": @"República Dominicana",@"key" :@(19)},
                           @{@"name": @"Uruguay",@"key" :@(20)},
                           @{@"name": @"Venezuela",@"key" :@(21)}];
    
    return countries;
    
}
- (void )setFirstdayDate:(NSDate *)date{
    
    [_persistencyManager setFirstdayDate:date];
}
- (void)logWaterItemWithMililiters:(NSInteger )mililiters
{
    [_persistencyManager logWaterItemWithMililiters:mililiters];
}
- (void)updateUserFoodItemWithFood:(UserFood *)userFood withFoodServing:(Food_serving *)foodServing numberOfServings:(NSInteger)numberOfServings completed:(void (^)())completed{
    [_persistencyManager updateUserFoodItemWithFood:userFood withFoodServing:foodServing numberOfServings:numberOfServings completed:completed];
}
- (void)deleteUserFood:(UserFood *)userFood
{
    [_persistencyManager deleteUserFood:userFood];
}
- (void)logFoodItemWithFood:(Food *)food foodServing:(Food_serving *)foodServing numberOfServings:(NSInteger)numberOfServings andMealType:(CCMealType)mealType
{
    [_persistencyManager logFoodItemWithFood:food foodServing:foodServing numberOfServings:numberOfServings andMealType:mealType];
}
- (void)logExercise:(Exercise *)exercise withMinutes:(NSInteger)minutes burnedCalories:(NSInteger)burnedCalories
{
    [_persistencyManager logExercise:exercise withMinutes:minutes burnedCalories:burnedCalories];
}
- (void)logUserWeight:(CGFloat)weight sync:(BOOL)sync completion:(void (^)())success
{
    [_persistencyManager logUserWeight:weight sync:sync completion:success];
}
- (void)saveProfileImage:(UIImage *)profileImage
{
    [_persistencyManager saveProfileImage:profileImage];
}
- (void)saveFacebookProfileImage:(UIImage *)profileImage
{
    [_persistencyManager saveFacebookProfileImage:profileImage];
}
- (UIImage *)getProfileImage
{
    return [_persistencyManager getProfileImage];
}
- (void)enrollUserInMealPlanWithId:(NSInteger)mealPlanId
{
    [_persistencyManager enrollUserInMealPlanWithId:mealPlanId];
    [self performSynchronization];
}
#pragma mark - Sync Engine

- (void)addOpToQueue:(CCAPISyncOp *)op
{
    [_persistencyManager addOpToQueue:op andSync:YES];
}
- (void)performSynchronization
{
    _syncEngine.user = [self getUser];
    [_syncEngine performSynchronization];
}

#pragma mark - Notification Manager
- (void)scheduleLocalNotificationsForMeals
{
    [_notificationsManager scheduleLocalNotificationsForMeals];
}
- (void)scheduleLocalNotificationsForArticles
{
    [_notificationsManager scheduleLocalNotificationsForArticles];
}
- (void)cancelLocalNotificationsForMeals
{
    [_notificationsManager cancelLocalNotificationsForMeals];
}
- (void)cancelLocalNotificationsForArticles
{
    [_notificationsManager cancelLocalNotificationsForArticles];
}
#pragma mark - CC Values
- (NSString *)getLocalizedGenderNameForCCGenderIndex:(CCGender)gender
{
    if (gender == CCGenderMale)
        return [TSLanguageManager localizedString:@"Male"];
    
    return [TSLanguageManager localizedString:@"Female"];
}
- (NSString *)getLocalizedActivityLevelForCCActivityLevelIndex:(CCActivityLevel)activityLevel
{
    NSArray *arrActivityLevels = @[[TSLanguageManager localizedString:@"Sedentary"],[TSLanguageManager localizedString:@"Slightly active"],[TSLanguageManager localizedString:@"Active"],[TSLanguageManager localizedString:@"Very active"]];
    return arrActivityLevels[activityLevel];
}
#pragma mark - Session Manager
- (void)userSubscriptionExpired
{
    _sessionManager.user.isProUserValue = NO;
    [_sessionManager saveUserWithCompletion:nil];
}
- (BOOL)isProUser
{
    User *user = [self getUser];
    return user.hasProFeatures;
}
- (void)setUserAsProUser
{
    [_sessionManager.user setIsProUserValue:YES];
    [_sessionManager saveUserWithCompletion:nil];
}
- (User *)getUser
{
    return  [_sessionManager getUser];
}
- (void)setUser:(User *)user
{
    [_sessionManager setUser:user];
}
- (void)setUsersActivityLevel:(CCActivityLevel)activityLevel
{
    [_sessionManager.user setActivityLevelValue:activityLevel];
}

- (CGFloat)getMaximumRecommendedWeightForUser:(User *)user
{
    CGFloat maximumWeight  = 24.9 * (pow((user.heightCmValue/100),2));
    
    return  maximumWeight;
}
- (CGFloat)getMinimumRecommendedWeightForUser:(User *)user
{
    CGFloat usersHeight = user.heightCmValue;
    CGFloat minimumWeight  = 18.5 * (pow((usersHeight/100),2));
    
    return minimumWeight;
}
- (CGFloat)getIdealWeightMetricForUser:(User *)user
{
    CGFloat idealWeight;
    if (user.gender == CCGenderFemale) {
        idealWeight = 45.5 + ((user.heightCmValue - 150)/2.5) * 2.27;
    } else {
        idealWeight = 47.7 + ((user.heightCmValue - 150)/2.5) * 2.72;
    }
    
    return idealWeight;
}
- (void)destroyActiveSession
{
    [[_httpClient operationQueue] cancelAllOperations];
    [_sessionManager destroyCurrentSession];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kCCMontlySubstriptionProductId];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kCCInitialSynchronizationWasPerformed];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kCCMealNotificationsEnabled];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kCCArticlesNotificationsEnabled];
    [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:kCCMaxRevisionNumber];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:kCCDataChangesSet];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kCCUseFacebookAvatar];
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate dateWithTimeIntervalSince1970:0] forKey:kCCLastSyncDate];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [_persistencyManager deleteProfileImage];
    [_persistencyManager deleteFacebookProfileImage];
    
    
    
    NSString *privateDir = [[CCAPI sharedInstance] getPrivateDocsDir];
    NSString *esArticlesPath = [privateDir stringByAppendingPathComponent:@"articles-es.plist"];
    NSString *ptArticlesPath = [privateDir stringByAppendingPathComponent:@"articles-pt.plist"];
    
    NSError *error;
    BOOL isDir;
    if ([[NSFileManager defaultManager] fileExistsAtPath:esArticlesPath isDirectory:&isDir]) {
        [[NSFileManager defaultManager] removeItemAtPath:esArticlesPath error:&error];
    }
    if (error) {
        NSLog(@"error deleting spanish articles file");
    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:ptArticlesPath isDirectory:&isDir]) {
        [[NSFileManager defaultManager] removeItemAtPath:ptArticlesPath error:&error];
    }
    if (error) {
        NSLog(@"error deleting portuguese articles file");
    }
    
    AppDelegate *appDelegate =  [[UIApplication sharedApplication] delegate];
    [appDelegate updateMainScreen];
}
- (void)loginToContarCaloriasWithFacebookToken:(NSString *)facebookAccessToken
{
    if(facebookAccessToken != (id)[NSNull class] && facebookAccessToken.length != 0)
    {
        
        [_httpClient exchangeToken:facebookAccessToken network:@"fb" success:^(id responseObject) {
            
            NSDictionary *authData = responseObject;
            NSDictionary *dictProfile = [authData objectForKey:@"profile"];
            
            [Crashlytics setUserIdentifier:[dictProfile objectForKey:@"id"]];
            [Crashlytics setUserName:[dictProfile objectForKey:@"full_name"]];
            
            User *user = [User MR_createEntity];
            user.username = [dictProfile objectForKey:@"full_name"];
            user.profilePictureUrl = [dictProfile objectForKey:@"avatar"];
            user.authenticationTypeValue = CCAuthenticationTypeFacebook;
            user.idUserValue = [[dictProfile objectForKey:@"id"] integerValue];
            user.email = [dictProfile objectForKey:@"email"];
            user.country = [dictProfile objectForKey:@"country"];
            user.magraAccessToken = [dictProfile objectForKey:@"session_token"];
            user.facebookId = [NSString stringWithFormat:@"%@",[dictProfile objectForKey:@"social_facebook"]];
            user.googleId = [dictProfile objectForKey:@"social_google"];
            [user setUsersBirthdate :[NSDate dateWithTimeIntervalSince1970:[dictProfile[@"birthday"] integerValue]]];
            user.genderValue =[dictProfile[@"gender"] isEqualToString:@"m"]?CCGenderMale:CCGenderFemale;
            user.unitSystemValue = [dictProfile[@"metric_type"] isEqualToString:@"metric"]? CCUnitSystemMetric:CCUnitSystemBritish;
           
            if ([[dictProfile objectForKey:@"height_cm"] floatValue] > 0)
                user.didCreateWeightLossPlanValue = YES;
            
            if (user.didCreateWeightLossPlanValue)
            {
                [user setFirstDayDate:[NSDate dateWithTimeIntervalSince1970: [[dictProfile objectForKey:@"date_firstday"] integerValue]]];
                [user setCurrentWeightKg:[dictProfile objectForKey:@"current_weight_kg"]];
                [user setDesiredWeight:[[dictProfile objectForKey:@"goal_weight_kg"] floatValue]];
                [user setInitialWeightKgValue:[[dictProfile objectForKey:@"initial_weight_kg"] floatValue]];
                [user setHeightCmValue:[[dictProfile objectForKey:@"height_cm"] floatValue]];
                user.dailyCaloriesValue = [[dictProfile objectForKey:@"calories_per_day"] integerValue];
                
                if ([dictProfile objectForKey:@"wakeup_time"] != [NSNull null]) {
                    user.wakeupTime = [NSDate dateWithTimeIntervalSince1970: [[dictProfile objectForKey:@"wakeup_time"] integerValue]];
                    
                } else {
                    NSDate * defaultWakeupTime = [CCUtils getDefaultWakeupTime];
                    user.wakeupTime = defaultWakeupTime;
                    [user setMealTimesUsingWakeupTime];
                }
                
                if ([dictProfile objectForKey:@"breakfast_time"] != [NSNull null]) {
                    user.breakfastTime = [NSDate dateWithTimeIntervalSince1970: [[dictProfile objectForKey:@"breakfast_time"] integerValue]];
                }
                
                if ([dictProfile objectForKey:@"firstsnack_time"] != [NSNull null]) {
                    user.firstDayDate= [NSDate dateWithTimeIntervalSince1970: [[dictProfile objectForKey:@"firstsnack_time"] integerValue]];
                }
                if ([dictProfile objectForKey:@"lunch_time"]  != [NSNull null]) {
                    user.lunchTime= [NSDate dateWithTimeIntervalSince1970: [[dictProfile objectForKey:@"lunch_time"] integerValue]];
                }
                if ([dictProfile objectForKey:@"secondsnack_time"] != [NSNull null]) {
                    user.secondSnackTime = [NSDate dateWithTimeIntervalSince1970: [[dictProfile objectForKey:@"secondsnack_time"] integerValue]];
                }
                if ([dictProfile objectForKey:@"dinner_time"] != [NSNull null]) {
                    user.dinnerTime = [NSDate dateWithTimeIntervalSince1970: [[dictProfile objectForKey:@"dinner_time"] integerValue]];
                }
                
            }
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kCCUseFacebookAvatar];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self saveUserWithCompletion:^{
                [self setApptentiveInitialValues];
            }];
            [self updateMainScreen];
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[TSLanguageManager localizedString:@"Sorry!"] message:[TSLanguageManager localizedString:@"There was an error, please try again"] delegate:self cancelButtonTitle:[TSLanguageManager localizedString:@"OK"] otherButtonTitles:nil];
            [alertView show];
            [[CCAPI sharedInstance] destroyActiveSession];
            
        }];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[TSLanguageManager localizedString:@"Sorry!"] message:[TSLanguageManager localizedString:@"There was an error getting your Facebook Account details, please try again"] delegate:self cancelButtonTitle:[TSLanguageManager localizedString:@"OK" ] otherButtonTitles:nil];
        [alertView show];
    }
}
- (void)updateMainScreen
{
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate updateMainScreen];
}
- (BOOL)isSessionAvailable
{
    return [_sessionManager isSessionAvailable];
}
- (void)saveUserWithCompletion:(void (^)(void))completionBlock
{
    [_sessionManager saveUserWithCompletion:completionBlock];
}
#pragma mark - HTTPClient

- (void)getMagraUser
{
    User *user = [self getUser];
    
    if (user) {
        
        NSDictionary *params = @{@"token":user.magraAccessToken,@"last_modified":user.lastModified?@([user.lastModified timeIntervalSince1970]):@0};
        
        [_httpClient getMagraUser:params Success:^(id responseObject) {
            
            NSDictionary *dictProfile = [responseObject objectForKey:@"profile"];
            
            BOOL didCreateWeightLossPlan = [[dictProfile objectForKey:@"did_create_weight_loss_plan"] boolValue];
            if(didCreateWeightLossPlan == YES) {
                
                CCActivityLevel activityLevel = -1;
                
                if([[dictProfile objectForKey:@"activity"] isEqualToString:@"sedentario"]) {
                    activityLevel = CCActivityLevelSedentary;
                } else if([[dictProfile objectForKey:@"activity"] isEqualToString:@"ligeramente activo"]) {
                    activityLevel = CCActivityLevelSlightlyActive;
                } else if([[dictProfile objectForKey:@"activity"] isEqualToString:@"activo"]) {
                    activityLevel = CCActivityLevelActive;
                } else if([[dictProfile objectForKey:@"activity"] isEqualToString:@"muy activo"]) {
                    activityLevel = CCActivityLevelVeryActive;
                }
                
                CCGender gender = [[dictProfile objectForKey:@"gender"] isEqualToString:@"m"] ? CCGenderMale:CCGenderFemale;
                user.desiredWeightKgValue = [[dictProfile objectForKey:@"goal_weight_kg"] doubleValue];
                
                user.genderValue = gender;
                user.googleId = [dictProfile objectForKey:@"social_google"];
                user.facebookId = [NSString stringWithFormat:@"%@",[dictProfile objectForKey:@"social_facebook"]];
                
                user.heightCmValue = [[dictProfile objectForKey:@"height_cm"] doubleValue];
                user.initialWeightKgValue = [[dictProfile objectForKey:@"initial_weight_kg"] doubleValue];
                user.currentWeightKgValue = [[dictProfile objectForKey:@"current_weight_kg"] doubleValue];
                user.didCreateWeightLossPlanValue= [[dictProfile objectForKey:@"did_create_weight_loss_plan"] boolValue];
                user.activityLevelValue = activityLevel;
                [user setUsersBirthdate:[NSDate dateWithTimeIntervalSince1970:[[dictProfile objectForKey:@"birthday"] integerValue]]];
                
                    //diets
                MealPlan *mealPlan = [MealPlan MR_findFirstByAttribute:@"idMealPlan" withValue:dictProfile[@"meal_plan_id"]];
                
                if (mealPlan)
                    user.mealPlan = mealPlan;
                
                user.mealPlanStartDate = [dictProfile[@"meal_plan_id"] integerValue] != 0 ?[NSDate dateWithTimeIntervalSince1970:[dictProfile[@"meal_plan_date"] integerValue]]:nil;
                
               if ([dictProfile objectForKey:@"date_firstday"] != [NSNull null]) {
                    user.firstDayDate = [NSDate dateWithTimeIntervalSince1970:[[dictProfile objectForKey:@"date_firstday"] integerValue]];
                }
            }
            if (user.hasValidData) {
                
                [[CCAPI sharedInstance] saveUserWithCompletion:nil];
                
            } else {
                
                [[CCAPI sharedInstance] destroyActiveSession];
            }
            
        } failure:^(NSError *error) {
            
        }];

    }
}
- (void)updateMagraUser
{
    User *user = [self getUser];
    
    NSString *gender = user.genderValue== CCGenderMale?@"m":@"f";
    NSString *metricType = user.unitSystemValue == CCUnitSystemMetric?@"metric":@"british";
    NSString *activityLevel = @"";
    
    if (user.activityLevelValue == CCActivityLevelSedentary) {
        activityLevel  = @"sedentario";
    } else if (user.activityLevelValue == CCActivityLevelSlightlyActive) {
        activityLevel  = @"ligeramente activo";
    } else if (user.activityLevelValue == CCActivityLevelActive) {
        activityLevel  = @"activo";
    } else if (user.activityLevelValue == CCActivityLevelVeryActive) {
        activityLevel  = @"muy activo";
    }
    
    NSDate *lastSyncDate = [NSDate date];
    NSString *lang = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    NSDictionary *dictProfile = @{
                                  @"id": user.idUser,
                                  @"full_name": user.username == nil? @"": user.username,
                                  @"birthday": user.birthdate == nil? @0:@([user.birthdate timeIntervalSince1970]),
                                  @"gender": gender == nil? @"":gender,
                                  @"wakeup_time":user.wakeupTime? @([user.wakeupTime timeIntervalSince1970]):@0,
                                  @"breakfast_time":user.breakfastTime? @([user.breakfastTime timeIntervalSince1970]):@0,
                                  @"lunch_time":user.lunchTime? @([user.lunchTime timeIntervalSince1970]):@0,
                                  @"dinner_time":user.dinnerTime? @([user.dinnerTime timeIntervalSince1970]):@0,
                                  @"firstsnack_time":user.firstSnackTime? @([user.firstSnackTime timeIntervalSince1970]):@0,
                                  @"secondsnack_time":user.secondSnackTime? @([user.secondSnackTime timeIntervalSince1970]):@0,
                                  @"country": user.country == nil? @"":user.country,
                                  @"email": user.email == nil? @"":user.email,
                                  @"session_token": user.magraAccessToken == nil? @"":user.magraAccessToken,
                                  @"calories_per_day": user.dailyCalories == nil? @0:user.dailyCalories,
                                  @"initial_weight_kg": user.initialWeightKg == nil? @0:user.initialWeightKg,
                                  @"current_weight_kg": user.currentWeightKg == nil? @0:user.currentWeightKg,
                                  @"goal_weight_kg": user.desiredWeightKg == nil? @0:user.desiredWeightKg,
                                  @"meal_plan_id": user.mealPlan.idMealPlan == nil? @0:user.mealPlan.idMealPlan,
                                  @"meal_plan_date" :user.mealPlanStartDate== nil?[NSNull null]:@([user.mealPlanStartDate timeIntervalSince1970]),
                                  @"height_cm": user.heightCm == nil? @0:user.heightCm,
                                  @"metric_type": metricType == nil? @"":metricType,
                                  @"activity": activityLevel == nil? @"":activityLevel,
                                  @"goal_weight_date": user.dateReachingDesiredWeight == nil? [NSNull null]:@([user.dateReachingDesiredWeight timeIntervalSince1970]),
                                  @"date_firstday": user.firstDayDate == nil? @0: @([user.firstDayDate timeIntervalSince1970]),
                                  @"device": @"iOS",
                                  @"last_modified":@([lastSyncDate timeIntervalSince1970]),
                                  @"language":lang,
                                  @"did_create_weight_loss_plan":@(user.didCreateWeightLossPlanValue),
                                  @"status": user.isProUserValue?@"pro": @"free",
                                  @"social_facebook": user.facebookId == nil? @"": user.facebookId,
                                  @"social_google": user.googleId == nil? @"": user.googleId,
                                  @"date_sync": lastSyncDate == nil? @"":@([lastSyncDate timeIntervalSince1970])
                                  };
    
     NSDictionary *params = @{@"token":user.magraAccessToken,@"profile":dictProfile};

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-retain-cycles"
    
    [_httpClient setMagraUser:params Success:^(id responseObject) {
        
        user.lastModified = lastSyncDate;
        [self saveUserWithCompletion:nil];
        
    } failure:^(NSError *error) {
        
    }];
#pragma clang diagnostic pop
    
    
}
- (void)setApptentiveInitialValues
{
    NSDateFormatter *df= [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-mm-yyyy"];
    User *user = [self getUser];
    [[ATConnect sharedConnection] addCustomPersonData:user.idUser withKey:@"id"];
    [[ATConnect sharedConnection] addCustomPersonData:user.username withKey:@"full_name"];
    [[ATConnect sharedConnection] addCustomPersonData:[df stringFromDate:user.birthdate] withKey:@"birthday"];
    [[ATConnect sharedConnection] addCustomPersonData:user.genderValue == CCGenderMale?@"m":@"f" withKey:@"gender"];
    [[ATConnect sharedConnection] addCustomPersonData:user.country withKey:@"country"];
    [[ATConnect sharedConnection] addCustomPersonData:user.email withKey:@"email"];
    [[ATConnect sharedConnection] addCustomPersonData:user.dailyCalories withKey:@"calories_per_day"];
    [[ATConnect sharedConnection] addCustomPersonData:user.initialWeightKg withKey:@"initial_weight_kg"];
    [[ATConnect sharedConnection] addCustomPersonData:user.currentWeightKg withKey:@"current_weight_kg"];
    [[ATConnect sharedConnection] addCustomPersonData:user.desiredWeightKg withKey:@"goal_weight_kg"];
    [[ATConnect sharedConnection] addCustomPersonData:user.hasProFeatures?@"pro":@"free" withKey:@"status"];
    [[ATConnect sharedConnection] addCustomPersonData:[df stringFromDate:user.registrationDate] withKey:@"date_registration"];
    [[ATConnect sharedConnection] addCustomPersonData:user.heightCm withKey:@"height_cm"];
    [[ATConnect sharedConnection] addCustomPersonData:@"iOS" withKey:@"device"];
    [[ATConnect sharedConnection] addCustomPersonData:[TSLanguageManager selectedLanguage]withKey:@"language"];
    
    [[ATConnect sharedConnection] setInitialUserName:user.username];
    [[ATConnect sharedConnection] setInitialUserEmailAddress:user.email];
}
- (void)validateReceiptsWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    if(!receipt) {
        /* No local receipt -- handle the error. */
    }
    NSString *jsonObjectString = [receipt base64EncodedStringWithOptions:0];
    
    NSDictionary *params = @{@"receipt-data":jsonObjectString,@"password":kCCMonthlySubscriptionIAPSharedSecret};
    [_httpClient validateReceipts:params
                          success:^(id responseObject) {
                              success(responseObject);
                          } failure:^(NSError *error) {
                              failure(error);
                          }];
}
- (void)validateReceipts
{
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    User *user = [self getUser];
    
    if(!receipt) {
        if (user.isProUserValue)     {
            [self userSubscriptionExpired];
        }
    } else {
        NSString *jsonObjectString = [receipt base64EncodedStringWithOptions:0];
        
        NSDictionary *params = @{@"receipt-data":jsonObjectString,@"password":kCCMonthlySubscriptionIAPSharedSecret};
        [_httpClient validateReceipts:params
                                     success:^(id responseObject) {
                                         
                                         NSDictionary *dictResponse = responseObject;
                                         NSArray *arrTransactions = dictResponse[@"latest_receipt_info"];
                                         
                                         NSTimeInterval unixDateExpirationMs =[[arrTransactions valueForKeyPath:@"@max.expires_date_ms"] doubleValue];
                                         
                                         NSDate *expirationDate = [NSDate dateWithTimeIntervalSince1970:(unixDateExpirationMs/1000)];
                                         NSTimeInterval timeIntervalSinceExpiration = [[NSDate date] timeIntervalSinceDate:expirationDate];
                                         if (timeIntervalSinceExpiration > 0) {
                                             
                                             [[CCAPI sharedInstance] userSubscriptionExpired];
                                             
                                             if (arrTransactions.count == 1 && ! [[NSUserDefaults standardUserDefaults] boolForKey:kCCTrialExpirationMessageWasShown]) {
                                                 
                                                 [UIAlertView showWithTitle:@"Trial Ended" message:@"Your trial ahs ended proper message should go here" cancelButtonTitle:@"Not now" otherButtonTitles:@[@"Buy Pro"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                     
                                                     if (buttonIndex == 1) {
                                                         AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                                                         UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Diets" bundle:nil];
                                                         CCDietsIntroductionVC *dietsMainVC = [mainStoryboard instantiateInitialViewController];
                                                         UIViewController *rootVC = appDelegate.window.rootViewController;
                                                         [rootVC presentViewController:dietsMainVC animated:YES completion:nil];
                                                         
                                                     }
                                                 }];
                                                 [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kCCTrialExpirationMessageWasShown];
                                                 [[NSUserDefaults standardUserDefaults] setObject:@(CCMealNotificationTypeLogMeal) forKey:kCCMealNotificationType];
                                                 [[NSUserDefaults standardUserDefaults] synchronize];
                                                 [[CCAPI sharedInstance] scheduleLocalNotificationsForMeals];
                                             }
                                         }
                                         
                                         
                                     } failure:^(NSError *error) {
                                         
                                     }];
    }
    
    
}
- (void)validateReceiptsSandbox
{
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    User *user = [self getUser];
    
    
    if(!receipt) {
        if (user.isProUserValue)     {
            [self userSubscriptionExpired];
        }
    } else {
        NSString *jsonObjectString = [receipt base64EncodedStringWithOptions:0];
        
        NSDictionary *params = @{@"receipt-data":jsonObjectString,@"password":kCCMonthlySubscriptionIAPSharedSecret};
        [_httpClient validateReceiptsSandbox:params
                                     success:^(id responseObject) {
                                         
                                         NSDictionary *dictResponse = responseObject;
                                         NSArray *arrTransactions = dictResponse[@"latest_receipt_info"];
                                         
                                         NSTimeInterval unixDateExpirationMs =[[arrTransactions valueForKeyPath:@"@max.expires_date_ms"] doubleValue];
                                         
                                         NSDate *expirationDate = [NSDate dateWithTimeIntervalSince1970:(unixDateExpirationMs/1000)];
                                         NSTimeInterval timeIntervalSinceExpiration = [[NSDate date] timeIntervalSinceDate:expirationDate];
                                         if (timeIntervalSinceExpiration > 0) {
                                             
                                             [[CCAPI sharedInstance] userSubscriptionExpired];
                                             
                                             if (arrTransactions.count == 1 && ! [[NSUserDefaults standardUserDefaults] boolForKey:kCCTrialExpirationMessageWasShown]) {
                                                 
                                                 [UIAlertView showWithTitle:@"Trial Ended" message:@"Your trial ahs ended proper message should go here" cancelButtonTitle:@"Not now" otherButtonTitles:@[@"Buy Pro"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                     
                                                     if (buttonIndex == 1) {
                                                         AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                                                         UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Diets" bundle:nil];
                                                         CCDietsIntroductionVC *dietsMainVC = [mainStoryboard instantiateInitialViewController];
                                                         UIViewController *rootVC = appDelegate.window.rootViewController;
                                                         [rootVC presentViewController:dietsMainVC animated:YES completion:nil];
                                                     }
                                                 }];
                                                 [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kCCTrialExpirationMessageWasShown];
                                                 [[NSUserDefaults standardUserDefaults] setObject:@(CCMealNotificationTypeLogMeal) forKey:kCCMealNotificationType];
                                                 [[NSUserDefaults standardUserDefaults] synchronize];
                                                 [[CCAPI sharedInstance] scheduleLocalNotificationsForMeals];
                                             }
                                         }
                                         
                                         
                                     } failure:^(NSError *error) {
                                         
                                     }];
    }
}

- (CCHTTPClient *)getHttpClient
{
    return _httpClient;
}

- (void)getOlderPostsWithSuccess:(void (^)())success Failure:(void (^)(NSError *error))failure
{
    User  *user = [self getUser];
    NSArray *currentPosts = [self getPosts];
    NSDate *oldestPostDate = [currentPosts valueForKeyPath:@"@min.date"];
    int unix_timestamp = 0;
    if (oldestPostDate == nil) {
        oldestPostDate = [NSDate date];
        unix_timestamp = [oldestPostDate timeIntervalSince1970];
        NSTimeZone* timeZone = [NSTimeZone localTimeZone];
        int offset = [timeZone secondsFromGMTForDate: oldestPostDate];
        unix_timestamp = unix_timestamp - offset;
    } else {
        unix_timestamp = [oldestPostDate timeIntervalSince1970];
    }
    
    NSDictionary *params = @{@"session_token":user.magraAccessToken,@"locale":[TSLanguageManager selectedLanguage],@"date":@(unix_timestamp),@"getolderposts":@YES};
    
    
    [_httpClient getPosts:params Success:^(id responseObject) {
        
        NSArray *arrArticlesJson = [responseObject objectForKey:@"posts"];
        
        NSMutableArray *arrPostsObjects = [NSMutableArray new];
        
        for (NSDictionary *dictArticle in arrArticlesJson) {
            Article *newPost = [Article new];
            newPost.articleId = [[dictArticle objectForKey:@"id"] integerValue];
            newPost.day = [[dictArticle objectForKey:@"day"] integerValue];
            newPost.content = [dictArticle objectForKey:@"description"];
            newPost.locale = [dictArticle objectForKey:@"locale"];
            newPost.title = [[dictArticle objectForKey:@"title"] xmlSimpleUnescapeString];
            newPost.type = [dictArticle objectForKey:@"type"];
            newPost.date = [NSDate dateWithTimeIntervalSince1970:[[dictArticle objectForKey:@"date"] integerValue]];
            newPost.imageUrl = [dictArticle objectForKey:@"image"];
            [arrPostsObjects addObject:newPost];
        }
        if (_persistencyManager.arrSessionLoadedPosts.count == 0)
        {
            NSArray *postsToStore = [arrPostsObjects objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, MIN(arrPostsObjects.count,10))]];
            [NSKeyedArchiver archiveRootObject:postsToStore toFile:[[CCAPI sharedInstance] getPostsFilePath]];
        }
        
        
        NSArray *arrAllPosts =  _persistencyManager.arrSessionLoadedPosts != nil? [_persistencyManager.arrSessionLoadedPosts arrayByAddingObjectsFromArray:arrPostsObjects]:arrPostsObjects;
        
        _persistencyManager.arrSessionLoadedPosts = [[arrAllPosts sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]]] mutableCopy];
        success();
        
        
    } failure:^(NSError *error) {
        failure(error);
    }];
}
- (void)getPostUpdatesWithSuccess:(void (^)())success Failure:(void (^)(NSError *error))failure
{
    User  *user = [self getUser];
    NSArray *currentPosts = [self getPosts];
    NSDate *latestPostDate = [currentPosts valueForKeyPath:@"@max.date"];
    NSDictionary *params = @{@"session_token":user.magraAccessToken,@"locale":[TSLanguageManager selectedLanguage],@"date":@([latestPostDate timeIntervalSince1970]),@"getolderposts":@NO};
    
    
    [_httpClient getPosts:params Success:^(id responseObject) {
        
        NSArray *arrArticlesJson = [responseObject objectForKey:@"posts"];
        
        NSMutableArray *arrPostsObjects = [NSMutableArray new];
        
        for (NSDictionary *dictArticle in arrArticlesJson) {
            Article *newPost = [Article new];
            newPost.articleId = [[dictArticle objectForKey:@"id"] integerValue];
            newPost.day = [[dictArticle objectForKey:@"day"] integerValue];
            newPost.content = [dictArticle objectForKey:@"description"];
            newPost.locale = [dictArticle objectForKey:@"locale"];
            newPost.title = [[dictArticle objectForKey:@"title"] xmlSimpleUnescapeString];
            newPost.type = [dictArticle objectForKey:@"type"];
            newPost.date = [NSDate dateWithTimeIntervalSince1970:[[dictArticle objectForKey:@"date"] integerValue]];
            newPost.imageUrl = [dictArticle objectForKey:@"image"];
            [arrPostsObjects addObject:newPost];
        }
        
        
        NSArray *arrAllPosts =  _persistencyManager.arrSessionLoadedPosts != nil? [_persistencyManager.arrSessionLoadedPosts arrayByAddingObjectsFromArray:arrPostsObjects]:arrPostsObjects;
        arrAllPosts = [arrAllPosts sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]]];
        
        if (arrPostsObjects.count >= 10) {
            
            NSArray *postsToStore = [arrPostsObjects objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, MIN(arrPostsObjects.count,10))]];
            [NSKeyedArchiver archiveRootObject:postsToStore toFile:[[CCAPI sharedInstance] getPostsFilePath]];
            
        } else {
            
            NSArray *postsToStore = [arrAllPosts objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, MIN(arrAllPosts.count,10))]];
            [NSKeyedArchiver archiveRootObject:postsToStore toFile:[[CCAPI sharedInstance] getPostsFilePath]];
            
        }
        
        
        _persistencyManager.arrSessionLoadedPosts = [[[arrAllPosts sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]]] objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, MIN(arrAllPosts.count,30))]] mutableCopy];
        
        success();
        
    } failure:^(NSError *error) {
        failure(error);
    }];
}
- (void)registerNewUser:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [_httpClient registerNewUser:parameters success:^(id responseObject) {
        success(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
}
- (void)resetUserPassword:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    
    [_httpClient resetUserPassword:parameters success:^(id responseObject) {
        success(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
    
}
- (void)openEmailAccountSession:(NSDictionary *)authenticationDetails success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    
    [_httpClient openEmailAccountSession:authenticationDetails success:^(id responseObject) {
        success(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
    
}
- (void)submitFood:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    User  *user = [self getUser] ;
    NSString *token = user.magraAccessToken;
    NSString *lang = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    NSString *title = [parameters objectForKey:@"title"];
    NSString *brand = [parameters objectForKey:@"brand"];
    NSDictionary *foodParameters = @{@"session_token":token,@"locale":lang,@"title":title,@"brand":brand};
    
    [_httpClient submitFood:foodParameters success:^(id responseObject) {
        
        NSString *id_food =[NSString stringWithFormat:@"%@", [responseObject objectForKey:@"id"]];
        NSString *barcode = [parameters objectForKey:@"barcode"];
        NSString *serving_size = [parameters objectForKey:@"serving_size"];
        NSString *serving_type = [parameters objectForKey:@"serving_type"];
        NSInteger calories = [[parameters objectForKey:@"calories"] integerValue];
        NSInteger fat = [[parameters objectForKey:@"fat"] integerValue];
        NSInteger saturated_fat  = [[parameters objectForKey:@"saturated_fat"] integerValue];
        NSInteger cholesterol  = [[parameters objectForKey:@"cholesterol"] integerValue];
        NSInteger sodium = [[parameters objectForKey:@"sodium"] integerValue];
        
        NSDictionary *foodServingParameters = @{@"session_token":token,@"locale":lang,@"id_food":id_food,@"barcode":barcode, @"calories":@(calories), @"fat": @(fat), @"saturated_fat": @(saturated_fat), @"cholesterol": @(cholesterol), @"sodium":@(sodium),@"serving_size": serving_size, @"serving_type": serving_type};
        
        [_httpClient submitFoodServing:foodServingParameters success:^(id responseObject) {
            
            success(responseObject);
            
        } failure:^(NSError *error) {
            failure(error);
        }];
        
    } failure:^(NSError *error) {
        failure(error);
    }];
}
- (void)sendEmailWithParameters:(NSDictionary *)params Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [_httpClient sendEmailWithParameters:params Success:success failure:failure];
}
- (void)getAllDietsWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [_httpClient  getAllDietsWithSuccess:success failure:failure];
}
- (void)getRecipeWithId:(NSInteger)recipeId Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [_httpClient  getRecipeWithId:recipeId Success:success failure:failure];
}
- (void)getDietDetailsWithDietId:(NSInteger)dietId Success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [_httpClient getDietDetailsWithDietId:dietId Success:success failure:failure];
}
- (void)getTodaysMealsWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure
{
    [_httpClient getTodaysMealsWithSuccess:success failure:failure];
    
}
- (void)userStartedTrial
{
    User *user = [[CCAPI sharedInstance] getUser];
    [_httpClient userStartedTrial:@{@"token": user.magraAccessToken} Success:^(id responseObject) {
        
    } failure:^(NSError *error) {
        
    }];

}
@end
