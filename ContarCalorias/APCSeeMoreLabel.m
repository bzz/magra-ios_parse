//
//  APCSeeMoreLabel.m
//  Andrés portillo
//
//  Created by andres portillo on 19/11/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "APCSeeMoreLabel.h"

@implementation APCSeeMoreLabel
{
    BOOL _textWillBeTruncated;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialization];
    }
    return self;
}
- (void)awakeFromNib
{
    [self initialization];
}
- (void)initialization
{
    _seeMoreTextAttributes = @{ NSForegroundColorAttributeName : [UIColor darkGrayColor],
                                NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Medium" size:14]};
}
- (void)setText:(NSString *)text withSeeMoreLocalizedText:(NSString *)seeMore
{
    _textWillBeTruncated = [self textWillBeTruncated:text];
    if (_textWillBeTruncated) {
        
        NSString *seeMoreText = [NSString stringWithFormat:@"...%@",seeMore];
        NSString *newText = [text stringByAppendingString:seeMoreText];
      
        NSMutableAttributedString *attrNewText = [[NSMutableAttributedString alloc] initWithString:newText];
        [attrNewText setAttributes:_seeMoreTextAttributes range:NSMakeRange(newText.length - seeMoreText.length, seeMoreText.length)];
        [attrNewText setAttributes:@{NSFontAttributeName:self.font} range:NSMakeRange(0, (newText.length - seeMoreText.length))];
        
        NSInteger numberOfCharactersThatFit = [self numberOfCharactersThatFitForText:attrNewText]
        ;
        NSInteger numberOfExtraCharacters = newText.length - numberOfCharactersThatFit;
        
        NSString *finalText = [[text substringWithRange:NSMakeRange(0, text.length - numberOfExtraCharacters)] stringByAppendingString:seeMoreText];
       
        NSMutableAttributedString *attrFinalText = [[NSMutableAttributedString alloc] initWithString:finalText];
        [attrFinalText setAttributes:_seeMoreTextAttributes range:NSMakeRange(finalText.length - seeMoreText.length, seeMoreText.length)];
        [attrFinalText setAttributes:@{NSFontAttributeName:self.font,NSForegroundColorAttributeName:self.textColor} range:NSMakeRange(0, (newText.length - seeMoreText.length))];
        
        
        [self setAttributedText:attrFinalText];
        
    } else {
        [self setText:text];
    }
}
- (BOOL)textWillBeTruncated:(NSString *)text
{
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text
                                                                         attributes:@{ NSFontAttributeName : self.font}];
    CGSize size = CGSizeMake(self.frame.size.width, CGFLOAT_MAX);
    CGRect textFrame = [attributedText boundingRectWithSize:size
                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                    context:nil];
    
    if (CGRectGetHeight(textFrame) > CGRectGetHeight(self.frame)) {
        
        return YES;
    }
    
    return NO;
}
- (NSInteger)numberOfCharactersThatFitForText:(NSAttributedString *)attrText
{
    NSInteger numberOfCharactersThatFit = 0;
    for (int i = [attrText length]; i >= 0; i--) {
        NSString *substring = [attrText.string substringToIndex:i];
        NSMutableAttributedString *subAttributedText = [[NSMutableAttributedString alloc] initWithString:substring];
        
        [attrText enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, subAttributedText.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
            if (value) {
                UIFont *font = (UIFont *)value;
                [subAttributedText addAttribute:NSFontAttributeName value:font range:range];
            }
        }];
        [attrText enumerateAttribute:NSForegroundColorAttributeName inRange:NSMakeRange(0, subAttributedText.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
            if (value) {
                UIColor *color = (UIColor *)value;
               [subAttributedText addAttribute:NSForegroundColorAttributeName value:color range:range];
            }
        }];
        CGSize size = CGSizeMake(self.frame.size.width, CGFLOAT_MAX);
        CGRect textFrame = [subAttributedText boundingRectWithSize:size
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                        context:nil];
        
        if (CGRectGetHeight(textFrame) <= CGRectGetHeight(self.frame)) {
            numberOfCharactersThatFit =  i;
            break;
        }
    }
    return numberOfCharactersThatFit;
}
@end
