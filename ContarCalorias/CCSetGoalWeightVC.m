//
//  CCSetGoalWeightVC.m
//  ContarCalorias
//
//  Created by andres portillo on 13/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCSetGoalWeightVC.h"
#import "DDUnitConversion.h"
#import "CCSetWeightLossPlanResultsVC.h"
#import "CCAPI.h"
#import "User.h"
#import "CCSetWeightLossPlanResultsVC.h"
#import "CCAPISyncOp.h"
#import "FontAwesomeKit.h"

@interface CCSetGoalWeightVC ()<UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
@end

@implementation CCSetGoalWeightVC{
    CGFloat _minimumRecommendedWeight;
    CGFloat _maximumRecommendedWeight;
    CGFloat _goalWeightKg;
    UIPickerView *_pickerWeight;
    UIToolbar *_inputAccessory;
    User *_user;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
        //User
    _user  = [[CCAPI sharedInstance] getUser];
    UIImageView *imgTitleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"set-goal-weight-title-view-img"]];
    self.navigationItem.titleView = imgTitleView;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self addButtonItem];
        
    });
    
    _pickerWeight =[[UIPickerView alloc] init];
    [_pickerWeight setBackgroundColor:[UIColor whiteColor]];
    [_pickerWeight setDelegate:self];
    [_txtGoalWeight setInputView:_pickerWeight];
    
    _inputAccessory = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 40)];
    UIBarButtonItem  *spacer = [[UIBarButtonItem alloc]    initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [_inputAccessory setBarStyle:UIBarStyleDefault];
    [_inputAccessory setBarTintColor:CCColorGreen];
    [_inputAccessory setTintColor:[UIColor whiteColor]];
    
    FAKFontAwesome *checkIcon = [FAKFontAwesome checkIconWithSize:28];
    [checkIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    [_inputAccessory setItems:[NSArray arrayWithObjects:spacer,
                              [[UIBarButtonItem alloc] initWithImage:[checkIcon imageWithSize:CGSizeMake(25, 25)] style:UIBarButtonItemStyleDone target:self action:@selector(endEditing)],nil ] animated:YES];
    [_txtGoalWeight setInputAccessoryView:_inputAccessory];
    
    _goalWeightKg = [[CCAPI sharedInstance] getIdealWeightMetricForUser:_user];
    
    [_txtGoalWeight setText:[NSString stringWithFormat:@"%.1f",_goalWeightKg]];
    
    if (_goalWeightKg >= 14) {
        [_pickerWeight selectRow:((NSInteger)_goalWeightKg - 14) inComponent:0 animated:NO];
    }
    
    CGFloat decimalPart = (_goalWeightKg - (NSInteger)_goalWeightKg) * 10;
    NSInteger row = round(decimalPart);
    
    if (row>0)
        [_pickerWeight selectRow:row inComponent:1 animated:NO];
    
    [_lblScreenTitle setText:[TSLanguageManager localizedString:@"Your goal weight"]];
    [_lblScreenDescription setText:[TSLanguageManager localizedString:@"Goal weight screen description"]];
    
}
- (void)endEditing
{
    [self.view endEditing:YES];
}
- (void)addButtonItem
{
    FAKFontAwesome *nextIcon = [FAKFontAwesome arrowRightIconWithSize:28];
    [nextIcon addAttribute:NSForegroundColorAttributeName value:CCColorOrange];
    UIImage *imgNext = [[nextIcon imageWithSize:CGSizeMake(25, 25)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *btnNext = [[UIBarButtonItem alloc] initWithImage:imgNext style:UIBarButtonItemStyleBordered target:self action:@selector(onNextButtonTapped)];
    [self.navigationItem setRightBarButtonItem:btnNext];
}
- (void)onNextButtonTapped
{
    [self performSegueWithIdentifier:@"Wakeup Time Segue" sender:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return textField.text.length + string. length <= 4;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"Wakeup Time Segue"]) {
  
        [_user setDesiredWeight:[_txtGoalWeight.text floatValue]];
    }
}

#pragma mark - UIPickerView DataSource
    // returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return 412;
            break;
        case 1:
            return 10;
            break;
        case 2:
            return 1;
            break;
        default:
            break;
    }

    return  0;
}

#pragma mark - UIPickerView Delegate

    // returns width of column and height of row for each component.
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if(component == 0) {
        return 50.0;
    } else if(component == 1) {
        return 25.0;
    } else if(component == 2) {
        return 20.0;
    }
    
    return 0.0;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 34;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* lblTitle = (UILabel*)view;
    
    if (!lblTitle) {
       
        if (component == 0) {
            lblTitle = [[UILabel alloc] init];
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
            [lblTitle setText:[NSString stringWithFormat:@"%d",(row + 14)]];
            [lblTitle setTextAlignment:NSTextAlignmentRight];
            [lblTitle setTextColor:[UIColor blackColor]];
            [view addSubview:lblTitle];
        } else if (component == 1) {
            lblTitle = [[UILabel alloc] init];
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
            [lblTitle setText:[NSString stringWithFormat:@".%d",row]];
            [lblTitle setTextAlignment:NSTextAlignmentCenter];
            [lblTitle setTextColor:[UIColor blackColor]];
            [view addSubview:lblTitle];
        } else if (component == 2) {
            lblTitle = [[UILabel alloc] init];
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
            [lblTitle setText:[NSString stringWithFormat:@"Kg"]];
            [lblTitle setTextAlignment:NSTextAlignmentLeft];
            [lblTitle setTextColor:[UIColor blackColor]];
            [view addSubview:lblTitle];
        }
    
    }
    
    return lblTitle;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == _pickerWeight) {
        
        NSInteger firstRow = [pickerView selectedRowInComponent:0] + 14;
        NSInteger secondRow = [pickerView selectedRowInComponent:1];
        [_txtGoalWeight setText:[NSString stringWithFormat:@"%d.%d",firstRow, secondRow]];
        
        _goalWeightKg = [_txtGoalWeight.text floatValue];
    }
    
    
}
@end
