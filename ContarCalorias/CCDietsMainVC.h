//
//  CCDietsMainVC.h
//  ContarCalorias
//
//  Created by andres portillo on 05/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CCTodaysMealsView;

@interface CCDietsMainVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblAllDiets;
- (IBAction)onSegmentedControlChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet CCTodaysMealsView *todaysMealsView;

@end
