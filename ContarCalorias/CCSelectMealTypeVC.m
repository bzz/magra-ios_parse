//
//  CCSelectMealTypeVC.m
//  ContarCalorias
//
//  Created by andres portillo on 24/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCSelectMealTypeVC.h"
#import "ViewUtils.h"
#import "CCSearchDiaryItemVC.h"

@interface CCSelectMealTypeVC ()

@end

@implementation CCSelectMealTypeVC {
    CCMealType _mealType;

}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self scrollViewWrapper] setContentSize:CGSizeMake(320, 465)];
    
    [ViewUtils drawBasicShadowOnView:_viewWrapperMealTypes];
    _viewWrapperMealTypes.layer.cornerRadius = 4;
    
    [self.view setBackgroundColor:[ViewUtils lightGrayColor]];
    [ViewUtils fixSeparatorsHeightInView:_viewWrapperMealTypes];
    
    
    
    [_lblBreakfast setText:[TSLanguageManager localizedString:@"Breakfast"]];
    [_lblLunch setText:[TSLanguageManager localizedString:@"Lunch"]];
    [_lblDinner setText:[TSLanguageManager localizedString:@"Dinner"]];
    [_lblSnack setText:[TSLanguageManager localizedString:@"Snack"]];

    
        //nav item
    
    [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Meal Type"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBActions
 
 - (IBAction)addBreakfast:(id)sender {
     _mealType = CCMealTypeBreakfast;
     [self performSegueWithIdentifier:@"Add Diary Item Segue" sender:nil];
     
 }
 - (IBAction)addLunch:(id)sender {
     _mealType = CCMealTypeLunch;
     [self performSegueWithIdentifier:@"Add Diary Item Segue" sender:nil];
     
 }
 - (IBAction)addDinner:(id)sender {
     _mealType = CCMealTypeDinner;
     [self performSegueWithIdentifier:@"Add Diary Item Segue" sender:nil];
     
 }
 - (IBAction)addSnack:(id)sender {
     _mealType = CCMealTypeSnack;
     [self performSegueWithIdentifier:@"Add Diary Item Segue" sender:nil];
 }
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

     CCSearchDiaryItemVC *searchDiaryItemVC = segue.destinationViewController;
     searchDiaryItemVC.currentDiaryItem = CCDiaryItemTypeMeal;
     searchDiaryItemVC.currentMealType = _mealType;
    
}
@end
