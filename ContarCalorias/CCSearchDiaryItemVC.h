//
//  AddMealViewController.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 11/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeleteDiaryItemProtocol.h"

@interface CCSearchDiaryItemVC : UIViewController <UITextFieldDelegate, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@property NSMutableArray *diaryItems;
@property NSMutableArray *searchResults;

@property (assign, nonatomic) CCDiaryItemType currentDiaryItem;
@property (assign, nonatomic) CCMealType currentMealType;

@end
