//
//  AppDelegate.m
//  ContarCalorias
//
//  Created by Adrian Ghitun on 27/12/13.
//  Copyright (c) 2013 Adrian Ghitun. All rights reserved.
//

#import "AppDelegate.h"
#import <Crashlytics/Crashlytics.h>
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>
#import "CCAPI.h"
#import "ContarCalendar.h"
#import <UIAlertView+Blocks.h>
#import "Food.h"
#import "FoodServing.h"
#import "CCHomeVC.h"
#import "CCSideMenuVC.h"
#import "SWRevealViewController.h"
#import "CCLoginVC.h"
#import "CCInitialVC.h" 
#import "CCIAPHelper.h"
#import "CCLanguageSelectionVC.h"
#import "InMobi.h"
#import "IMConstants.h"
#import "GAI.h"
#import "ATConnect.h"
#import "TSMessage.h"
#import "User.h"
#import "CCUtils.h"
#import "CCDietsIntroductionVC.h"
#import "Food.h"
#import <Parse/Parse.h>

@interface AppDelegate()<SWRevealViewControllerDelegate>
@end
@implementation AppDelegate
{
    BOOL _appNeedsNewLogin;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
        //APPTENTIVE
    [ATConnect sharedConnection].apiKey = @"2dfcaf1fc66b04a573dafb5d6fd14b0ea13ee25badfad0cec6f903c30a44776b";
        //GOOGLE ANALYTICS
        // Optional: automatically send uncaught exceptions to Google Analytics.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unreadMessageCountChanged:) name:ATMessageCenterUnreadCountChangedNotification object:nil];
    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
        // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
        // Optional: set Logger to VERBOSE for debug information.
        //[[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
        // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-39616291-12"];
    
    
        //Initialize InMobi SDK with your property-ID
    [InMobi initialize:@"f86fa054f66e462f825db2ff30b3520f"];
        //[InMobi setLocationWithCity:@"San Francisco" state:@"California" country:@"USA"];
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;

    NSString *parseApplicationId = @"alyM19zxDz96JFMye7FFrPNwQco0qdkZrEmNeGMU";
    NSString *parseClientKey = @"M7rjYz8XjD3FPwW2aekVTospRvBO2cqV2S4YMfvC";
    [Parse setApplicationId:parseApplicationId clientKey:parseClientKey];
    
    
        //configure database
    [self configureDataBase];
        //configure user defaults
    [self configureUserDefaults];
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window = window;
    
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        if (! _appNeedsNewLogin)
            [self setInitialScreen];
        else
            [self setLoginScreen];
        
            // If there's one, just open the session silently, without showing the user the login UI
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"]
                                           allowLoginUI:NO
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                            [self sessionStateChanged:session State:state error:error];
                                        }];
    }  else {
            //check if there is an email active session
        if ([[CCAPI sharedInstance] isSessionAvailable]) {
            [self setHomeScreen];
        } else {
            [self setLoginScreen];
        }
        
    }
    
        //configure crashlytics
    [Crashlytics startWithAPIKey:@"ce959398e7989b1a9f2f97a0ee4c2aa3cc44b9b2"];
    
        //count launches
    NSInteger  launchesCount = [[[NSUserDefaults standardUserDefaults] objectForKey:kCCLaunchesCount] integerValue];
    launchesCount ++;
    [[NSUserDefaults standardUserDefaults] setObject:@(launchesCount) forKey:kCCLaunchesCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self preventBackup];
    
    //[self checkForFoodsWithoutServings];
    
    [CCIAPHelper sharedInstance];
  
    [self.window makeKeyAndVisible];
    
    return YES;
}
- (void)checkForFoodsWithoutServings
{
    NSFetchRequest *frequest = [Food MR_requestAllInContext:[NSManagedObjectContext MR_defaultContext]];
    [frequest setFetchBatchSize:20];
    [frequest setResultType:NSDictionaryResultType];
    [frequest setPropertiesToFetch:@[@"title",@"food_id"]];
    NSArray *foods = [[Food MR_executeFetchRequest:frequest inContext:[NSManagedObjectContext MR_defaultContext]] valueForKeyPath:@"@distinctUnionOfObjects.food_id"];
    
    NSFetchRequest *fsrequest = [FoodServing MR_requestAllInContext:[NSManagedObjectContext MR_defaultContext]];
    [fsrequest setFetchBatchSize:20];
    [fsrequest setResultType:NSDictionaryResultType];
    [fsrequest setPropertiesToFetch:@[@"food_id"]];
    NSArray *foodServings = [[Food MR_executeFetchRequest:fsrequest inContext:[NSManagedObjectContext MR_defaultContext]]  valueForKeyPath:@"@distinctUnionOfObjects.food_id"];
    
}
- (void)unreadMessageCountChanged:(NSNotification *)notification {
        // Unread message count is contained in the notification's userInfo dictionary.
    NSNumber *unreadMessageCount = [notification.userInfo objectForKey:@"count"];
    NSNumber *lastUnreadMessageCount = [[NSUserDefaults standardUserDefaults] objectForKey:kCCUnreadMessagesCount];
    
        // Update your UI or alert the user when a new message arrives.
    if (unreadMessageCount.integerValue > lastUnreadMessageCount.integerValue) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCCNotificationNewMessageReceived object:nil];
    }
    [[NSUserDefaults standardUserDefaults] setObject:unreadMessageCount forKey:kCCUnreadMessagesCount];
}
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    } else {
        NSLog(@"file at %@ excluded from backup",URL);
        
    }
    return success;
}

    // This method will handle ALL the session state changes in the app
- (void)sessionStateChanged:(FBSession *)session State:(FBSessionState)state error:(NSError *)error
{
   
        // If the session was opened successfully
    if (!error && state == FBSessionStateOpen){
        
        [self completeFacebookAuthentication:session.accessTokenData.accessToken];
        return;
    }
    
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
            // If the session is closed
        NSLog(@"Session closed");
        [[CCAPI sharedInstance] destroyActiveSession];
        [self setLoginScreen];
    }
        // Handle errors
    if (error){
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
            // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            [self showMessage:alertText withTitle:alertTitle];
        } else {
            
                // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");
                
                    // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                [self showMessage:alertText withTitle:alertTitle];
                
            } else {
                    //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                    // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                [self showMessage:alertText withTitle:alertTitle];
            }
        }
            // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        [[CCAPI sharedInstance] destroyActiveSession];
        [self setLoginScreen];
    }
}
- (void)completeFacebookAuthentication:(NSString *)facebookAccessToken
{
    
    if ([[CCAPI sharedInstance] isSessionAvailable]) {
        
        [self updateMainScreen];
        
    } else {
        
        [[CCAPI sharedInstance] loginToContarCaloriasWithFacebookToken:facebookAccessToken];
        
    }
    
}
- (void)showMessage:(NSString *)message withTitle:(NSString *)title
{
    [UIAlertView showWithTitle:title message:message cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:nil];
}
- (void)updateMainScreen
{
    if ([[CCAPI sharedInstance] isSessionAvailable]) {
        
        [self setHomeScreen];
        [[CCAPI sharedInstance] performSynchronization];
    
    } else {
        [self setLoginScreen];
    }
    
}
- (void)setInitialScreen
{
    CCInitialVC *initialVC = [[CCInitialVC alloc] init];
    self.window.rootViewController = initialVC;
    
}
- (void)setLoginScreen
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kCCLanguageWasSelected]) {
        CCLoginVC *loginVC = [loginStoryboard instantiateViewControllerWithIdentifier:@"Login"];
        self.window.rootViewController = loginVC;
    } else {
        CCLanguageSelectionVC *languageSelectionVC = [loginStoryboard instantiateInitialViewController];
        self.window.rootViewController = languageSelectionVC;
    }
}
- (void)setHomeScreen
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIImage *profileDefaultImage = [UIImage imageNamed:@"account-default"];
    UIImage *profileImg = [[CCAPI sharedInstance] getProfileImage];
    CCSideMenuVC *rearViewController = [[CCSideMenuVC alloc] initWithMainImage:profileImg == nil?profileDefaultImage:profileImg];
    UINavigationController *homeNavigationController = [mainStoryboard instantiateInitialViewController];
    
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearViewController frontViewController:homeNavigationController];
    revealController.delegate = self;
    
    [homeNavigationController.view addGestureRecognizer: revealController.panGestureRecognizer];
    self.viewController = revealController;
    self.window.rootViewController = self.viewController;
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}
- (void)configureDataBase
{
        //copy pre-populated database
    NSArray *paths = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *documentPath = [paths lastObject];
    
    NSURL *storeURL = [documentPath URLByAppendingPathComponent:@"Model.sqlite"];
    NSURL *storeURLShmFile = [documentPath URLByAppendingPathComponent:@"Model.sqlite-shm"];
    NSURL *storeURLWalFile = [documentPath URLByAppendingPathComponent:@"Model.sqlite-wal"];
    
    NSURL *preloadURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"CoreData" ofType:@"sqlite"]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:[storeURL path]]) {
        
        NSError* err = nil;
        if (![[NSFileManager defaultManager] copyItemAtURL:preloadURL toURL:storeURL error:&err]) {
            NSLog(@"Error: Unable to copy preloaded database.");
        }
        else{
            NSLog(@"Copying preloaded database");
            [[NSUserDefaults standardUserDefaults] setObject:@(CCDatabaseVersion) forKey:kCCDatabaseVersion];
            [[NSUserDefaults standardUserDefaults] setObject:@0 forKey:kCCMaxRevisionNumber];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
    }else {
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:kCCDatabaseVersion] integerValue] != CCDatabaseVersion) {
            [[NSUserDefaults standardUserDefaults] setObject:@(CCDatabaseVersion) forKey:kCCDatabaseVersion];
            [[NSUserDefaults standardUserDefaults] setObject:@0 forKey:kCCMaxRevisionNumber];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSLog(@"database outdated");
            
            NSError* err = nil;
            [[NSFileManager defaultManager] removeItemAtURL:storeURL error:&err];
            [[NSFileManager defaultManager] removeItemAtURL:storeURLWalFile error:&err];
            [[NSFileManager defaultManager] removeItemAtURL:storeURLShmFile error:&err];
           
            if (![[NSFileManager defaultManager] copyItemAtURL:preloadURL toURL:storeURL error:&err]) {
                NSLog(@"Error: Unable to copy new preloaded database");
            }
            else{
                NSLog(@"Copying new preloaded database");
            }
            _appNeedsNewLogin = YES;
            
        } else {
            NSLog(@"Database up to date");
        }
        
    }
    
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"Model.sqlite"];
    
    if (_appNeedsNewLogin) {
        [[CCAPI sharedInstance] destroyActiveSession];
    }
    
}

-(void)preventBackup
{   int count;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];

    
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
    for (count = 0; count < (int)[directoryContent count]; count++)
    {
   
     [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",path,[directoryContent objectAtIndex:count]]]];
    }
}
- (void)configureUserDefaults
{
    NSDictionary *appDefaults =@{kCCTrialExpirationMessageWasShown:@NO,kCCDatabaseVersion:@(CCDatabaseVersion),kCCLaunchesCount:@0,kCCUnreadMessagesCount:@0,kCCLanguageWasSelected:@NO,kCCInitialSynchronizationWasPerformed: @NO,kCCAppIntroductionWasPresented:@NO,kCCArticlesNotificationsEnabled: @YES, kCCMealNotificationsEnabled: @NO, kCCMaxRevisionNumber:@(0), kCCDataChangesSet:@[],kCCUseFacebookAvatar:@(NO),kCCLastSyncDate:[NSDate dateWithTimeIntervalSince1970:0]};
    
    User *user = [[CCAPI sharedInstance] getUser];
    
    if (user.heightCmValue > 0) {
        user.didCreateWeightLossPlanValue = YES;
    }
    if (user && [[NSUserDefaults standardUserDefaults] objectForKey:kCCFirstDayDate] && ! user.firstDayDate) {
        user.firstDayDate = [[NSUserDefaults standardUserDefaults] objectForKey:kCCFirstDayDate];
    }
    if (user && ! [[NSUserDefaults standardUserDefaults] objectForKey:kCCUsersWakeupTime] && !user.wakeupTime ) {
       
        NSDate * defaultWakeupTime = [CCUtils getDefaultWakeupTime];
        user.wakeupTime = defaultWakeupTime;
        [user setMealTimesUsingWakeupTime];
    
    } else if (user && [[NSUserDefaults standardUserDefaults] objectForKey:kCCUsersWakeupTime] &&! user.wakeupTime){
    
        user.wakeupTime = [[NSUserDefaults standardUserDefaults] objectForKey:kCCUsersWakeupTime];
        [user setMealTimesUsingWakeupTime];
    }
    
    [[CCAPI sharedInstance] saveUserWithCompletion:nil];
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
            //set used language
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"lang"]) {
        
        NSString *lang =  [TSLanguageManager selectedLanguage];
        
        if (! [lang isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"lang"]] && ([lang isEqualToString:@"pt"] || [lang isEqualToString:@"es"])) { 
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    NSString *lang =  [TSLanguageManager selectedLanguage];
    
    [[NSUserDefaults standardUserDefaults] setObject:lang forKey:@"lang"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIApplicationState state = [application applicationState];
    
    if (state == UIApplicationStateActive) {
        
    } else if(state == UIApplicationStateBackground || state == UIApplicationStateInactive) {
        
        if([[CCAPI sharedInstance] isSessionAvailable]) {
            
            [self.window.rootViewController.revealViewController performSegueWithIdentifier:@"sw_front" sender:nil];
        
        }
    
    }
        // Set icon badge number to zero
    application.applicationIconBadgeNumber = 0;
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[ContarCalendar sharedInstance] setDateShowing:[NSDate date]];
  
    User *user = [[CCAPI sharedInstance] getUser];
    
    if(user.didCreateWeightLossPlanValue) {
    
        [[CCAPI sharedInstance] performSynchronization];
        
    }
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSession.activeSession handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [MagicalRecord cleanUp];
}
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    [UIAlertView showWithTitle:@"Notification" message:[userInfo[@"aps"] objectForKey:@"alert"] cancelButtonTitle:@"Dismiss" otherButtonTitles:nil tapBlock:nil];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    
    // Note this handler block should be the exact same as the handler passed to any open calls.
     [FBSession.activeSession setStateChangeHandler:
      ^(FBSession *session, FBSessionState state, NSError *error) {
          
          [self sessionStateChanged:session State:state error:error];
      }];
     
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}
#pragma mark - Application's Documents directory

    // Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
