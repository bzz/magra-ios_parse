//
//  NutrientCell.m
//  ContarCalorias
//
//  Created by Lucian Gherghel on 26/02/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "NutrientCell.h"

@implementation NutrientCell
    
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self buildCellComponents];
    }
    return self;
}

- (void)buildCellComponents
{
    self.nutrientName = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 6.5, 200, 21)];
    self.quantity = [[UILabel alloc] initWithFrame:CGRectMake(205.0, 6.5, 53, 21)];

    
    self.nutrientName.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    [self.nutrientName setLineBreakMode:NSLineBreakByTruncatingTail];
    self.nutrientName.textColor = [UIColor blackColor];
    [self.nutrientName setTextAlignment:NSTextAlignmentLeft];
    self.nutrientName.backgroundColor = [UIColor clearColor];
    
    self.quantity.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    [self.quantity setLineBreakMode:NSLineBreakByTruncatingTail];
    self.quantity.textColor = [UIColor lightGrayColor];
    [self.quantity setTextAlignment:NSTextAlignmentRight];
    self.quantity.backgroundColor = [UIColor clearColor];
    
    [self addSubview:self.nutrientName];
    [self addSubview:self.quantity];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
