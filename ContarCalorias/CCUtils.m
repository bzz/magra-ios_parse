//
//  CCUtils.m
//  ContarCalorias
//
//  Created by andres portillo on 30/05/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCUtils.h"
#import "User.h"
#import "CCAPI.h"

@implementation CCUtils
+ (BOOL)emailIsValid:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
+ (NSString *)stringFromTimestamp:(NSDate *)date
{
    NSDateFormatter *dt = [NSDateFormatter new];
    [dt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    return [dt stringFromDate:date];
}
+ (NSDate *)timestampFromString:(NSString *)date
{
    NSDateFormatter *dt = [NSDateFormatter new];
    [dt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    return [dt dateFromString:date];
}
+ (NSDate *)dateFromString:(NSString *)date
{
    NSDateFormatter *dt = [NSDateFormatter new];
    [dt setDateFormat:@"yyyy-MM-dd"];
    
    return [dt dateFromString:date];
}
+ (NSString *)stringFromDate:(NSDate *)date
{
    NSDateFormatter *dt = [NSDateFormatter new];
    [dt setDateFormat:@"yyyy-MM-dd"];
    
    return [dt stringFromDate:date];
}
+ (NSString *)stringFromBirthdate:(NSDate *)date
{
    NSDateFormatter *dt = [NSDateFormatter new];
    [dt setDateFormat:@"dd-MM-yyyy"];
    
    return [dt stringFromDate:date];
}
+ (BOOL)datesHaveSameDay:(NSDate *)firstDate SecondDate:(NSDate*)secondDate
{
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *firstDateComponents = [gregorianCalendar components:NSDayCalendarUnit|NSMonthCalendarUnit
                                                                   fromDate:firstDate];
    NSDateComponents *secondUpdateComponents = [gregorianCalendar components:NSDayCalendarUnit|NSMonthCalendarUnit
                                                                  fromDate:secondDate];
    return firstDateComponents.day == secondUpdateComponents.day && firstDateComponents.month == secondUpdateComponents.month;
}
+ (CGFloat)getHeightForText:(NSString *)text Font:(UIFont *)font andWidth:(CGFloat)width
{
    NSAttributedString *attributedText =
    [[NSAttributedString alloc]
     initWithString:text
     attributes:@
     {
     NSFontAttributeName: font
     }];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize size = rect.size;
    return size.height;
}
+ (NSInteger)numberOfDaysWithinFirstDate:(NSDate *)firstDate andSecondDate:(NSDate *)secondDate
{
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *firstDateComponents = [gregorianCalendar components:NSDayCalendarUnit|NSMonthCalendarUnit
                                                                 fromDate:firstDate];
    [firstDateComponents setCalendar:gregorianCalendar];
    NSDateComponents *secondDateComponents = [gregorianCalendar components:NSDayCalendarUnit|NSMonthCalendarUnit
                                                                    fromDate:secondDate];
    [secondDateComponents setCalendar:gregorianCalendar];
    
    firstDateComponents.hour = 0;
    secondDateComponents.hour = 0;
    
    NSDateComponents *finalDateComponents = [gregorianCalendar components:NSDayCalendarUnit
                                                 fromDate:firstDateComponents.date toDate:secondDateComponents.date  options:0];
    
    return finalDateComponents.day;
}
+ (NSInteger)ageForUserWithBirthdate:(NSDate *)birthdate
{
    NSCalendar *calendar=[NSCalendar currentCalendar];
    NSDateComponents *components=[calendar components:NSYearCalendarUnit fromDate:birthdate toDate:[NSDate date]  options:0];
    return [components year];
}
+ (NSDate *)getDefaultWakeupTime
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [gregorianCalendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:[NSDate date]];
    [dateComponents setCalendar:gregorianCalendar];
    dateComponents.hour = 7;
    dateComponents.minute = 0;
    
    return dateComponents.date;
}
+ (NSString *)cleanWhiteSpacesForString:(NSString *)string
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^\\S\\r\\n]{2,}" options:NSRegularExpressionCaseInsensitive error:&error];
    NSString *modifiedString = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, [string length]) withTemplate:@" "];
    
    return  modifiedString;
    
}
@end
