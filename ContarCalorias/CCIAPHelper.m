//
//  CCIAPHelper.m
//  ContarCalorias
//
//  Created by andres portillo on 08/09/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCIAPHelper.h"
#import "CCConstants.h"
@implementation CCIAPHelper

+ (CCIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static CCIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      kCCMontlySubstriptionProductId,
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

@end
