//
//  NSString+HTML.m
//  ContarCalorias
//
//  Created by andres portillo on 02/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "NSString+HTML.h"
#import "NSMutableString+HTML.h"

@implementation NSString (HTML)
- (NSString *)xmlSimpleUnescapeString
{
    NSMutableString *unescapeStr = [NSMutableString stringWithString:self];
    
    return [unescapeStr xmlSimpleUnescape];
}


- (NSString *)xmlSimpleEscapeString
{
    NSMutableString *escapeStr = [NSMutableString stringWithString:self];
    
    return [escapeStr xmlSimpleEscape];
}
@end
