//
//  PickerData.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 26/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PickerData : NSObject

@property NSString *apiValue;
@property NSString *interfaceValue;

-(id)initWithInterfaceValue:(NSString *)interfaceValue andAPIValue:(NSString *)apiValue;

@end
