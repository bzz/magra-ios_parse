    //
    //  APLineChart.m
    //  ContarCalorias
    //
    //  Created by andres portillo on 12/8/14.
    //  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
    //

#import "APLineChart.h"
#import "UserValue.h"

@implementation APLineChart
{
    CGFloat _maxValue,_minValue,_lostWeight,_intervalValue,_pointsPerInterval;
    NSInteger _intervalsAmount;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}
- (void)render
{
    self.backgroundColor = [UIColor whiteColor];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    for(UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    [df setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"es_ES"]];
    [df setDateFormat:@"dd MMM"];
    _maxValue = [[_arrMarkers valueForKeyPath:@"@max.value"] floatValue];
    _minValue = MIN([[_arrMarkers valueForKeyPath:@"@min.value"] floatValue],_userGoalWeight);
    _lostWeight = _maxValue - _minValue;
    
    if (_lostWeight / 5 < 2 && _lostWeight / 5 < 1.5) {
        _intervalValue = 1.0;
    } else if (_lostWeight / 5 < 2 && _lostWeight / 5 > 1.5) {
        _intervalValue = 1.5;
    } else if (_lostWeight / 5 < 4) {
        _intervalValue = 3.0;
    } else if (_lostWeight / 5 < 7) {
        _intervalValue = 5.0;
    } else if (_lostWeight / 7.5 < 7) {
        _intervalValue = 7.5;
    } else if (_lostWeight / 10 < 7) {
        _intervalValue = 10.0;
    } else if (_lostWeight / 20.0 < 7) {
        _intervalValue = 20.0;
    } else if (_lostWeight / 40.0 < 7) {
        _intervalValue = 40.0;
    }
    
    _intervalsAmount =  MIN(7,(_lostWeight / _intervalValue) + 1);
    _pointsPerInterval = 160 / _intervalsAmount;
    
        //add yAxis labels
    for (int i = 0 ; i < _intervalsAmount; i++) {
        
        UILabel *lblInterval = [[UILabel alloc] initWithFrame:CGRectMake(5, 230 - (_pointsPerInterval * i), 20, 20)];
        [lblInterval setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10]];
        [lblInterval setTextAlignment:NSTextAlignmentRight];
        CGFloat intervalValue = _minValue + (i *_intervalValue);
        [lblInterval setText:[NSString stringWithFormat:@"%.0f",intervalValue]];
        [self addSubview:lblInterval];
        
    }
        //add xAxis labels
    
    UserValue *initialUserWeight = [_arrMarkers objectAtIndex:0];
    UILabel *lblInterval = [[UILabel alloc] initWithFrame:CGRectMake(5, 240, 40, 20)];
    [lblInterval setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:8]];
    [lblInterval setTextAlignment:NSTextAlignmentRight];
    [lblInterval setText:[NSString stringWithFormat:@"%@",[df stringFromDate:initialUserWeight.date]]];
    [self addSubview:lblInterval];
    NSMutableArray *arrArrangedMarkers = [_arrMarkers mutableCopy];
    
    if (_arrMarkers.count == 6) {
        [arrArrangedMarkers removeObjectAtIndex:3];
    } else if (_arrMarkers.count == 7) {
        NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSetWithIndex:3];
        [indexSet addIndex:5];
        [arrArrangedMarkers removeObjectsAtIndexes:indexSet];
    } else if (_arrMarkers.count == 8) {
        NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSetWithIndex:1];
        [indexSet addIndex:3];
        [indexSet addIndex:5];
        [arrArrangedMarkers removeObjectsAtIndexes:indexSet];
    } else if (_arrMarkers.count == 9) {
        NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSetWithIndex:1];
        [indexSet addIndex:3];
        [indexSet addIndex:5];
        [indexSet addIndex:7];
        [arrArrangedMarkers removeObjectsAtIndexes:indexSet];
    } else if (_arrMarkers.count >= 10) {
        
        NSInteger count = 1;
        
        while(arrArrangedMarkers.count > 5) {
            NSInteger thirdPart = (arrArrangedMarkers.count - 1)/3;
            NSInteger index =(int)(thirdPart * count);
            if((index + 1) ==  arrArrangedMarkers.count) {
                index = index - 1;
            }
            [arrArrangedMarkers removeObjectAtIndex:index];
            count ++;
            if(count == 4) count = 1;
        }
    }
    
    for (int i = 1 ; i < arrArrangedMarkers.count + 1; i++)
    {
        UserValue *userWeightEntry = [_arrMarkers objectAtIndex:MIN(i,arrArrangedMarkers.count - 1)];
        
        if (i < arrArrangedMarkers.count) {
            UILabel *lblInterval = [[UILabel alloc] initWithFrame:CGRectMake(5 + ((230 / 5) * i), 240, 40, 20)];
            [lblInterval setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:8]];
            [lblInterval setTextAlignment:NSTextAlignmentRight];
            [lblInterval setText:[NSString stringWithFormat:@"%@",[df stringFromDate:userWeightEntry.date]]];
            [self addSubview:lblInterval];
        } else {
            UILabel *lblInterval = [[UILabel alloc] initWithFrame:CGRectMake(5 + ((230 / 5) * i), 240, 40, 20)];
            [lblInterval setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:8]];
            [lblInterval setTextAlignment:NSTextAlignmentRight];
            [lblInterval setText:[NSString stringWithFormat:@"%@",[df stringFromDate:[userWeightEntry.date dateByAddingTimeInterval:60*60*24]]]];
            [self addSubview:lblInterval];
        }
        
    }
    
        //add markers
    
    UserValue *initialWeight = [_arrMarkers objectAtIndex:0];
    CGFloat markerValue =  initialWeight.valueValue;
    CGFloat diff = markerValue - _minValue;
    CGFloat yAxis =  230 - ((diff / _intervalValue) * _pointsPerInterval);
    CGFloat xAxis = 30 + ((230 / (_arrMarkers.count - 1)) * 0);
    UIImageView *imgInitialMarker = [[UIImageView alloc] init];
    
    imgInitialMarker = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"initial-marker"]];
    [imgInitialMarker setFrame:CGRectMake(xAxis - 2, yAxis - 6, imgInitialMarker.frame.size.width, imgInitialMarker.frame.size.height)];
    [self addSubview:imgInitialMarker];
    
    UIImageView *imgOrangeBubble = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"orange-pop"]];
    [imgOrangeBubble setFrame:CGRectMake(xAxis - 11, yAxis - 30 , imgOrangeBubble.frame.size.width, imgOrangeBubble.frame.size.height)];
    [self addSubview:imgOrangeBubble];
    
    UILabel *lblInitialValue = [[UILabel alloc] initWithFrame:CGRectMake(xAxis - 11, yAxis - 28, imgOrangeBubble.frame.size.width, 15)];
    [lblInitialValue setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:11]];
    [lblInitialValue setTextColor:[UIColor whiteColor]];
    [lblInitialValue setTextAlignment:NSTextAlignmentCenter];
    [lblInitialValue setText:[NSString stringWithFormat:@"%.1f",markerValue]];
    [self addSubview:lblInitialValue];
    
    
    UserValue *currentWeight = [_arrMarkers objectAtIndex:(_arrMarkers.count - 1)];
    markerValue =  currentWeight.valueValue;
    diff = markerValue - _minValue;
    yAxis =  230 - ((diff / _intervalValue) * _pointsPerInterval);
    if (_arrMarkers.count >= 5) {
        xAxis = 212;
    } else {
        xAxis = 30 + ((230 / 5) * (_arrMarkers.count - 1));
    }
    
    UIImageView *imgFinalMarker = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"final-marker"]];
    [imgFinalMarker setFrame:CGRectMake(xAxis - 2, yAxis - 6, imgFinalMarker.frame.size.width, imgFinalMarker.frame.size.height)];
    [self addSubview:imgFinalMarker];
    
    UIImageView *imgGreenBubble = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"green-pop"]];
    [imgGreenBubble setFrame:CGRectMake(xAxis - 12, yAxis - 30 , imgGreenBubble.frame.size.width, imgGreenBubble.frame.size.height)];
    [self addSubview:imgGreenBubble];
    
    UILabel *lblFinalValue = [[UILabel alloc] initWithFrame:CGRectMake(xAxis - 11, yAxis - 28, imgGreenBubble.frame.size.width, 15)];
    [lblFinalValue setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:11]];
    [lblFinalValue setTextColor:[UIColor whiteColor]];
    [lblFinalValue setTextAlignment:NSTextAlignmentCenter];
    [lblFinalValue setText:[NSString stringWithFormat:@"%.1f",markerValue]];
    [self addSubview:lblFinalValue];
    
    [self setNeedsDisplay];
}
- (void)drawRect:(CGRect)rect {
    
    if (_arrMarkers.count > 0) {
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSaveGState(context);
        
        CGFloat dash[] = {0.0, 0.0};
        CGContextSetLineDash(context, 0.0, dash, 2);
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:3.0/255.0 green:174.0/255.0 blue:220.0/255.0 alpha:0].CGColor); //change color here
        
        
            // fill path
        CGFloat lineWidth = 2.0;
        CGContextSetLineWidth(context, lineWidth);
        
        CGFloat yAxis =  self.bounds.size.height - 30;
        CGFloat xAxis = 30 + 3;
        CGPoint firstPoint = CGPointMake(xAxis, yAxis);
        CGContextMoveToPoint(context, firstPoint.x + lineWidth/2, firstPoint.y);
        
        UserValue *userWeight = [_arrMarkers objectAtIndex:0];
        CGFloat initialMarker = userWeight.valueValue;
        CGFloat diff = initialMarker - _minValue;
        yAxis =  230 - ((diff / _intervalValue) * _pointsPerInterval) + 3;
        xAxis = 33;
        CGPoint endPoint = CGPointMake(xAxis, yAxis);
        
        CGContextAddLineToPoint(context, endPoint.x + lineWidth/2, endPoint.y + lineWidth/2);
        
        for (int i = 1 ; i < _arrMarkers.count ; i++)
        {
            
            UserValue *userWeight = [_arrMarkers objectAtIndex:i];
            CGFloat finalMarker = userWeight.valueValue;
            diff = finalMarker - _minValue;
            yAxis =  230 - ((diff / _intervalValue) * _pointsPerInterval);
            if (_arrMarkers.count >= 5) {
                if ((i+1) == _arrMarkers.count) {
                    xAxis = 218;
                } else {
                    xAxis = 26 + ((195 / (_arrMarkers.count - 1)) * i);
                }
            } else {
                xAxis = 33 + ((230 / 5) * i);
            }
            CGPoint endPoint = CGPointMake(xAxis, yAxis);
            
            CGContextAddLineToPoint(context, endPoint.x + lineWidth/2, endPoint.y + lineWidth/2);
            
            if((i+1) == _arrMarkers.count) {
                CGContextAddLineToPoint(context,endPoint.x + lineWidth/2, firstPoint.y);
            }
        }
        
        CGContextSetFillColorWithColor(context, [UIColor colorWithRed:235.0/255.0 green:249.0/255.0 blue:252.0/255.0 alpha:1].CGColor); //change color here
        
        CGContextClosePath(context);
        CGContextDrawPath(context, kCGPathFillStroke);
        
            //background
        
            //dashed horizontal lines
        lineWidth = 1.0;
        
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1].CGColor); //change color here
        
        for (int i = 0 ; i < _intervalsAmount; i++) {
            
                //change line width here
            CGContextSetLineWidth(context, lineWidth);
            CGPoint startPoint =CGPointMake(30, 240 - (_pointsPerInterval * i)); //change start point here
            CGPoint endPoint = CGPointMake(290, 240 - (_pointsPerInterval * i));
            CGContextMoveToPoint(context, startPoint.x + lineWidth/2, startPoint.y + lineWidth/2);
            CGContextAddLineToPoint(context, endPoint.x + lineWidth/2, endPoint.y + lineWidth/2);
            CGFloat dash[] = {8.0, 4.0};
            CGContextSetLineDash(context, 0.0, dash, 2);
        }
        
        CGContextDrawPath(context, kCGPathStroke);
            //    CGContextRestoreGState(context);
        
        CGFloat dash2[] = {0.1,0.1};
        CGContextSetLineDash(context, 0.0, dash2,0);
        
            //goal weight line
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:173.0/255.0 green:202.0/255.0 blue:0.0/255.0 alpha:1].CGColor);
        diff = _userGoalWeight - _minValue;
        yAxis =  230 - ((diff / _intervalValue) * _pointsPerInterval);
        CGContextMoveToPoint(context, 30, yAxis);
        CGContextAddLineToPoint(context, 295, yAxis);
        CGContextDrawPath(context, kCGPathStroke);
        
            //vertical lines
        
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1].CGColor); //change color here
        
        for (int i = 1 ; i < 6 ; i++)
        {
            CGContextSetLineWidth(context, lineWidth);
            CGPoint startPoint =CGPointMake(34 + ((230 / 5) * i), 20); //change start point here
            CGPoint endPoint = CGPointMake(34 + ((230 / 5) * i), 240);
            CGContextMoveToPoint(context, startPoint.x + lineWidth/2, startPoint.y + lineWidth/2);
            CGContextAddLineToPoint(context, endPoint.x + lineWidth/2, endPoint.y + lineWidth/2);
        }
        CGContextDrawPath(context, kCGPathStroke);
        
            //stroke path
        
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:4.0/255.0 green:174.0/255.0 blue:220.0/255.0 alpha:1].CGColor); //change color here
        lineWidth = 2.0;
        CGContextSetLineWidth(context, lineWidth);
        
        UserValue *firstUserWeight = [_arrMarkers objectAtIndex:0];
        
        initialMarker = firstUserWeight.valueValue;
        diff = initialMarker - _minValue;
        yAxis =  230 - ((diff / _intervalValue) * _pointsPerInterval) + 3;
        xAxis = 33;
        firstPoint = CGPointMake(xAxis, yAxis);
        CGContextMoveToPoint(context, firstPoint.x + lineWidth/2, firstPoint.y + lineWidth/2);
        CGContextAddLineToPoint(context, endPoint.x + lineWidth/2, endPoint.y + lineWidth/2);
        
        for (int i = 1 ; i < _arrMarkers.count ; i++)
        {
            UserValue *userWeight = [_arrMarkers objectAtIndex:i];
            
            CGFloat finalMarker = userWeight.valueValue;
            diff = finalMarker - _minValue;
            yAxis =  230 - ((diff / _intervalValue) * _pointsPerInterval);
            if (_arrMarkers.count >= 5) {
                if ((i+1) == _arrMarkers.count) {
                    xAxis = 218;
                } else {
                    xAxis = 26 + ((195 / (_arrMarkers.count - 1)) * i);
                }
            } else {
                xAxis = 33 + ((230 / 5) * i);
            }
            CGPoint endPoint = CGPointMake(xAxis, yAxis);
            
            CGContextAddLineToPoint(context, endPoint.x + lineWidth/2, endPoint.y + lineWidth/2);
        }
        CGContextDrawPath(context, kCGPathStroke);

    }
}
@end
