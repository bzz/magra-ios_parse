// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Meal.m instead.

#import "_Meal.h"

const struct MealAttributes MealAttributes = {
	.dayOfPlan = @"dayOfPlan",
	.descriptionMeal = @"descriptionMeal",
	.idMeal = @"idMeal",
	.title = @"title",
};

const struct MealRelationships MealRelationships = {
	.mealPlan = @"mealPlan",
	.mealType = @"mealType",
	.recipe = @"recipe",
};

const struct MealFetchedProperties MealFetchedProperties = {
};

@implementation MealID
@end

@implementation _Meal

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Meal" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Meal";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Meal" inManagedObjectContext:moc_];
}

- (MealID*)objectID {
	return (MealID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"dayOfPlanValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"dayOfPlan"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"idMealValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"idMeal"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic dayOfPlan;



- (int64_t)dayOfPlanValue {
	NSNumber *result = [self dayOfPlan];
	return [result longLongValue];
}

- (void)setDayOfPlanValue:(int64_t)value_ {
	[self setDayOfPlan:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveDayOfPlanValue {
	NSNumber *result = [self primitiveDayOfPlan];
	return [result longLongValue];
}

- (void)setPrimitiveDayOfPlanValue:(int64_t)value_ {
	[self setPrimitiveDayOfPlan:[NSNumber numberWithLongLong:value_]];
}





@dynamic descriptionMeal;






@dynamic idMeal;



- (int64_t)idMealValue {
	NSNumber *result = [self idMeal];
	return [result longLongValue];
}

- (void)setIdMealValue:(int64_t)value_ {
	[self setIdMeal:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveIdMealValue {
	NSNumber *result = [self primitiveIdMeal];
	return [result longLongValue];
}

- (void)setPrimitiveIdMealValue:(int64_t)value_ {
	[self setPrimitiveIdMeal:[NSNumber numberWithLongLong:value_]];
}





@dynamic title;






@dynamic mealPlan;

	
- (NSMutableSet*)mealPlanSet {
	[self willAccessValueForKey:@"mealPlan"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mealPlan"];
  
	[self didAccessValueForKey:@"mealPlan"];
	return result;
}
	

@dynamic mealType;

	

@dynamic recipe;

	






@end
