// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Recipe.h instead.

#import <CoreData/CoreData.h>


extern const struct RecipeAttributes {
	__unsafe_unretained NSString *cookingTime;
	__unsafe_unretained NSString *idRecipe;
	__unsafe_unretained NSString *image;
	__unsafe_unretained NSString *instructions;
	__unsafe_unretained NSString *servingSize;
	__unsafe_unretained NSString *title;
} RecipeAttributes;

extern const struct RecipeRelationships {
	__unsafe_unretained NSString *ingredients;
	__unsafe_unretained NSString *meal;
	__unsafe_unretained NSString *nutritionalFacts;
} RecipeRelationships;

extern const struct RecipeFetchedProperties {
} RecipeFetchedProperties;

@class Ingredient;
@class Meal;
@class NutritionalFact_Recipe;








@interface RecipeID : NSManagedObjectID {}
@end

@interface _Recipe : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (RecipeID*)objectID;





@property (nonatomic, strong) NSString* cookingTime;



//- (BOOL)validateCookingTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* idRecipe;



@property int64_t idRecipeValue;
- (int64_t)idRecipeValue;
- (void)setIdRecipeValue:(int64_t)value_;

//- (BOOL)validateIdRecipe:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* image;



//- (BOOL)validateImage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* instructions;



//- (BOOL)validateInstructions:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* servingSize;



//- (BOOL)validateServingSize:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *ingredients;

- (NSMutableSet*)ingredientsSet;




@property (nonatomic, strong) Meal *meal;

//- (BOOL)validateMeal:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *nutritionalFacts;

- (NSMutableSet*)nutritionalFactsSet;





@end

@interface _Recipe (CoreDataGeneratedAccessors)

- (void)addIngredients:(NSSet*)value_;
- (void)removeIngredients:(NSSet*)value_;
- (void)addIngredientsObject:(Ingredient*)value_;
- (void)removeIngredientsObject:(Ingredient*)value_;

- (void)addNutritionalFacts:(NSSet*)value_;
- (void)removeNutritionalFacts:(NSSet*)value_;
- (void)addNutritionalFactsObject:(NutritionalFact_Recipe*)value_;
- (void)removeNutritionalFactsObject:(NutritionalFact_Recipe*)value_;

@end

@interface _Recipe (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveCookingTime;
- (void)setPrimitiveCookingTime:(NSString*)value;




- (NSNumber*)primitiveIdRecipe;
- (void)setPrimitiveIdRecipe:(NSNumber*)value;

- (int64_t)primitiveIdRecipeValue;
- (void)setPrimitiveIdRecipeValue:(int64_t)value_;




- (NSString*)primitiveImage;
- (void)setPrimitiveImage:(NSString*)value;




- (NSString*)primitiveInstructions;
- (void)setPrimitiveInstructions:(NSString*)value;




- (NSString*)primitiveServingSize;
- (void)setPrimitiveServingSize:(NSString*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;





- (NSMutableSet*)primitiveIngredients;
- (void)setPrimitiveIngredients:(NSMutableSet*)value;



- (Meal*)primitiveMeal;
- (void)setPrimitiveMeal:(Meal*)value;



- (NSMutableSet*)primitiveNutritionalFacts;
- (void)setPrimitiveNutritionalFacts:(NSMutableSet*)value;


@end
