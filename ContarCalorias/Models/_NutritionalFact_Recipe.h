// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to NutritionalFact_Recipe.h instead.

#import <CoreData/CoreData.h>


extern const struct NutritionalFact_RecipeAttributes {
	__unsafe_unretained NSString *value;
} NutritionalFact_RecipeAttributes;

extern const struct NutritionalFact_RecipeRelationships {
	__unsafe_unretained NSString *nutritionalFact;
	__unsafe_unretained NSString *recipe;
} NutritionalFact_RecipeRelationships;

extern const struct NutritionalFact_RecipeFetchedProperties {
} NutritionalFact_RecipeFetchedProperties;

@class NutritionalFact;
@class Recipe;



@interface NutritionalFact_RecipeID : NSManagedObjectID {}
@end

@interface _NutritionalFact_Recipe : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (NutritionalFact_RecipeID*)objectID;





@property (nonatomic, strong) NSNumber* value;



@property float valueValue;
- (float)valueValue;
- (void)setValueValue:(float)value_;

//- (BOOL)validateValue:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NutritionalFact *nutritionalFact;

//- (BOOL)validateNutritionalFact:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Recipe *recipe;

//- (BOOL)validateRecipe:(id*)value_ error:(NSError**)error_;





@end

@interface _NutritionalFact_Recipe (CoreDataGeneratedAccessors)

@end

@interface _NutritionalFact_Recipe (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveValue;
- (void)setPrimitiveValue:(NSNumber*)value;

- (float)primitiveValueValue;
- (void)setPrimitiveValueValue:(float)value_;





- (NutritionalFact*)primitiveNutritionalFact;
- (void)setPrimitiveNutritionalFact:(NutritionalFact*)value;



- (Recipe*)primitiveRecipe;
- (void)setPrimitiveRecipe:(Recipe*)value;


@end
