// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to User.h instead.

#import <CoreData/CoreData.h>


extern const struct UserAttributes {
	__unsafe_unretained NSString *activityLevel;
	__unsafe_unretained NSString *age;
	__unsafe_unretained NSString *authenticationType;
	__unsafe_unretained NSString *birthdate;
	__unsafe_unretained NSString *breakfastTime;
	__unsafe_unretained NSString *country;
	__unsafe_unretained NSString *currentWeightKg;
	__unsafe_unretained NSString *dailyCalories;
	__unsafe_unretained NSString *dailyCarbs;
	__unsafe_unretained NSString *dailyFats;
	__unsafe_unretained NSString *dailyProteins;
	__unsafe_unretained NSString *dateReachingDesiredWeight;
	__unsafe_unretained NSString *dateSubscribedToPro;
	__unsafe_unretained NSString *desiredWeightKg;
	__unsafe_unretained NSString *didCreateWeightLossPlan;
	__unsafe_unretained NSString *dinnerTime;
	__unsafe_unretained NSString *email;
	__unsafe_unretained NSString *facebookAccessToken;
	__unsafe_unretained NSString *facebookId;
	__unsafe_unretained NSString *firstDayDate;
	__unsafe_unretained NSString *firstSnackTime;
	__unsafe_unretained NSString *gender;
	__unsafe_unretained NSString *googleId;
	__unsafe_unretained NSString *heightCm;
	__unsafe_unretained NSString *idUser;
	__unsafe_unretained NSString *initialWeightKg;
	__unsafe_unretained NSString *isProUser;
	__unsafe_unretained NSString *lastModified;
	__unsafe_unretained NSString *lunchTime;
	__unsafe_unretained NSString *magraAccessToken;
	__unsafe_unretained NSString *mealPlanStartDate;
	__unsafe_unretained NSString *profilePictureUrl;
	__unsafe_unretained NSString *registrationDate;
	__unsafe_unretained NSString *secondSnackTime;
	__unsafe_unretained NSString *unitSystem;
	__unsafe_unretained NSString *username;
	__unsafe_unretained NSString *wakeupTime;
	__unsafe_unretained NSString *weightToLose;
} UserAttributes;

extern const struct UserRelationships {
	__unsafe_unretained NSString *loggedExercises;
	__unsafe_unretained NSString *loggedFoods;
	__unsafe_unretained NSString *loggedValues;
	__unsafe_unretained NSString *loggedWaters;
	__unsafe_unretained NSString *mealPlan;
} UserRelationships;

extern const struct UserFetchedProperties {
} UserFetchedProperties;

@class UserExercise;
@class UserFood;
@class UserValue;
@class UserWater;
@class MealPlan;








































@interface UserID : NSManagedObjectID {}
@end

@interface _User : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (UserID*)objectID;





@property (nonatomic, strong) NSNumber* activityLevel;



@property int16_t activityLevelValue;
- (int16_t)activityLevelValue;
- (void)setActivityLevelValue:(int16_t)value_;

//- (BOOL)validateActivityLevel:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* age;



@property int16_t ageValue;
- (int16_t)ageValue;
- (void)setAgeValue:(int16_t)value_;

//- (BOOL)validateAge:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* authenticationType;



@property float authenticationTypeValue;
- (float)authenticationTypeValue;
- (void)setAuthenticationTypeValue:(float)value_;

//- (BOOL)validateAuthenticationType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* birthdate;



//- (BOOL)validateBirthdate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* breakfastTime;



//- (BOOL)validateBreakfastTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* country;



//- (BOOL)validateCountry:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* currentWeightKg;



@property float currentWeightKgValue;
- (float)currentWeightKgValue;
- (void)setCurrentWeightKgValue:(float)value_;

//- (BOOL)validateCurrentWeightKg:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* dailyCalories;



@property int32_t dailyCaloriesValue;
- (int32_t)dailyCaloriesValue;
- (void)setDailyCaloriesValue:(int32_t)value_;

//- (BOOL)validateDailyCalories:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* dailyCarbs;



@property int32_t dailyCarbsValue;
- (int32_t)dailyCarbsValue;
- (void)setDailyCarbsValue:(int32_t)value_;

//- (BOOL)validateDailyCarbs:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* dailyFats;



@property int32_t dailyFatsValue;
- (int32_t)dailyFatsValue;
- (void)setDailyFatsValue:(int32_t)value_;

//- (BOOL)validateDailyFats:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* dailyProteins;



@property int32_t dailyProteinsValue;
- (int32_t)dailyProteinsValue;
- (void)setDailyProteinsValue:(int32_t)value_;

//- (BOOL)validateDailyProteins:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* dateReachingDesiredWeight;



//- (BOOL)validateDateReachingDesiredWeight:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* dateSubscribedToPro;



//- (BOOL)validateDateSubscribedToPro:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* desiredWeightKg;



@property float desiredWeightKgValue;
- (float)desiredWeightKgValue;
- (void)setDesiredWeightKgValue:(float)value_;

//- (BOOL)validateDesiredWeightKg:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* didCreateWeightLossPlan;



@property BOOL didCreateWeightLossPlanValue;
- (BOOL)didCreateWeightLossPlanValue;
- (void)setDidCreateWeightLossPlanValue:(BOOL)value_;

//- (BOOL)validateDidCreateWeightLossPlan:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* dinnerTime;



//- (BOOL)validateDinnerTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* email;



//- (BOOL)validateEmail:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* facebookAccessToken;



//- (BOOL)validateFacebookAccessToken:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* facebookId;



//- (BOOL)validateFacebookId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* firstDayDate;



//- (BOOL)validateFirstDayDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* firstSnackTime;



//- (BOOL)validateFirstSnackTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* gender;



@property int16_t genderValue;
- (int16_t)genderValue;
- (void)setGenderValue:(int16_t)value_;

//- (BOOL)validateGender:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* googleId;



//- (BOOL)validateGoogleId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* heightCm;



@property float heightCmValue;
- (float)heightCmValue;
- (void)setHeightCmValue:(float)value_;

//- (BOOL)validateHeightCm:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* idUser;



@property int64_t idUserValue;
- (int64_t)idUserValue;
- (void)setIdUserValue:(int64_t)value_;

//- (BOOL)validateIdUser:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* initialWeightKg;



@property float initialWeightKgValue;
- (float)initialWeightKgValue;
- (void)setInitialWeightKgValue:(float)value_;

//- (BOOL)validateInitialWeightKg:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* isProUser;



@property BOOL isProUserValue;
- (BOOL)isProUserValue;
- (void)setIsProUserValue:(BOOL)value_;

//- (BOOL)validateIsProUser:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* lastModified;



//- (BOOL)validateLastModified:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* lunchTime;



//- (BOOL)validateLunchTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* magraAccessToken;



//- (BOOL)validateMagraAccessToken:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* mealPlanStartDate;



//- (BOOL)validateMealPlanStartDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* profilePictureUrl;



//- (BOOL)validateProfilePictureUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* registrationDate;



//- (BOOL)validateRegistrationDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* secondSnackTime;



//- (BOOL)validateSecondSnackTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* unitSystem;



@property int16_t unitSystemValue;
- (int16_t)unitSystemValue;
- (void)setUnitSystemValue:(int16_t)value_;

//- (BOOL)validateUnitSystem:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* username;



//- (BOOL)validateUsername:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* wakeupTime;



//- (BOOL)validateWakeupTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* weightToLose;



@property float weightToLoseValue;
- (float)weightToLoseValue;
- (void)setWeightToLoseValue:(float)value_;

//- (BOOL)validateWeightToLose:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *loggedExercises;

- (NSMutableSet*)loggedExercisesSet;




@property (nonatomic, strong) NSSet *loggedFoods;

- (NSMutableSet*)loggedFoodsSet;




@property (nonatomic, strong) NSSet *loggedValues;

- (NSMutableSet*)loggedValuesSet;




@property (nonatomic, strong) NSSet *loggedWaters;

- (NSMutableSet*)loggedWatersSet;




@property (nonatomic, strong) MealPlan *mealPlan;

//- (BOOL)validateMealPlan:(id*)value_ error:(NSError**)error_;





@end

@interface _User (CoreDataGeneratedAccessors)

- (void)addLoggedExercises:(NSSet*)value_;
- (void)removeLoggedExercises:(NSSet*)value_;
- (void)addLoggedExercisesObject:(UserExercise*)value_;
- (void)removeLoggedExercisesObject:(UserExercise*)value_;

- (void)addLoggedFoods:(NSSet*)value_;
- (void)removeLoggedFoods:(NSSet*)value_;
- (void)addLoggedFoodsObject:(UserFood*)value_;
- (void)removeLoggedFoodsObject:(UserFood*)value_;

- (void)addLoggedValues:(NSSet*)value_;
- (void)removeLoggedValues:(NSSet*)value_;
- (void)addLoggedValuesObject:(UserValue*)value_;
- (void)removeLoggedValuesObject:(UserValue*)value_;

- (void)addLoggedWaters:(NSSet*)value_;
- (void)removeLoggedWaters:(NSSet*)value_;
- (void)addLoggedWatersObject:(UserWater*)value_;
- (void)removeLoggedWatersObject:(UserWater*)value_;

@end

@interface _User (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveActivityLevel;
- (void)setPrimitiveActivityLevel:(NSNumber*)value;

- (int16_t)primitiveActivityLevelValue;
- (void)setPrimitiveActivityLevelValue:(int16_t)value_;




- (NSNumber*)primitiveAge;
- (void)setPrimitiveAge:(NSNumber*)value;

- (int16_t)primitiveAgeValue;
- (void)setPrimitiveAgeValue:(int16_t)value_;




- (NSNumber*)primitiveAuthenticationType;
- (void)setPrimitiveAuthenticationType:(NSNumber*)value;

- (float)primitiveAuthenticationTypeValue;
- (void)setPrimitiveAuthenticationTypeValue:(float)value_;




- (NSDate*)primitiveBirthdate;
- (void)setPrimitiveBirthdate:(NSDate*)value;




- (NSDate*)primitiveBreakfastTime;
- (void)setPrimitiveBreakfastTime:(NSDate*)value;




- (NSString*)primitiveCountry;
- (void)setPrimitiveCountry:(NSString*)value;




- (NSNumber*)primitiveCurrentWeightKg;
- (void)setPrimitiveCurrentWeightKg:(NSNumber*)value;

- (float)primitiveCurrentWeightKgValue;
- (void)setPrimitiveCurrentWeightKgValue:(float)value_;




- (NSNumber*)primitiveDailyCalories;
- (void)setPrimitiveDailyCalories:(NSNumber*)value;

- (int32_t)primitiveDailyCaloriesValue;
- (void)setPrimitiveDailyCaloriesValue:(int32_t)value_;




- (NSNumber*)primitiveDailyCarbs;
- (void)setPrimitiveDailyCarbs:(NSNumber*)value;

- (int32_t)primitiveDailyCarbsValue;
- (void)setPrimitiveDailyCarbsValue:(int32_t)value_;




- (NSNumber*)primitiveDailyFats;
- (void)setPrimitiveDailyFats:(NSNumber*)value;

- (int32_t)primitiveDailyFatsValue;
- (void)setPrimitiveDailyFatsValue:(int32_t)value_;




- (NSNumber*)primitiveDailyProteins;
- (void)setPrimitiveDailyProteins:(NSNumber*)value;

- (int32_t)primitiveDailyProteinsValue;
- (void)setPrimitiveDailyProteinsValue:(int32_t)value_;




- (NSDate*)primitiveDateReachingDesiredWeight;
- (void)setPrimitiveDateReachingDesiredWeight:(NSDate*)value;




- (NSDate*)primitiveDateSubscribedToPro;
- (void)setPrimitiveDateSubscribedToPro:(NSDate*)value;




- (NSNumber*)primitiveDesiredWeightKg;
- (void)setPrimitiveDesiredWeightKg:(NSNumber*)value;

- (float)primitiveDesiredWeightKgValue;
- (void)setPrimitiveDesiredWeightKgValue:(float)value_;




- (NSNumber*)primitiveDidCreateWeightLossPlan;
- (void)setPrimitiveDidCreateWeightLossPlan:(NSNumber*)value;

- (BOOL)primitiveDidCreateWeightLossPlanValue;
- (void)setPrimitiveDidCreateWeightLossPlanValue:(BOOL)value_;




- (NSDate*)primitiveDinnerTime;
- (void)setPrimitiveDinnerTime:(NSDate*)value;




- (NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(NSString*)value;




- (NSString*)primitiveFacebookAccessToken;
- (void)setPrimitiveFacebookAccessToken:(NSString*)value;




- (NSString*)primitiveFacebookId;
- (void)setPrimitiveFacebookId:(NSString*)value;




- (NSDate*)primitiveFirstDayDate;
- (void)setPrimitiveFirstDayDate:(NSDate*)value;




- (NSDate*)primitiveFirstSnackTime;
- (void)setPrimitiveFirstSnackTime:(NSDate*)value;




- (NSNumber*)primitiveGender;
- (void)setPrimitiveGender:(NSNumber*)value;

- (int16_t)primitiveGenderValue;
- (void)setPrimitiveGenderValue:(int16_t)value_;




- (NSString*)primitiveGoogleId;
- (void)setPrimitiveGoogleId:(NSString*)value;




- (NSNumber*)primitiveHeightCm;
- (void)setPrimitiveHeightCm:(NSNumber*)value;

- (float)primitiveHeightCmValue;
- (void)setPrimitiveHeightCmValue:(float)value_;




- (NSNumber*)primitiveIdUser;
- (void)setPrimitiveIdUser:(NSNumber*)value;

- (int64_t)primitiveIdUserValue;
- (void)setPrimitiveIdUserValue:(int64_t)value_;




- (NSNumber*)primitiveInitialWeightKg;
- (void)setPrimitiveInitialWeightKg:(NSNumber*)value;

- (float)primitiveInitialWeightKgValue;
- (void)setPrimitiveInitialWeightKgValue:(float)value_;




- (NSNumber*)primitiveIsProUser;
- (void)setPrimitiveIsProUser:(NSNumber*)value;

- (BOOL)primitiveIsProUserValue;
- (void)setPrimitiveIsProUserValue:(BOOL)value_;




- (NSDate*)primitiveLastModified;
- (void)setPrimitiveLastModified:(NSDate*)value;




- (NSDate*)primitiveLunchTime;
- (void)setPrimitiveLunchTime:(NSDate*)value;




- (NSString*)primitiveMagraAccessToken;
- (void)setPrimitiveMagraAccessToken:(NSString*)value;




- (NSDate*)primitiveMealPlanStartDate;
- (void)setPrimitiveMealPlanStartDate:(NSDate*)value;




- (NSString*)primitiveProfilePictureUrl;
- (void)setPrimitiveProfilePictureUrl:(NSString*)value;




- (NSDate*)primitiveRegistrationDate;
- (void)setPrimitiveRegistrationDate:(NSDate*)value;




- (NSDate*)primitiveSecondSnackTime;
- (void)setPrimitiveSecondSnackTime:(NSDate*)value;




- (NSNumber*)primitiveUnitSystem;
- (void)setPrimitiveUnitSystem:(NSNumber*)value;

- (int16_t)primitiveUnitSystemValue;
- (void)setPrimitiveUnitSystemValue:(int16_t)value_;




- (NSString*)primitiveUsername;
- (void)setPrimitiveUsername:(NSString*)value;




- (NSDate*)primitiveWakeupTime;
- (void)setPrimitiveWakeupTime:(NSDate*)value;




- (NSNumber*)primitiveWeightToLose;
- (void)setPrimitiveWeightToLose:(NSNumber*)value;

- (float)primitiveWeightToLoseValue;
- (void)setPrimitiveWeightToLoseValue:(float)value_;





- (NSMutableSet*)primitiveLoggedExercises;
- (void)setPrimitiveLoggedExercises:(NSMutableSet*)value;



- (NSMutableSet*)primitiveLoggedFoods;
- (void)setPrimitiveLoggedFoods:(NSMutableSet*)value;



- (NSMutableSet*)primitiveLoggedValues;
- (void)setPrimitiveLoggedValues:(NSMutableSet*)value;



- (NSMutableSet*)primitiveLoggedWaters;
- (void)setPrimitiveLoggedWaters:(NSMutableSet*)value;



- (MealPlan*)primitiveMealPlan;
- (void)setPrimitiveMealPlan:(MealPlan*)value;


@end
