// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to NutritionalFact.m instead.

#import "_NutritionalFact.h"

const struct NutritionalFactAttributes NutritionalFactAttributes = {
	.name = @"name",
};

const struct NutritionalFactRelationships NutritionalFactRelationships = {
	.nutritionalFact_Recipes = @"nutritionalFact_Recipes",
};

const struct NutritionalFactFetchedProperties NutritionalFactFetchedProperties = {
};

@implementation NutritionalFactID
@end

@implementation _NutritionalFact

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"NutritionalFact" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"NutritionalFact";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"NutritionalFact" inManagedObjectContext:moc_];
}

- (NutritionalFactID*)objectID {
	return (NutritionalFactID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic name;






@dynamic nutritionalFact_Recipes;

	
- (NSMutableSet*)nutritionalFact_RecipesSet {
	[self willAccessValueForKey:@"nutritionalFact_Recipes"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"nutritionalFact_Recipes"];
  
	[self didAccessValueForKey:@"nutritionalFact_Recipes"];
	return result;
}
	






@end
