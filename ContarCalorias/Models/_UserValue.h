// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserValue.h instead.

#import <CoreData/CoreData.h>


extern const struct UserValueAttributes {
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *type;
	__unsafe_unretained NSString *userValueId;
	__unsafe_unretained NSString *value;
} UserValueAttributes;

extern const struct UserValueRelationships {
	__unsafe_unretained NSString *user;
} UserValueRelationships;

extern const struct UserValueFetchedProperties {
} UserValueFetchedProperties;

@class User;






@interface UserValueID : NSManagedObjectID {}
@end

@interface _UserValue : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (UserValueID*)objectID;





@property (nonatomic, strong) NSDate* date;



//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* userValueId;



@property int64_t userValueIdValue;
- (int64_t)userValueIdValue;
- (void)setUserValueIdValue:(int64_t)value_;

//- (BOOL)validateUserValueId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* value;



@property float valueValue;
- (float)valueValue;
- (void)setValueValue:(float)value_;

//- (BOOL)validateValue:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) User *user;

//- (BOOL)validateUser:(id*)value_ error:(NSError**)error_;





@end

@interface _UserValue (CoreDataGeneratedAccessors)

@end

@interface _UserValue (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;




- (NSNumber*)primitiveUserValueId;
- (void)setPrimitiveUserValueId:(NSNumber*)value;

- (int64_t)primitiveUserValueIdValue;
- (void)setPrimitiveUserValueIdValue:(int64_t)value_;




- (NSNumber*)primitiveValue;
- (void)setPrimitiveValue:(NSNumber*)value;

- (float)primitiveValueValue;
- (void)setPrimitiveValueValue:(float)value_;





- (User*)primitiveUser;
- (void)setPrimitiveUser:(User*)value;


@end
