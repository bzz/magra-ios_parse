#import "User.h"
#import "DDUnitConversion.h"
#import "CCConstants.h"
#import "CCUtils.h"

@interface User ()

// Private interface goes here.

@end


@implementation User

// Custom logic goes here.
- (double)getCurrentWeight
{
    return  self.unitSystemValue == CCUnitSystemBritish? [self currentWeightLbs]: self.currentWeightKgValue;
}
- (double)getInitialWeight
{
    return self.unitSystemValue == CCUnitSystemBritish? [self initialWeightLbs]: self.initialWeightKgValue;
}

- (NSString *)getWeightUnit
{
    return self.unitSystemValue == CCUnitSystemBritish?@"lb":@"kg";
    
}
- (double)initialWeightLbs
{
    return [[[DDUnitConverter massUnitConverter] convertNumber:self.initialWeightKg fromUnit:DDMassUnitKilograms toUnit:DDMassUnitUSPounds] floatValue];
}
- (double)currentWeightLbs
{
    return [[[DDUnitConverter massUnitConverter] convertNumber:self.currentWeightKg fromUnit:DDMassUnitKilograms toUnit:DDMassUnitUSPounds] floatValue];
}
- (double)getHeight
{
    return self.unitSystemValue == CCUnitSystemBritish? [self heightInches]:[[[DDUnitConverter lengthUnitConverter] convertNumber:@(self.heightCmValue) fromUnit:DDLengthUnitCentimeters toUnit:DDLengthUnitMeters] floatValue];
}
- (NSString *)weightUnitCurrentUnitSystem
{
    return self.unitSystemValue == CCUnitSystemBritish? @"lb":@"kg";
}
- (NSString *)heightUnitCurrentUnitSystem
{
    return self.unitSystemValue == CCUnitSystemBritish? @"ft":@"m";
}
- (BOOL)hasValidData
{
    if (self.heightCmValue > 0) {
            //  return self.currentWeightKgValue > 0 && self.birthdate && self.initialWeightKgValue > 0 && self.email.length > 0 && self.magraAccessToken.length > 0;
        return self.birthdate && self.initialWeightKgValue > 0 && self.email.length > 0 && self.magraAccessToken.length > 0;
    } else {
        return self.email.length > 0 && self.magraAccessToken.length > 0;
    }
}
- (double)getGoalWeight
{
    return self.unitSystemValue == CCUnitSystemBritish? [self goalWeightLbs]: self.desiredWeightKgValue;
}
- (double)goalWeightLbs
{
    return [[[DDUnitConverter massUnitConverter] convertNumber:self.desiredWeightKg fromUnit:DDMassUnitKilograms toUnit:DDMassUnitUSPounds] floatValue];
}
- (double)heightInches
{
    return [[[DDUnitConverter lengthUnitConverter] convertNumber:self.heightCm fromUnit:DDLengthUnitCentimeters toUnit:DDLengthUnitInches] floatValue];
}
- (void)setHeight:(CGFloat)height
{
    if(self.unitSystemValue == CCUnitSystemMetric) {
        
        self.heightCmValue = height;
        
    } else {
        
        self.heightCmValue =[[[DDUnitConverter lengthUnitConverter] convertNumber:@(height) fromUnit:DDLengthUnitFeet toUnit:DDLengthUnitCentimeters] floatValue];

    }
}
- (void)setInitialWeight:(CGFloat)weight
{
    if(self.unitSystemValue == CCUnitSystemMetric) {
        
        self.initialWeightKgValue = weight;
        
    } else {

        self.initialWeightKgValue = [[[DDUnitConverter massUnitConverter] convertNumber:@(weight) fromUnit:DDMassUnitUSPounds toUnit:DDMassUnitKilograms] floatValue];
    }
}
- (void)setCurrentWeight:(CGFloat)weight
{
    if(self.unitSystemValue == CCUnitSystemMetric) {
        
        self.currentWeightKgValue = weight;
    } else {
        self.currentWeightKgValue = [[[DDUnitConverter massUnitConverter] convertNumber:@(weight) fromUnit:DDMassUnitUSPounds toUnit:DDMassUnitKilograms] floatValue];
    }
}
- (void)setDesiredWeight:(CGFloat)desiredWeight
{
    if(self.unitSystemValue == CCUnitSystemMetric) {
        self.desiredWeightKgValue = desiredWeight;
    } else {
        self.desiredWeightKgValue = [[[DDUnitConverter massUnitConverter] convertNumber:@(desiredWeight) fromUnit:DDMassUnitUSPounds toUnit:DDMassUnitKilograms] floatValue];
    }
}
- (void)setUsersBirthdate:(NSDate *)birthdate
{
    self.birthdate = birthdate;
    self.ageValue = [CCUtils ageForUserWithBirthdate:birthdate];
}
- (NSInteger)daysToReachDesiredWeight
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *currentDateComponents = [gregorianCalendar components:NSDayCalendarUnit
                                                                       fromDate:[NSDate date] toDate:self.dateReachingDesiredWeight options:0];
    return currentDateComponents.day;
        
}
- (BOOL)hasProFeatures
{
    return self.isProUserValue;
}
- (void)setMealTimesUsingWakeupTime
{
    self.breakfastTime = [self.wakeupTime dateByAddingTimeInterval:60 * 60 * 0.5];
    self.firstSnackTime = [self.wakeupTime dateByAddingTimeInterval:60 * 60 * 3];
    self.lunchTime = [self.wakeupTime dateByAddingTimeInterval:60 * 60 * 5.5];
    self.secondSnackTime = [self.wakeupTime dateByAddingTimeInterval:60 * 60 * 8];
    self.dinnerTime = [self.wakeupTime dateByAddingTimeInterval:60 * 60 * 10.5];
    
}
- (void)updateDateReachingDesiredWeight
{
    CGFloat weeklyWeightToLose =(self.initialWeightKgValue > 90 ?   self.initialWeightKgValue * 0.01 :self.initialWeightKgValue * 0.005) * 1000;
    CGFloat totalWeightToLose = 0;
    int weeksForDiet;
    int daysToReachDesiredWeight = 0;
    totalWeightToLose =  (self.currentWeightKgValue - self.desiredWeightKgValue
                          ) * 1000; //(grams)
    if (totalWeightToLose > weeklyWeightToLose) {
        weeksForDiet = totalWeightToLose / fabsf(weeklyWeightToLose);
        daysToReachDesiredWeight = 7*weeksForDiet;
    } else {
        daysToReachDesiredWeight = 0;
    }
    
    NSDate *dateReachingDesiredWeight = [[NSDate date] dateByAddingTimeInterval:(60 * 60 * 24)  * daysToReachDesiredWeight];
    
    self.dateReachingDesiredWeight = dateReachingDesiredWeight;

}
@end
