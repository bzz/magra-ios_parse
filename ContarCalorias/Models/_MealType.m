// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MealType.m instead.

#import "_MealType.h"

const struct MealTypeAttributes MealTypeAttributes = {
	.name = @"name",
};

const struct MealTypeRelationships MealTypeRelationships = {
	.meals = @"meals",
};

const struct MealTypeFetchedProperties MealTypeFetchedProperties = {
};

@implementation MealTypeID
@end

@implementation _MealType

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MealType" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MealType";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MealType" inManagedObjectContext:moc_];
}

- (MealTypeID*)objectID {
	return (MealTypeID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic name;






@dynamic meals;

	
- (NSMutableSet*)mealsSet {
	[self willAccessValueForKey:@"meals"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"meals"];
  
	[self didAccessValueForKey:@"meals"];
	return result;
}
	






@end
