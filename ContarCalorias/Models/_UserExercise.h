// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserExercise.h instead.

#import <CoreData/CoreData.h>


extern const struct UserExerciseAttributes {
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *minutes;
	__unsafe_unretained NSString *totalCalories;
	__unsafe_unretained NSString *userExercise_id;
} UserExerciseAttributes;

extern const struct UserExerciseRelationships {
	__unsafe_unretained NSString *exercise;
	__unsafe_unretained NSString *user;
} UserExerciseRelationships;

extern const struct UserExerciseFetchedProperties {
} UserExerciseFetchedProperties;

@class Exercise;
@class User;






@interface UserExerciseID : NSManagedObjectID {}
@end

@interface _UserExercise : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (UserExerciseID*)objectID;





@property (nonatomic, strong) NSDate* date;



//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* minutes;



@property int16_t minutesValue;
- (int16_t)minutesValue;
- (void)setMinutesValue:(int16_t)value_;

//- (BOOL)validateMinutes:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* totalCalories;



@property int16_t totalCaloriesValue;
- (int16_t)totalCaloriesValue;
- (void)setTotalCaloriesValue:(int16_t)value_;

//- (BOOL)validateTotalCalories:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* userExercise_id;



@property int16_t userExercise_idValue;
- (int16_t)userExercise_idValue;
- (void)setUserExercise_idValue:(int16_t)value_;

//- (BOOL)validateUserExercise_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Exercise *exercise;

//- (BOOL)validateExercise:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) User *user;

//- (BOOL)validateUser:(id*)value_ error:(NSError**)error_;





@end

@interface _UserExercise (CoreDataGeneratedAccessors)

@end

@interface _UserExercise (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;




- (NSNumber*)primitiveMinutes;
- (void)setPrimitiveMinutes:(NSNumber*)value;

- (int16_t)primitiveMinutesValue;
- (void)setPrimitiveMinutesValue:(int16_t)value_;




- (NSNumber*)primitiveTotalCalories;
- (void)setPrimitiveTotalCalories:(NSNumber*)value;

- (int16_t)primitiveTotalCaloriesValue;
- (void)setPrimitiveTotalCaloriesValue:(int16_t)value_;




- (NSNumber*)primitiveUserExercise_id;
- (void)setPrimitiveUserExercise_id:(NSNumber*)value;

- (int16_t)primitiveUserExercise_idValue;
- (void)setPrimitiveUserExercise_idValue:(int16_t)value_;





- (Exercise*)primitiveExercise;
- (void)setPrimitiveExercise:(Exercise*)value;



- (User*)primitiveUser;
- (void)setPrimitiveUser:(User*)value;


@end
