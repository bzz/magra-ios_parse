// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserWater.h instead.

#import <CoreData/CoreData.h>


extern const struct UserWaterAttributes {
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *mililiters;
	__unsafe_unretained NSString *userWater_id;
} UserWaterAttributes;

extern const struct UserWaterRelationships {
	__unsafe_unretained NSString *user;
} UserWaterRelationships;

extern const struct UserWaterFetchedProperties {
} UserWaterFetchedProperties;

@class User;





@interface UserWaterID : NSManagedObjectID {}
@end

@interface _UserWater : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (UserWaterID*)objectID;





@property (nonatomic, strong) NSDate* date;



//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* mililiters;



@property int64_t mililitersValue;
- (int64_t)mililitersValue;
- (void)setMililitersValue:(int64_t)value_;

//- (BOOL)validateMililiters:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* userWater_id;



@property int64_t userWater_idValue;
- (int64_t)userWater_idValue;
- (void)setUserWater_idValue:(int64_t)value_;

//- (BOOL)validateUserWater_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) User *user;

//- (BOOL)validateUser:(id*)value_ error:(NSError**)error_;





@end

@interface _UserWater (CoreDataGeneratedAccessors)

@end

@interface _UserWater (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;




- (NSNumber*)primitiveMililiters;
- (void)setPrimitiveMililiters:(NSNumber*)value;

- (int64_t)primitiveMililitersValue;
- (void)setPrimitiveMililitersValue:(int64_t)value_;




- (NSNumber*)primitiveUserWater_id;
- (void)setPrimitiveUserWater_id:(NSNumber*)value;

- (int64_t)primitiveUserWater_idValue;
- (void)setPrimitiveUserWater_idValue:(int64_t)value_;





- (User*)primitiveUser;
- (void)setPrimitiveUser:(User*)value;


@end
