// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Food.h instead.

#import <CoreData/CoreData.h>


extern const struct FoodAttributes {
	__unsafe_unretained NSString *brand;
	__unsafe_unretained NSString *food_id;
	__unsafe_unretained NSString *isSearchable;
	__unsafe_unretained NSString *locale;
	__unsafe_unretained NSString *normalizedTitle;
	__unsafe_unretained NSString *title;
} FoodAttributes;

extern const struct FoodRelationships {
	__unsafe_unretained NSString *userFoods;
} FoodRelationships;

extern const struct FoodFetchedProperties {
} FoodFetchedProperties;

@class UserFood;








@interface FoodID : NSManagedObjectID {}
@end

@interface _Food : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FoodID*)objectID;





@property (nonatomic, strong) NSString* brand;



//- (BOOL)validateBrand:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* food_id;



@property int64_t food_idValue;
- (int64_t)food_idValue;
- (void)setFood_idValue:(int64_t)value_;

//- (BOOL)validateFood_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* isSearchable;



@property BOOL isSearchableValue;
- (BOOL)isSearchableValue;
- (void)setIsSearchableValue:(BOOL)value_;

//- (BOOL)validateIsSearchable:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* locale;



//- (BOOL)validateLocale:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* normalizedTitle;



//- (BOOL)validateNormalizedTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *userFoods;

- (NSMutableSet*)userFoodsSet;





@end

@interface _Food (CoreDataGeneratedAccessors)

- (void)addUserFoods:(NSSet*)value_;
- (void)removeUserFoods:(NSSet*)value_;
- (void)addUserFoodsObject:(UserFood*)value_;
- (void)removeUserFoodsObject:(UserFood*)value_;

@end

@interface _Food (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveBrand;
- (void)setPrimitiveBrand:(NSString*)value;




- (NSNumber*)primitiveFood_id;
- (void)setPrimitiveFood_id:(NSNumber*)value;

- (int64_t)primitiveFood_idValue;
- (void)setPrimitiveFood_idValue:(int64_t)value_;




- (NSNumber*)primitiveIsSearchable;
- (void)setPrimitiveIsSearchable:(NSNumber*)value;

- (BOOL)primitiveIsSearchableValue;
- (void)setPrimitiveIsSearchableValue:(BOOL)value_;




- (NSString*)primitiveLocale;
- (void)setPrimitiveLocale:(NSString*)value;




- (NSString*)primitiveNormalizedTitle;
- (void)setPrimitiveNormalizedTitle:(NSString*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;





- (NSMutableSet*)primitiveUserFoods;
- (void)setPrimitiveUserFoods:(NSMutableSet*)value;


@end
