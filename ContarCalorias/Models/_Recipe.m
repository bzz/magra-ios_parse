// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Recipe.m instead.

#import "_Recipe.h"

const struct RecipeAttributes RecipeAttributes = {
	.cookingTime = @"cookingTime",
	.idRecipe = @"idRecipe",
	.image = @"image",
	.instructions = @"instructions",
	.servingSize = @"servingSize",
	.title = @"title",
};

const struct RecipeRelationships RecipeRelationships = {
	.ingredients = @"ingredients",
	.meal = @"meal",
	.nutritionalFacts = @"nutritionalFacts",
};

const struct RecipeFetchedProperties RecipeFetchedProperties = {
};

@implementation RecipeID
@end

@implementation _Recipe

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Recipe" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Recipe";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Recipe" inManagedObjectContext:moc_];
}

- (RecipeID*)objectID {
	return (RecipeID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"idRecipeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"idRecipe"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic cookingTime;






@dynamic idRecipe;



- (int64_t)idRecipeValue {
	NSNumber *result = [self idRecipe];
	return [result longLongValue];
}

- (void)setIdRecipeValue:(int64_t)value_ {
	[self setIdRecipe:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveIdRecipeValue {
	NSNumber *result = [self primitiveIdRecipe];
	return [result longLongValue];
}

- (void)setPrimitiveIdRecipeValue:(int64_t)value_ {
	[self setPrimitiveIdRecipe:[NSNumber numberWithLongLong:value_]];
}





@dynamic image;






@dynamic instructions;






@dynamic servingSize;






@dynamic title;






@dynamic ingredients;

	
- (NSMutableSet*)ingredientsSet {
	[self willAccessValueForKey:@"ingredients"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"ingredients"];
  
	[self didAccessValueForKey:@"ingredients"];
	return result;
}
	

@dynamic meal;

	

@dynamic nutritionalFacts;

	
- (NSMutableSet*)nutritionalFactsSet {
	[self willAccessValueForKey:@"nutritionalFacts"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"nutritionalFacts"];
  
	[self didAccessValueForKey:@"nutritionalFacts"];
	return result;
}
	






@end
