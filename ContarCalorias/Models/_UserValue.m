// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserValue.m instead.

#import "_UserValue.h"

const struct UserValueAttributes UserValueAttributes = {
	.date = @"date",
	.type = @"type",
	.userValueId = @"userValueId",
	.value = @"value",
};

const struct UserValueRelationships UserValueRelationships = {
	.user = @"user",
};

const struct UserValueFetchedProperties UserValueFetchedProperties = {
};

@implementation UserValueID
@end

@implementation _UserValue

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserValue" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserValue";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserValue" inManagedObjectContext:moc_];
}

- (UserValueID*)objectID {
	return (UserValueID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"userValueIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userValueId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"valueValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"value"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic date;






@dynamic type;






@dynamic userValueId;



- (int64_t)userValueIdValue {
	NSNumber *result = [self userValueId];
	return [result longLongValue];
}

- (void)setUserValueIdValue:(int64_t)value_ {
	[self setUserValueId:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveUserValueIdValue {
	NSNumber *result = [self primitiveUserValueId];
	return [result longLongValue];
}

- (void)setPrimitiveUserValueIdValue:(int64_t)value_ {
	[self setPrimitiveUserValueId:[NSNumber numberWithLongLong:value_]];
}





@dynamic value;



- (float)valueValue {
	NSNumber *result = [self value];
	return [result floatValue];
}

- (void)setValueValue:(float)value_ {
	[self setValue:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveValueValue {
	NSNumber *result = [self primitiveValue];
	return [result floatValue];
}

- (void)setPrimitiveValueValue:(float)value_ {
	[self setPrimitiveValue:[NSNumber numberWithFloat:value_]];
}





@dynamic user;

	






@end
