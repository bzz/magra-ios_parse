// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to NutritionalFact.h instead.

#import <CoreData/CoreData.h>


extern const struct NutritionalFactAttributes {
	__unsafe_unretained NSString *name;
} NutritionalFactAttributes;

extern const struct NutritionalFactRelationships {
	__unsafe_unretained NSString *nutritionalFact_Recipes;
} NutritionalFactRelationships;

extern const struct NutritionalFactFetchedProperties {
} NutritionalFactFetchedProperties;

@class NutritionalFact_Recipe;



@interface NutritionalFactID : NSManagedObjectID {}
@end

@interface _NutritionalFact : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (NutritionalFactID*)objectID;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *nutritionalFact_Recipes;

- (NSMutableSet*)nutritionalFact_RecipesSet;





@end

@interface _NutritionalFact (CoreDataGeneratedAccessors)

- (void)addNutritionalFact_Recipes:(NSSet*)value_;
- (void)removeNutritionalFact_Recipes:(NSSet*)value_;
- (void)addNutritionalFact_RecipesObject:(NutritionalFact_Recipe*)value_;
- (void)removeNutritionalFact_RecipesObject:(NutritionalFact_Recipe*)value_;

@end

@interface _NutritionalFact (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (NSMutableSet*)primitiveNutritionalFact_Recipes;
- (void)setPrimitiveNutritionalFact_Recipes:(NSMutableSet*)value;


@end
