// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserWater.m instead.

#import "_UserWater.h"

const struct UserWaterAttributes UserWaterAttributes = {
	.date = @"date",
	.mililiters = @"mililiters",
	.userWater_id = @"userWater_id",
};

const struct UserWaterRelationships UserWaterRelationships = {
	.user = @"user",
};

const struct UserWaterFetchedProperties UserWaterFetchedProperties = {
};

@implementation UserWaterID
@end

@implementation _UserWater

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserWater" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserWater";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserWater" inManagedObjectContext:moc_];
}

- (UserWaterID*)objectID {
	return (UserWaterID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"mililitersValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"mililiters"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"userWater_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userWater_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic date;






@dynamic mililiters;



- (int64_t)mililitersValue {
	NSNumber *result = [self mililiters];
	return [result longLongValue];
}

- (void)setMililitersValue:(int64_t)value_ {
	[self setMililiters:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveMililitersValue {
	NSNumber *result = [self primitiveMililiters];
	return [result longLongValue];
}

- (void)setPrimitiveMililitersValue:(int64_t)value_ {
	[self setPrimitiveMililiters:[NSNumber numberWithLongLong:value_]];
}





@dynamic userWater_id;



- (int64_t)userWater_idValue {
	NSNumber *result = [self userWater_id];
	return [result longLongValue];
}

- (void)setUserWater_idValue:(int64_t)value_ {
	[self setUserWater_id:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveUserWater_idValue {
	NSNumber *result = [self primitiveUserWater_id];
	return [result longLongValue];
}

- (void)setPrimitiveUserWater_idValue:(int64_t)value_ {
	[self setPrimitiveUserWater_id:[NSNumber numberWithLongLong:value_]];
}





@dynamic user;

	






@end
