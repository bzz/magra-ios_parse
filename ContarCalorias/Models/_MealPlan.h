// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MealPlan.h instead.

#import <CoreData/CoreData.h>


extern const struct MealPlanAttributes {
	__unsafe_unretained NSString *days;
	__unsafe_unretained NSString *descriptionMealplan;
	__unsafe_unretained NSString *idMealPlan;
	__unsafe_unretained NSString *image;
	__unsafe_unretained NSString *locale;
	__unsafe_unretained NSString *title;
} MealPlanAttributes;

extern const struct MealPlanRelationships {
	__unsafe_unretained NSString *meals;
	__unsafe_unretained NSString *shoppingListItems;
	__unsafe_unretained NSString *user;
} MealPlanRelationships;

extern const struct MealPlanFetchedProperties {
} MealPlanFetchedProperties;

@class Meal;
@class ShoppingListItem;
@class User;








@interface MealPlanID : NSManagedObjectID {}
@end

@interface _MealPlan : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MealPlanID*)objectID;





@property (nonatomic, strong) NSNumber* days;



@property int16_t daysValue;
- (int16_t)daysValue;
- (void)setDaysValue:(int16_t)value_;

//- (BOOL)validateDays:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* descriptionMealplan;



//- (BOOL)validateDescriptionMealplan:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* idMealPlan;



@property int64_t idMealPlanValue;
- (int64_t)idMealPlanValue;
- (void)setIdMealPlanValue:(int64_t)value_;

//- (BOOL)validateIdMealPlan:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* image;



//- (BOOL)validateImage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* locale;



//- (BOOL)validateLocale:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *meals;

- (NSMutableSet*)mealsSet;




@property (nonatomic, strong) NSSet *shoppingListItems;

- (NSMutableSet*)shoppingListItemsSet;




@property (nonatomic, strong) User *user;

//- (BOOL)validateUser:(id*)value_ error:(NSError**)error_;





@end

@interface _MealPlan (CoreDataGeneratedAccessors)

- (void)addMeals:(NSSet*)value_;
- (void)removeMeals:(NSSet*)value_;
- (void)addMealsObject:(Meal*)value_;
- (void)removeMealsObject:(Meal*)value_;

- (void)addShoppingListItems:(NSSet*)value_;
- (void)removeShoppingListItems:(NSSet*)value_;
- (void)addShoppingListItemsObject:(ShoppingListItem*)value_;
- (void)removeShoppingListItemsObject:(ShoppingListItem*)value_;

@end

@interface _MealPlan (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveDays;
- (void)setPrimitiveDays:(NSNumber*)value;

- (int16_t)primitiveDaysValue;
- (void)setPrimitiveDaysValue:(int16_t)value_;




- (NSString*)primitiveDescriptionMealplan;
- (void)setPrimitiveDescriptionMealplan:(NSString*)value;




- (NSNumber*)primitiveIdMealPlan;
- (void)setPrimitiveIdMealPlan:(NSNumber*)value;

- (int64_t)primitiveIdMealPlanValue;
- (void)setPrimitiveIdMealPlanValue:(int64_t)value_;




- (NSString*)primitiveImage;
- (void)setPrimitiveImage:(NSString*)value;




- (NSString*)primitiveLocale;
- (void)setPrimitiveLocale:(NSString*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;





- (NSMutableSet*)primitiveMeals;
- (void)setPrimitiveMeals:(NSMutableSet*)value;



- (NSMutableSet*)primitiveShoppingListItems;
- (void)setPrimitiveShoppingListItems:(NSMutableSet*)value;



- (User*)primitiveUser;
- (void)setPrimitiveUser:(User*)value;


@end
