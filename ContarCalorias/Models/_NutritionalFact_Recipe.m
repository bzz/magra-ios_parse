// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to NutritionalFact_Recipe.m instead.

#import "_NutritionalFact_Recipe.h"

const struct NutritionalFact_RecipeAttributes NutritionalFact_RecipeAttributes = {
	.value = @"value",
};

const struct NutritionalFact_RecipeRelationships NutritionalFact_RecipeRelationships = {
	.nutritionalFact = @"nutritionalFact",
	.recipe = @"recipe",
};

const struct NutritionalFact_RecipeFetchedProperties NutritionalFact_RecipeFetchedProperties = {
};

@implementation NutritionalFact_RecipeID
@end

@implementation _NutritionalFact_Recipe

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"NutritionalFact_Recipe" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"NutritionalFact_Recipe";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"NutritionalFact_Recipe" inManagedObjectContext:moc_];
}

- (NutritionalFact_RecipeID*)objectID {
	return (NutritionalFact_RecipeID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"valueValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"value"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic value;



- (float)valueValue {
	NSNumber *result = [self value];
	return [result floatValue];
}

- (void)setValueValue:(float)value_ {
	[self setValue:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveValueValue {
	NSNumber *result = [self primitiveValue];
	return [result floatValue];
}

- (void)setPrimitiveValueValue:(float)value_ {
	[self setPrimitiveValue:[NSNumber numberWithFloat:value_]];
}





@dynamic nutritionalFact;

	

@dynamic recipe;

	






@end
