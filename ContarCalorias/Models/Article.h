//
//  Article.h
//  ContarCalorias
//
//  Created by andres portillo on 1/8/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Article : NSObject
@property(nonatomic) NSInteger articleId;
@property(nonatomic) BOOL completed;
@property(nonatomic) NSInteger day;
@property(nonatomic, strong) NSDate *date;
@property(nonatomic, strong) NSString *imageUrl;
@property(nonatomic, strong) NSData *imageData;
@property(nonatomic, strong) UIImage *image;
@property(nonatomic, strong) NSString *locale;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *content;
@property(nonatomic ,strong) NSString *type;
@end
