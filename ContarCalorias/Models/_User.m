// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to User.m instead.

#import "_User.h"

const struct UserAttributes UserAttributes = {
	.activityLevel = @"activityLevel",
	.age = @"age",
	.authenticationType = @"authenticationType",
	.birthdate = @"birthdate",
	.breakfastTime = @"breakfastTime",
	.country = @"country",
	.currentWeightKg = @"currentWeightKg",
	.dailyCalories = @"dailyCalories",
	.dailyCarbs = @"dailyCarbs",
	.dailyFats = @"dailyFats",
	.dailyProteins = @"dailyProteins",
	.dateReachingDesiredWeight = @"dateReachingDesiredWeight",
	.dateSubscribedToPro = @"dateSubscribedToPro",
	.desiredWeightKg = @"desiredWeightKg",
	.didCreateWeightLossPlan = @"didCreateWeightLossPlan",
	.dinnerTime = @"dinnerTime",
	.email = @"email",
	.facebookAccessToken = @"facebookAccessToken",
	.facebookId = @"facebookId",
	.firstDayDate = @"firstDayDate",
	.firstSnackTime = @"firstSnackTime",
	.gender = @"gender",
	.googleId = @"googleId",
	.heightCm = @"heightCm",
	.idUser = @"idUser",
	.initialWeightKg = @"initialWeightKg",
	.isProUser = @"isProUser",
	.lastModified = @"lastModified",
	.lunchTime = @"lunchTime",
	.magraAccessToken = @"magraAccessToken",
	.mealPlanStartDate = @"mealPlanStartDate",
	.profilePictureUrl = @"profilePictureUrl",
	.registrationDate = @"registrationDate",
	.secondSnackTime = @"secondSnackTime",
	.unitSystem = @"unitSystem",
	.username = @"username",
	.wakeupTime = @"wakeupTime",
	.weightToLose = @"weightToLose",
};

const struct UserRelationships UserRelationships = {
	.loggedExercises = @"loggedExercises",
	.loggedFoods = @"loggedFoods",
	.loggedValues = @"loggedValues",
	.loggedWaters = @"loggedWaters",
	.mealPlan = @"mealPlan",
};

const struct UserFetchedProperties UserFetchedProperties = {
};

@implementation UserID
@end

@implementation _User

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"User";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"User" inManagedObjectContext:moc_];
}

- (UserID*)objectID {
	return (UserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"activityLevelValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"activityLevel"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ageValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"age"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"authenticationTypeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"authenticationType"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"currentWeightKgValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"currentWeightKg"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"dailyCaloriesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"dailyCalories"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"dailyCarbsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"dailyCarbs"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"dailyFatsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"dailyFats"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"dailyProteinsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"dailyProteins"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"desiredWeightKgValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"desiredWeightKg"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"didCreateWeightLossPlanValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"didCreateWeightLossPlan"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"genderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gender"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"heightCmValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"heightCm"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"idUserValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"idUser"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"initialWeightKgValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"initialWeightKg"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isProUserValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isProUser"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"unitSystemValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"unitSystem"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"weightToLoseValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"weightToLose"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic activityLevel;



- (int16_t)activityLevelValue {
	NSNumber *result = [self activityLevel];
	return [result shortValue];
}

- (void)setActivityLevelValue:(int16_t)value_ {
	[self setActivityLevel:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveActivityLevelValue {
	NSNumber *result = [self primitiveActivityLevel];
	return [result shortValue];
}

- (void)setPrimitiveActivityLevelValue:(int16_t)value_ {
	[self setPrimitiveActivityLevel:[NSNumber numberWithShort:value_]];
}





@dynamic age;



- (int16_t)ageValue {
	NSNumber *result = [self age];
	return [result shortValue];
}

- (void)setAgeValue:(int16_t)value_ {
	[self setAge:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveAgeValue {
	NSNumber *result = [self primitiveAge];
	return [result shortValue];
}

- (void)setPrimitiveAgeValue:(int16_t)value_ {
	[self setPrimitiveAge:[NSNumber numberWithShort:value_]];
}





@dynamic authenticationType;



- (float)authenticationTypeValue {
	NSNumber *result = [self authenticationType];
	return [result floatValue];
}

- (void)setAuthenticationTypeValue:(float)value_ {
	[self setAuthenticationType:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveAuthenticationTypeValue {
	NSNumber *result = [self primitiveAuthenticationType];
	return [result floatValue];
}

- (void)setPrimitiveAuthenticationTypeValue:(float)value_ {
	[self setPrimitiveAuthenticationType:[NSNumber numberWithFloat:value_]];
}





@dynamic birthdate;






@dynamic breakfastTime;






@dynamic country;






@dynamic currentWeightKg;



- (float)currentWeightKgValue {
	NSNumber *result = [self currentWeightKg];
	return [result floatValue];
}

- (void)setCurrentWeightKgValue:(float)value_ {
	[self setCurrentWeightKg:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveCurrentWeightKgValue {
	NSNumber *result = [self primitiveCurrentWeightKg];
	return [result floatValue];
}

- (void)setPrimitiveCurrentWeightKgValue:(float)value_ {
	[self setPrimitiveCurrentWeightKg:[NSNumber numberWithFloat:value_]];
}





@dynamic dailyCalories;



- (int32_t)dailyCaloriesValue {
	NSNumber *result = [self dailyCalories];
	return [result intValue];
}

- (void)setDailyCaloriesValue:(int32_t)value_ {
	[self setDailyCalories:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveDailyCaloriesValue {
	NSNumber *result = [self primitiveDailyCalories];
	return [result intValue];
}

- (void)setPrimitiveDailyCaloriesValue:(int32_t)value_ {
	[self setPrimitiveDailyCalories:[NSNumber numberWithInt:value_]];
}





@dynamic dailyCarbs;



- (int32_t)dailyCarbsValue {
	NSNumber *result = [self dailyCarbs];
	return [result intValue];
}

- (void)setDailyCarbsValue:(int32_t)value_ {
	[self setDailyCarbs:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveDailyCarbsValue {
	NSNumber *result = [self primitiveDailyCarbs];
	return [result intValue];
}

- (void)setPrimitiveDailyCarbsValue:(int32_t)value_ {
	[self setPrimitiveDailyCarbs:[NSNumber numberWithInt:value_]];
}





@dynamic dailyFats;



- (int32_t)dailyFatsValue {
	NSNumber *result = [self dailyFats];
	return [result intValue];
}

- (void)setDailyFatsValue:(int32_t)value_ {
	[self setDailyFats:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveDailyFatsValue {
	NSNumber *result = [self primitiveDailyFats];
	return [result intValue];
}

- (void)setPrimitiveDailyFatsValue:(int32_t)value_ {
	[self setPrimitiveDailyFats:[NSNumber numberWithInt:value_]];
}





@dynamic dailyProteins;



- (int32_t)dailyProteinsValue {
	NSNumber *result = [self dailyProteins];
	return [result intValue];
}

- (void)setDailyProteinsValue:(int32_t)value_ {
	[self setDailyProteins:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveDailyProteinsValue {
	NSNumber *result = [self primitiveDailyProteins];
	return [result intValue];
}

- (void)setPrimitiveDailyProteinsValue:(int32_t)value_ {
	[self setPrimitiveDailyProteins:[NSNumber numberWithInt:value_]];
}





@dynamic dateReachingDesiredWeight;






@dynamic dateSubscribedToPro;






@dynamic desiredWeightKg;



- (float)desiredWeightKgValue {
	NSNumber *result = [self desiredWeightKg];
	return [result floatValue];
}

- (void)setDesiredWeightKgValue:(float)value_ {
	[self setDesiredWeightKg:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveDesiredWeightKgValue {
	NSNumber *result = [self primitiveDesiredWeightKg];
	return [result floatValue];
}

- (void)setPrimitiveDesiredWeightKgValue:(float)value_ {
	[self setPrimitiveDesiredWeightKg:[NSNumber numberWithFloat:value_]];
}





@dynamic didCreateWeightLossPlan;



- (BOOL)didCreateWeightLossPlanValue {
	NSNumber *result = [self didCreateWeightLossPlan];
	return [result boolValue];
}

- (void)setDidCreateWeightLossPlanValue:(BOOL)value_ {
	[self setDidCreateWeightLossPlan:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveDidCreateWeightLossPlanValue {
	NSNumber *result = [self primitiveDidCreateWeightLossPlan];
	return [result boolValue];
}

- (void)setPrimitiveDidCreateWeightLossPlanValue:(BOOL)value_ {
	[self setPrimitiveDidCreateWeightLossPlan:[NSNumber numberWithBool:value_]];
}





@dynamic dinnerTime;






@dynamic email;






@dynamic facebookAccessToken;






@dynamic facebookId;






@dynamic firstDayDate;






@dynamic firstSnackTime;






@dynamic gender;



- (int16_t)genderValue {
	NSNumber *result = [self gender];
	return [result shortValue];
}

- (void)setGenderValue:(int16_t)value_ {
	[self setGender:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveGenderValue {
	NSNumber *result = [self primitiveGender];
	return [result shortValue];
}

- (void)setPrimitiveGenderValue:(int16_t)value_ {
	[self setPrimitiveGender:[NSNumber numberWithShort:value_]];
}





@dynamic googleId;






@dynamic heightCm;



- (float)heightCmValue {
	NSNumber *result = [self heightCm];
	return [result floatValue];
}

- (void)setHeightCmValue:(float)value_ {
	[self setHeightCm:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveHeightCmValue {
	NSNumber *result = [self primitiveHeightCm];
	return [result floatValue];
}

- (void)setPrimitiveHeightCmValue:(float)value_ {
	[self setPrimitiveHeightCm:[NSNumber numberWithFloat:value_]];
}





@dynamic idUser;



- (int64_t)idUserValue {
	NSNumber *result = [self idUser];
	return [result longLongValue];
}

- (void)setIdUserValue:(int64_t)value_ {
	[self setIdUser:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveIdUserValue {
	NSNumber *result = [self primitiveIdUser];
	return [result longLongValue];
}

- (void)setPrimitiveIdUserValue:(int64_t)value_ {
	[self setPrimitiveIdUser:[NSNumber numberWithLongLong:value_]];
}





@dynamic initialWeightKg;



- (float)initialWeightKgValue {
	NSNumber *result = [self initialWeightKg];
	return [result floatValue];
}

- (void)setInitialWeightKgValue:(float)value_ {
	[self setInitialWeightKg:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveInitialWeightKgValue {
	NSNumber *result = [self primitiveInitialWeightKg];
	return [result floatValue];
}

- (void)setPrimitiveInitialWeightKgValue:(float)value_ {
	[self setPrimitiveInitialWeightKg:[NSNumber numberWithFloat:value_]];
}





@dynamic isProUser;



- (BOOL)isProUserValue {
	NSNumber *result = [self isProUser];
	return [result boolValue];
}

- (void)setIsProUserValue:(BOOL)value_ {
	[self setIsProUser:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsProUserValue {
	NSNumber *result = [self primitiveIsProUser];
	return [result boolValue];
}

- (void)setPrimitiveIsProUserValue:(BOOL)value_ {
	[self setPrimitiveIsProUser:[NSNumber numberWithBool:value_]];
}





@dynamic lastModified;






@dynamic lunchTime;






@dynamic magraAccessToken;






@dynamic mealPlanStartDate;






@dynamic profilePictureUrl;






@dynamic registrationDate;






@dynamic secondSnackTime;






@dynamic unitSystem;



- (int16_t)unitSystemValue {
	NSNumber *result = [self unitSystem];
	return [result shortValue];
}

- (void)setUnitSystemValue:(int16_t)value_ {
	[self setUnitSystem:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveUnitSystemValue {
	NSNumber *result = [self primitiveUnitSystem];
	return [result shortValue];
}

- (void)setPrimitiveUnitSystemValue:(int16_t)value_ {
	[self setPrimitiveUnitSystem:[NSNumber numberWithShort:value_]];
}





@dynamic username;






@dynamic wakeupTime;






@dynamic weightToLose;



- (float)weightToLoseValue {
	NSNumber *result = [self weightToLose];
	return [result floatValue];
}

- (void)setWeightToLoseValue:(float)value_ {
	[self setWeightToLose:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveWeightToLoseValue {
	NSNumber *result = [self primitiveWeightToLose];
	return [result floatValue];
}

- (void)setPrimitiveWeightToLoseValue:(float)value_ {
	[self setPrimitiveWeightToLose:[NSNumber numberWithFloat:value_]];
}





@dynamic loggedExercises;

	
- (NSMutableSet*)loggedExercisesSet {
	[self willAccessValueForKey:@"loggedExercises"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"loggedExercises"];
  
	[self didAccessValueForKey:@"loggedExercises"];
	return result;
}
	

@dynamic loggedFoods;

	
- (NSMutableSet*)loggedFoodsSet {
	[self willAccessValueForKey:@"loggedFoods"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"loggedFoods"];
  
	[self didAccessValueForKey:@"loggedFoods"];
	return result;
}
	

@dynamic loggedValues;

	
- (NSMutableSet*)loggedValuesSet {
	[self willAccessValueForKey:@"loggedValues"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"loggedValues"];
  
	[self didAccessValueForKey:@"loggedValues"];
	return result;
}
	

@dynamic loggedWaters;

	
- (NSMutableSet*)loggedWatersSet {
	[self willAccessValueForKey:@"loggedWaters"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"loggedWaters"];
  
	[self didAccessValueForKey:@"loggedWaters"];
	return result;
}
	

@dynamic mealPlan;

	






@end
