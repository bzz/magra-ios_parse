// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ShoppingListItem.m instead.

#import "_ShoppingListItem.h"

const struct ShoppingListItemAttributes ShoppingListItemAttributes = {
	.amount = @"amount",
	.name = @"name",
};

const struct ShoppingListItemRelationships ShoppingListItemRelationships = {
	.mealPlan = @"mealPlan",
};

const struct ShoppingListItemFetchedProperties ShoppingListItemFetchedProperties = {
};

@implementation ShoppingListItemID
@end

@implementation _ShoppingListItem

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ShoppingListItem" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ShoppingListItem";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ShoppingListItem" inManagedObjectContext:moc_];
}

- (ShoppingListItemID*)objectID {
	return (ShoppingListItemID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic amount;






@dynamic name;






@dynamic mealPlan;

	






@end
