// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ShoppingListItem.h instead.

#import <CoreData/CoreData.h>


extern const struct ShoppingListItemAttributes {
	__unsafe_unretained NSString *amount;
	__unsafe_unretained NSString *name;
} ShoppingListItemAttributes;

extern const struct ShoppingListItemRelationships {
	__unsafe_unretained NSString *mealPlan;
} ShoppingListItemRelationships;

extern const struct ShoppingListItemFetchedProperties {
} ShoppingListItemFetchedProperties;

@class MealPlan;




@interface ShoppingListItemID : NSManagedObjectID {}
@end

@interface _ShoppingListItem : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ShoppingListItemID*)objectID;





@property (nonatomic, strong) NSString* amount;



//- (BOOL)validateAmount:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) MealPlan *mealPlan;

//- (BOOL)validateMealPlan:(id*)value_ error:(NSError**)error_;





@end

@interface _ShoppingListItem (CoreDataGeneratedAccessors)

@end

@interface _ShoppingListItem (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAmount;
- (void)setPrimitiveAmount:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (MealPlan*)primitiveMealPlan;
- (void)setPrimitiveMealPlan:(MealPlan*)value;


@end
