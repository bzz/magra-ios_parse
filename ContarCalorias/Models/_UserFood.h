// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserFood.h instead.

#import <CoreData/CoreData.h>


extern const struct UserFoodAttributes {
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *meanEnumType;
	__unsafe_unretained NSString *numberOfServings;
	__unsafe_unretained NSString *userFood_id;
} UserFoodAttributes;

extern const struct UserFoodRelationships {
	__unsafe_unretained NSString *food;
	__unsafe_unretained NSString *foodServing;
	__unsafe_unretained NSString *user;
} UserFoodRelationships;

extern const struct UserFoodFetchedProperties {
} UserFoodFetchedProperties;

@class Food;
@class FoodServing;
@class User;






@interface UserFoodID : NSManagedObjectID {}
@end

@interface _UserFood : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (UserFoodID*)objectID;





@property (nonatomic, strong) NSDate* date;



//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* meanEnumType;



@property int16_t meanEnumTypeValue;
- (int16_t)meanEnumTypeValue;
- (void)setMeanEnumTypeValue:(int16_t)value_;

//- (BOOL)validateMeanEnumType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* numberOfServings;



@property int16_t numberOfServingsValue;
- (int16_t)numberOfServingsValue;
- (void)setNumberOfServingsValue:(int16_t)value_;

//- (BOOL)validateNumberOfServings:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* userFood_id;



@property int64_t userFood_idValue;
- (int64_t)userFood_idValue;
- (void)setUserFood_idValue:(int64_t)value_;

//- (BOOL)validateUserFood_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Food *food;

//- (BOOL)validateFood:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) FoodServing *foodServing;

//- (BOOL)validateFoodServing:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) User *user;

//- (BOOL)validateUser:(id*)value_ error:(NSError**)error_;





@end

@interface _UserFood (CoreDataGeneratedAccessors)

@end

@interface _UserFood (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;




- (NSNumber*)primitiveMeanEnumType;
- (void)setPrimitiveMeanEnumType:(NSNumber*)value;

- (int16_t)primitiveMeanEnumTypeValue;
- (void)setPrimitiveMeanEnumTypeValue:(int16_t)value_;




- (NSNumber*)primitiveNumberOfServings;
- (void)setPrimitiveNumberOfServings:(NSNumber*)value;

- (int16_t)primitiveNumberOfServingsValue;
- (void)setPrimitiveNumberOfServingsValue:(int16_t)value_;




- (NSNumber*)primitiveUserFood_id;
- (void)setPrimitiveUserFood_id:(NSNumber*)value;

- (int64_t)primitiveUserFood_idValue;
- (void)setPrimitiveUserFood_idValue:(int64_t)value_;





- (Food*)primitiveFood;
- (void)setPrimitiveFood:(Food*)value;



- (FoodServing*)primitiveFoodServing;
- (void)setPrimitiveFoodServing:(FoodServing*)value;



- (User*)primitiveUser;
- (void)setPrimitiveUser:(User*)value;


@end
