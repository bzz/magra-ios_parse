// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Exercise.m instead.

#import "_Exercise.h"

const struct ExerciseAttributes ExerciseAttributes = {
	.exercise_id = @"exercise_id",
	.lang = @"lang",
	.met = @"met",
	.name = @"name",
};

const struct ExerciseRelationships ExerciseRelationships = {
	.userExercises = @"userExercises",
};

const struct ExerciseFetchedProperties ExerciseFetchedProperties = {
};

@implementation ExerciseID
@end

@implementation _Exercise

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Exercise" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Exercise";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Exercise" inManagedObjectContext:moc_];
}

- (ExerciseID*)objectID {
	return (ExerciseID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"exercise_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"exercise_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"metValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"met"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic exercise_id;



- (int64_t)exercise_idValue {
	NSNumber *result = [self exercise_id];
	return [result longLongValue];
}

- (void)setExercise_idValue:(int64_t)value_ {
	[self setExercise_id:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveExercise_idValue {
	NSNumber *result = [self primitiveExercise_id];
	return [result longLongValue];
}

- (void)setPrimitiveExercise_idValue:(int64_t)value_ {
	[self setPrimitiveExercise_id:[NSNumber numberWithLongLong:value_]];
}





@dynamic lang;






@dynamic met;



- (float)metValue {
	NSNumber *result = [self met];
	return [result floatValue];
}

- (void)setMetValue:(float)value_ {
	[self setMet:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveMetValue {
	NSNumber *result = [self primitiveMet];
	return [result floatValue];
}

- (void)setPrimitiveMetValue:(float)value_ {
	[self setPrimitiveMet:[NSNumber numberWithFloat:value_]];
}





@dynamic name;






@dynamic userExercises;

	
- (NSMutableSet*)userExercisesSet {
	[self willAccessValueForKey:@"userExercises"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"userExercises"];
  
	[self didAccessValueForKey:@"userExercises"];
	return result;
}
	






@end
