// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MealPlan.m instead.

#import "_MealPlan.h"

const struct MealPlanAttributes MealPlanAttributes = {
	.days = @"days",
	.descriptionMealplan = @"descriptionMealplan",
	.idMealPlan = @"idMealPlan",
	.image = @"image",
	.locale = @"locale",
	.title = @"title",
};

const struct MealPlanRelationships MealPlanRelationships = {
	.meals = @"meals",
	.shoppingListItems = @"shoppingListItems",
	.user = @"user",
};

const struct MealPlanFetchedProperties MealPlanFetchedProperties = {
};

@implementation MealPlanID
@end

@implementation _MealPlan

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MealPlan" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MealPlan";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MealPlan" inManagedObjectContext:moc_];
}

- (MealPlanID*)objectID {
	return (MealPlanID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"daysValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"days"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"idMealPlanValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"idMealPlan"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic days;



- (int16_t)daysValue {
	NSNumber *result = [self days];
	return [result shortValue];
}

- (void)setDaysValue:(int16_t)value_ {
	[self setDays:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveDaysValue {
	NSNumber *result = [self primitiveDays];
	return [result shortValue];
}

- (void)setPrimitiveDaysValue:(int16_t)value_ {
	[self setPrimitiveDays:[NSNumber numberWithShort:value_]];
}





@dynamic descriptionMealplan;






@dynamic idMealPlan;



- (int64_t)idMealPlanValue {
	NSNumber *result = [self idMealPlan];
	return [result longLongValue];
}

- (void)setIdMealPlanValue:(int64_t)value_ {
	[self setIdMealPlan:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveIdMealPlanValue {
	NSNumber *result = [self primitiveIdMealPlan];
	return [result longLongValue];
}

- (void)setPrimitiveIdMealPlanValue:(int64_t)value_ {
	[self setPrimitiveIdMealPlan:[NSNumber numberWithLongLong:value_]];
}





@dynamic image;






@dynamic locale;






@dynamic title;






@dynamic meals;

	
- (NSMutableSet*)mealsSet {
	[self willAccessValueForKey:@"meals"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"meals"];
  
	[self didAccessValueForKey:@"meals"];
	return result;
}
	

@dynamic shoppingListItems;

	
- (NSMutableSet*)shoppingListItemsSet {
	[self willAccessValueForKey:@"shoppingListItems"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"shoppingListItems"];
  
	[self didAccessValueForKey:@"shoppingListItems"];
	return result;
}
	

@dynamic user;

	






@end
