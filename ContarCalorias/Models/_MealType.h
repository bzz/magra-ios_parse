// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MealType.h instead.

#import <CoreData/CoreData.h>


extern const struct MealTypeAttributes {
	__unsafe_unretained NSString *name;
} MealTypeAttributes;

extern const struct MealTypeRelationships {
	__unsafe_unretained NSString *meals;
} MealTypeRelationships;

extern const struct MealTypeFetchedProperties {
} MealTypeFetchedProperties;

@class Meal;



@interface MealTypeID : NSManagedObjectID {}
@end

@interface _MealType : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MealTypeID*)objectID;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *meals;

- (NSMutableSet*)mealsSet;





@end

@interface _MealType (CoreDataGeneratedAccessors)

- (void)addMeals:(NSSet*)value_;
- (void)removeMeals:(NSSet*)value_;
- (void)addMealsObject:(Meal*)value_;
- (void)removeMealsObject:(Meal*)value_;

@end

@interface _MealType (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (NSMutableSet*)primitiveMeals;
- (void)setPrimitiveMeals:(NSMutableSet*)value;


@end
