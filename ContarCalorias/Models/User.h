#import "_User.h"

@interface User : _User {}
// Custom logic goes here.

- (NSString *)heightUnitCurrentUnitSystem;
- (NSString *)weightUnitCurrentUnitSystem;
- (BOOL)hasValidData;
- (double)getHeight;
- (double)heightInches;
- (NSString *)getWeightUnit;
- (double)getInitialWeight;
- (double)initialWeightLbs;
- (double)getCurrentWeight;
- (double)currentWeightLbs;
- (double)goalWeightLbs;
- (double)getGoalWeight;
- (void)setHeight:(CGFloat)height;
- (void)setInitialWeight:(CGFloat)weight;
- (void)setCurrentWeight:(CGFloat)weight;
- (void)setUsersBirthdate:(NSDate *)birthdate;
- (void)setDesiredWeight:(CGFloat)desiredWeight;
- (NSInteger)daysToReachDesiredWeight;
- (void)setMealTimesUsingWakeupTime;
- (BOOL)hasProFeatures;
- (void)updateDateReachingDesiredWeight;
@end
