// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Exercise.h instead.

#import <CoreData/CoreData.h>


extern const struct ExerciseAttributes {
	__unsafe_unretained NSString *exercise_id;
	__unsafe_unretained NSString *lang;
	__unsafe_unretained NSString *met;
	__unsafe_unretained NSString *name;
} ExerciseAttributes;

extern const struct ExerciseRelationships {
	__unsafe_unretained NSString *userExercises;
} ExerciseRelationships;

extern const struct ExerciseFetchedProperties {
} ExerciseFetchedProperties;

@class UserExercise;






@interface ExerciseID : NSManagedObjectID {}
@end

@interface _Exercise : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ExerciseID*)objectID;





@property (nonatomic, strong) NSNumber* exercise_id;



@property int64_t exercise_idValue;
- (int64_t)exercise_idValue;
- (void)setExercise_idValue:(int64_t)value_;

//- (BOOL)validateExercise_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* lang;



//- (BOOL)validateLang:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* met;



@property float metValue;
- (float)metValue;
- (void)setMetValue:(float)value_;

//- (BOOL)validateMet:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *userExercises;

- (NSMutableSet*)userExercisesSet;





@end

@interface _Exercise (CoreDataGeneratedAccessors)

- (void)addUserExercises:(NSSet*)value_;
- (void)removeUserExercises:(NSSet*)value_;
- (void)addUserExercisesObject:(UserExercise*)value_;
- (void)removeUserExercisesObject:(UserExercise*)value_;

@end

@interface _Exercise (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveExercise_id;
- (void)setPrimitiveExercise_id:(NSNumber*)value;

- (int64_t)primitiveExercise_idValue;
- (void)setPrimitiveExercise_idValue:(int64_t)value_;




- (NSString*)primitiveLang;
- (void)setPrimitiveLang:(NSString*)value;




- (NSNumber*)primitiveMet;
- (void)setPrimitiveMet:(NSNumber*)value;

- (float)primitiveMetValue;
- (void)setPrimitiveMetValue:(float)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (NSMutableSet*)primitiveUserExercises;
- (void)setPrimitiveUserExercises:(NSMutableSet*)value;


@end
