// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserFood.m instead.

#import "_UserFood.h"

const struct UserFoodAttributes UserFoodAttributes = {
	.date = @"date",
	.meanEnumType = @"meanEnumType",
	.numberOfServings = @"numberOfServings",
	.userFood_id = @"userFood_id",
};

const struct UserFoodRelationships UserFoodRelationships = {
	.food = @"food",
	.foodServing = @"foodServing",
	.user = @"user",
};

const struct UserFoodFetchedProperties UserFoodFetchedProperties = {
};

@implementation UserFoodID
@end

@implementation _UserFood

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserFood" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserFood";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserFood" inManagedObjectContext:moc_];
}

- (UserFoodID*)objectID {
	return (UserFoodID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"meanEnumTypeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"meanEnumType"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"numberOfServingsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"numberOfServings"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"userFood_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userFood_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic date;






@dynamic meanEnumType;



- (int16_t)meanEnumTypeValue {
	NSNumber *result = [self meanEnumType];
	return [result shortValue];
}

- (void)setMeanEnumTypeValue:(int16_t)value_ {
	[self setMeanEnumType:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveMeanEnumTypeValue {
	NSNumber *result = [self primitiveMeanEnumType];
	return [result shortValue];
}

- (void)setPrimitiveMeanEnumTypeValue:(int16_t)value_ {
	[self setPrimitiveMeanEnumType:[NSNumber numberWithShort:value_]];
}





@dynamic numberOfServings;



- (int16_t)numberOfServingsValue {
	NSNumber *result = [self numberOfServings];
	return [result shortValue];
}

- (void)setNumberOfServingsValue:(int16_t)value_ {
	[self setNumberOfServings:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveNumberOfServingsValue {
	NSNumber *result = [self primitiveNumberOfServings];
	return [result shortValue];
}

- (void)setPrimitiveNumberOfServingsValue:(int16_t)value_ {
	[self setPrimitiveNumberOfServings:[NSNumber numberWithShort:value_]];
}





@dynamic userFood_id;



- (int64_t)userFood_idValue {
	NSNumber *result = [self userFood_id];
	return [result longLongValue];
}

- (void)setUserFood_idValue:(int64_t)value_ {
	[self setUserFood_id:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveUserFood_idValue {
	NSNumber *result = [self primitiveUserFood_id];
	return [result longLongValue];
}

- (void)setPrimitiveUserFood_idValue:(int64_t)value_ {
	[self setPrimitiveUserFood_id:[NSNumber numberWithLongLong:value_]];
}





@dynamic food;

	

@dynamic foodServing;

	

@dynamic user;

	






@end
