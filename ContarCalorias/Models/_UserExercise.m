// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserExercise.m instead.

#import "_UserExercise.h"

const struct UserExerciseAttributes UserExerciseAttributes = {
	.date = @"date",
	.minutes = @"minutes",
	.totalCalories = @"totalCalories",
	.userExercise_id = @"userExercise_id",
};

const struct UserExerciseRelationships UserExerciseRelationships = {
	.exercise = @"exercise",
	.user = @"user",
};

const struct UserExerciseFetchedProperties UserExerciseFetchedProperties = {
};

@implementation UserExerciseID
@end

@implementation _UserExercise

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserExercise" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserExercise";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserExercise" inManagedObjectContext:moc_];
}

- (UserExerciseID*)objectID {
	return (UserExerciseID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"minutesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"minutes"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"totalCaloriesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"totalCalories"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"userExercise_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userExercise_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic date;






@dynamic minutes;



- (int16_t)minutesValue {
	NSNumber *result = [self minutes];
	return [result shortValue];
}

- (void)setMinutesValue:(int16_t)value_ {
	[self setMinutes:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveMinutesValue {
	NSNumber *result = [self primitiveMinutes];
	return [result shortValue];
}

- (void)setPrimitiveMinutesValue:(int16_t)value_ {
	[self setPrimitiveMinutes:[NSNumber numberWithShort:value_]];
}





@dynamic totalCalories;



- (int16_t)totalCaloriesValue {
	NSNumber *result = [self totalCalories];
	return [result shortValue];
}

- (void)setTotalCaloriesValue:(int16_t)value_ {
	[self setTotalCalories:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveTotalCaloriesValue {
	NSNumber *result = [self primitiveTotalCalories];
	return [result shortValue];
}

- (void)setPrimitiveTotalCaloriesValue:(int16_t)value_ {
	[self setPrimitiveTotalCalories:[NSNumber numberWithShort:value_]];
}





@dynamic userExercise_id;



- (int16_t)userExercise_idValue {
	NSNumber *result = [self userExercise_id];
	return [result shortValue];
}

- (void)setUserExercise_idValue:(int16_t)value_ {
	[self setUserExercise_id:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveUserExercise_idValue {
	NSNumber *result = [self primitiveUserExercise_id];
	return [result shortValue];
}

- (void)setPrimitiveUserExercise_idValue:(int16_t)value_ {
	[self setPrimitiveUserExercise_id:[NSNumber numberWithShort:value_]];
}





@dynamic exercise;

	

@dynamic user;

	






@end
