// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Meal.h instead.

#import <CoreData/CoreData.h>


extern const struct MealAttributes {
	__unsafe_unretained NSString *dayOfPlan;
	__unsafe_unretained NSString *descriptionMeal;
	__unsafe_unretained NSString *idMeal;
	__unsafe_unretained NSString *title;
} MealAttributes;

extern const struct MealRelationships {
	__unsafe_unretained NSString *mealPlan;
	__unsafe_unretained NSString *mealType;
	__unsafe_unretained NSString *recipe;
} MealRelationships;

extern const struct MealFetchedProperties {
} MealFetchedProperties;

@class MealPlan;
@class MealType;
@class Recipe;






@interface MealID : NSManagedObjectID {}
@end

@interface _Meal : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MealID*)objectID;





@property (nonatomic, strong) NSNumber* dayOfPlan;



@property int64_t dayOfPlanValue;
- (int64_t)dayOfPlanValue;
- (void)setDayOfPlanValue:(int64_t)value_;

//- (BOOL)validateDayOfPlan:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* descriptionMeal;



//- (BOOL)validateDescriptionMeal:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* idMeal;



@property int64_t idMealValue;
- (int64_t)idMealValue;
- (void)setIdMealValue:(int64_t)value_;

//- (BOOL)validateIdMeal:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *mealPlan;

- (NSMutableSet*)mealPlanSet;




@property (nonatomic, strong) MealType *mealType;

//- (BOOL)validateMealType:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Recipe *recipe;

//- (BOOL)validateRecipe:(id*)value_ error:(NSError**)error_;





@end

@interface _Meal (CoreDataGeneratedAccessors)

- (void)addMealPlan:(NSSet*)value_;
- (void)removeMealPlan:(NSSet*)value_;
- (void)addMealPlanObject:(MealPlan*)value_;
- (void)removeMealPlanObject:(MealPlan*)value_;

@end

@interface _Meal (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveDayOfPlan;
- (void)setPrimitiveDayOfPlan:(NSNumber*)value;

- (int64_t)primitiveDayOfPlanValue;
- (void)setPrimitiveDayOfPlanValue:(int64_t)value_;




- (NSString*)primitiveDescriptionMeal;
- (void)setPrimitiveDescriptionMeal:(NSString*)value;




- (NSNumber*)primitiveIdMeal;
- (void)setPrimitiveIdMeal:(NSNumber*)value;

- (int64_t)primitiveIdMealValue;
- (void)setPrimitiveIdMealValue:(int64_t)value_;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;





- (NSMutableSet*)primitiveMealPlan;
- (void)setPrimitiveMealPlan:(NSMutableSet*)value;



- (MealType*)primitiveMealType;
- (void)setPrimitiveMealType:(MealType*)value;



- (Recipe*)primitiveRecipe;
- (void)setPrimitiveRecipe:(Recipe*)value;


@end
