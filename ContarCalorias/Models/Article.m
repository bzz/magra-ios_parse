//
//  Article.m
//  ContarCalorias
//
//  Created by andres portillo on 1/8/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "Article.h"

@implementation Article
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeInteger:self.articleId forKey:@"articleId"];
    [encoder encodeBool:self.completed forKey:@"completed"];
    [encoder encodeInteger:self.day forKey:@"day"];
    [encoder encodeObject:self.locale forKey:@"locale"];
    [encoder encodeObject:self.imageData forKey:@"imageData"];
    [encoder encodeObject:self.imageUrl forKey:@"imageUrl"];
    [encoder encodeObject:self.title   forKey:@"title"];
    [encoder encodeObject:self.content    forKey:@"content"];
    [encoder encodeObject:self.type forKey:@"type"];
    [encoder encodeObject:self.date forKey:@"date"];

}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        
        self.articleId = [decoder decodeIntegerForKey:@"articleId"];
        self.completed = [decoder decodeBoolForKey:@"completed"];
        self.day = [decoder decodeIntegerForKey:@"day"];
        self.locale = [decoder decodeObjectForKey:@"locale"];
        self.imageData = [decoder decodeObjectForKey:@"imageData"];
        self.imageUrl = [decoder decodeObjectForKey:@"imageUrl"];
        self.title = [decoder decodeObjectForKey:@"title"];
        self.content = [decoder decodeObjectForKey:@"content"];
        self.type = [decoder decodeObjectForKey:@"type"];
        self.date = [decoder decodeObjectForKey:@"date"];
    }
    return self;
}
@end
