// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Food.m instead.

#import "_Food.h"

const struct FoodAttributes FoodAttributes = {
	.brand = @"brand",
	.food_id = @"food_id",
	.isSearchable = @"isSearchable",
	.locale = @"locale",
	.normalizedTitle = @"normalizedTitle",
	.title = @"title",
};

const struct FoodRelationships FoodRelationships = {
	.userFoods = @"userFoods",
};

const struct FoodFetchedProperties FoodFetchedProperties = {
};

@implementation FoodID
@end

@implementation _Food

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Food" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Food";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Food" inManagedObjectContext:moc_];
}

- (FoodID*)objectID {
	return (FoodID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"food_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"food_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isSearchableValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isSearchable"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic brand;






@dynamic food_id;



- (int64_t)food_idValue {
	NSNumber *result = [self food_id];
	return [result longLongValue];
}

- (void)setFood_idValue:(int64_t)value_ {
	[self setFood_id:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveFood_idValue {
	NSNumber *result = [self primitiveFood_id];
	return [result longLongValue];
}

- (void)setPrimitiveFood_idValue:(int64_t)value_ {
	[self setPrimitiveFood_id:[NSNumber numberWithLongLong:value_]];
}





@dynamic isSearchable;



- (BOOL)isSearchableValue {
	NSNumber *result = [self isSearchable];
	return [result boolValue];
}

- (void)setIsSearchableValue:(BOOL)value_ {
	[self setIsSearchable:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsSearchableValue {
	NSNumber *result = [self primitiveIsSearchable];
	return [result boolValue];
}

- (void)setPrimitiveIsSearchableValue:(BOOL)value_ {
	[self setPrimitiveIsSearchable:[NSNumber numberWithBool:value_]];
}





@dynamic locale;






@dynamic normalizedTitle;






@dynamic title;






@dynamic userFoods;

	
- (NSMutableSet*)userFoodsSet {
	[self willAccessValueForKey:@"userFoods"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"userFoods"];
  
	[self didAccessValueForKey:@"userFoods"];
	return result;
}
	






@end
