// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FoodServing.h instead.

#import <CoreData/CoreData.h>


extern const struct FoodServingAttributes {
	__unsafe_unretained NSString *bardode;
	__unsafe_unretained NSString *calories;
	__unsafe_unretained NSString *carbohydrates;
	__unsafe_unretained NSString *cholesterol;
	__unsafe_unretained NSString *fat;
	__unsafe_unretained NSString *fiber;
	__unsafe_unretained NSString *food_id;
	__unsafe_unretained NSString *id_food_serving;
	__unsafe_unretained NSString *locale;
	__unsafe_unretained NSString *protein;
	__unsafe_unretained NSString *saturated_fat;
	__unsafe_unretained NSString *serving_part;
	__unsafe_unretained NSString *serving_size;
	__unsafe_unretained NSString *serving_type;
	__unsafe_unretained NSString *sodium;
	__unsafe_unretained NSString *sugar;
} FoodServingAttributes;

extern const struct FoodServingRelationships {
	__unsafe_unretained NSString *userFoods;
} FoodServingRelationships;

extern const struct FoodServingFetchedProperties {
} FoodServingFetchedProperties;

@class UserFood;


















@interface FoodServingID : NSManagedObjectID {}
@end

@interface _FoodServing : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FoodServingID*)objectID;





@property (nonatomic, strong) NSNumber* bardode;



@property int64_t bardodeValue;
- (int64_t)bardodeValue;
- (void)setBardodeValue:(int64_t)value_;

//- (BOOL)validateBardode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* calories;



@property int64_t caloriesValue;
- (int64_t)caloriesValue;
- (void)setCaloriesValue:(int64_t)value_;

//- (BOOL)validateCalories:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* carbohydrates;



@property float carbohydratesValue;
- (float)carbohydratesValue;
- (void)setCarbohydratesValue:(float)value_;

//- (BOOL)validateCarbohydrates:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* cholesterol;



@property float cholesterolValue;
- (float)cholesterolValue;
- (void)setCholesterolValue:(float)value_;

//- (BOOL)validateCholesterol:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* fat;



@property float fatValue;
- (float)fatValue;
- (void)setFatValue:(float)value_;

//- (BOOL)validateFat:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* fiber;



@property float fiberValue;
- (float)fiberValue;
- (void)setFiberValue:(float)value_;

//- (BOOL)validateFiber:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* food_id;



@property int64_t food_idValue;
- (int64_t)food_idValue;
- (void)setFood_idValue:(int64_t)value_;

//- (BOOL)validateFood_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* id_food_serving;



@property int64_t id_food_servingValue;
- (int64_t)id_food_servingValue;
- (void)setId_food_servingValue:(int64_t)value_;

//- (BOOL)validateId_food_serving:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* locale;



//- (BOOL)validateLocale:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* protein;



@property float proteinValue;
- (float)proteinValue;
- (void)setProteinValue:(float)value_;

//- (BOOL)validateProtein:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* saturated_fat;



@property float saturated_fatValue;
- (float)saturated_fatValue;
- (void)setSaturated_fatValue:(float)value_;

//- (BOOL)validateSaturated_fat:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* serving_part;



@property float serving_partValue;
- (float)serving_partValue;
- (void)setServing_partValue:(float)value_;

//- (BOOL)validateServing_part:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* serving_size;



@property float serving_sizeValue;
- (float)serving_sizeValue;
- (void)setServing_sizeValue:(float)value_;

//- (BOOL)validateServing_size:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* serving_type;



//- (BOOL)validateServing_type:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* sodium;



@property float sodiumValue;
- (float)sodiumValue;
- (void)setSodiumValue:(float)value_;

//- (BOOL)validateSodium:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* sugar;



@property float sugarValue;
- (float)sugarValue;
- (void)setSugarValue:(float)value_;

//- (BOOL)validateSugar:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *userFoods;

- (NSMutableSet*)userFoodsSet;





@end

@interface _FoodServing (CoreDataGeneratedAccessors)

- (void)addUserFoods:(NSSet*)value_;
- (void)removeUserFoods:(NSSet*)value_;
- (void)addUserFoodsObject:(UserFood*)value_;
- (void)removeUserFoodsObject:(UserFood*)value_;

@end

@interface _FoodServing (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveBardode;
- (void)setPrimitiveBardode:(NSNumber*)value;

- (int64_t)primitiveBardodeValue;
- (void)setPrimitiveBardodeValue:(int64_t)value_;




- (NSNumber*)primitiveCalories;
- (void)setPrimitiveCalories:(NSNumber*)value;

- (int64_t)primitiveCaloriesValue;
- (void)setPrimitiveCaloriesValue:(int64_t)value_;




- (NSNumber*)primitiveCarbohydrates;
- (void)setPrimitiveCarbohydrates:(NSNumber*)value;

- (float)primitiveCarbohydratesValue;
- (void)setPrimitiveCarbohydratesValue:(float)value_;




- (NSNumber*)primitiveCholesterol;
- (void)setPrimitiveCholesterol:(NSNumber*)value;

- (float)primitiveCholesterolValue;
- (void)setPrimitiveCholesterolValue:(float)value_;




- (NSNumber*)primitiveFat;
- (void)setPrimitiveFat:(NSNumber*)value;

- (float)primitiveFatValue;
- (void)setPrimitiveFatValue:(float)value_;




- (NSNumber*)primitiveFiber;
- (void)setPrimitiveFiber:(NSNumber*)value;

- (float)primitiveFiberValue;
- (void)setPrimitiveFiberValue:(float)value_;




- (NSNumber*)primitiveFood_id;
- (void)setPrimitiveFood_id:(NSNumber*)value;

- (int64_t)primitiveFood_idValue;
- (void)setPrimitiveFood_idValue:(int64_t)value_;




- (NSNumber*)primitiveId_food_serving;
- (void)setPrimitiveId_food_serving:(NSNumber*)value;

- (int64_t)primitiveId_food_servingValue;
- (void)setPrimitiveId_food_servingValue:(int64_t)value_;




- (NSString*)primitiveLocale;
- (void)setPrimitiveLocale:(NSString*)value;




- (NSNumber*)primitiveProtein;
- (void)setPrimitiveProtein:(NSNumber*)value;

- (float)primitiveProteinValue;
- (void)setPrimitiveProteinValue:(float)value_;




- (NSNumber*)primitiveSaturated_fat;
- (void)setPrimitiveSaturated_fat:(NSNumber*)value;

- (float)primitiveSaturated_fatValue;
- (void)setPrimitiveSaturated_fatValue:(float)value_;




- (NSNumber*)primitiveServing_part;
- (void)setPrimitiveServing_part:(NSNumber*)value;

- (float)primitiveServing_partValue;
- (void)setPrimitiveServing_partValue:(float)value_;




- (NSNumber*)primitiveServing_size;
- (void)setPrimitiveServing_size:(NSNumber*)value;

- (float)primitiveServing_sizeValue;
- (void)setPrimitiveServing_sizeValue:(float)value_;




- (NSString*)primitiveServing_type;
- (void)setPrimitiveServing_type:(NSString*)value;




- (NSNumber*)primitiveSodium;
- (void)setPrimitiveSodium:(NSNumber*)value;

- (float)primitiveSodiumValue;
- (void)setPrimitiveSodiumValue:(float)value_;




- (NSNumber*)primitiveSugar;
- (void)setPrimitiveSugar:(NSNumber*)value;

- (float)primitiveSugarValue;
- (void)setPrimitiveSugarValue:(float)value_;





- (NSMutableSet*)primitiveUserFoods;
- (void)setPrimitiveUserFoods:(NSMutableSet*)value;


@end
