// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FoodServing.m instead.

#import "_FoodServing.h"

const struct FoodServingAttributes FoodServingAttributes = {
	.bardode = @"bardode",
	.calories = @"calories",
	.carbohydrates = @"carbohydrates",
	.cholesterol = @"cholesterol",
	.fat = @"fat",
	.fiber = @"fiber",
	.food_id = @"food_id",
	.id_food_serving = @"id_food_serving",
	.locale = @"locale",
	.protein = @"protein",
	.saturated_fat = @"saturated_fat",
	.serving_part = @"serving_part",
	.serving_size = @"serving_size",
	.serving_type = @"serving_type",
	.sodium = @"sodium",
	.sugar = @"sugar",
};

const struct FoodServingRelationships FoodServingRelationships = {
	.userFoods = @"userFoods",
};

const struct FoodServingFetchedProperties FoodServingFetchedProperties = {
};

@implementation FoodServingID
@end

@implementation _FoodServing

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"FoodServing" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"FoodServing";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"FoodServing" inManagedObjectContext:moc_];
}

- (FoodServingID*)objectID {
	return (FoodServingID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"bardodeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"bardode"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"caloriesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"calories"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"carbohydratesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"carbohydrates"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cholesterolValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cholesterol"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"fatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"fat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"fiberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"fiber"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"food_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"food_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"id_food_servingValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"id_food_serving"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"proteinValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"protein"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"saturated_fatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"saturated_fat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"serving_partValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"serving_part"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"serving_sizeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"serving_size"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sodiumValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sodium"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sugarValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sugar"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic bardode;



- (int64_t)bardodeValue {
	NSNumber *result = [self bardode];
	return [result longLongValue];
}

- (void)setBardodeValue:(int64_t)value_ {
	[self setBardode:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveBardodeValue {
	NSNumber *result = [self primitiveBardode];
	return [result longLongValue];
}

- (void)setPrimitiveBardodeValue:(int64_t)value_ {
	[self setPrimitiveBardode:[NSNumber numberWithLongLong:value_]];
}





@dynamic calories;



- (int64_t)caloriesValue {
	NSNumber *result = [self calories];
	return [result longLongValue];
}

- (void)setCaloriesValue:(int64_t)value_ {
	[self setCalories:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveCaloriesValue {
	NSNumber *result = [self primitiveCalories];
	return [result longLongValue];
}

- (void)setPrimitiveCaloriesValue:(int64_t)value_ {
	[self setPrimitiveCalories:[NSNumber numberWithLongLong:value_]];
}





@dynamic carbohydrates;



- (float)carbohydratesValue {
	NSNumber *result = [self carbohydrates];
	return [result floatValue];
}

- (void)setCarbohydratesValue:(float)value_ {
	[self setCarbohydrates:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveCarbohydratesValue {
	NSNumber *result = [self primitiveCarbohydrates];
	return [result floatValue];
}

- (void)setPrimitiveCarbohydratesValue:(float)value_ {
	[self setPrimitiveCarbohydrates:[NSNumber numberWithFloat:value_]];
}





@dynamic cholesterol;



- (float)cholesterolValue {
	NSNumber *result = [self cholesterol];
	return [result floatValue];
}

- (void)setCholesterolValue:(float)value_ {
	[self setCholesterol:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveCholesterolValue {
	NSNumber *result = [self primitiveCholesterol];
	return [result floatValue];
}

- (void)setPrimitiveCholesterolValue:(float)value_ {
	[self setPrimitiveCholesterol:[NSNumber numberWithFloat:value_]];
}





@dynamic fat;



- (float)fatValue {
	NSNumber *result = [self fat];
	return [result floatValue];
}

- (void)setFatValue:(float)value_ {
	[self setFat:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveFatValue {
	NSNumber *result = [self primitiveFat];
	return [result floatValue];
}

- (void)setPrimitiveFatValue:(float)value_ {
	[self setPrimitiveFat:[NSNumber numberWithFloat:value_]];
}





@dynamic fiber;



- (float)fiberValue {
	NSNumber *result = [self fiber];
	return [result floatValue];
}

- (void)setFiberValue:(float)value_ {
	[self setFiber:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveFiberValue {
	NSNumber *result = [self primitiveFiber];
	return [result floatValue];
}

- (void)setPrimitiveFiberValue:(float)value_ {
	[self setPrimitiveFiber:[NSNumber numberWithFloat:value_]];
}





@dynamic food_id;



- (int64_t)food_idValue {
	NSNumber *result = [self food_id];
	return [result longLongValue];
}

- (void)setFood_idValue:(int64_t)value_ {
	[self setFood_id:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveFood_idValue {
	NSNumber *result = [self primitiveFood_id];
	return [result longLongValue];
}

- (void)setPrimitiveFood_idValue:(int64_t)value_ {
	[self setPrimitiveFood_id:[NSNumber numberWithLongLong:value_]];
}





@dynamic id_food_serving;



- (int64_t)id_food_servingValue {
	NSNumber *result = [self id_food_serving];
	return [result longLongValue];
}

- (void)setId_food_servingValue:(int64_t)value_ {
	[self setId_food_serving:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveId_food_servingValue {
	NSNumber *result = [self primitiveId_food_serving];
	return [result longLongValue];
}

- (void)setPrimitiveId_food_servingValue:(int64_t)value_ {
	[self setPrimitiveId_food_serving:[NSNumber numberWithLongLong:value_]];
}





@dynamic locale;






@dynamic protein;



- (float)proteinValue {
	NSNumber *result = [self protein];
	return [result floatValue];
}

- (void)setProteinValue:(float)value_ {
	[self setProtein:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveProteinValue {
	NSNumber *result = [self primitiveProtein];
	return [result floatValue];
}

- (void)setPrimitiveProteinValue:(float)value_ {
	[self setPrimitiveProtein:[NSNumber numberWithFloat:value_]];
}





@dynamic saturated_fat;



- (float)saturated_fatValue {
	NSNumber *result = [self saturated_fat];
	return [result floatValue];
}

- (void)setSaturated_fatValue:(float)value_ {
	[self setSaturated_fat:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSaturated_fatValue {
	NSNumber *result = [self primitiveSaturated_fat];
	return [result floatValue];
}

- (void)setPrimitiveSaturated_fatValue:(float)value_ {
	[self setPrimitiveSaturated_fat:[NSNumber numberWithFloat:value_]];
}





@dynamic serving_part;



- (float)serving_partValue {
	NSNumber *result = [self serving_part];
	return [result floatValue];
}

- (void)setServing_partValue:(float)value_ {
	[self setServing_part:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveServing_partValue {
	NSNumber *result = [self primitiveServing_part];
	return [result floatValue];
}

- (void)setPrimitiveServing_partValue:(float)value_ {
	[self setPrimitiveServing_part:[NSNumber numberWithFloat:value_]];
}





@dynamic serving_size;



- (float)serving_sizeValue {
	NSNumber *result = [self serving_size];
	return [result floatValue];
}

- (void)setServing_sizeValue:(float)value_ {
	[self setServing_size:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveServing_sizeValue {
	NSNumber *result = [self primitiveServing_size];
	return [result floatValue];
}

- (void)setPrimitiveServing_sizeValue:(float)value_ {
	[self setPrimitiveServing_size:[NSNumber numberWithFloat:value_]];
}





@dynamic serving_type;






@dynamic sodium;



- (float)sodiumValue {
	NSNumber *result = [self sodium];
	return [result floatValue];
}

- (void)setSodiumValue:(float)value_ {
	[self setSodium:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSodiumValue {
	NSNumber *result = [self primitiveSodium];
	return [result floatValue];
}

- (void)setPrimitiveSodiumValue:(float)value_ {
	[self setPrimitiveSodium:[NSNumber numberWithFloat:value_]];
}





@dynamic sugar;



- (float)sugarValue {
	NSNumber *result = [self sugar];
	return [result floatValue];
}

- (void)setSugarValue:(float)value_ {
	[self setSugar:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveSugarValue {
	NSNumber *result = [self primitiveSugar];
	return [result floatValue];
}

- (void)setPrimitiveSugarValue:(float)value_ {
	[self setPrimitiveSugar:[NSNumber numberWithFloat:value_]];
}





@dynamic userFoods;

	
- (NSMutableSet*)userFoodsSet {
	[self willAccessValueForKey:@"userFoods"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"userFoods"];
  
	[self didAccessValueForKey:@"userFoods"];
	return result;
}
	






@end
