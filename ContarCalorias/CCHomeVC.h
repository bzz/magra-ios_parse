//
//  CCHomeTVC.h
//  ContarCalorias
//
//  Created by andres portillo on 15/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CCTaskProgressView;
@class CCDairyTopBar;
#import "GAITrackedViewController.h"

@interface CCHomeVC : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UIButton *btnAddDiaryItem;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet CCDairyTopBar *diaryTopBar;
-(IBAction)onAddDiaryItemButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintDiaryBarTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTableHeight;
@end
