//
//  CCLoadingCell.h
//  ContarCalorias
//
//  Created by andres portillo on 22/11/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CCLoadingCell;
@protocol CCLoadingCellDelegate
- (void)theRefreshButtonWasTappedOnLoadingCell:(CCLoadingCell *)loadingCell;
@end
@interface CCLoadingCell : UITableViewCell
@property (weak, nonatomic) id<CCLoadingCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (void)loadingDidFail;
- (void)noMoreToLoad;
@end
