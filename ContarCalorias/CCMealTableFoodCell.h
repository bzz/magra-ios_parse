//
//  CCMealTableFoodCell.h
//  ContarCalorias
//
//  Created by andres portillo on 12/10/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CCMealTableFoodCellDelegate
- (void)didTapSeeRecipeButtonWithRecipeId:(NSInteger)recipeId;
@end
@interface CCMealTableFoodCell : UITableViewCell
@property(weak,nonatomic) id<CCMealTableFoodCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSeeRecipe;
@property (nonatomic) NSInteger recipeId;
- (IBAction)onSeeRecipeButtonTapped:(id)sender;

@end
