//
//  AddExerciseViewController.m
//  ContarCalorias
//
//  Created by Adrian Ghitun on 12/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "AddExerciseViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "UIAlertView+Blocks.h"
#import "ViewUtils.h"
#import "ContarCalendar.h"
#import "CCAPI.h"
#import "helper.h"
#import "Exercise.h"
#import "UserExercise.h"
#import "User.h"

@interface AddExerciseViewController ()
@end

@implementation AddExerciseViewController
{
    NSInteger _caloriesBurned,_minutesPerformed;
    User *_user;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [_viewWrapperInfo.layer setCornerRadius:4];
    [ViewUtils drawBasicShadowOnView:_viewWrapperInfo];
    [ViewUtils fixSeparatorsHeightInView:_viewWrapperInfo];
    [self.view setBackgroundColor:[ViewUtils lightGrayColor]];
    
    self.currentExerciseLbl.text = self.exercise.name;
    self.caloriesLbl.text = @"0";
    self.txtMinutes.delegate = self;
    
    [self.txtMinutes addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [singleTap setNumberOfTapsRequired:1];
    [singleTap setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:singleTap];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[TSLanguageManager localizedString:@"Save"] style:UIBarButtonItemStylePlain target:self action:@selector(saveExerciseItem:)];
    [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Add exercise"]];
    _user = [[CCAPI sharedInstance] getUser];
    [self setLocalizedStrings];
}
- (void)setLocalizedStrings
{
    [_lblCaloriesBurned setText:[TSLanguageManager localizedString:@"Burned calories"]];
    [_lblMinutesPerformed setText:[TSLanguageManager localizedString:@"Minutes performed"]];
}
#pragma mark - UITextField delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField.text intValue] == 0)
        textField.text = @"";
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if([textField.text isEqualToString:@""])
    {
        textField.text = 0;
        self.caloriesLbl.text = [NSString stringWithFormat:@"%i", [textField.text intValue] * self.diaryItemInfo.met];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidChange :(UITextField *)textField
{
    CGFloat currentWeight;
    
    currentWeight  = _user.initialWeightKgValue;
    _minutesPerformed = [textField.text intValue];
    _caloriesBurned =lroundf( (([self.exercise.met floatValue] * 3.5*currentWeight) / 200 ) * _minutesPerformed);
    self.caloriesLbl.text = [NSString stringWithFormat:@"%li",(long)_caloriesBurned];
}

- (void)dismissKeyboard
{
    [self.txtMinutes resignFirstResponder];
}
- (void)saveExerciseItem:(UIBarButtonItem *)sender
{
    [[CCAPI sharedInstance] logExercise:self.exercise withMinutes:_minutesPerformed burnedCalories:_caloriesBurned];
    [[NSNotificationCenter defaultCenter] postNotificationName:kCCNotificationExerciseWasLogged object:nil];
        
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
