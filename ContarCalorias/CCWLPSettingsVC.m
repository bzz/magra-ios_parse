//
//  CCSettingsVC.m
//  ContarCalorias
//
//  Created by andres portillo on 2/7/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCWLPSettingsVC.h"
#import "FontAwesomeKit.h"
#import "CCConstants.h"
#import "CCAPI.h"
#import "User.h"
#import "SWRevealViewController.h"
#import "DDUnitConversion.h"
#import "CCUtils.h"
#import "FontAwesomeKit.h"

#define IconSize 30.0

typedef NS_ENUM(NSInteger, CCSettingsSection) {
    CCSettingsSectionUserProfile,
    CCSettingsSectionReminders
};
typedef NS_ENUM(NSInteger, CCSettingsUserProfileCell) {
    CCSettingsUserProfileCellCurrentWeightCell,
    CCSettingsUserProfileCellGoalWeight,
    CCSettingsUserProfileCellBirthdayCell,
    CCSettingsUserProfileCellGenderCell,
    CCSettingsUserProfileCellHeightCell,
    CCSettingsUserProfileCellActivityLevel,
};
typedef NS_ENUM(NSInteger, CCSettingsUserProfileCellSubview) {
    CCSettingsUserProfileCellSubviewTitleLabel = 1,
    CCSettingsUserProfileCellSubviewTextField
};
typedef NS_ENUM(NSInteger, CCSettingsReminderCell) {
    CCSettingsReminderCellMealReminders,
    CCSettingsReminderCellExerciseReminders
};
@interface CCWLPSettingsVC ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource>

@end

@implementation CCWLPSettingsVC{
    BOOL _doSelection;
    UIImage *_circleImg;
    UIView *_dimView;
    UIImage *_circleCheckImg;
    CCUnitSystem _selectedUnitSystem;
    CCGender _selectedGender;
    CCActivityLevel _selectedActivityLevel;
    NSDate *_selectedBirthdate;
    NSArray *_arrUserProfileCellTitles;
    User *_user;
    NSIndexPath *_editingIndexPath;
    UIToolbar *_inputAccessory;
    UIDatePicker *_pickerBirthdate;
    UIPickerView *_pickerGender, *_pickerHeight, *_pickerActivityLevel, *_pickerCurrentWeight, *_pickerGoalWeight;
    NSArray *_arrGenders, *_arrActivityLevels;
    UILabel *_lblHeightFirstUnit, *_lblHeightSecondUnit;
    NSString * _heightText, *_activityLevelText, *_currentWeightText, *_goalWeightText, *_genderText, *_birthdateText;
    NSInteger _currentHeightCentimeters, _currentHeightInches;
    UITextField  *_txtCurrentWeight,*_txtGoalWeight,*_txtHeight, *_txtGender, *_txtActivityLevel, *_txtBirthdate;
    CGFloat _currentWeight, _goalWeight;
    UILabel *_lblCurrentField;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
        //Dim view
    _dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 600)];
    [_dimView setBackgroundColor:[UIColor blackColor]];
    [_dimView setAlpha:0.35];
    
        //check icons
    
    FAKFontAwesome *circleIcon = [FAKFontAwesome circleOIconWithSize:IconSize];
    [circleIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    FAKFontAwesome *circleCheckIcon = [FAKFontAwesome checkCircleOIconWithSize:IconSize];
    [circleCheckIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    _circleImg = [circleIcon imageWithSize:CGSizeMake(IconSize, IconSize)];
    _circleCheckImg = [circleCheckIcon imageWithSize:CGSizeMake(IconSize, IconSize)];

    
    
    
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"settings-header-bg"]]];
    
        //Cell Titles
    
    _arrUserProfileCellTitles = @[[TSLanguageManager localizedString:@"Current Weight"],[TSLanguageManager localizedString:@"Goal Weight"], [TSLanguageManager localizedString:@"Birthdate"],[TSLanguageManager localizedString:@"Gender"], [TSLanguageManager localizedString:@"Height"],[TSLanguageManager localizedString:@"Activity Level"]];
    
    
        //genders
    _arrGenders = @[[TSLanguageManager localizedString:@"Female"],[TSLanguageManager localizedString:@"Male"]];
        //Activity levels
    _arrActivityLevels =  @[[TSLanguageManager localizedString:@"Sedentary"],[TSLanguageManager localizedString:@"Slightly Active"],[TSLanguageManager localizedString:@"Active"],[TSLanguageManager localizedString:@"Very Active"]];
    
         //get user
    _user = [[CCAPI sharedInstance] getUser];
    _selectedUnitSystem = _user.unitSystemValue;
    _selectedGender = _user.genderValue;
    _selectedActivityLevel = _user.activityLevelValue;
    _selectedBirthdate  = _user.birthdate;
    _currentWeight = [_user getCurrentWeight];
    _currentHeightCentimeters = _user.heightCmValue;
    _goalWeight = [_user getGoalWeight];
    
    
    [_lblBritishSystem setText:[TSLanguageManager localizedString:@"British System"]];
    [_lblMetricSystem setText:[TSLanguageManager localizedString:@"Metric System"]];
        //register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
        //configure Input Accessory
    [self configureInputAccessory];
    
        //navigation item
    UIBarButtonItem *saveBarButton = [[UIBarButtonItem alloc] initWithTitle:[TSLanguageManager localizedString:@"Save"] style:UIBarButtonItemStylePlain target:self action:@selector(onSaveButtonTapped)];
    [self.navigationItem setRightBarButtonItem:saveBarButton];
    [self.navigationItem setTitle:[TSLanguageManager localizedString:@"Weight loss plan"]];
    
    //Date picker
    _pickerBirthdate = [UIDatePicker new];
    [_pickerBirthdate setDatePickerMode:UIDatePickerModeDate];
    [_pickerBirthdate setBackgroundColor:[UIColor whiteColor]];
    
    if ([[TSLanguageManager selectedLanguage] isEqualToString:@"es"]) {
        [_pickerBirthdate setLocale:[NSLocale localeWithLocaleIdentifier:@"es_es"]];
    } else {
        [_pickerBirthdate setLocale:[NSLocale localeWithLocaleIdentifier:@"pt_br"]];
    }
    
    if (_selectedBirthdate) {
        [_pickerBirthdate setDate:_selectedBirthdate];
    }
    [_pickerBirthdate addTarget:self action:@selector(onBirthdateChanged:) forControlEvents:UIControlEventValueChanged];
    
        //Picker Views
    _pickerGender = [UIPickerView new];
    _pickerGender.delegate = self;
    [_pickerGender setBackgroundColor:[UIColor whiteColor]];
    [_pickerGender selectRow:_user.genderValue inComponent:0 animated:NO];
    
    
    _pickerHeight = [UIPickerView new];
    _lblHeightFirstUnit = [[UILabel alloc] initWithFrame:CGRectMake( 90, 97, 40, 20)];
    [_lblHeightFirstUnit setTextAlignment:NSTextAlignmentCenter];
    [_lblHeightFirstUnit setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [_lblHeightFirstUnit setTextColor:[UIColor lightGrayColor]];
    [_pickerHeight addSubview:_lblHeightFirstUnit];
    
    _lblHeightSecondUnit  = [[UILabel alloc] initWithFrame:CGRectMake( 210, 97, 40, 20)];
    [_lblHeightSecondUnit setTextAlignment:NSTextAlignmentCenter];
    [_lblHeightSecondUnit setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [_lblHeightSecondUnit setTextColor:[UIColor lightGrayColor]];
    [_pickerHeight addSubview:_lblHeightSecondUnit];
    
    
    if (_selectedUnitSystem == CCUnitSystemMetric) {
        [_btnBritishSystem setImage:_circleImg forState:UIControlStateNormal];
        [_btnMetricSystem setImage:_circleCheckImg forState:UIControlStateNormal];
        [_lblHeightFirstUnit setText:[TSLanguageManager localizedString:@"Meter Abbrev"]];
        [_lblHeightSecondUnit setText:[TSLanguageManager localizedString:@"Centimeter Abbrev"]];
        
    } else {
        [_btnBritishSystem setImage:_circleCheckImg forState:UIControlStateNormal];
        [_btnMetricSystem setImage:_circleImg forState:UIControlStateNormal];
        [_lblHeightFirstUnit setText:[TSLanguageManager localizedString:@"ft"]];
        [_lblHeightSecondUnit setText:[TSLanguageManager localizedString:@"in"]];
    }

    
    
    _pickerGoalWeight =[[UIPickerView alloc] init];
    [_pickerGoalWeight setBackgroundColor:[UIColor whiteColor]];
    [_pickerGoalWeight setDelegate:self];
    
    if ([_user getGoalWeight] >= 14) {
        CGFloat decimalPart = ([_user getGoalWeight] - (NSInteger)[_user getGoalWeight]) * 10;
        NSInteger decimalRow = round(decimalPart);
        
        [_pickerGoalWeight selectRow:([_user getGoalWeight] - 14) inComponent:0 animated:NO];
        [_pickerGoalWeight selectRow:decimalRow inComponent:1 animated:NO];
        
    }
    
    _pickerCurrentWeight =[[UIPickerView alloc] init];
    [_pickerCurrentWeight setBackgroundColor:[UIColor whiteColor]];
    [_pickerCurrentWeight setDelegate:self];
    
    CGFloat currentWeight = [_user getCurrentWeight];
    
    if (currentWeight >= 14) {
    
        CGFloat decimalPart = ([_user getCurrentWeight] - (NSInteger)[_user getCurrentWeight]) * 10;
        NSInteger decimalRow = round(decimalPart);
        
        if (_user.unitSystemValue == CCUnitSystemMetric) {
            [_pickerCurrentWeight selectRow:( currentWeight - 14) inComponent:0 animated:NO];
            [_pickerCurrentWeight selectRow:decimalRow inComponent:1 animated:NO];
            
        } else {
            CGFloat currentWeight = [_user getCurrentWeight];
            [_pickerCurrentWeight selectRow:( currentWeight - 30) inComponent:0 animated:NO];
            [_pickerCurrentWeight selectRow:decimalRow inComponent:1 animated:NO];
            
        }

    }
    
    
    _pickerActivityLevel = [UIPickerView new];
    _pickerHeight.delegate = self;
    _pickerActivityLevel.delegate = self;
    [_pickerHeight setBackgroundColor:[UIColor whiteColor]];
    [_pickerActivityLevel setBackgroundColor:[UIColor whiteColor]];
    
    [_pickerActivityLevel selectRow:_user.activityLevelValue inComponent:0 animated:NO];
    
    
 
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Class Methods
- (void)configureInputAccessory
{
    _lblCurrentField = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, 220, 22)];
    [_lblCurrentField setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16]];
    [_lblCurrentField setTextColor:[UIColor whiteColor]];
    [_lblCurrentField setTextAlignment:NSTextAlignmentCenter];
    
    _inputAccessory = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 40)];
    UIBarButtonItem  *spacer = [[UIBarButtonItem alloc]    initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [_inputAccessory addSubview:_lblCurrentField];
    
    [_inputAccessory setBarStyle:UIBarStyleDefault];
    [_inputAccessory setTintColor:[UIColor whiteColor]];
    [_inputAccessory setBarTintColor:CCColorGreen];
    
    FAKFontAwesome *checkIcon = [FAKFontAwesome checkIconWithSize:28];
    [checkIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    FAKFontAwesome *cancelIcon = [FAKFontAwesome closeIconWithSize:28];
    [cancelIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    UIBarButtonItem *checkButton = [[UIBarButtonItem alloc] initWithImage:[checkIcon imageWithSize:CGSizeMake(25, 25)] style:UIBarButtonItemStyleDone target:self action:@selector(doSelection)];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithImage:[cancelIcon imageWithSize:CGSizeMake(25, 25)] style:UIBarButtonItemStyleDone target:self action:@selector(cancelSelection)];
    
    
    [_inputAccessory setItems:[NSArray arrayWithObjects:cancelButton, spacer,checkButton,nil] animated:YES];
    
}
- (void)doSelection
{
    _doSelection = YES;
    [self dismissKeyboard];
}
- (void)cancelSelection
{
    _doSelection = NO;
    [self dismissKeyboard];
}
- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}

#pragma mark - Keyboard events
- (void)keyboardWillShow:(NSNotification *)notification
{
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    [self.view addSubview:_dimView];
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets;
    
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
    }
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    [self.tableView scrollToRowAtIndexPath:_editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    [_dimView removeFromSuperview];
    NSNumber *rate = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:rate.floatValue animations:^{
        [self.tableView setContentOffset:CGPointMake(0,0)];
        self.tableView.contentInset = UIEdgeInsetsZero;
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }];
}
#pragma mark - UITextField Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == _txtCurrentWeight) {
        [_lblCurrentField setText:[TSLanguageManager localizedString:@"Current Weight"]];
    } else if (textField == _txtGoalWeight) {
        [_lblCurrentField setText:[TSLanguageManager localizedString:@"Goal Weight"]];
    }  else if (textField == _txtBirthdate) {
        [_lblCurrentField setText:[TSLanguageManager localizedString:@"Birthdate"]];
    }  else if (textField == _txtGender) {
        [_lblCurrentField setText:[TSLanguageManager localizedString:@"Gender"]];
    }  else if (textField == _txtActivityLevel) {
        [_lblCurrentField setText:[TSLanguageManager localizedString:@"Activity Level"]];
    }  else if (textField == _txtHeight) {
        [_lblCurrentField setText:[TSLanguageManager localizedString:@"Height"]];
    }
    [self didStartEditingTextFieldInCell:textField];
}
-(void)didStartEditingTextFieldInCell:(UITextField *)textField
{
    _editingIndexPath = [self indexPathForTextField:textField];
}
-(NSIndexPath *)indexPathForTextField:(UITextField *)textField
{
    CGPoint point = [textField convertPoint:CGPointZero toView:self.tableView];
    return [self.tableView indexPathForRowAtPoint:point];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _txtCurrentWeight && _doSelection) {
        [textField setText:_currentWeightText];
    } else if (textField == _txtGoalWeight && _doSelection) {
        [textField setText:_goalWeightText];
    }  else if (textField == _txtBirthdate && _doSelection) {
        [textField setText:_birthdateText];
    }  else if (textField == _txtGender && _doSelection) {
        [textField setText:_genderText];
    }  else if (textField == _txtActivityLevel && _doSelection) {
        [textField setText:_activityLevelText];
    }  else if (textField == _txtHeight && _doSelection) {
        [textField setText:_heightText];
    }
}
#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0;
}
#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrUserProfileCellTitles.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier  = @"Settings Cell";
    UITableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    [((UILabel *)[cell.contentView viewWithTag:CCSettingsUserProfileCellSubviewTitleLabel]) setText:[_arrUserProfileCellTitles objectAtIndex:row]];
    [((UITextField *)[cell.contentView viewWithTag:CCSettingsUserProfileCellSubviewTextField]) setInputAccessoryView:_inputAccessory];
    NSString *weightUnit = [_user getWeightUnit];
    
    if (indexPath.row == CCSettingsUserProfileCellCurrentWeightCell) {
        NSString *value = [NSString stringWithFormat:@"%.1f %@",_currentWeight, weightUnit];
        _txtCurrentWeight = ((UITextField *)[cell.contentView viewWithTag:CCSettingsUserProfileCellSubviewTextField]);
        [_txtCurrentWeight setText:value];
        [_txtCurrentWeight setInputView:_pickerCurrentWeight];
        [_txtCurrentWeight setInputAccessoryView:_inputAccessory];
        [_txtCurrentWeight setKeyboardType:UIKeyboardTypeDecimalPad];
    
    } else if (indexPath.row == CCSettingsUserProfileCellGoalWeight) {
       _goalWeight = [_user getGoalWeight];
        NSString *value = [NSString stringWithFormat:@"%.1f %@",_goalWeight, weightUnit];
        _txtGoalWeight = ((UITextField *)[cell.contentView viewWithTag:CCSettingsUserProfileCellSubviewTextField]);
        [_txtGoalWeight setText:value];
        [_txtGoalWeight setInputView:_pickerGoalWeight];
        [_txtGoalWeight setInputAccessoryView:_inputAccessory];
        [_txtGoalWeight setKeyboardType:UIKeyboardTypeDecimalPad];
   
    } if (indexPath.row == CCSettingsUserProfileCellBirthdayCell) {
     
        NSDateFormatter *bdFormater = [[NSDateFormatter alloc] init];
        [bdFormater setDateFormat:@"dd-MM-yyyy"];
        NSString *value = [NSString stringWithFormat:@"%@",[bdFormater stringFromDate:_user.birthdate]];
        _txtBirthdate = ((UITextField *)[cell.contentView viewWithTag:CCSettingsUserProfileCellSubviewTextField]);
        [_txtBirthdate setText:value];
        [_txtBirthdate setInputView:_pickerBirthdate];
        
    } else if (indexPath.row == CCSettingsUserProfileCellGenderCell) {
        
        NSString *value = [[CCAPI sharedInstance] getLocalizedGenderNameForCCGenderIndex: _selectedGender];
        _txtGender = ((UITextField *)[cell.contentView viewWithTag:CCSettingsUserProfileCellSubviewTextField]);
        [_txtGender setText:value];
        [_txtGender setInputView:_pickerGender];
        [_pickerGender selectRow:_selectedGender inComponent:0 animated:NO];
        
    } else if (indexPath.row == CCSettingsUserProfileCellHeightCell) {
        
        
        CGFloat userHeight = [_user getHeight];
        NSString *heightUnit =  [_user heightUnitCurrentUnitSystem];
        NSString *value  =  @"";
        if(_user.unitSystemValue == CCUnitSystemMetric) {
            
            CGFloat userHeightCm = _user.heightCmValue;
            NSInteger rowForCentimeters = abs(userHeightCm - 100);
            NSInteger rowForMeters = (int)(userHeightCm/100);
            
            [_pickerHeight selectRow:rowForMeters inComponent:0 animated:NO];
            [_pickerHeight selectRow:rowForCentimeters inComponent:1 animated:NO];
            value = [NSString stringWithFormat:@"%.2f %@",userHeight,heightUnit];

        } else {
        
            CGFloat userHeightIn = [_user heightInches];
            NSInteger inches = (int)(fmod(userHeightIn,12));
            NSInteger feet = (int)userHeightIn/12;
            
            [_pickerHeight selectRow:feet inComponent:0 animated:NO];
            [_pickerHeight selectRow:inches inComponent:1 animated:NO];
            value = [NSString stringWithFormat:@"%d.%d %@",feet,inches,heightUnit];
            
        }
        _txtHeight = ((UITextField *)[cell.contentView viewWithTag:CCSettingsUserProfileCellSubviewTextField]);
        [_txtHeight setText:value];
        [_txtHeight setInputView:_pickerHeight];
        
        
        
    } else if (indexPath.row == CCSettingsUserProfileCellActivityLevel) {
        NSString *value = [[CCAPI sharedInstance] getLocalizedActivityLevelForCCActivityLevelIndex: _user.activityLevelValue];
        _txtActivityLevel = ((UITextField *)[cell.contentView viewWithTag:CCSettingsUserProfileCellSubviewTextField]);
        [_txtActivityLevel  setText:value];
        [_txtActivityLevel setInputView:_pickerActivityLevel];
        [_pickerActivityLevel selectRow:_selectedActivityLevel inComponent:0 animated:NO];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row== CCSettingsUserProfileCellCurrentWeightCell) {
        [_txtCurrentWeight becomeFirstResponder];
    } else if (indexPath.row== CCSettingsUserProfileCellGoalWeight) {
        [_txtGoalWeight becomeFirstResponder];
    } else if (indexPath.row== CCSettingsUserProfileCellBirthdayCell) {
        [_txtBirthdate becomeFirstResponder];
    } else if (indexPath.row== CCSettingsUserProfileCellGenderCell) {
        [_txtGender becomeFirstResponder];
    } else if (indexPath.row== CCSettingsUserProfileCellHeightCell) {
        [_txtHeight becomeFirstResponder];
    } else if (indexPath.row== CCSettingsUserProfileCellActivityLevel) {
        [_txtActivityLevel becomeFirstResponder];
    }
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
#pragma mark - IBActions
- (void)onSaveButtonTapped
{
    _user.unitSystemValue = _selectedUnitSystem;
    _user.activityLevelValue = _selectedActivityLevel;
    [_user setUsersBirthdate:_selectedBirthdate];
    _user.genderValue = _selectedGender;
    [_user  setCurrentWeight:_currentWeight];
    
    if ([_txtCurrentWeight.text floatValue] != _currentWeight) {
       
        [_user updateDateReachingDesiredWeight];
        [[CCAPI sharedInstance] logUserWeight:[_txtCurrentWeight.text floatValue] sync:YES completion:nil];
    }
    [_user setHeightCmValue:_currentHeightCentimeters];
    
    [_user setDesiredWeight:[_txtGoalWeight.text floatValue] ];
    [[CCAPI sharedInstance] saveUserWithCompletion:^{
        [[CCAPI sharedInstance] updateMagraUser];
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
}
- (IBAction)onMetricSystemButtonTapped:(id)sender {
    
    [_btnBritishSystem setImage:_circleImg forState:UIControlStateNormal];
    [_btnMetricSystem setImage:_circleCheckImg forState:UIControlStateNormal];
    _selectedUnitSystem = CCUnitSystemMetric;
    _user.unitSystemValue = _selectedUnitSystem;
    _currentWeight = [_user getCurrentWeight];
    
    _currentWeight = [_user getCurrentWeight];
    
    CGFloat decimalPart = ([_user getCurrentWeight] - (NSInteger)[_user getCurrentWeight]) * 10;
    CGFloat decimalRow = round(decimalPart);
    
    [_pickerCurrentWeight reloadAllComponents];
    
    if (_currentWeight >= 14) {
        [_pickerCurrentWeight selectRow:(_currentWeight - 14) inComponent:0 animated:NO];
        [_pickerCurrentWeight selectRow:decimalRow inComponent:1 animated:NO];
    }
    
    CGFloat  goalWeight = (NSInteger)[_user getGoalWeight];
    decimalPart = (goalWeight - (NSInteger)goalWeight) * 10;
    decimalRow = round(decimalPart);
    
    [_pickerGoalWeight reloadAllComponents];
    [_pickerGoalWeight selectRow:(goalWeight - 14) inComponent:0 animated:NO];
    [_pickerGoalWeight selectRow:decimalRow inComponent:1 animated:NO];
    
    
    [_pickerCurrentWeight reloadAllComponents];
    [_pickerGoalWeight reloadAllComponents];
    [_pickerHeight reloadAllComponents];
    
    [self.tableView reloadData];
    
    [_lblHeightSecondUnit setText:[TSLanguageManager localizedString:@"Meter Abbrev"]];
    [_lblHeightSecondUnit setText:[TSLanguageManager localizedString:@"Centimeter Abbrev"]];
}

- (IBAction)onBritishSystemButtonTapped:(id)sender {
   
    [_btnBritishSystem setImage:_circleCheckImg forState:UIControlStateNormal];
    [_btnMetricSystem setImage:_circleImg forState:UIControlStateNormal];
    _selectedUnitSystem = CCUnitSystemBritish;
    _user.unitSystemValue = _selectedUnitSystem;
    _currentWeight = [_user getCurrentWeight];
    
    CGFloat decimalPart = ([_user getCurrentWeight] - (NSInteger)[_user getCurrentWeight]) * 10;
    CGFloat decimalRow = round(decimalPart);
    
    [_pickerCurrentWeight reloadAllComponents];
    [_pickerCurrentWeight selectRow:(_currentWeight - 30) inComponent:0 animated:NO];
    [_pickerCurrentWeight selectRow:decimalRow inComponent:1 animated:NO];

    CGFloat  goalWeight = (NSInteger)[_user getGoalWeight];
    decimalPart = (goalWeight - (NSInteger)goalWeight) * 10;
    decimalRow = round(decimalPart);
    
    [_pickerGoalWeight reloadAllComponents];
    [_pickerGoalWeight selectRow:(goalWeight - 30) inComponent:0 animated:NO];
    [_pickerGoalWeight selectRow:decimalRow inComponent:1 animated:NO];

    
    
    [_pickerGoalWeight reloadAllComponents];
    [_pickerHeight reloadAllComponents];
    

    
    [_lblHeightFirstUnit setText:[TSLanguageManager localizedString:@"ft"]];
    [_lblHeightSecondUnit setText:[TSLanguageManager localizedString:@"in"]];
    
    [self.tableView reloadData];
}
- (void)onBirthdateChanged:(UIDatePicker *)datePicker
{
    _selectedBirthdate = datePicker.date;
    _birthdateText = [CCUtils stringFromBirthdate:datePicker.date];
}
#pragma mark - UIPickerView Data Source
    // returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if(pickerView == _pickerHeight) {
        return 2;
    } else if (pickerView == _pickerGender) {
        return 1;
    } else if (pickerView == _pickerGoalWeight || pickerView == _pickerCurrentWeight) {
        return 3;
    }

    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == _pickerActivityLevel){
        return 4;
    } else if (pickerView == _pickerGender){
        return 2;
    } else if (pickerView == _pickerHeight){
            switch (component) {
                case 0:
                    return _user.unitSystemValue == CCUnitSystemMetric?3:8;
                    break;
                case 1:
                    return _user.unitSystemValue == CCUnitSystemMetric?100:12;
                    break;
                default:
                    break;
            }
            return  0;
        
    } else if (pickerView == _pickerCurrentWeight || pickerView == _pickerGoalWeight) {
        switch (component) {
            case 0:
                return _user.unitSystemValue == CCUnitSystemMetric?412:968;
                break;
            case 1:
                return 10;
                break;
            case 2:
                return 1;
                break;
            default:
                break;
        }
    }
    return 0;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (pickerView == _pickerHeight) {
        return 80.0;
    } else if (pickerView == _pickerGender || pickerView == _pickerActivityLevel) {
        return 280;
    } else if (pickerView == _pickerGoalWeight || pickerView == _pickerCurrentWeight) {
        if(component == 0) {
            return 50.0;
        } else if(component == 1) {
            return 25.0;
        } else if(component == 2) {
            return 20.0;
        }
    }
    return 0;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel* lblTitle = (UILabel*)view;
    if (!lblTitle) {
        lblTitle = [[UILabel alloc] init];
    }
    if(pickerView == _pickerHeight) {
    
        [lblTitle setText:[NSString stringWithFormat:@"%d",row]];
        
    } else if (pickerView == _pickerGender) {
     
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
        [lblTitle setText:[_arrGenders objectAtIndex:row]];
        [lblTitle setTextColor:[UIColor blackColor]];
        [lblTitle setTextAlignment:NSTextAlignmentCenter];
        
    } else if (pickerView == _pickerActivityLevel) {
    
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
        [lblTitle setText:[_arrActivityLevels objectAtIndex:row]];
        [lblTitle setTextColor:[UIColor blackColor]];
        [lblTitle setTextAlignment:NSTextAlignmentCenter];
        
    } else if (pickerView == _pickerGoalWeight || pickerView == _pickerCurrentWeight) {
        if (component == 0) {
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
            NSInteger firstWeightUnit = _user.unitSystemValue == CCUnitSystemMetric?(row + 14):(row + 30);
            [lblTitle setText:[NSString stringWithFormat:@"%d",firstWeightUnit]];
            [lblTitle setTextAlignment:NSTextAlignmentRight];
            [lblTitle setTextColor:[UIColor blackColor]];
        } else if (component == 1) {
            lblTitle = [[UILabel alloc] init];
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
            [lblTitle setText:[NSString stringWithFormat:@".%d",row]];
            [lblTitle setTextAlignment:NSTextAlignmentCenter];
            [lblTitle setTextColor:[UIColor blackColor]];
        } else if (component == 2) {
            lblTitle = [[UILabel alloc] init];
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
            [lblTitle setText:_user.weightUnitCurrentUnitSystem];
            [lblTitle setTextAlignment:NSTextAlignmentLeft];
            [lblTitle setTextColor:[UIColor blackColor]];
        }
    }
    
    return lblTitle;
}
#pragma  mark - UIPickerView Delegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == _pickerHeight) {
        NSInteger firstRow = [pickerView selectedRowInComponent:0];
        NSInteger secondRow = [pickerView selectedRowInComponent:1];
        
        _heightText = [NSString stringWithFormat:@"%d.%d %@",firstRow, secondRow,[TSLanguageManager localizedString:@"Meter Abbrev"]];
        
        if (_selectedUnitSystem == CCUnitSystemBritish) {
            _currentHeightInches =  firstRow * 12 + secondRow;
            _currentHeightCentimeters = [[[DDUnitConverter lengthUnitConverter] convertNumber:@(_currentHeightInches) fromUnit:DDLengthUnitInches toUnit:DDLengthUnitCentimeters] integerValue];
        } else {
            _currentHeightCentimeters = firstRow * 100 + secondRow;
            _currentHeightInches = [[[DDUnitConverter lengthUnitConverter] convertNumber:@(_currentHeightCentimeters) fromUnit:DDLengthUnitCentimeters toUnit:DDLengthUnitInches] floatValue];
        }

    } else if (pickerView == _pickerGender) {
    
        _selectedGender = row;
        _genderText  = _selectedGender == CCGenderMale? [TSLanguageManager localizedString:@"Male"] :[TSLanguageManager localizedString:@"Female"];
        
    } else if (pickerView == _pickerActivityLevel) {
        
        _selectedActivityLevel = row;
        switch (_selectedActivityLevel) {
            case CCActivityLevelSedentary:
                _activityLevelText = [TSLanguageManager localizedString:@"Sedentary"];
                break;
            case CCActivityLevelSlightlyActive:
                _activityLevelText = [TSLanguageManager localizedString:@"Slightly Active"];
                break;
            case CCActivityLevelActive:
                _activityLevelText = [TSLanguageManager localizedString:@"Active"];
                break;
            case CCActivityLevelVeryActive:
                _activityLevelText = [TSLanguageManager localizedString:@"Very Active"];
                break;
            default:
                break;
        }
    } else if (pickerView == _pickerGoalWeight) {
        
        NSInteger firstRow = [pickerView selectedRowInComponent:0] +  (_user.unitSystemValue == CCUnitSystemMetric ? 14:30);
        NSInteger secondRow = [pickerView selectedRowInComponent:1];
        _goalWeightText = [NSString stringWithFormat:@"%d.%d %@",firstRow, secondRow, _user.weightUnitCurrentUnitSystem];
        _goalWeight = [ _goalWeightText floatValue];
        
    } else if (pickerView == _pickerCurrentWeight) {
        
        NSInteger firstRow = [pickerView selectedRowInComponent:0] + (_user.unitSystemValue == CCUnitSystemMetric ? 14:30);
        NSInteger secondRow = [pickerView selectedRowInComponent:1];
        _currentWeightText =[NSString stringWithFormat:@"%d.%d %@",firstRow, secondRow,_user.weightUnitCurrentUnitSystem];
        _currentWeight = [_currentWeightText floatValue];
    }
}
@end
