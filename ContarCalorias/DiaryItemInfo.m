//
//  FoodItem.m
//  ContarCalorias
//
//  Created by Adrian Ghitun on 12/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "DiaryItemInfo.h"

@implementation DiaryItemInfo

-(id)initWithTile:(NSString *)title andDescription:(NSString *)description{
    self = [super init];
    if (self) {
        self.title = title;
        self.description = description;
    }
    return self;
}
@end
