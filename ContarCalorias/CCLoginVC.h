//
//  CCFirstLaunchVC.h
//  ContarCalorias
//
//  Created by andres portillo on 9/6/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CCConnectView;
@class CCLoginView;
@class CCForgotPasswordView;
@class CCRegistrationView;

#import "GAITrackedViewController.h"

@interface CCLoginVC : GAITrackedViewController
@property(strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property(strong, nonatomic) IBOutlet UIPageControl *pageControl;@property(strong, nonatomic) IBOutlet UIImageView *targetView;
@property (weak, nonatomic) IBOutlet CCConnectView *connectView;
@property (weak, nonatomic) IBOutlet CCRegistrationView *registrationView;
@property (weak, nonatomic) IBOutlet CCLoginView *loginView;
@property (weak, nonatomic) IBOutlet CCForgotPasswordView *forgotPasswordView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblsCollection;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgsCollection;

    //labels
@property (weak, nonatomic) IBOutlet UILabel *lblMagraTasks;
@property (weak, nonatomic) IBOutlet UILabel *lblLearnHow;
@property (weak, nonatomic) IBOutlet UILabel *lblLogMeals;
@property (weak, nonatomic) IBOutlet UILabel *lblArticles;
@property (weak, nonatomic) IBOutlet UILabel *lblKeepTrack;



@end
