//
//  LoginViewController.m
//  ContarCalorias
//
//  Created by Adrian Ghitun on 29/12/13.
//  Copyright (c) 2013 Adrian Ghitun. All rights reserved.
//

#import "CCLoginView.h"
#import <AFNetworking/AFURLResponseSerialization.h>
#import "ViewUtils.h"
#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import <QuartzCore/QuartzCore.h>
#import "CCAPI.h"
#import "User.h"
#import "AppDelegate.h"
#import "helper.h"
#import "CCUtils.h"

@interface CCLoginView ()

@end

@implementation CCLoginView {
    bool validEmail;
    bool validPassword;
    BOOL fotgotPassword;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setLocalizedStrings];
}
- (void)setLocalizedStrings
{
    [_txtEmail setPlaceholder:[TSLanguageManager localizedString:@"Email"]];
    [_txtPassword setPlaceholder:[TSLanguageManager localizedString:@"Password"]];
    [_forgotPasswordBtn setTitle:[TSLanguageManager localizedString:@"Forgot password?"] forState:UIControlStateNormal];
    [_btnLoginIn setTitle:[TSLanguageManager localizedString:@"Login"] forState:UIControlStateNormal];
    [_lblLogin setText:[TSLanguageManager localizedString:@"Login"]];
}
- (IBAction)onLoginButtonTapped:(id)sender
{
    [self endEditing:YES];
    
    if (_txtEmail.text.length > 0 && _txtPassword.text.length > 0) {
        if ([CCUtils emailIsValid:_txtEmail.text]) {
            if (_txtPassword.text.length >= 6) {
                NSDictionary *params = @{@"email":self.txtEmail.text, @"password":self.txtPassword.text};
                [_activityIndicator startAnimating];
                [[CCAPI sharedInstance] openEmailAccountSession:params success:^(id responseObject) {
                    
                    [_activityIndicator stopAnimating];
                    
                    NSDictionary *userData = responseObject;
                    User *user = [User MR_createEntity];
                    user.idUser = userData[@"id"];
                    user.magraAccessToken = userData[@"token"];
                    
                    user.authenticationType = CCAuthenticationTypeEmail;
                    NSDictionary *dictProfile = [userData objectForKey:@"profile"];
                    
                    user.username = [dictProfile objectForKey:@"full_name"];
                    
                    if ([dictProfile objectForKey:@"birthday"] != [NSNull null]) {
                        [user setUsersBirthdate:[NSDate dateWithTimeIntervalSince1970:[[dictProfile objectForKey:@"birthday"] integerValue]]];
                    }
                    
                    user.email = [dictProfile objectForKey:@"email"];
                    user.country = [dictProfile objectForKey:@"country"];
                    user.didCreateWeightLossPlanValue = [[dictProfile objectForKey:@"did_create_weight_loss_plan"] boolValue];
                   
                    if(user.didCreateWeightLossPlanValue) {
                        
                        if ([dictProfile objectForKey:@"wakeup_time"] == [NSNull null]) {
                            NSDate * defaultWakeupTime = [CCUtils getDefaultWakeupTime];
                            user.wakeupTime = defaultWakeupTime;
                            [user setMealTimesUsingWakeupTime];
                            
                        } else {
                            user.wakeupTime = [NSDate dateWithTimeIntervalSince1970:[[dictProfile objectForKey:@"wakeup_time"] integerValue]];
                        }
                        CCActivityLevel activityLevel = -1;
                        
                        if([[dictProfile objectForKey:@"activity"] isEqualToString:@"sedentario"]) {
                            activityLevel = CCActivityLevelSedentary;
                        } else if([[dictProfile objectForKey:@"activity"] isEqualToString:@"ligeramente activo"]) {
                            activityLevel = CCActivityLevelSlightlyActive;
                        } else if([[dictProfile objectForKey:@"activity"] isEqualToString:@"activo"]) {
                            activityLevel = CCActivityLevelActive;
                        } else if([[dictProfile objectForKey:@"activity"] isEqualToString:@"muy activo"]) {
                            activityLevel = CCActivityLevelVeryActive;
                        }
                        
                        CCGender gender = [[dictProfile objectForKey:@"gender"] isEqualToString:@"m"] ? CCGenderMale:CCGenderFemale;
                        [user setDesiredWeight:[[dictProfile objectForKey:@"goal_weight_kg"] doubleValue]];
                        
                        [user setGenderValue:gender];
                        user.googleId = [NSString stringWithFormat:@"%@",[dictProfile objectForKey:@"social_google"]];
                        user.facebookId = [NSString stringWithFormat:@"%@",[dictProfile objectForKey:@"social_facebook"]];
                        
                        [user setHeightCmValue:[[dictProfile objectForKey:@"height_cm"] doubleValue]];
                        [user setInitialWeightKgValue:[[dictProfile objectForKey:@"initial_weight_kg"] doubleValue]];
                        [user setCurrentWeightKgValue:[[dictProfile objectForKey:@"current_weight_kg"] doubleValue]];
                        [user setActivityLevelValue:activityLevel];
                       
                        user.firstDayDate = [NSDate dateWithTimeIntervalSince1970:[[dictProfile objectForKey:@"date_firstday"] integerValue]];
                    }
                    
                    [[CCAPI sharedInstance] saveUserWithCompletion:^{
                        [[CCAPI sharedInstance] setApptentiveInitialValues];
                    }];
                    
                    [self displayHomeController];
                  
                } failure:^(NSError *error) {
                    
                    [_activityIndicator stopAnimating];
                    NSMutableDictionary *userInfo = [error.userInfo mutableCopy];
                    NSInteger statusCode = [[userInfo objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                    
                    
                    if (statusCode == 404) {
                        [UIAlertView showWithTitle:@"" message:[TSLanguageManager localizedString:@"This email is not registered"] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:nil];
                        
                    } else if (statusCode == 406) {
                        [UIAlertView showWithTitle:@"" message:[TSLanguageManager localizedString:@"Username or password invalid"]cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:nil];
                    } else {
                        [UIAlertView showWithTitle:@"" message:[TSLanguageManager localizedString:@"There was an unexpected error, please try again"] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:nil];
                    }
                }];
            }
            else {
                [UIAlertView showWithTitle:@"" message:[TSLanguageManager localizedString:@"Password must have 6"] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:nil];
            }
        } else {
            [UIAlertView showWithTitle:@"" message:[TSLanguageManager localizedString:@"Please enter a valid email"] cancelButtonTitle:@"Ok" otherButtonTitles:nil tapBlock:nil];
        }
    } else {
        [[[UIAlertView alloc] initWithTitle:@"" message:[TSLanguageManager localizedString:@""] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    }
}
- (IBAction)onForgotPasswordButtonTapped:sender
{
    [self.delegate theForgotPasswordButtonWasPressedOntheLoginView];
}
- (void)displayHomeController
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];

    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate updateMainScreen];
}
- (IBAction)onBackButtonTapped:(id)sender
{
    [self endEditing:YES];
    [(UIScrollView *)self.superview scrollRectToVisible:CGRectMake(self.frame.origin.x
                                                                     - 320, self.frame.origin.y , self.frame.size.width, self.frame.size.height) animated:YES];
}
#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtEmail) {
        [_txtPassword becomeFirstResponder];
    } else {
        [self endEditing:YES];
        [self onLoginButtonTapped:nil];
    }
    return YES;
}
@end
