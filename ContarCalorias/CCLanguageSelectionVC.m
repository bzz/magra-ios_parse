//
//  CCLanguageSelectionVC.m
//  ContarCalorias
//
//  Created by andres portillo on 01/09/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import "CCLanguageSelectionVC.h"
#import "FontAwesomeKit.h"
#import "CCLoginVC.h"

@interface CCLanguageSelectionVC ()

@end

typedef NS_ENUM(NSInteger, CCLanguage) {
    CCLanguageSpanish,
    CCLanguagePortuguese
};
@implementation CCLanguageSelectionVC
{
    BOOL _iphone5;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
        //nav controller
  
    _iphone5 =[[UIScreen mainScreen] bounds].size.height > 480;
        // Background Image
    
    if (_iphone5) {
        [_imgBg setImage:[UIImage imageNamed:@"launch-bg-536h"]];
    }
    NSLog(@"img height: %f and scroll view: %f",_imgBg.frame.size.height, _scrollView.frame.size.height);
    self.automaticallyAdjustsScrollViewInsets = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didSelectSpanish:(id)sender {
    [TSLanguageManager setSelectedLanguage:@"es"];
    
    [[NSUserDefaults standardUserDefaults] setObject:[TSLanguageManager selectedLanguage] forKey:@"lang"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kCCLanguageWasSelected];
    [[NSUserDefaults standardUserDefaults] synchronize];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    CCLoginVC *LoginVC = [storyboard instantiateViewControllerWithIdentifier:@"Login"];
    [self.navigationController pushViewController:LoginVC animated:YES];
}
- (IBAction)didSelectPortuguese:(id)sender {
    [TSLanguageManager setSelectedLanguage:@"pt"];
    
    [[NSUserDefaults standardUserDefaults] setObject:[TSLanguageManager selectedLanguage] forKey:@"lang"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kCCLanguageWasSelected];
    [[NSUserDefaults standardUserDefaults] synchronize];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    CCLoginVC *LoginVC = [storyboard instantiateViewControllerWithIdentifier:@"Login"];
    [self.navigationController pushViewController:LoginVC animated:YES];
}
@end
