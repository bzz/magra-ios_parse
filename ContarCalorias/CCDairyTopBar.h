//
//  CCDairyTopBar.h
//  ContarCalorias
//
//  Created by andres portillo on 23/09/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCTaskProgressView.h"

@interface CCDairyTopBar : UIView
@property (weak,nonatomic) IBOutlet UILabel *lblRemainingValue;
@property (weak,nonatomic) IBOutlet UILabel *lblGoalValue;
@property (weak,nonatomic) IBOutlet UILabel *lblFoodValue;
@property (weak,nonatomic) IBOutlet UILabel *lblExerciseValue;
@property (weak,nonatomic) IBOutlet UILabel *lblRemainingText;
@property (weak,nonatomic) IBOutlet UILabel *lblGoalText;
@property (weak,nonatomic) IBOutlet UILabel *lblFoodText;
@property (weak,nonatomic) IBOutlet UILabel *lblExerciseText;
@property (weak,nonatomic) IBOutlet UILabel *lblCurrentWater;
@property (weak,nonatomic) IBOutlet UILabel *lblWaterText;
@property (weak,nonatomic) IBOutlet CCTaskProgressView *progressViewWater;
- (void)updateContent;

@end
