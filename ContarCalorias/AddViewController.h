//
//  AddViewController.h
//  ContarCalorias
//
//  Created by Adrian Ghitun on 07/01/14.
//  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h" 

@interface AddViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewWrapper;

@property (weak, nonatomic) IBOutlet UIView *viewWrapperDate;
@property (weak, nonatomic) IBOutlet UIView *viewWrapperWater;
@property (weak, nonatomic) IBOutlet UIView *viewWrapperMeals;
@property (weak, nonatomic) IBOutlet UIView *viewWrapperExercises;
@property (weak, nonatomic) IBOutlet UIButton *goToPreviousDateBtn;
@property (weak, nonatomic) IBOutlet UIButton *goToNextDateBtn;
@property (weak, nonatomic) IBOutlet UILabel *currentDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblAddFood;
@property (weak, nonatomic) IBOutlet UILabel *lblAddWater;
@property (weak, nonatomic) IBOutlet UILabel *lblAddExercise;

@end
