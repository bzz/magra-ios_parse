    //
    //  setWeightLossPlanResultsVC.m
    //  ContarCalorias
    //
    //  Created by andres portillo on 21/6/14.
    //  Copyright (c) 2014 Adrian Ghitun. All rights reserved.
    //

#import "CCSetWeightLossPlanResultsVC.h"
#import "CCAPI.h"
#import "User.h"
#import "CCConstants.h"
#import "FontAwesomeKit.h"

@interface CCSetWeightLossPlanResultsVC ()

@end

@implementation CCSetWeightLossPlanResultsVC

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
            // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kCCNotificationUserCreatedWeightLossPlan object:nil userInfo:nil];
    
    self.user = [[CCAPI sharedInstance] getUser];
   
    _lblDailyCalories.text = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%d", _user.dailyCaloriesValue]];
    _lblDailyCarbs.text = [NSString stringWithFormat:@"%@g/%@",[NSString stringWithFormat:@"%d", _user.dailyCarbsValue],[[TSLanguageManager localizedString:@"Day"] lowercaseString]];
    _lblDailyFats.text = [NSString stringWithFormat:@"%@g/%@", [NSString stringWithFormat:@"%d", _user.dailyFatsValue],[[TSLanguageManager localizedString:@"Day"] lowercaseString]];
    _lblDailyProteins.text = [NSString stringWithFormat:@"%@g/%@",[NSString stringWithFormat:@"%d", _user.dailyProteinsValue],[[TSLanguageManager localizedString:@"Day"] lowercaseString]];
    
        // Do any additional setup after loading the view.
    
    NSDateFormatter *dateOutputFormat = [[NSDateFormatter alloc] init];
    [dateOutputFormat setDateFormat:@"dd MMM"];
    
    NSString *metricExtension =  [TSLanguageManager localizedString:@"Lb"];
    
    metricExtension = [TSLanguageManager localizedString:@"Kg"];
    
    CGFloat weeklyWeightToLose = _user.initialWeightKgValue > 90 ?   _user.initialWeightKgValue * 0.01 :_user.initialWeightKgValue * 0.005;
    CGFloat totalWeightToLose = _user.initialWeightKgValue  - _user.desiredWeightKgValue;
    
    _lblWeightDiffPerWeek.text = [NSString stringWithFormat:@"%.2f %@ %@", weeklyWeightToLose,metricExtension,[TSLanguageManager localizedString:@"per week"]];
   
    
    _lblGoalWeightDate.text = [NSString stringWithFormat:@"%.2f %@ %@ %@",totalWeightToLose, metricExtension, [TSLanguageManager localizedString:@"for the"],[dateOutputFormat stringFromDate:_user.dateReachingDesiredWeight]];
    
        //title view
    
    UIImageView *imgTitleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"weight-loss-plan-results-title-view"]];
    self.navigationItem.titleView = imgTitleView;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self addButtonItem];
    });
    
    [self.navigationItem setHidesBackButton:YES];
    
        //localized strings
    [self setLocalizedStrings];
}
- (void)setLocalizedStrings
{
    [_lblCarbs setText:[TSLanguageManager localizedString:@"Carbs"]];
    [_lblFats setText:[TSLanguageManager localizedString:@"Fats"]];
    [_lblDescription setText:[TSLanguageManager localizedString:@"Results screen description"]];
    [_lblCongrats setText:[TSLanguageManager localizedString:@"Congrats!"]];
    [_lblProteins setText:[TSLanguageManager localizedString:@"Proteins"]];
    [_lblWeeklyWeightLoss setText:[TSLanguageManager localizedString:@"Your weekly weight loss"]];
    [_lblYouShouldLose setText:[TSLanguageManager localizedString:@"You should lose"]];
    [_lblCaloriesBudget setText:[TSLanguageManager localizedString:@"Your calorie budget is"]];
}
- (void)addButtonItem
{
    FAKFontAwesome *checkIcon = [FAKFontAwesome checkIconWithSize:24];
    [checkIcon addAttribute:NSForegroundColorAttributeName value:CCColorGreen];
    
    UIButton *checkButton = [[UIButton alloc] init];
    [checkButton setImage:[checkIcon imageWithSize:CGSizeMake(24, 24)] forState:UIControlStateNormal];
    [checkButton setFrame:CGRectMake(0, 0, 28, 28)];
    [checkButton addTarget:self action:@selector(onNextButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *nextButtonItem = [[UIBarButtonItem alloc] initWithCustomView:checkButton];
    [self.navigationItem setRightBarButtonItem:nextButtonItem];
    
}
- (void)onNextButtonTapped
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

@end
